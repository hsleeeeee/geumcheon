﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Geumcheon.Web.Consult.Models
{
    public class DropDownData
    {
        public static SelectList GradeItem()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add(string.Empty, "학년을선택하세요");
            for (int k = 1; k <= 3; k++)
            {
                dic.Add(k.ToString(), k + " 학년");
            }

            return new SelectList(dic, "key", "value");
        }

        public static SelectList ConsultTypeItem()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("", "문의유형을 선택하세요");
            //dic.Add("S", "수시상담");
            //dic.Add("J", "정시상담");
            dic.Add("C", "진학상담");
            dic.Add("L", "학습상담");            
            dic.Add("E", "기타");

            return new SelectList(dic, "key", "value");
        }

        public static SelectList PrgressItem()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("I", "상담중");
            dic.Add("C", "상담완료");

            return new SelectList(dic, "key", "value");
        }

        public static SelectList PrgressItem_T()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("I", "작성중");
            dic.Add("C", "작성완료");

            return new SelectList(dic, "key", "value");
        }

        public static SelectList ConsultantItem()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("0", "선택하세요");
            dic.Add("1", "consult01");
            dic.Add("2", "consult02");
            dic.Add("3", "consult03");
            

            return new SelectList(dic, "key", "value");
        }
    }
}