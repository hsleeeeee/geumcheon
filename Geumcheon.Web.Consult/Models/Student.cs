﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class Student
    {
        public int StudentIdx { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Hp { get; set; }
        public string School { get; set; }
        public int Grade { get; set; }
        public string Password { get; set; }
    }
}