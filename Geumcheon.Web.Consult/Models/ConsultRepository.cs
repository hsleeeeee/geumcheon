﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
namespace Geumcheon.Web.Consult.Models
{
    public class ConsultRepository : BaseRepository
    {
        #region Singleton Instance

        private static volatile ConsultRepository _instance;

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static ConsultRepository Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (typeof(ConsultRepository))
                    {
                        if (_instance == null)
                        {
                            _instance = new ConsultRepository();
                        }
                    }
                }

                return _instance;
            }
        }

        #endregion

        public int ModifyApplication(Application entity)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ApplicationIdx", entity.ApplicationIdx);
                parameters.Add("@CompayCode", entity.CompanyCode);
                parameters.Add("@Progress", entity.Progress);
                parameters.Add("@ConsultType", entity.ConsultType);
                parameters.Add("@ConsultWay", entity.ConsultWay);
                parameters.Add("@Title", entity.Title);
                parameters.Add("@Note", entity.Note);
                parameters.Add("@ScoreHtml", entity.ScoreHtml);
                parameters.Add("@ActivityItem", entity.ActivityItem);
                parameters.Add("@StudentIdx", entity.StudentIdx);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Application_Modify", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {                
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int ModifyStudent(Student entity)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Name", entity.Name);
                parameters.Add("@Email", entity.Email);
                parameters.Add("@Hp", entity.Hp);
                parameters.Add("@School", entity.School);
                parameters.Add("@Grade", entity.Grade);
                parameters.Add("@Password", entity.Password);                
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Student_Insert", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int RegApplication(Application entity)
        {
            try
            {
                int studentIdx = entity.StudentIdx;
                int result = 0;
                if (studentIdx <= 0)
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@Name", entity.Student.Name);
                    parameters.Add("@Email", entity.Student.Email);
                    parameters.Add("@Hp", entity.Student.Hp);
                    parameters.Add("@School", entity.Student.School);
                    parameters.Add("@Grade", entity.Student.Grade);
                    parameters.Add("@Password", entity.Student.Password);
                    parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                    result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Student_Insert", parameters, commandType: CommandType.StoredProcedure);

                    studentIdx = parameters.Get<int>("@Result");
                }
                int applicationIdx = 0;
                if (studentIdx > 0)
                {
                    var parameters2 = new DynamicParameters();
                    parameters2.Add("@ApplicationIdx", entity.ApplicationIdx);
                    parameters2.Add("@CompanyCode", entity.CompanyCode);
                    parameters2.Add("@Progress", entity.Progress);
                    parameters2.Add("@ConsultType", entity.ConsultType);
                    parameters2.Add("@ConsultWay", entity.ConsultWay);
                    parameters2.Add("@Title", entity.Title);
                    parameters2.Add("@Note", entity.Note);
                    parameters2.Add("@ActivityItem", entity.ActivityItem);
                    parameters2.Add("@ScoreHtml", entity.ScoreHtml);

                    parameters2.Add("@StudentIdx", studentIdx);
                    parameters2.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                    result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Application_Modify", parameters2, commandType: CommandType.StoredProcedure);

                    applicationIdx = parameters2.Get<int>("@Result");
                }

                if (applicationIdx > 0 && !entity.ConsultWay.Equals("B"))
                {
                    var parameters3 = new DynamicParameters();
                    parameters3.Add("@ApplicationIdx", applicationIdx);
                    parameters3.Add("@ConsultTimeIdx", entity.Schedule.ConsultTimeIdx);
                    parameters3.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                    result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Schedule_Insert", parameters3, commandType: CommandType.StoredProcedure);
                    result = parameters3.Get<int>("@Result");
                }

                var parameters4 = new DynamicParameters();
                parameters4.Add("@CompanyCode", entity.CompanyCode);
                parameters4.Add("@ApplicationIdx", applicationIdx);
                parameters4.Add("@ConsultWay", entity.ConsultWay);
                parameters4.Add("@ConsultTimeIdx", entity.Schedule.ConsultTimeIdx);
                parameters4.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Reply_Teacher_Auto_Insert", parameters4, commandType: CommandType.StoredProcedure);
                result = parameters4.Get<int>("@Result");
                if (result > 0)
                {
                    result = applicationIdx;
                }
                return result;
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public string GetScheduleDate(string companyCode)
        {
            try
            {
                string ableDate = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyCode", companyCode);

                ableDate = dbConnection.ExecuteScalar<string>("dbo.sp_Consult_Schedule_Select", parameters, commandType: CommandType.StoredProcedure);
                return ableDate;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public List<ScheduleTime> GetScheduleTime(string companyCode, string consultDate)
        {
            try
            {
                string[] date = consultDate.Split('-');
                int month = 0;
                int dte = 0;

                try
                {
                    month = Convert.ToInt32(date[1]);
                }
                catch
                {
                    month = Convert.ToInt32(date[1].Substring(1, 1));
                }
                try
                {
                    dte = Convert.ToInt32(date[2]);
                }
                catch
                {
                    dte = Convert.ToInt32(date[2].Substring(1, 1));
                }
                List<ScheduleTime> lstScheduleTime = new List<ScheduleTime>();
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyCode", companyCode);
                parameters.Add("@ConsultDate", date[0]+"-"+month+"-"+dte);

                lstScheduleTime = dbConnection.Query<ScheduleTime>("dbo.sp_Consult_Schedule_Time_Select", parameters, commandType: CommandType.StoredProcedure).ToList();
                return lstScheduleTime;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int SetSchedule(int applicationIdx, int consultTimeIdx)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ApplicationIdx", applicationIdx);
                parameters.Add("@ConsultTimeIdx", consultTimeIdx);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Schedule_Insert", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }  

        public Application GetApplicationByApplicationIdx(int applicationIdx)
        {
            try
            {
                Application application = new Application();
                Student student = new Student();
                Schedule schedule = new Schedule();
                
                var parameters = new DynamicParameters();
                parameters.Add("@ApplicationIdx", applicationIdx);


                var rVal = dbConnection.Query<Application, Student, Schedule, Reply, Application>("dbo.sp_Consult_Select_By_ApplicationIdx"
                    ,(ca, std, csd, cr) =>
                    {

                        ca.Student = std;
                        ca.Schedule = csd;
                        ca.Reply = cr;
                        return ca;
                    }
                    ,param: parameters
                    ,commandType: CommandType.StoredProcedure
                    ,splitOn: "StudentIdx,ConsultTimeIdx,ReplyNote").ToList()[0];
                //application = rVal.Read<Application>().ToList()[0];
                //student = rVal.Read<Student>().ToList()[0];
                //if (!"B".Equals(application.ConsultWay))
                //{
                //    schedule = rVal.Read<Schedule>().ToList()[0];
                //}

                return rVal ;

            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }


        public bool GetApplicationByConsultType(ViewModelList<Application, SearchInfo> model)
        {
            try
            {
                List<Application> lstApplication = new List<Application>();
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyCode", model.Search.CompanyCode);
                parameters.Add("@ConsultWay", model.Search.ConsultWay);
                parameters.Add("@Name", model.Search.Name);
                parameters.Add("@TeacherId", model.Search.TeacherId);
                parameters.Add("@currentPage", model.PagingInfo.CurrentPage);
                parameters.Add("@pageSize", model.PagingInfo.PageSize);
                parameters.Add("@orderBy", model.PagingInfo.OrderType);
                
                parameters.Add("@totalCount", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                //lstApplication = dbConnection.Query<Application>("dbo.sp_Consult_Select_By_ConsultType", parameters, commandType: CommandType.StoredProcedure).ToList();
                //return lstApplication;

                var rVal = dbConnection.Query<Application, Student, Schedule, Application>("dbo.sp_Consult_Select_By_ConsultType"
                , (ca, std, csd) =>
                {

                    ca.Student = std;
                    ca.Schedule = csd;
                    return ca;
                }
                , param: parameters
                , commandType: CommandType.StoredProcedure
                , splitOn: "StudentIdx,ConsultDateIdx").ToList();
                model.Entities = rVal;
                model.PagingInfo.TotalCount = parameters.Get<int>("@totalCount");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public string LoginTeacher(string companyCode, string id, string pwd)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyCode", companyCode);
                parameters.Add("@LoginId", id);
                parameters.Add("@Password", pwd);
                string result = dbConnection.ExecuteScalar<string>("dbo.sp_Consult_Teacher_Select", parameters, commandType: CommandType.StoredProcedure);

                return result;

            }
            catch (Exception ex)
            {
                return string.Empty;
            }

            finally
            {
                dbConnection.Close();
            }
        }
        public int GetRemainCnt(int time, int app)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ConsultTimeIdx", time);
                parameters.Add("@ApplicationIdx", app);
                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Schedule_Time_Remain_Select", parameters, commandType: CommandType.StoredProcedure);

                return result;

            }
            catch (Exception ex)
            {
                return 0;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public Application GetApplicationForStudent(string applicationIdx, string password)
        {
            try
            {
                Application application = new Application();
                Student student = new Student();
                Schedule schedule = new Schedule();

                var parameters = new DynamicParameters();
                parameters.Add("@ApplicationIdx", applicationIdx);
                parameters.Add("@Password", password);


                var rVal = dbConnection.Query<Application, Student, Schedule, Reply, Application>("dbo.sp_Consult_Select_For_Student"
                    , (ca, std, csd, cr) =>
                    {

                        ca.Student = std;
                        ca.Schedule = csd;
                        ca.Reply = cr;
                        return ca;
                    }
                    , param: parameters
                    , commandType: CommandType.StoredProcedure
                    , splitOn: "StudentIdx,ConsultTimeIdx, ReplyNote").ToList()[0];
                //application = rVal.Read<Application>().ToList()[0];
                //student = rVal.Read<Student>().ToList()[0];
                //if (!"B".Equals(application.ConsultWay))
                //{
                //    schedule = rVal.Read<Schedule>().ToList()[0];
                //}

                return rVal;

            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int SetTeacher (string applicationIdx, string teacheridx)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ApplicationIdx", applicationIdx);
                parameters.Add("@TeacherIdx", teacheridx);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Reply_Teacher_Insert", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int SetReply(Reply entity)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@ApplicationIdx", entity.ApplicationIdx);
                parameters.Add("@TeacherIdx", entity.TeacherIdx);
                parameters.Add("@Note", entity.Note);
                parameters.Add("@Progress", entity.Progress);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);

                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Reply_Insert", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public Reply GetReply(int applicationIdx, string consultId)
        {
            try
            {
                Reply reply = new Reply();
                var parameters = new DynamicParameters();

                parameters.Add("@ApplicationIdx", applicationIdx);
                parameters.Add("@TeacherId", consultId);

                reply = dbConnection.Query<Reply>("dbo.sp_Consult_Reply_Select_By_ApplicationIdx", parameters, commandType: CommandType.StoredProcedure).ToList()[0];
                return reply;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }


        public List<ScheduleTime> GetScheduleTimeDetail(string companyCode, int teacherIdx)
        {
            try
            {

                List<ScheduleTime> lstScheduleTime = new List<ScheduleTime>();
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyCode", companyCode);
                parameters.Add("@TeacherIdx", teacherIdx);

                lstScheduleTime = dbConnection.Query<ScheduleTime>("dbo.sp_Consult_Schedule_Detail_Select", parameters, commandType: CommandType.StoredProcedure).ToList();
                return lstScheduleTime;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }


        public int SetScheduleDetil(string companyCode, int teacherIdx, int consultTimeIdx, int isConfirm)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@CompanyCode", companyCode);
                parameters.Add("@TeacherIdx", teacherIdx);
                parameters.Add("@ConsultTimeIdx", consultTimeIdx);
                parameters.Add("@IsConfirm", isConfirm);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);

                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Schedule_Detail_Modify", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }
    }
}