﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class ViewModelList<T, SearchT>
    {

        public List<T> Entities { get; set; }
        public SearchT Search { get; set; }
        public PagingInfo PagingInfo { get; set; }

        //public List<KeyValuePair<string, object>> KeyValuePair { get; set;}
    }
    public class SearchInfo
    {
        public string SearchType { get; set; }
        public string Name { get; set; }
        public string TeacherId { get; set; }
        public string CompanyCode { get; set; }
        public string ConsultWay { get; set; }
        public string OrderType { get; set; }
    }
    public class PagingInfo
    {
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public long TotalItems { get; set; }                                
        public int CurrentPage { get; set; }
        public string OrderType { get; set; }
        //public int TotalPages
        //{
        //    get
        //    {
        //        var totalPage = (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);

        //        return totalPage > 0 ? totalPage : 1;
        //    }
        //}

        public PagingInfo()
        {
            //ItemsPerPage = 40;
        }

        public PagingInfo(int currentPage, int pageSize, int totalItems, string orderType= null)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalItems = totalItems;
            OrderType = orderType;
        }
    }
    public class Application
    {
        public int ApplicationIdx { get; set; }
        public string CompanyCode { get; set; }
        public string Progress { get; set; }
        public string ConsultType { get; set; }
        public string ConsultWay { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public string TeacherId { get; set; }
        public string TeacherIdx { get; set; }
        public string ScoreHtml { get; set; }
        public string ActivityItem { get; set; }


        public int StudentIdx { get; set; }
        public int RowIdx { get; set; }

        public string CreateDate { get; set; }
        public string ConfirmDate { get; set; }
        public string DeadlineDate { get; set; }
        public string ConsultTypeName
        {
            get
            {
                string rVal = string.Empty;
                switch (ConsultType)
                {
                    case "S":
                        {
                            rVal = "수시상담";
                            break;
                        }
                    case "J":
                        {
                            rVal = "정시상담";
                            break;
                        }
                    case "L":
                        {
                            rVal = "학습상담";
                            break;
                        }
                    case "C":
                        {
                            rVal = "진학상담";
                            break;
                        }
                    case "E":
                        {
                            rVal = "기타";
                            break;
                        }
                }
                return rVal;
               
            }            
            
        }

        public string ConsultWayName
        {
            get
            {
                string rVal = string.Empty;
                switch (ConsultWay)
                {
                    case "B": //Board
                        {
                            rVal = "온라인상담";
                            break;
                        }
                    case "F": //Face
                        {
                            rVal = "대면상담";
                            break;
                        }
                    case "T": //Tel
                        {
                            rVal = "전화상담";
                            break;
                        }
                }

                return rVal;
            }
            
        }

        public string ProgressName
        {
            get
            {
                string rVal = string.Empty;
                switch(Progress)
                {
                    case "R":
                        {
                            rVal = "접수";
                            break;
                        }
                    case "I":
                        {
                            rVal = "준비중";
                            break;
                        }
                    case "C":
                        {
                            rVal = "완료";
                            break;
                        }
                }
                return rVal;
            }
        }

        public Student Student { get; set; }

        public Schedule Schedule { get; set; }

        public Reply Reply { get; set; }
    }
}