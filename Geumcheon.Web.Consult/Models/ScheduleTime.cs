﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class ScheduleTime
    {
        public int ConsultTimeIdx { get; set; }
        public int ScheduledTimeIdx { get; set; }
        public int ConsultDateIdx { get; set; }
        public string ConsultTime { get; set; }
        public int Cnt { get; set; }
    }
}