﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class Reply
    {
        public int ApplicationIdx { get; set; }
        public int TeacherIdx { get; set; }
        public string Note { get; set; }
        public string ReplyNote { get; set; }
        public string Progress { get; set; }


        public string ProgressName
        {
            get
            {
                string rVal = string.Empty;
                switch (Progress)
                {
                    case "R":
                        {
                            rVal = "대기중";
                            break;
                        }
                    case "I":
                        {
                            rVal = "작성중";
                            break;
                        }
                    case "C":
                        {
                            rVal = "상담완료";
                            break;
                        }
                }
                return rVal;
            }
        }

        public DateTime ReplyDate { get; set; }
    }
}