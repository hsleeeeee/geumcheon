﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class Schedule
    {
        public int ScheduleIdx { get; set; }
        public int ApplicaionIdx { get; set; }
        public int ConsultTimeIdx { get; set; }

        public string ConsultDate { get; set; }
        public string ConsultTime { get; set; }
    }
}