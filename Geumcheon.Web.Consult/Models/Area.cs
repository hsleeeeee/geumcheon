﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class Area
    {
        public int AreaIDX { get; set; }
        public string AreaName { get; set; }
        public int AreaToPosition { get; set; }
    }
}