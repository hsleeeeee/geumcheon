﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class Answer
    {
        public int AnswerIdx { get; set; }
        public int MemberIdx { get; set; }
        public string AnswerJson { get; set; }
        public string WriterType { get; set; }

    }

    public class ApplyUniversity
    {
        public int ApplyUnivIdx { get; set; }
        public int MemberIdx { get; set; }
        public int Ranking { get; set; }
        public string UniversityName { get; set; }
        public string StypeRem { get; set; }
        public string MajorName { get; set; }
        public string WriterType { get; set; }
        public DataTable tbUnivesity { get; set; }
    }

    public class DefaultInfo
    {
        public  List<ApplyUniversity>ApplyUniversityItem { get; set; }
        public Answer Answer { get; set; }
    }
}