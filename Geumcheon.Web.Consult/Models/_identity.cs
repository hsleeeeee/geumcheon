﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class _identity
    {
        public static ClaimsIdentity ManualIdentityClaims;

        protected static ClaimsIdentity _identityClaims
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return ManualIdentityClaims;
                }
                return HttpContext.Current.User.Identity as ClaimsIdentity;
            }
        }
        public static void InitIdentity()
        {
           // HttpContext.Current.User.
           // _identityClaims.RemoveClaim(_identityClaims.FindFirst("Idx"));
           //_identityClaims.RemoveClaim(_identityClaims.FindFirst("roll"));
           // _identityClaims.RemoveClaim(_identityClaims.FindFirst("pwd"));
           // _identityClaims.RemoveClaim(_identityClaims.FindFirst("id"));

        }
        public static string Roll
        {
            get
            {
                return _identityClaims.FindFirstValue("roll");
            }
        }

        public static string Password
        {
            get
            {
                return _identityClaims.FindFirstValue("pwd");
            }
        }

        public static string ApplicationIdx
        {
            get
            {
                return _identityClaims.FindFirstValue("Idx");
            }
        }

        public static string ConsultId
        {
            get
            {
                return _identityClaims.FindFirstValue("id");
            }
        }

        public static string TeacherIdx
        {
            get
            {
                return _identityClaims.FindFirstValue("teacherIdx");
            }
        }

        public static string MemberIdx
        {
            get
            {
                return _identityClaims.FindFirstValue("memberIdx");
            }
        }

        public static string Name
        {
            get
            {
                return _identityClaims.FindFirstValue("memberName");
            }
        }

        public static string SchoolType
        {
            get
            {
                return _identityClaims.FindFirstValue("schollType");
            }
        }
    }
}