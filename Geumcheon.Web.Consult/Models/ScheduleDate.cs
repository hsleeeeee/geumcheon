﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class ScheduleDate
    {
        public int ConsultDateIdx { get; set; }
        public string ConsultDate { get; set; }
    }
}