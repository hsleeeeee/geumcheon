﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class Compay
    {
        public string CompayCode { get; set; }
        public string CompayName { get; set; }
    }
}