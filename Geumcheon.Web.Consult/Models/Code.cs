﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Consult.Models
{
    public class Code
    {
        public string Category { get; set; }
        public string Value { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
    }
}