﻿
// 날자 변환
convertDate = function (data) {
    var date = new Date(data);

    if (data != null && !isNaN(date.getTime())) {
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();

        return year.toString() + "-" + (month < 10 ? "0" : "") + month.toString() + "-" + (day < 10 ? "0" : "") + day.toString();
    }
    else {
        return "없음";
    }
}

// 날짜 및 시간 변환
convertDateTime = function (data) {
    var date = new Date(data);
    if (!isNaN(date.getTime())) {
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let week = "월";
        switch (date.getDate()) {
            case 0: date = "일"; break;
            case 1: date = "월"; break;
            case 2: date = "화"; break;
            case 3: date = "수"; break;
            case 4: date = "목"; break;
            case 5: date = "금"; break;
            case 6: date = "토"; break;
        }
        return year.toString() + "-" + (month < 10 ? "0" : "") + month.toString() + "-" + (day < 10 ? "0" : "") + day.toString() + "(" + week + ") " + (hour < 10 ? "0" : "") + hour.toString() + ":" + (minute < 10 ? "0" : "") + minute.toString();
    }
    else {
        return "-";
    }
}
setTitle = function (data,idx) {
    var title = $("<div class='title'><div class='btns'><a style='font-weight:bold; text-decoration:underline;' href=''>" + data + "</a></div></div>");
    title.find("a").attr("href", "Detail?requestIdx=" + idx)
    return title;
}

setStuName = function (data) {
    if (data.length <= 2) {
        return data;
    }
    else {
        var len = data.length - 2;
        var m = '';
        for (var i = 0; i < len; i++) {
            m += '*';
        }
        var s = data.substr(0, 1);
        var e = data.substr(data.length - 1, 1);
        return s + m + e;
    }
}

makeRow = function (row, item, listType) {
    if (listType == 'S_V') {
        row.append("<div>" + item.RowIdx + "</div>");
        row.append("<div>" + setTitle(item.Title, item.RequestIdx) + "</div>");
        row.append("<div>" + item.ProgressName + "</div>");
        row.append("<div>" + item.StudentName + "</div>");
        row.append("<div>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div>" + convertDateTime(item.Schedule.ConsultDate + " " + item.Schedule.ConsultTime) + "</div>");
        return row;
    }

    else if (listType == 'S_I') {
        row.append("<div>" + item.RowIdx + "</div>");
        row.append("<div>" + setTitle(item.Title, item.RequestIdx) + "</div>");
        row.append("<div>" + item.ProgressName + "</div>");
        row.append("<div>" + item.StudentName + "</div>");
        row.append("<div>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div>" + convertDateTime(item.Schedule.ConsultDate + " " + item.Schedule.ConsultTime) + "</div>");
        return row;
    }

    else if (listType == 'S_O') {
        row.append("<div class='rowIdx'>" + item.RowIdx + "</div>");
        row.append("<div class='title'>" + item.Title + "</div>");
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='name'>" + setStuName(item.StudentName) + "</div>");
        //row.append("<div class='name'>" + item.StudentName + "</div>");
        row.append("<div class='date15'>" + convertDate(item.CreateDate) + "</div>");
        
        return row;
    }

    else if (listType == 'C_O') {
        row.append("<div class='rowIdx'>" + item.RowIdx + " </div>");
        row.append(setTitle(item.Title, item.RequestIdx));
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='etc15'>[" + item.member.SchoolTypeName + "]" + item.StudentName + "</div>");
        row.append("<div class='etc15'>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div class='etc15'>" + convertDate(item.DeadlineDate) + "</div>");
      
        return row;
    }

    else if (listType == 'C_V') {
        row.append("<div class='rowIdx'>" + item.RowIdx + " </div>");
        row.append(setTitle(item.Title, item.RequestIdx));
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='etc10'>[" + item.member.SchoolTypeName + "]" + item.StudentName + "</div>");
        row.append("<div class='etc15'>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div class='etc15'>" + convertDate(item.Schedule.ConsultDate) + "</div>");
        row.append(createLinkBtn(item.member.Idx, item.RequestIdx));
        return row;
    }

    else if (listType == 'C_I' ) {
        row.append("<div class='rowIdx'>" + item.RowIdx + " </div>");
        row.append(setTitle(item.Title, item.RequestIdx));
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='etc10'>[" + item.member.SchoolTypeName + "]" + item.StudentName + "</div>");
        row.append("<div class='etc15'>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div class='etc15'>" + convertDate(item.Schedule.ConsultDate) + "</div>");
        row.append(createLinkBtn(item.member.Idx, item.RequestIdx));
        return row;
    }

    else if (listType == 'A_O' || listType == 'F_O') {
        row.append("<div class='rowIdx'>" + item.RowIdx + "</div>");
        row.append(setTitle(item.Title, item.RequestIdx));
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='etc10'>[" + item.member.SchoolTypeName+']'+item.StudentName + '(' + item.member.ID+')' + "</div>");
        row.append("<div class='etc10'>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div class='etc10'>" + convertDate(item.DeadlineDate) + "</div>");
        row.append("<div class='etc15'>" + item.TeacherName + "</div>");

        return row;
    }

    else if (listType == 'A_V' || listType == 'F_V' ) {
        row.append("<div class='rowIdx'>" + item.RowIdx + "</div>");
        row.append(setTitle(item.Title, item.RequestIdx));
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='etc10'>[" + item.member.SchoolTypeName+"]" + item.StudentName + '(' + item.member.ID + ')' + "</div>");
        row.append("<div class='etc10'>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div class='etc10'>" + convertDate(item.Schedule.ConsultDate) + "</div>");
        row.append("<div class='etc10'>" + item.TeacherName + "</div>");        
        
        if (listType == 'A_V') {
            row.append(createLinkBtn(item.member.Idx, item.RequestIdx));
        }
        else {
            row.append(createLinkBtn_Fture(item.member.Idx, item.RequestIdx));
        }
        return row;
    }

    else if (listType == 'A_I' || listType == 'F_I') {
        row.append("<div class='rowIdx'>" + item.RowIdx + "</div>");
        row.append(setTitle(item.Title, item.RequestIdx));
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='etc10'>[" + item.member.SchoolTypeName + "]" + item.StudentName + '(' + item.member.ID + ')' + "</div>");
        row.append("<div class='etc10'>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div class='etc10'>-</div>");
        row.append("<div class='etc10'>-</div>");
        if (listType = 'A_I') {
            row.append(createLinkBtn(item.member.Idx, item.RequestIdx));
        }
        else {
            row.append(createLinkBtn_Fture(item.member.Idx, item.RequestIdx));
        }
        return row;
    }

    else {
        row.append("<div class='rowIdx'>" + item.RowIdx + "</div>");
        row.append("<div class='title'>"+item.Title+"</div>");
        row.append("<div class='etc15'>" + item.ProgressName + "</div>");
        row.append("<div class='name'>" + setStuName(item.StudentName) + "</div>");
        //row.append("<div class='name'>" + item.StudentName + "</div>");
        row.append("<div class='date15'>" + convertDate(item.CreateDate) + "</div>");
        row.append("<div class='date15'>" + convertDateTime(item.Schedule.ConsultDate + " " + item.Schedule.ConsultTime) + "</div>");
        return row;
    }
   
}
// 상담신청 리스트 만들기
createConsultList = function (data, listType) {    
    // 리스트 클리어
    $("div[class=consultlist]").html("");
    $("ul[class=pagination]").html("");    
    if (data.Entities.length == 0) {
        var row = $("<div style='text-align:center; padding:8px; border:1px solid #9E9E9E;'>");
        row.text("상담 신청 내역이 없습니다.");
        $("div[class=consultlist]").append(row);
    }    
    else {      
       
        $.each(data.Entities, function (index,item) {            
            var row = $("<div class='row'>");
            makeRow(row, item, listType);
            $("div[class=consultlist]").append(row);
        });
    }
    //alert('ttt333');
    $("div[class=totalCount]").find("span").text(data.PagingInfo.TotalCount);

    let startpage = (parseInt(data.PagingInfo.CurrentPage / 10) * 10);
    let page = startpage + 1;


    do {
        var row = $("<li>");

        if (page == data.PagingInfo.CurrentPage) {
            row.addClass("active");
            var atag = $("<a>");
            atag.text(page);
            row.append(atag);
        }
        else {
            var atag = $("<a>");
            atag[0].href = "#";
            atag.click(function (e) {
                e.preventDefault();
                requestConsultList(parseInt(e.target.text));
            });
            atag.text(page);
            row.append(atag);
        }

        $("ul[class=pagination]").append(row);

        page = ++page;
    }
    while ($("ul[class=pagination] > li").length * data.PagingInfo.PageSize <= data.PagingInfo.TotalCount && $("ul[class=pagination] > li").length < 10);

    if (page > 10) {
        let prevBtn = $("<a>");
        prevBtn[0].href = "#";
        prevBtn.attr("page", startpage);
        prevBtn.click(function (e) {
            e.preventDefault();
            alert(parseInt(e.target.attributes["page"].value));
        });
        prevBtn.text("<");
        $("ul[class=pagination]").prepend($("<li>").append(prevBtn));
    }
    if (page * data.PagingInfo.PageSize < data.PagingInfo.TotalCount) {
        let nextBtn = $("<a>");
        nextBtn[0].href = "#";
        nextBtn.attr("page", page);
        nextBtn.click(function (e) {
            e.preventDefault();
            alert(parseInt(e.target.attributes["page"].value));
        });
        nextBtn.text(">");
        $("ul[class=pagination]").append($("<li>").append(nextBtn));
    }

};

// 상태 값에 따라 버튼 생성
createStateBtn = function (state) {
    var rtn = $("<div><div class='btns'><a class='btn' /></div></div>");

    if (state == "R") {
        rtn.find("a").addClass("white").text("승인대기");
    }
    else if (state == "I") {
        rtn.find("a").addClass("blue").text("신청승인");
    }
    else {
        rtn.find("a").addClass("grey").text("상담완료");
    }

    return rtn;
}

createLinkBtn = function (memberIdx, requestIdx) {
    var rtn = $("<div class='etc20' style='display:flex;'><div class='btns' style='width:100%;'><a class='btn basicInfo test' /> <a class='btn smartTest test' /></div></div>");
    rtn.find(".basicInfo").text("기초조사서");
    rtn.find(".smartTest").text("검사");

    rtn.find(".basicInfo").click(function () {
        location.href = "StudentInfo?idx=" + memberIdx + "&requestIdx=" + requestIdx;
       
    });
    rtn.find(".smartTest").click(function () {        
        window.open("SmartTestReport?memberIdx=" + memberIdx, "smartTest", "width=700,height=900");
    });
    return rtn;
}

createLinkBtn_Fture = function (memberIdx, requestIdx) {
    var rtn = $("<div class='etc20' style='display:flex;'><div class='btns' style='width:100%;'><a class='btn basicInfo test' /> <a class='btn smartTest test' /></div></div>");
    rtn.find(".basicInfo").text("상담보고서");

    rtn.find(".basicInfo").click(function () {
        location.href = "Reply?w=V&requestIdx=" + requestIdx;
    });
    return rtn;
}

// 페이지 이동
goPage = function (page) {
    requestConsultList(page);
    return false;
}