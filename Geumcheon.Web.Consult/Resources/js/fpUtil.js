﻿var isTest = true;

mlParse = function (str) {
    var jsonStr = str.replace(/&quot;/g, '"').replace(/\n/gi, '\\n').replace(/\r/gi, '\\r').replace(/&#39;/g, '\\"');
    return JSON.parse(jsonStr);
}

mlDebug = function (str) {
    if (isTest) {
        alert(str);
    }
}
//fail(function (request, status, error) {
//    mlAjaxError(request, status, error);
//});
mlAjaxError = function (request, status, error) {
    if (isTest) {
        alert("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
    }
    else {
        alert("처리중오류가발생했습니다.")
    }
}

mlisEmpty = function (value) {
    if (value == "" || value == null || value == undefined || (value != null && typeof value == "object" && !Object.keys(value).length)) {
        return true
    }
    else {
        return false
    }
}