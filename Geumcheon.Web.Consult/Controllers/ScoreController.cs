﻿using Geumcheon.Web.Consult.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Geumcheon.Web.Consult.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BasicInfo()
        {
            return View();
        }

        public ActionResult Moi()
        {
            return View();
        }

        public ActionResult Nesin()
        {
            return View();
        }



        [HttpPost]
        public JsonResult ModifyAnswer(string writerType, string answer)
        {
            Answer model = new Answer
            {
                MemberIdx  = 1
            ,   WriterType = writerType
            ,   AnswerJson = answer
            };
            int result = ConsultRepository.Instance.SetAnswer(model);

            if (result > 0)
            {
                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }

        [HttpPost]
        public JsonResult ModifyUniversityitem(string writerType, string univItemJson)
        {
            DataTable dtUnivItem = JsonConvert.DeserializeObject<DataTable>(univItemJson);
            ApplyUniversity model = new ApplyUniversity
            {
                MemberIdx = 1
            ,   WriterType = writerType
            ,   tbUnivesity = dtUnivItem
            };            

            int result = ConsultRepository.Instance.SetApplyUniversity(model);
            if (result > 0)
            {
                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }
    }
}