﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Geumcheon.Web.Core.ViewModel;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Geumcheon.Web.Consult.Models;
using System.IO;
using Geumcheon.Web.Consult.App_Start;

namespace Geumcheon.Web.Consult.Controllers
{
    public class MyPageController : Controller
    {
        static string _COMPANY_CODE = "0001";
        static int _PAGESIZE = 15;

        // GET: Score
        public ActionResult Index()
        {
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult BasicInfo(string writerType = "p", int memberIdx = 0)
        {
            DefaultInfo model = new DefaultInfo();
            ViewBag.WriterType = writerType.ToUpper();
            if (_identity.Roll == "STUDENT" || _identity.Roll == "PARENT")
            {
                model = MyPageRepository.Instance.GetDefaultInfo(Convert.ToInt32(_identity.MemberIdx), writerType.ToUpper(), true);
            }
            else
            {
                model = MyPageRepository.Instance.GetDefaultInfo(memberIdx, writerType.ToUpper(), true);
            }
            
            if (model.ApplyUniversityItem==null)
            {
                ViewBag.UnivJson = string.Empty;               
            }
            else
            {
                var jsMaster = new JavaScriptSerializer();
                string jsonMaster = jsMaster.Serialize(model.ApplyUniversityItem);
                ViewBag.UnivJson = jsonMaster;
            }
            if (model.Answer==null)
            {
                ViewBag.AnswerJson = string.Empty;
            }
            else
            {
                ViewBag.AnswerJson = model.Answer.AnswerJson;
            }
            

            return View();
        }
        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult FileUpload()
        {
            List<Files> lstFileInfo = new List<Files>();
            lstFileInfo = MyPageRepository.Instance.GetFiles(_identity.MemberIdx);

            return View(lstFileInfo);
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult Moi()
        {
            string info = MyPageRepository.Instance.GetMoiInfo(Convert.ToInt32(_identity.MemberIdx));
            ViewBag.TestInfo = info;
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult Nesin()
        {
            Nesin model = MyPageRepository.Instance.GetNesin(Convert.ToInt32(_identity.MemberIdx));
            if(model == null)
            {
                ViewBag.IsCreate = 1;
                model = new Nesin();
            }
            else
            {
                ViewBag.IsCreate = 0;
            }
            return View(model);
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult SmartTest()
        {
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult RequestList()
        {
            ViewBag.Navigation = "REQUESTLIST";
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult StudentRecord()
        {
            ViewBag.Navigation = "REQUESTLIST";
            List<Files> file = MyPageRepository.Instance.GetFiles(_identity.MemberIdx);
            
            return View(file);
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult Survey()
        {
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult SmartCoaching()
        {
            ViewBag.Navigation = "REQUESTLIST";
            string rVal = MyPageRepository.Instance.GetConsultSmartCoaching(_identity.MemberIdx);
            if (string.IsNullOrEmpty(rVal))
            {
                ViewBag.ID = "NOT FOUND";
                ViewBag.PWD = "NOT FOUND";
            }
            else
            {

                try
                {
                    ViewBag.ID = rVal.Split('/')[0];
                    ViewBag.PWD = rVal.Split('/')[1];
                }
                catch
                {
                    ViewBag.ID = "NOT FOUND";
                    ViewBag.PWD = "NOT FOUND";
                }
            }
            return View();
        }




        //[HttpPost]
        //public ActionResult UploadFile()
        //{
        //    string[] arrFileType = Request.Form["file_type"].ToString().Split(',');
        //    List<Files> entities = new List<Files>();
        //    try
        //    {
        //        int i = 0;
        //        foreach (string fle in Request.Files)
        //        {
        //            HttpPostedFileBase file = Request.Files[i];
        //            int lectureSeq = Convert.ToInt32(Request.Form["___memberidx___"].ToString());
        //            string filetype = arrFileType[i];
        //            string fileId = Guid.NewGuid().ToString();
        //            string fileName = file.FileName;
        //            i++;

        //            string pathForSaving = Server.MapPath("~/Uploads");
        //            file.SaveAs(Path.Combine(pathForSaving, fileId));

        //            Files entity = new Files();
        //            entity.MemberIdx = _identity.MemberIdx;
        //            entity.FileID = fileId;
        //            entity.FileName = fileName;
        //            entity.FileSize = file.ContentLength.ToString();
        //            entity.FileType = filetype;

        //            entities.Add(entity);

        //        }

        //        int rVal = MyPageRepository.Instance.FilesUpsert(entities[0]);

        //        if (rVal == 1)
        //        {
        //            return Json(new
        //            {
        //                result = "T"
        //            });
        //        }
        //        else
        //        {
        //            return Json(new
        //            {
        //                result = "F"
        //            });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new
        //        {
        //            result = "F"
        //        });
        //    }
        //}
        [HttpPost]
        [Authorize(Roles = "GEUMCHEON")]
        public ActionResult UploadFile()
        {
            string updateFileid = Request.Form["___update_file___"];
            string uploadId = _identity.MemberIdx;    


            Files updateFiles = new Files();
            if (string.IsNullOrEmpty(updateFileid))
            {
                updateFileid = "";
            }
            updateFiles.FileID = "|" + updateFileid.Replace(",", "|") + "|";
            updateFiles.MemberIdx = uploadId;

            try
            {
                List<Files> list = new List<Files>();
                List<HttpPostedFileBase> filelist = new List<HttpPostedFileBase>();
                int i = 0;
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    try
                    {
                        string fileId = Guid.NewGuid().ToString();
                        list.Add(new Files
                        {
                            FileID = fileId
                            ,
                            FileName = file.FileName
                            ,
                            FileSize = file.ContentLength.ToString()

                        });
                        string pathForSaving = Server.MapPath("~/Uploads");
                        file.SaveAs(Path.Combine(pathForSaving, fileId));                        
                        filelist.Add(file);
                        i++;
                    }
                    catch
                    {

                    }
                }


                foreach (Files file in list)
                {
                    file.MemberIdx = uploadId;
                }

                //if (list.Count() > 0)
                //{
                //    FileSave(list, filelist);
                //}
                int rVal = MyPageRepository.Instance.FileInsert(list, updateFiles);
                if (rVal > 0)
                {
                    return Json(new
                    {
                        result = "T"
                    });
                }
                else
                {
                    return Json(new
                    {
                        result = "F"
                    });
                }
            }

            catch (Exception ex)
            {
                return Json(new
                {
                    result = ex.Message
                });
            }

        }

        [HttpPost]
        public JsonResult ModifyDefailtInfo(string writerType, string answer, string univitem)
        {
            Answer ans= new Answer
            {
                MemberIdx = Convert.ToInt32(_identity.MemberIdx)
            ,    WriterType = writerType
            ,    AnswerJson = answer
            };
            DataTable dtUnivItem = JsonConvert.DeserializeObject<DataTable>(univitem);

            DefaultInfo defaultInfo = new DefaultInfo
            {
                Answer = ans,
                tbUnivesity = dtUnivItem
            };

            int result = MyPageRepository.Instance.setDefaultInfo(defaultInfo);

            if (result > 0)
            {
                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }

        [HttpPost]
        public JsonResult ModifyAnswer(string writerType, string answer)
        {

            Answer ans= new Answer
            {
                MemberIdx = Convert.ToInt32(_identity.MemberIdx)
            ,    WriterType = writerType
            ,    AnswerJson = answer
            };
            
            int result = MyPageRepository.Instance.SetAnswer(ans);

            if (result > 0)
            {
                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }

        [HttpPost]
        public JsonResult ModifyUniversityitem(string writerType, string univItemJson)
        {
            DataTable dtUnivItem = JsonConvert.DeserializeObject<DataTable>(univItemJson);
            ApplyUniversity model = new ApplyUniversity
            {
                MemberIdx = Convert.ToInt32(_identity.MemberIdx)
                ,
                WriterType = writerType
                ,tbUnivesity = dtUnivItem
            };

            int result = MyPageRepository.Instance.SetApplyUniversity(model);
            if (result > 0)
            {
                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }

        [HttpPost]
        public JsonResult ModifyNesin(string nesinJson, double kor
                                                      , double eng
                                                      , double math
                                                      , double soc
                                                      , double sci)
        {

            Nesin nesin = new Nesin
            {
                stu_id = Convert.ToInt32(_identity.MemberIdx)
               ,scoreJson = nesinJson
               ,kor_grade = kor
               ,eng_grade = eng
               ,math_grade = math
               ,sci_grade = sci
               ,soc_grade = soc
            };

            int result = MyPageRepository.Instance.SetNesin(nesin);

            if (result > 0)
            {
                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }



        [HttpPost]
        public JsonResult GetSat(string stuid, string grade,  string groupKey)
        {
            List<JsonSat> lstJsonScore = new List<JsonSat>();
            List<Sat> lstSat = MyPageRepository.Instance.GetSat(stuid,grade,groupKey);
            if(lstSat == null)
            {
                return Json(new
                {
                    jsonMsg = string.Empty
                });
            }        
            else
            {
                Sat sat = lstSat[0];
                SatItem Kor = new SatItem
                {
                    PartName = sat.PartNameKor,
                    PartCode = sat.PartCodeKor,
                    Gra = sat.GraKor.ToString(),
                    Std = sat.StdKor.ToString(),
                    Per = sat.PerKor.ToString(),
                    Won = sat.WonKor.ToString(),
                };
                SatItem Eng = new SatItem
                {
                    PartName = sat.PartNameEng,
                    PartCode = sat.PartCodeEng,
                    Gra = sat.GraEng.ToString(),
                    Std = "-",
                    Per = "-",
                    Won = sat.WonEng.ToString(),
                };
                SatItem Mat = new SatItem
                {
                    PartName = sat.PartNameMat,
                    PartCode = sat.PartCodeMat,
                    Gra = sat.GraMat.ToString(),
                    Std = sat.StdMat.ToString(),
                    Per = sat.PerMat.ToString(),
                    Won = sat.WonMat.ToString(),
                };
                SatItem Tam1 = new SatItem
                {
                    PartName = sat.PartNameTam1,
                    PartCode = sat.PartCodeTam1,
                    Gra = sat.GraTam1.ToString(),
                    Std = sat.StdTam1.ToString(),
                    Per = sat.PerTam1.ToString(),
                    Won = sat.WonTam1.ToString(),
                };
                SatItem Tam2 = new SatItem
                {
                    PartName = sat.PartNameTam2,
                    PartCode = sat.PartCodeTam2,
                    Gra = sat.GraTam2.ToString(),
                    Std = sat.StdTam2.ToString(),
                    Per = sat.PerTam2.ToString(),
                    Won = sat.WonTam2.ToString(),
                };
                SatItem His = new SatItem
                {
                    PartName = sat.PartNameHis,
                    PartCode = sat.PartCodeHis,
                    Gra = sat.GraHis.ToString(),
                    Std = "-",
                    Per = "-",
                    Won = sat.WonHis.ToString(),
                };
                SatItem Lang2 = new SatItem
                {
                    PartName = sat.PartNameLang2,
                    PartCode = sat.PartCodeLang2,
                    Gra = sat.GraLang2.ToString(),
                    Std = sat.StdLang2.ToString(),
                    Per = sat.PerLang2.ToString(),       
                    Won = sat.WonLang2.ToString(),
                };
                JsonSat jsonScore = new JsonSat
                {
                    SchoolYear = sat.SchoolYear,
                    TestYear = sat.TestYear,
                    TestDate = sat.TestDate,   
                    GroupKey = groupKey,
                    Kor = Kor,
                    Mat = Mat,
                    Eng = Eng,
                    Tam1 = Tam1,
                    Tam2 = Tam2,
                    His = His,
                    Lang2 = Lang2,                    
                };
                lstJsonScore.Add(jsonScore);
                var jsMaster = new JavaScriptSerializer();
                string jsonMaster = jsMaster.Serialize(lstJsonScore);

                return Json(new
                {
                    jsonMsg = jsonMaster
                });
            }
            
        }

        [HttpPost]
        public JsonResult GetTestDate(int grade)
        {
            List<ScorePer>lstScorePer  =  MyPageRepository.Instance.GetScorePer_TestDate(grade);
            var jsMaster = new JavaScriptSerializer();
            string jsonMaster = jsMaster.Serialize(lstScorePer);
            return Json(new
            {
                jsonMsg = jsonMaster
            });
        }


        [HttpPost]
        public JsonResult GetPartCode(string groupKey, int grade, string cssCode=null)
        {
            List<ScorePer> lstScorePer = MyPageRepository.Instance.GetScorePer_PartCode(groupKey,grade, cssCode);
            var jsMaster = new JavaScriptSerializer();
            string jsonMaster = jsMaster.Serialize(lstScorePer);
            return Json(new
            {
                jsonMsg = jsonMaster
            });
        }

        [HttpPost]
        public JsonResult ConvertStdToPer(string groupKey, string partCode, string grade, string std)
            {
            SatItem sat = MyPageRepository.Instance.ConverStdToPer(groupKey, partCode, grade, std);
            if(sat == null)
            {
                return Json(new
                {
                     per = 0
                    ,grd = 0
                });
            }
            else
            {
               return Json(new
                {
                     per = sat.Per
                    ,grd = sat.Gra
                });
            }
            
        }

        [HttpPost]
        public JsonResult ConvertOriToStd(string groupKey, string partCode, string grade, string ori)
        {
            SatItem sat = MyPageRepository.Instance.ConverOriToStd(groupKey, partCode, grade, ori);
            if (sat == null)
            {
                return Json(new
                {
                     per = 0
                    ,grd = 0
                    ,std = 0
                });
            }
            else
            {
                if (partCode == "004" || partCode == "052")
                {
                    return Json(new
                    {
                        per = 0
                       ,grd = sat.Gra
                       ,std = 0

                    });
                }
                else
                {
                    return Json(new
                    {
                         per = sat.Per
                       ,grd = sat.Gra
                       ,std = sat.Std
                       
                    });
                }                
            }

        }

        [HttpPost]
        public JsonResult SaveScore(string json, int grade, string groupkey)
        {
            Sat sat = new Sat();
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(json);
            foreach(DataRow dr in dt.Rows)
            {
                switch(dr["cssName"].ToString())
                {
                    case "kor":
                        {
                            sat.PartCodeKor = dr["partCode"].ToString();
                            sat.GraKor = ConvertStringToDouble(dr["Grd"]);
                            sat.StdKor = ConvertStringToDouble(dr["Std"]);
                            sat.PerKor = ConvertStringToDouble(dr["Per"]);
                            sat.WonKor = ConvertStringToDouble(dr["Ori"]);
                            break;
                        }
                    case "mat":
                        {
                            sat.PartCodeMat = dr["partCode"].ToString();
                            sat.GraMat = ConvertStringToDouble(dr["Grd"]);
                            sat.StdMat = ConvertStringToDouble(dr["Std"]);
                            sat.PerMat = ConvertStringToDouble(dr["Per"]);
                            sat.WonMat = ConvertStringToDouble(dr["Ori"]);
                            break;
                        }
                    case "eng":
                        {
                            sat.PartCodeEng = dr["partCode"].ToString();
                            sat.GraEng = ConvertStringToDouble(dr["Grd"]);
                            sat.StdEng = 0;
                            sat.PerEng = 0;
                            sat.WonEng = ConvertStringToDouble(dr["Ori"]);
                            break;
                        }
                    case "his":
                        {
                            sat.PartCodeHis = dr["partCode"].ToString();
                            sat.GraHis = ConvertStringToDouble(dr["Grd"]);
                            sat.StdHis = 0;
                            sat.PerHis = 0;
                            sat.WonHis = ConvertStringToDouble(dr["Ori"]);
                            break;
                        }
                    case "tam1":
                        {
                            sat.PartCodeTam1 = dr["partCode"].ToString();
                            sat.GraTam1 = ConvertStringToDouble(dr["Grd"]);
                            sat.StdTam1 = ConvertStringToDouble(dr["Std"]);
                            sat.PerTam1 = ConvertStringToDouble(dr["Per"]);
                            sat.WonTam1 = ConvertStringToDouble(dr["Ori"]);
                            break;
                        }
                    case "tam2":
                        {
                            sat.PartCodeTam2 = dr["partCode"].ToString();
                            sat.GraTam2 = ConvertStringToDouble(dr["Grd"]);
                            sat.StdTam2 = ConvertStringToDouble(dr["Std"]);
                            sat.PerTam2 = ConvertStringToDouble(dr["Per"]);
                            sat.WonTam2 = ConvertStringToDouble(dr["Ori"]);
                            break;
                        }
                    case "lang2":
                        {
                            sat.PartCodeLang2 = dr["partCode"].ToString();
                            sat.GraLang2 = ConvertStringToDouble(dr["Grd"]);
                            sat.StdLang2 = ConvertStringToDouble(dr["Std"]);
                            sat.PerLang2 = ConvertStringToDouble(dr["Per"]);
                            sat.WonLang2 = ConvertStringToDouble(dr["Ori"]);
                            break;
                        }
                }
            }
            int rVal = MyPageRepository.Instance.SetScore(sat, grade, Convert.ToInt32(_identity.MemberIdx), groupkey);
            return Json(new
            {
                jsonMsg = rVal
            });
        }

        public double ConvertStringToDouble(object a)
        {

            try
            {
                return Convert.ToDouble(a.ToString());
            }
            catch
            {
                return 0;
            }
        }

        [HttpPost]
        public JsonResult MyRequestList()
        {
            try
            {
                JObject data = JObject.Parse(new System.IO.StreamReader(Request.InputStream).ReadToEnd());

                int page = data.SelectToken("page")?.Value<int>() ?? 1;
                string consultType = data.SelectToken("type")?.Value<string>();
                string search = data.SelectToken("search")?.Value<string>();
                string order = data.SelectToken("order")?.Value<string>();


                ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>()
                {
                    Entities = new List<EntranceRequest>(),
                    Search = new SearchInfo(),
                    PagingInfo = new PagingInfo(page, _PAGESIZE, 0, order)
                };

                model.Search.CompanyCode = _COMPANY_CODE;
                model.Search.ConsultWay = null;
                model.Search.Name = search;
                model.Search.TeacherId = null;

                EntranceRequestRepository.Instance.EntranceRequestSelectPaging(int.Parse(_identity.MemberIdx), model);
                List<PresentationRequest> lstPresentationRequest =  PresentationRepository.Instance.PresentationRequestSelectList(int.Parse(_identity.MemberIdx), null);
                return Json(new
                {
                    ok = true,
                    data1 = model,
                    data2 = lstPresentationRequest
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = true,
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }    
    
}