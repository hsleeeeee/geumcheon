﻿using Geumcheon.Web.Consult.Models;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;

using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Geumcheon.Web.Consult.Controllers
{
    public class AccountController : Controller
    {
        static string _COMPANY_CODE = "0001";

        #region View

        // GET: Account
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        // 회원가입
        public ActionResult Join()
        {
            return View();
        }

        // 로그인
        public ActionResult Login(string returnUrl = null)
        {
            returnUrl = string.IsNullOrEmpty(returnUrl) ? "/Application/Info" : returnUrl;
            ViewBag.rtnUrl = returnUrl;

            return View();
        }

        // 로그아웃
        public ActionResult Logout()
        {
            Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        // 사용자 정보 수정
        [Authorize(Roles = "GEUMCHEON")]
        public ActionResult Modify()
        {
            var member = MemberRepository.Instance.MemberSelectItem(_identity.ConsultId);
            if(member.AccountType == "S")
            {
                if (string.IsNullOrEmpty(member.Hp_S))
                {
                    member.Hp_S = member.Hp;
                }
                if (string.IsNullOrEmpty(member.Hp_P))
                {
                    member.Hp_P = "00000000000";
                }

            }
            else
            {
                if (string.IsNullOrEmpty(member.Hp_P))
                {
                    member.Hp_P = member.Hp;
                }
                if (string.IsNullOrEmpty(member.Hp_S))
                {
                    member.Hp_S = "00000000000";
                }
            }
            return View(member);
        }

        #endregion


        #region AJAX

        [HttpPost]
        public JsonResult InsertAccount()
        {
            try
            {
                JObject model = JObject.Parse(new System.IO.StreamReader(Request.InputStream).ReadToEnd());
                string hp = model.SelectToken("hp_p").Value<string>();
                if (model.SelectToken("type").Value<string>() == "S")
                {
                    hp = model.SelectToken("hp_s").Value<string>();
                }

                Member member = JsonConvert.DeserializeObject<Member>(model.ToString());
                member.ID = model.SelectToken("id").Value<string>();
                member.Password = model.SelectToken("pwd").Value<string>();
                member.Name = model.SelectToken("name").Value<string>();
                member.Hp = hp;
                member.Email = model.SelectToken("email").Value<string>();
                member.School = model.SelectToken("school").Value<string>();
                member.Grade = model.SelectToken("grade").Value<int>();
                member.Gender = model.SelectToken("gender").Value<int>();
                member.AccountType = model.SelectToken("type").Value<string>();
                member.Address = model.SelectToken("address").Value<string>();
                member.JoinCase = model.SelectToken("joincase").Value<int>();
                member.JoinCaseEtc = model.SelectToken("joincaseetc").Value<string>();
                member.Hp_P = model.SelectToken("hp_p").Value<string>();
                member.Hp_S = model.SelectToken("hp_s").Value<string>();
                member.SchoolType = model.SelectToken("SchoolType").Value<string>();

                int idx = MemberRepository.Instance.MemberInsert(member);

                if (idx > 0)
                {
                    var hope = model.SelectToken("hope");

                    DataTable univ = new DataTable();
                    univ.Columns.Add(new DataColumn("ApplyUnivIdx", typeof(int)));
                    univ.Columns.Add(new DataColumn("Ranking", typeof(int)));
                    univ.Columns.Add(new DataColumn("UniversityName", typeof(string)));
                    univ.Columns.Add(new DataColumn("StypeRem", typeof(string)));
                    univ.Columns.Add(new DataColumn("MajorName", typeof(string)));

                    foreach (var item in hope)
                    {
                        DataRow row = univ.NewRow();

                        row["ApplyUnivIdx"] = 0;

                        row["Ranking"] = item.SelectToken("rank").Value<int>();
                        row["UniversityName"] = item.SelectToken("univ").Value<string>();
                        row["StypeRem"] = item.SelectToken("mode").Value<string>();
                        row["MajorName"] = item.SelectToken("department").Value<string>();
                        univ.Rows.Add(row);
                        row = null;
                    }

                    if (univ.Rows.Count > 0)
                    {
                        MyPageRepository.Instance.SetApplyUniversity(new ApplyUniversity
                        {
                            MemberIdx = idx,
                            WriterType = member.AccountType,
                            tbUnivesity = univ
                        });
                    }
                }

                return Json(new
                {
                    ok = true,
                    message = "가입되었습니다."
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = "오류가 발생하였습니다."
                });
            }
        }

        [HttpGet]
        public JsonResult CheckAccount(string id)
        {
            try
            {
                Member member = new Member();

                if (MemberRepository.Instance.MemberIDCheck(HttpUtility.UrlDecode(id)) == null)
                {
                    return Json(new
                    {
                        ok = true,
                        message = "사용 가능한 ID 입니다."
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        ok = false,
                        message = "이미 사용 중인 ID 입니다."
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = "오류가 발생하였습니다."
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize(Roles = "GEUMCHEON")]
        public JsonResult UpdateAccount()
        {
            try
            {
                JObject model = JObject.Parse(new System.IO.StreamReader(Request.InputStream).ReadToEnd());                
                string hp = model.SelectToken("hp_s").Value<string>();
                if (_identity.Roll == "PARENT")
                {
                    hp = model.SelectToken("hp_p").Value<string>();
                }
                Member member = new Member
                {
                    ID = _identity.ConsultId,
                    Password = model.SelectToken("pwd")?.Value<string>(),
                    Name = model.SelectToken("name")?.Value<string>(),
                    Hp = hp,
                    Email = model.SelectToken("email")?.Value<string>(),
                    School = model.SelectToken("school")?.Value<string>(),
                    Grade = model.SelectToken("grade")?.Value<int>() ?? 1,
                    Gender = model.SelectToken("gender")?.Value<int>() ?? 1,
                    Address = model.SelectToken("address").Value<string>(),
                    IsUse = true,
                    Hp_P = model.SelectToken("hp_p").Value<string>(),
                    Hp_S = model.SelectToken("hp_s").Value<string>(),
                    SchoolType = model.SelectToken("SchoolType").Value<string>()
            };

                int idx = MemberRepository.Instance.MemberUpdate(member);

                var hope = model.SelectToken("hope");

                if (hope != null)
                {
                    DataTable univ = new DataTable();
                    univ.Columns.Add(new DataColumn("ApplyUnivIdx", typeof(int)));
                    univ.Columns.Add(new DataColumn("Ranking", typeof(int)));
                    univ.Columns.Add(new DataColumn("UniversityName", typeof(string)));
                    univ.Columns.Add(new DataColumn("StypeRem", typeof(string)));
                    univ.Columns.Add(new DataColumn("MajorName", typeof(string)));

                    foreach (var item in hope)
                    {
                        int unividx = 0;
                        int.TryParse(item.SelectToken("unividx")?.Value<string>(), out unividx);

                        DataRow row = univ.NewRow();

                        row["ApplyUnivIdx"] = unividx;
                        row["Ranking"] = item.SelectToken("rank").Value<int>();
                        row["UniversityName"] = item.SelectToken("univ").Value<string>();
                        row["StypeRem"] = item.SelectToken("mode").Value<string>();
                        row["MajorName"] = item.SelectToken("department").Value<string>();
                        univ.Rows.Add(row);
                        row = null;
                    }

                    if (univ.Rows.Count > 0)
                    {
                        MyPageRepository.Instance.SetApplyUniversity(new ApplyUniversity
                        {
                            MemberIdx = idx,
                            WriterType = _identity.Roll == "PARENT" ? "P" : "S",
                            tbUnivesity = univ
                        });
                    }
                }

                if (idx > 0)
                {
                    return Json(new
                    {
                        ok = true,
                        message = "수정하였습니다."
                    });
                }
                else
                {
                    return Json(new
                    {
                        ok = true,
                        message = "실패하였습니다."
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = "오류가 발생하였습니다."
                });
            }
        }

        [HttpPost]
        public async Task<JsonResult> AccountLogin(string id, string pwd)
        {
            try
            {
                var member = MemberRepository.Instance.MemberLogin(id, pwd);

                if (member == null)
                {
                    Teacher teacher = ConsultRepository.Instance.LoginTeacher(_COMPANY_CODE, id, pwd);
                    if (teacher == null)
                    {
                        return Json(new
                        {
                            ok = false,
                            message = "ID 또는 비밀번호가 틀렸습니다."
                        });
                    }
                    else
                    {
                        var identity = new ClaimsIdentity(
                            DefaultAuthenticationTypes.ApplicationCookie,
                            ClaimTypes.NameIdentifier,
                            ClaimTypes.Role
                        );

                        identity.AddClaim(new Claim("id", id));
                        identity.AddClaim(new Claim("teacherIdx",teacher.TeacherIdx.ToString()));
                        identity.AddClaim(new Claim("roll", "CONSULT"));
                        identity.AddClaim(new Claim("memberName", teacher.TeacherName));
                        identity.AddClaim(new Claim(ClaimTypes.Role, "GEUMCHEON"));

                        var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                        if (authenticationContext != null)
                        {
                            Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                                identity, authenticationContext.Properties);

                        }

                        Authentication.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true,
                            ExpiresUtc = DateTime.UtcNow.AddHours(24)
                        },
                        identity);

                        return Json(new
                        {
                            ok = true,
                            message = ""
                        });
                    }
                }
                else if (!member.IsUse)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "사용이 중지 된 ID 입니다."
                    });
                }
                else
                {
                    var identity = new ClaimsIdentity(
                        DefaultAuthenticationTypes.ApplicationCookie,
                        ClaimTypes.NameIdentifier,
                        ClaimTypes.Role
                    );http://geumcheon.eduplan.co.kr/Account/Logout

                    identity.AddClaim(new Claim("id", id));
                    identity.AddClaim(new Claim("memberIdx", member.Idx.ToString()));
                    identity.AddClaim(new Claim("schollType", member.SchoolType.ToString()));
                    identity.AddClaim(new Claim(ClaimTypes.Role, "GEUMCHEON"));

                    if (member.AccountType == "S")
                    {
                        identity.AddClaim(new Claim("roll", "STUDENT"));
                    }
                    else if (member.AccountType == "P")
                    {
                        identity.AddClaim(new Claim("roll", "PARENT"));
                    }

                    var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                    if (authenticationContext != null)
                    {
                        Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                            identity, authenticationContext.Properties);

                    }

                    Authentication.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTime.UtcNow.AddHours(24)
                    },
                    identity);


                    return Json(new
                    {
                        ok = true,
                        message = ""
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = "ID 또는 비밀번호가 틀렸습니다."
                });
            }
        }

        #endregion

        #region Private

        IAuthenticationManager Authentication
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        #endregion
    }
}