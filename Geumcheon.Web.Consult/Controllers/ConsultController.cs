﻿using Geumcheon.Web.Consult.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Security.Claims;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using Geumcheon.Web.Core.ViewModel;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Consult.App_Start;

namespace Geumcheon.Web.Consult.Controllers
{
    public class ConsultController : Controller
    {
        static string _COMPANY_CODE = "0001";
        static int _PAGESIZE = 15;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string actionName = filterContext.ActionDescriptor.ActionName.ToLower();
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.ToLower();

            base.OnActionExecuting(filterContext);

        }

        public string SetTitle(string w,bool isBehind=true)
        {
            if (isBehind)
            {
                if (w == "B")
                {
                    return "온라인 상담 - ";
                }
                else if (w == "O")
                {
                    return "온라인 컨설팅 - ";
                }
                else if (w == "V")
                {
                    return "화상 컨설팅 - ";
                }
                else if (w == "I")
                {
                    return "대면 컨설팅 - ";
                }
                else
                {
                    return "전화 상담 - ";
                }
            }
            else
            {
                if (w == "B")
                {
                    return " - 온라인 상담";
                }
                else if (w == "O")
                {
                    return " - 온라인 컨설팅";
                }
                else if (w == "V")
                {
                    return " - 화상 컨설팅";
                }
                else if (w == "I")
                {
                    return " - 대면 컨설팅";
                }
                else
                {
                    return " - 전화 상담";
                }
            }
        }

        public string SetNavigation(string w, string roll=null)
        {
            string rVal = string.Empty;
            switch (w)
            {
                case "B":
                    {
                        rVal = "BOARD";
                        break;
                    }
                case "T":
                    {
                        rVal = "TEL";
                        break;
                    }
            }
            return rVal;
        }

        // GET: Consult
        public ActionResult Index(string w="B",int page=1,SearchInfo searchInfo = null)
        {

            ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>()
            {
                Entities = new List<EntranceRequest>(),
                Search = new SearchInfo(),
                PagingInfo = new PagingInfo(page, _PAGESIZE, 0, searchInfo.OrderType)
            };

            if (searchInfo.ConsultWay != null)
            {
                w = searchInfo.ConsultWay;
            }
            
            model.Search.CompanyCode = _COMPANY_CODE;
            model.Search.ConsultWay = w;
            model.Search.Name = searchInfo.Name;
            model.Search.TeacherId = searchInfo.TeacherId;
            
            //model.Search.SearchType = search.SearchType;

            ConsultRepository.Instance.EntranceRequestSelectPaging(model);
            model.Search.Name = searchInfo.Name;
            model.Search.TeacherId = searchInfo.TeacherId;
            model.Search.OrderType = searchInfo.OrderType;

            ViewBag.ConsultWay = w;
            
            if (_identity.Roll == null)
            {
                ViewBag.Title = SetTitle(w).Replace("-", "");                
            }
            else
            {
                if (_identity.Roll.ToString() == "CONSULT")
                {
                    ViewBag.Title = SetTitle(w) + "신청목록";
                }
                else
                {
                    ViewBag.Title = SetTitle(w).Replace("-", "");
                }
            }
            ViewBag.Navigation = SetNavigation(w);
            return View(model);

        }

        public ActionResult Admin(string w="O", int page = 1, int state = 0, string keyword = "" )
        {
            ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>()
            {
                Entities = new List<EntranceRequest>(),
                Search = new SearchInfo(),
                PagingInfo = new PagingInfo(page, _PAGESIZE, 0)
            };
            model.Search.CompanyCode = _COMPANY_CODE;
            model.Search.ConsultWay = w;
            model.Search.SearchType = state.ToString();
            model.Search.SearchText = keyword;
            ConsultRepository.Instance.EntranceRequestSelectPaging(model);

           List<Teacher> lstTeacher =  ConsultRepository.Instance.GetTeacher(_COMPANY_CODE);

            ViewBag.Teacher = lstTeacher;
            ViewBag.ConsultWay = w;
            ViewBag.Title =  "교사관리"+ SetTitle(w, false);
            ViewBag.Navigation = "M_"+SetNavigation(w);
            return View(model);
        }

        public ActionResult Form(string w="B",int idx = 0)
        {
            ViewBag.Title = SetTitle(w) + "상담입력";
            Application application = new Application();
            if (idx > 0)
            {
                application = ConsultRepository.Instance.GetApplicationForStudent(_identity.ApplicationIdx, _identity.Password);
                ViewBag.ConsultWay = application.ConsultWay;
                ViewBag.ApplicationIdx = application.ApplicationIdx;
                if (application.ConsultWay == "B")
                {
                    ViewBag.ConsultDate = "";
                    ViewBag.ConsultTimeIdx = "";
                }
                else
                {
                    ViewBag.ConsultDate = application.Schedule.ConsultDate;
                    ViewBag.ConsultTimeIdx = application.Schedule.ConsultTimeIdx;
                    ViewBag.ConsultTime = application.Schedule.ConsultTime;
                }
            }
            else
            {
                ViewBag.ConsultWay = w;
                ViewBag.ApplicationIdx = 0;
                ViewBag.ConsultDate = "";
                ViewBag.ConsultTimeIdx = "";
                ViewBag.ConsultTime = "";

            }

            //application.ApplicationIdx = 0;
            ViewBag.Navigation = SetNavigation(w);
            return View(application);
        }

        public ActionResult Detail(int idx)
        {
            Application application = new Application();
            if (_identity.Roll == "STUDENT" || _identity.Roll == "PARENT")
            {
                if (idx > 0)
                {
                    application = ConsultRepository.Instance.GetApplicationForStudent(_identity.ApplicationIdx, _identity.Password);
                    ViewBag.ConsultWay = application.ConsultWay;
                }
                else
                {

                }
            }
            else
            {
                application = ConsultRepository.Instance.GetApplicationByApplicationIdx(idx);
                
                ViewBag.ConsultWay = application.ConsultWay;
            }

            ViewBag.Title = SetTitle(application.ConsultWay) + "상담내용 상세보기";
            ViewBag.Navigation = SetNavigation(application.ConsultWay);
            return View(application);
            
        }

        public ActionResult Reply(string w, int idx = 0)
        {
            Reply reply = new Reply();
            if(_identity.Roll == "CONSULT")
            {
                reply =  ConsultRepository.Instance.GetReply(idx, _identity.ConsultId);
            }
            if (w == "B")
            {
                ViewBag.Title = "상담입력";
            }
            else
            {
                ViewBag.Title = "상담보고서 작성";
            }
            ViewBag.ConsultWay = w;
            ViewBag.Navigation = SetNavigation(w);
            return View(reply);
        }

        public ActionResult Info()
        {
            ViewBag.Title = "온라인상담실 이용안내";
            ViewBag.Navigation = "INFO";
            return View();
        }

        public ActionResult Notice()
        {
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult ScheduleView(int idx=1)
        {
            if(_identity.ConsultId != "future")
            {
                idx = Convert.ToInt32(_identity.TeacherIdx);
            }
            else
            {
                List<Teacher> lstTeacher = ConsultRepository.Instance.GetTeacher(_COMPANY_CODE);
                ViewBag.Teacher = lstTeacher;
            }

            ViewBag.TeacherIdx = idx;
            List<ScheduleTime> lstScheduleTime = new List<ScheduleTime>();
            lstScheduleTime = ConsultRepository.Instance.GetScheduleTimeDetail("0001", idx);



            

            var jsMaster = new JavaScriptSerializer();
            string jsonMaster = jsMaster.Serialize(lstScheduleTime);

            ViewBag.ConsultTime = jsonMaster;
            ViewBag.TeacherIdx = idx;
            ViewBag.Title = "스케쥴관리";
            ViewBag.Navigation = "SCHEDULE";
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public ActionResult GetAbleDate()
        {
            string ableDate = ConsultRepository.Instance.GetScheduleDate(_COMPANY_CODE,_identity.SchoolType);
            return Json(new
            {
                jsonMsg = ableDate
            });
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public JsonResult GetAbleTime(string date)
        {
            List<ScheduleTime> rVal = ConsultRepository.Instance.GetScheduleTime(_COMPANY_CODE,date,_identity.SchoolType);            

            var jsMaster = new JavaScriptSerializer();
            string jsonMaster = jsMaster.Serialize(rVal);
            return Json(new
            {
                jsonMsg = jsonMaster
            });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Form(Application model)
        {

            model.CompanyCode = _COMPANY_CODE;
            //model.ConsultWay = "B";
            //model.ConsultType = "S";            
            if (model.ApplicationIdx == 0)
            {
                //R:등록
                //I:상담진행중
                //C:완료
                model.Progress = "R";
                model.StudentIdx = 0;
            }
            int rVal = ConsultRepository.Instance.RegApplication(model);

            if (rVal >= 0)
            {
                if (_identity.Roll == null)
                {
                    var identity = new ClaimsIdentity(
     DefaultAuthenticationTypes.ApplicationCookie,
     ClaimTypes.NameIdentifier,
     ClaimTypes.Role
 );

                    identity.TryRemoveClaim(identity.FindFirst("Idx"));
                    identity.TryRemoveClaim(identity.FindFirst("pwd"));
                    identity.TryRemoveClaim(identity.FindFirst("roll"));

                    identity.AddClaim(new Claim("Idx", rVal.ToString()));
                    identity.AddClaim(new Claim("pwd",model.Student.Password));
                    identity.AddClaim(new Claim("roll", "STUDENT"));

                    var authenticationContext = Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                    //if (authenticationContext != null)
                    //{
                    //    Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                    //        identity, authenticationContext.Properties);

                    //}

                    Authentication.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTime.UtcNow.AddHours(24)
                    },
                    identity);
                }
                return Json(new { ok = rVal });
            }
            else
            {
                return Json(new { ok = rVal });
            }
        }
        [HttpPost]
        public ActionResult Process(string actionnm, int idx)
        {
            bool retOK = false;
            string retRedirect = string.Empty;

            try
            {
                switch (actionnm)
                {
                    //수정 Modify
                    case "MODDIFY":                        
                        retOK = true;
                        retRedirect = "Form?idx=" + idx.ToString();                        
                        break;         
                }
                return Json(new { ok = retOK, Redirect = retRedirect });
            }
            catch
            {
                return Json(new { ok = retOK, Redirect = "올바르지 않은 접근입니다." });
            }
        }

        IAuthenticationManager Authentication
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        [HttpPost]
        public async Task<JsonResult> SetStudentConfing(string idx, string pwd)
        {
            var identity = new ClaimsIdentity(
                 DefaultAuthenticationTypes.ApplicationCookie,
                 ClaimTypes.NameIdentifier,
                 ClaimTypes.Role
             );


            Application application = ConsultRepository.Instance.GetApplicationForStudent(idx, pwd);

            if (application == null)
            {
                return Json(new
                {
                    jsonMsg = "F"
                });
            }
            else
            {
                identity.TryRemoveClaim(identity.FindFirst("Idx"));
                identity.TryRemoveClaim(identity.FindFirst("pwd"));
                identity.TryRemoveClaim(identity.FindFirst("roll"));

                identity.AddClaim(new Claim("Idx", application.ApplicationIdx.ToString()));
                identity.AddClaim(new Claim("pwd", pwd));
                identity.AddClaim(new Claim("roll", "STUDENT"));

                var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                if (authenticationContext != null)
                {
                    Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                        identity, authenticationContext.Properties);

                }

                Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.UtcNow.AddHours(24)
                },
                identity);
                return Json(new
                {
                    jsonMsg = "T"
                });
            }

            
        }

        [HttpPost]
        public async Task<JsonResult> LoginTeacher(string id, string pwd)
        {
            try
            {
                string rVal = string.Empty;
                if (string.IsNullOrEmpty(rVal))
                {
                    return Json(new
                    {
                        jsonMsg = "F"
                    });
                }
                

                else
                {
                    var identity = new ClaimsIdentity(
                      DefaultAuthenticationTypes.ApplicationCookie,
                      ClaimTypes.NameIdentifier,
                      ClaimTypes.Role
                  );
                    identity.TryRemoveClaim(identity.FindFirst("id"));
                    identity.TryRemoveClaim(identity.FindFirst("roll"));
                    
                    identity.AddClaim(new Claim("id", id));
                    identity.AddClaim(new Claim("teacherIdx", rVal));
                    identity.AddClaim(new Claim("roll", "CONSULT"));

                    var authenticationContext = await Authentication.AuthenticateAsync(DefaultAuthenticationTypes.ApplicationCookie);

                    if (authenticationContext != null)
                    {
                        Authentication.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                            identity, authenticationContext.Properties);

                    }

                    Authentication.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTime.UtcNow.AddHours(24)
                    },
                    identity);
                    return Json(new
                    {
                        jsonMsg = "T"
                    });
                }
            }
            catch
            {
                return Json(new
                {
                    jsonMsg = "F"
                });
            }

        }

        [HttpPost]
        public JsonResult SetConsultTeacher(string idx, string teacheridx)
        {
            int rVal = ConsultRepository.Instance.SetTeacher(idx, teacheridx);
            if (rVal > 0)
            {
                return Json(new
                {
                    jsonMsg = "T"
                });
            }
            else
            {
                return Json(new
                {
                    jsonMsg = "F"
                });
            }
        }

        [HttpPost]
        public JsonResult ChgProgress(string idx, string progress)
        {
            int rVal = ConsultRepository.Instance.ChgProgress(idx, progress);
            if (rVal > 0)
            {
                return Json(new
                {
                    jsonMsg = "T"
                });
            }
            else
            {
                return Json(new
                {
                    jsonMsg = "F"
                });
            }
        }

        public ActionResult Default()
        {
            Authentication.SignOut();
            Session.Abandon();
            // _identity.InitIdentity();
             return Redirect("/Application/Info");
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult InsertReply(Reply reply)
        {
            reply.TeacherIdx = Convert.ToInt32(_identity.TeacherIdx);

            if (string.IsNullOrEmpty(reply.Progress))
            {
                reply.Progress = "C";
            }
            int result = ConsultRepository.Instance.SetReply(reply);

            if (result > 0)
            {
                if (reply.Progress == "C")
                {
                    // 완료 푸시 발송 테스트
                    MobileRepository.Instance.MobilePushSend(reply.RequestIdx);
                }

                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult UpdateReply(Reply reply)
        {
            if (reply.RequestIdx > 0)
            {
                int result = ConsultRepository.Instance.UpdateReply(reply);

                if (result > 0)
                {
                    return Json(new { ok = true });
                }
                else
                {
                    return Json(new { ok = false });
                }
            }
            else
            {
                return Json(new { ok = false });
            }
        }

        [HttpPost]        
        public JsonResult ModifyDetail(int ct, int isc, int t)
        {
            if (t == 0)
            {
                t = Convert.ToInt32(_identity.TeacherIdx);
            }

            int result = ConsultRepository.Instance.SetScheduleDetil(_COMPANY_CODE,t,ct,isc);
            if (result > 0)
            {
                return Json(new { ok = true });
            }
            else
            {
                return Json(new { ok = false });
            }
        }


        [HttpPost]
        public JsonResult GetSehduleTable (string date, string way)
        {
            SheduleTable result = ManagementRepository.Instance.GetScheduleTimeAdmin(_COMPANY_CODE, date, way);
            var jsMaster = new JavaScriptSerializer();
            string jsonSheduleDate = jsMaster.Serialize(result.lstScheduleDate);
            string jsonSheduleTime = jsMaster.Serialize(result.lstScheduleTime);

            return Json(new
            {
                jsonSheduleDate = result.lstScheduleDate
               ,jsonSheduleTime = result.lstScheduleTime
            });
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public JsonResult RemoveEntranceRequest(int requestIdx)
        {
            int rVal = 0;
            rVal =  EntranceRequestRepository.Instance.RemoveEntranceRequest(requestIdx);
            return Json(new { result = rVal });
        }
    }
}


