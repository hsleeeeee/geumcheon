﻿

using Geumcheon.Web.Consult.App_Start;
using Geumcheon.Web.Consult.Models;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Geumcheon.Web.Consult.Controllers
{
    public class PresentationController : Controller
    {
        // GET: Application
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Occasional()
        {
            return View();
        }

        public ActionResult OnTime()
        {
            return View();
        }

        public ActionResult ADMISSIONS()
        {
            return View();
        }

        public ActionResult ParentAcademy()
        {
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        //public ActionResult RequestForm(int pIdx=5, int rIdx=0)
        //{
        //    pIdx = 5;
        //    PresentationRequest model = PresentationRepository.Instance.PresentationRequestSelectByPtIdx(int.Parse(_identity.MemberIdx), pIdx, rIdx);

        //    return View(model);
        //}

        public ActionResult RequestForm(int pidx)
        {
            int midx = 0;
            int.TryParse(_identity.MemberIdx, out midx);

            List<PresentationRequest> rtn;

            if (midx == 0)
            {
                var pt = PresentationRepository.Instance.PresentationSelectItem(pidx);
                rtn = new List<PresentationRequest>();
                rtn.Add(new PresentationRequest
                {
                    PtIdx = pt.Idx,
                    PtRequestIdx = 0,
                    Address = pt.Address,
                    PresentationType = pt.PresentationType,
                    PresentationDate_DP = pt.PresentationDate_DP,
                    Name = "",
                    HP = "",
                    Email = "",
                    Memo = "",
                });
            }
            else
            {
                rtn = PresentationRepository.Instance.PresentationRequestSelectByPtIdx(midx, pidx, 0);
               
            }

            return View(rtn);
        }

        public ActionResult RequestList(int pidx)
        {

            List<PresentationRequest> model = PresentationRepository.Instance.PresentationRequestSelectByPtIdx(0,pidx, 0).ToList();
            ViewBag.PIDx = pidx;
            return View(model);
        }

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public JsonResult InsertPtRequest(PresentationRequest entity)
        {
            entity.MemberIdx = int.Parse(_identity.MemberIdx);
            string rVal = PresentationRepository.Instance.PresentationRequestModify(entity);
            return Json(new
            {
                jsonMsg = rVal
            });

        }

        public ActionResult BatchDown()
        {
            Presentation rVal = PresentationRepository.Instance.CheckBatchDown();
            if (rVal == null)
            {
                return Content("<script>alert('*행사당일 18시00분~ 22시30분 까지 다운로드 받을수 있습니다.');window.close();</script>");
                
            }
            else
            {
                //return Content(string.Format("<script>location.href('http://geumcheon.eduplan.co.kr/Resources/{0}','_blank');window.close();</script>", rVal.DownFile));
                return Content(string.Format("<script>location.href='http://geumcheon.eduplan.co.kr/Resources/{0}';</script>", rVal.DownFile));
            }
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult ExcelDown(int pidx)
        {
            try
            {
                string fileName = string.Empty;
                string date = string.Format(DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day);
                string[] headerTitleList = null;
                fileName = string.Format("presentation_request_{0}_{1}.xls", date, "d");
                headerTitleList = new string[] { "이름", "핸드폰", "이메일", "비고", "신청일" };

                List<PresentationRequest> lstPresentationRequest = PresentationRepository.Instance.PresentationRequestSelectByPtIdx(0, pidx, 0).ToList();

                DataTable dt = new DataTable();

                int columnIdx = 0;
                foreach (string columName in headerTitleList)
                {
                    if (columnIdx > headerTitleList.Length - 1)
                    {
                        break;
                    }
                    columnIdx++;
                    dt.Columns.Add(columName);
                }

                foreach (PresentationRequest sr in lstPresentationRequest)
                {
                    DataRow dr = dt.NewRow();
                    string hp = string.Empty;
                    try
                    {
                        hp = (sr.HP.Length > 3 ? sr.HP.Substring(0, 3) : "") + "-" + (sr.HP.Length > 7 ? sr.HP.Substring(3, 4) : "") + "-" + sr.HP.Substring(7, sr.HP.Length - 7);
                    }
                    catch
                    {
                        hp = sr.HP;
                    }
                    dr["이름"] = sr.Name;
                    dr["핸드폰"] = hp;
                    dr["이메일"] = sr.Email;


                    dr["비고"] = sr.Memo;
                    dr["신청일"] = sr.CreateDate.ToString("yyyy년 MM월 dd일 HH:mm");

                    dt.Rows.Add(dr);
                }
                return new ExcelResult<PresentationRequest>(fileName, dt, headerTitleList, "euc-kr");
            }
            catch
            {
                string script = string.Format("<script>alert('파일전송 중 오류가 발생했습니다.');window.close();</script>");
                return Content(script);
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult InsertAnoymousePtRequest()
        {
            try
            {
                JObject model = JObject.Parse(new System.IO.StreamReader(Request.InputStream).ReadToEnd());
                int ptidx = model["ptidx"].Value<int>();
                int ptrequestidx = model["ptrequestidx"].Value<int>();
                string memo = model["memo"].Value<string>();
                int memberIdx = -1;
                int.TryParse(_identity.MemberIdx, out memberIdx);

                List<string> rVal = new List<string>();

                foreach (var child in model["requestuserinfo"])
                {
                    if (!string.IsNullOrEmpty(child["name"]?.Value<string>()))
                    {
                        PresentationRequest entity = new PresentationRequest
                        {
                            MemberIdx = memberIdx < 0 ? 0 : memberIdx,
                            PtIdx = ptidx,
                            PtRequestIdx = child["ptrequestidx"].Value<int>(),
                            Name = child["name"]?.Value<string>() ?? "",
                            HP = child["hp"]?.Value<string>() ?? "",
                            Email = child["email"]?.Value<string>() ?? "",
                            AccountType = child["atype"]?.Value<string>() ?? "",
                            Memo = memo
                        };

                        rVal.Add(PresentationRepository.Instance.PresentationRequestModifyAnonymouse(entity));
                    }
                    else
                    {
                        rVal.Add("");
                    }
                }

                return Json(new
                {
                    ok = true,
                    jsonMsg = rVal
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = false,
                    message = ex.ToString()
                });
            }
        }

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public JsonResult DeletePresentationRequest(int pidx)
        {
            try
            {

                int rVal = PresentationRepository.Instance.PresentationRequestDelete(pidx);
                return Json(new
                {
                    jsonMsg = rVal
                });

            }
            catch
            {
                return Json(new
                {
                    jsonMsg = -1
                });
            }

        }

    }

    public class ExcelResult<T> : ActionResult
    {
        public string FileName { get; set; }
        public string CharSet { get; set; }
        public List<T> DataList { get; set; }
        public string[] ColumnTitles { get; set; }
        public DataTable dt { get; set; }
        public bool IsUseDataTable = false;
        public ExcelResult() { }
        public ExcelResult(string fileName, List<T> dataList, string[] columnTitles = null, string charSet = "euc-kr")
        {
            this.FileName = fileName;
            this.DataList = dataList;
            this.CharSet = charSet;
            this.ColumnTitles = columnTitles;
        }

        public ExcelResult(string fileName, DataTable dt = null, string[] columnTitles = null, string charSet = "euc-kr")
        {
            this.FileName = fileName;
            this.dt = dt;
            this.CharSet = charSet;
            this.ColumnTitles = columnTitles;
            this.IsUseDataTable = true;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (dt.Rows.Count > 0 || DataList.Count > 0)
            {
                var gv = new GridView();
                if (IsUseDataTable)
                {
                    gv.DataSource = dt;
                }
                else
                {
                    gv.DataSource = DataList;
                }
                gv.DataBind();
                StringWriter objStringWriter = new StringWriter();
                HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);


                gv.RenderControl(objHtmlTextWriter);

                string htmlData = objStringWriter.ToString();
                if (ColumnTitles != null && ColumnTitles.Length > 0)
                {
                    List<string> fieldList = FpReflection.GetFieldNameList<T>();
                    for (int i = 0; i < ColumnTitles.Length; i++)
                    {
                        htmlData = htmlData.Replace(fieldList[i], ColumnTitles[i]);
                    }
                }

                HttpResponseBase Response = context.HttpContext.Response;
                Response.ClearContent();
                Response.Buffer = true;
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = CharSet;
                Response.ContentEncoding = System.Text.Encoding.GetEncoding(CharSet);
                Response.Output.Write(htmlData);
                Response.Flush();
                Response.End();
            }

            else
            {
                HttpResponseBase Response = context.HttpContext.Response;
                Response.ClearContent();
                Response.Buffer = true;
                Response.Clear();
                Response.Charset = CharSet;
                Response.ContentEncoding = System.Text.Encoding.GetEncoding(CharSet);
                Response.Output.Write("<script>alert('데이터가 없습니다.');history.back();</script>");
                Response.Flush();
                Response.End();
            }
        }
    }
    public class FpReflection
    {
        public static List<string> GetFieldNameList<T>()
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));

            List<string> list = new List<string>(props.Count);

            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                list.Add(prop.Name);
            }

            return list;
        }
    }
}