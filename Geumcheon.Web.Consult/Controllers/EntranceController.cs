﻿
using Geumcheon.Web.Consult.Models;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Geumcheon.Web.Consult.App_Start;
using System.Threading;

namespace Geumcheon.Web.Consult.Controllers
{
    public class EntranceController : Controller
    {
        static string _COMPANY_CODE = "0001";
        static int _PAGESIZE = 15;

        #region View

        // GET: Application
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Info(string way = "I", int eidx = 3)
        {
            ViewBag.ConsultWay = way;
            Entrance entrance = EntranceRequestRepository.Instance.EntranceInfoSelect(eidx);
            if (entrance == null)
            {
                entrance = new Entrance
                {
                    EntranceIdx = eidx
                };
            }
            return PartialView(entrance);
        }

        public ActionResult Consult(string way = "I", int eidx = 4, int page = 1, SearchInfo searchInfo = null)
        {
            try
            {

                if (way.ToUpper() == "O")
                {
                    eidx = 1;
                }
                else if (way.ToUpper() == "V")
                {
                    eidx = 2;
                }
                //else
                //{

                //}

                int rVal = EntranceRequestRepository.Instance.EntranceSelect(way,eidx);
                ViewBag.ConsultWay = way;
                ViewBag.Page = page;
                ViewBag.Title = SetTitle(way);
                ViewBag.Navigation = SetNavigation(way);
                ViewBag.EntranceCnt = rVal;
                ViewBag.EntranceIdx = eidx;
                return View();
            }
            catch (Exception ex)
            {
                if (way.ToUpper() == "O")
                {
                    eidx = 1;
                }
                else if (way.ToUpper() == "V")
                {
                    eidx = 2;
                }
                //else
                //{

                //}

                ViewBag.ConsultWay = way;
                ViewBag.Page = page;
                ViewBag.Title = SetTitle(way);
                ViewBag.Navigation = SetNavigation(way);
                ViewBag.EntranceIdx = eidx;

                return View();
            }
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult Detail(int requestIdx)
        {
            if (_identity.Roll == "CONSULT")
            {
                var item = EntranceRequestRepository.Instance.EntranceRequestSelectItem(requestIdx);
                var member = MemberRepository.Instance.MemberSelectItem(item.member.ID);
                var entrance = EntranceRequestRepository.Instance.EntranceInfoSelect(item.EIdx);
                item.DisplayInputType = entrance?.DisplayInputType;

                if (member != null)
                {
                    ViewData["Member"] = member;
                }
                else
                {
                    ViewData["Member"] = new Geumcheon.Web.Core.Entity.Member();
                }
                    
                ViewBag.Title = SetTitle(item.ConsultWay);

                return View(item);
            }
            else
            {
                try
                {
                    var member = MemberRepository.Instance.MemberSelectItem(_identity.ConsultId);
                    if (member != null)
                    {
                        ViewData["Member"] = member;
                    }
                    else
                    {
                        ViewData["Member"] = new Geumcheon.Web.Core.Entity.Member();
                    }

                    var item = EntranceRequestRepository.Instance.EntranceRequestSelectItem(requestIdx);
                    var entrance = EntranceRequestRepository.Instance.EntranceInfoSelect(item.EIdx);
                    item.DisplayInputType = entrance?.DisplayInputType;
                    if (!item.member.ID.Equals(_identity.ConsultId))
                    {
                        throw new HttpException(401, "unauthorized");
                    }
                    ViewBag.Title = SetTitle(item.ConsultWay);

                    return View(item);
                }
                catch(Exception ex)
                {
                    if (ex.Message == "unauthorized")
                    {
                        return Redirect("/Error/err?status=401");
                    }
                    else
                    {
                        return View(new EntranceRequest());
                    }
                }                
            }            
        }

        public ActionResult Intro()
        {
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult Modify(string way = "I",int requestIdx = 0, int eidx = 0)
        {
            ViewBag.ConsultWay = way;
            ViewBag.Title = SetTitle(way);
            ViewBag.EntranceIdx = eidx;
            var member = MemberRepository.Instance.MemberSelectItem(_identity.ConsultId);
            var entrance = EntranceRequestRepository.Instance.EntranceInfoSelect(eidx);
            if (member != null)
            {
                ViewData["Member"] = member;
            }
            else
            {
                ViewData["Member"] = new Geumcheon.Web.Core.Entity.Member();
            }

            if (requestIdx != 0)
            {
                var item = EntranceRequestRepository.Instance.EntranceRequestSelectItem(requestIdx);                
                ViewBag.ConsultWay = item.ConsultWay;
                item.DisplayInputType = entrance?.DisplayInputType;

                return View(item);
            }
            else
            {
                return View(new EntranceRequest { DisplayInputType = entrance?.DisplayInputType });
            }
        }        
        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult StudentInfo(string idx, string requestIdx)
        {
            if (_identity.Roll == "CONSULT")
            {
                var item = EntranceRequestRepository.Instance.EntranceRequestSelectItem(int.Parse(requestIdx));
                var member = MemberRepository.Instance.MemberSelectItemForConsult(item.member.ID, requestIdx);
                if (member != null)
                {
                    ViewData["Member"] = member;
                }
                else
                {
                    ViewData["Member"] = new Geumcheon.Web.Core.Entity.Member();
                }

                ViewBag.MemberIdx = idx;
                return View(item);
            }
            else
            {
                throw new HttpException(401, "Unauthorized access");
            }
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult SmartTestReport(string memberIdx)
        {
            string stuIdx = MyPageRepository.Instance.GetReportLinkCode("600", memberIdx);
            if(string.IsNullOrEmpty(stuIdx) || stuIdx.Equals("0"))
            {
                return Content("<script language='javascript' type='text/javascript'>alert('검사를 완료하지 않았습니다');</script>");
            }
            else
            {
                
                return Redirect("http://psy.futureplan.co.kr/Report/rptcs?stuIdx=" + stuIdx);
            }
        }

        #endregion


        #region AJAX

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public JsonResult RequestConsult()
        {            
            JObject model = JObject.Parse(new System.IO.StreamReader(Request.InputStream).ReadToEnd());
            string consultType = model.SelectToken("type")?.Value<string>() ?? "I";

            int requestIdx, scheduleTimeIdx,eidx = 0;
            int.TryParse(model.SelectToken("consulttime")?.Value<string>(), out scheduleTimeIdx);
            int.TryParse(model.SelectToken("requestidx").Value<string>(), out requestIdx);
            int.TryParse(model.SelectToken("eidx").Value<string>(), out eidx);
            if (consultType == "V")
            {
                List<ScheduleTime> rVal = ConsultRepository.Instance.GetScheduleTime(_COMPANY_CODE, model.SelectToken("consultdate")?.Value<string>(), _identity.SchoolType);
                if (rVal != null && requestIdx == 0 && (rVal.Where(x => x.ConsultTimeIdx == scheduleTimeIdx).FirstOrDefault()?.Cnt ?? -1) < 1)
                {
                    return Json(new
                    {
                        ok = false,
                        message = "해당 희망 날짜에 좌석이 없습니다."
                    });
                }
            }

            Geumcheon.Web.Core.Entity.Member member = new Geumcheon.Web.Core.Entity.Member
            {
                ID = _identity.ConsultId,
                Name = model.SelectToken("name")?.Value<string>(),
                Hp = model.SelectToken("hp")?.Value<string>(),
                Email = model.SelectToken("email")?.Value<string>(),
                School = model.SelectToken("school")?.Value<string>(),
                Grade = model.SelectToken("grade")?.Value<int>() ?? 1,
                IsUse = true,
            };

            string studentName = model.SelectToken("studentname").Value<string>();
            int gender = model.SelectToken("gender")?.Value<int>() ?? 1;
            string areaName = model.SelectToken("areaname").Value<string>();

            int idx = MemberRepository.Instance.MemberUpdate(member);

            var hope = model.SelectToken("hope");
            if (hope != null)
            {
                DataTable univ = new DataTable();
                univ.Columns.Add(new DataColumn("ApplyUnivIdx", typeof(int)));
                univ.Columns.Add(new DataColumn("Ranking", typeof(int)));
                univ.Columns.Add(new DataColumn("UniversityName", typeof(string)));
                univ.Columns.Add(new DataColumn("StypeRem", typeof(string)));
                univ.Columns.Add(new DataColumn("MajorName", typeof(string)));

                foreach (var item in hope)
                {
                    int unividx = 0;
                    int.TryParse(item.SelectToken("unividx")?.Value<string>(), out unividx);

                    DataRow row = univ.NewRow();

                    row["ApplyUnivIdx"] = unividx;
                    row["Ranking"] = item.SelectToken("rank").Value<int>();
                    row["UniversityName"] = item.SelectToken("univ").Value<string>();
                    row["StypeRem"] = item.SelectToken("mode").Value<string>();
                    row["MajorName"] = item.SelectToken("department").Value<string>();
                    univ.Rows.Add(row);
                    row = null;
                }

                if (univ.Rows.Count > 0)
                {
                    MyPageRepository.Instance.SetApplyUniversity(new ApplyUniversity
                    {
                        MemberIdx = idx,
                        WriterType = _identity.Roll == "PARENT" ? "P" : "S",
                        tbUnivesity = univ
                    });
                }
            }

            EntranceRequest app = new EntranceRequest
            {
                RequestIdx = requestIdx,
                MemberIdx = int.Parse(_identity.MemberIdx),                
                CompanyCode = _COMPANY_CODE,
                Progress = "R",
                ConsultWay = model.SelectToken("type")?.Value<string>() ?? "I",
                Title = model.SelectToken("title")?.Value<string>(),
                ScoreHtml = model.SelectToken("score")?.Value<string>(),
                StudentName = model.SelectToken("studentname")?.Value<string>(),
                StudentGender = model.SelectToken("gender")?.Value<int>() ?? 1,
                AreaName = model.SelectToken("areaname")?.Value<string>(),
                EIdx = eidx,
                Re_Note = model.SelectToken("re_note")?.Value<string>(),
                NeedValue1 = model.SelectToken("needvalue1")?.Value<string>(),
                NeedValue2 = model.SelectToken("needvalue2")?.Value<string>(),
                Schedule = new Schedule
                {
                    ConsultTimeIdx = scheduleTimeIdx
                }
            };

            string[] entranceRequesRval = EntranceRequestRepository.Instance.EntranceRequestUpsert(app, _identity.SchoolType).Split('/');
            if (consultType == "V")
            {
                if (Convert.ToInt32(entranceRequesRval[0]) == -99)
                {
                    return Json(new
                    {
                        ok = true,
                        message = requestIdx == 0 ? "신청은 되었지만 해당 희망 날짜에 좌석이 없어 예약하지 못하였습니다." : "수정 되었지만 해당 희망 날짜에 좌석이 없어 변경하지 못하였습니다."
                    });
                }
            }
            string msg = entranceRequesRval[1];
            if (entranceRequesRval[1] == "R")
            {
                msg = "신청되었습니다";
            }
            else
            {
                msg = "대기신청되었습니다";
            }
            
            return Json(new
            {
                ok = true,
                message = requestIdx == 0 ? msg : "수정되었습니다."
            });
        }

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public JsonResult RequestList()
        {
            try
            {
                JObject data = JObject.Parse(new System.IO.StreamReader(Request.InputStream).ReadToEnd());

                int page = data.SelectToken("page")?.Value<int>() ?? 1;
                string consultType = data.SelectToken("type")?.Value<string>();
                string search = data.SelectToken("search")?.Value<string>();
                string order = data.SelectToken("order")?.Value<string>();


                ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>()
                {
                    Entities = new List<EntranceRequest>(),
                    Search = new SearchInfo(),
                    PagingInfo = new PagingInfo(page, _PAGESIZE, 0, order)
                };

                model.Search.CompanyCode = _COMPANY_CODE;
                model.Search.ConsultWay = consultType;
                model.Search.Name = search;
                model.Search.TeacherId = null;

                EntranceRequestRepository.Instance.EntranceRequestSelectPaging_Teacher(int.Parse(_identity.TeacherIdx), model);

                return Json(new
                {
                    ok = true,
                    data = model
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = true,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public JsonResult GetListData()
        {
            try
            {
                JObject data = JObject.Parse(new System.IO.StreamReader(Request.InputStream).ReadToEnd());

                int page = data.SelectToken("page")?.Value<int>() ?? 1;
                string consultType = data.SelectToken("type")?.Value<string>();
                string search = data.SelectToken("search")?.Value<string>();
                string order = data.SelectToken("order")?.Value<string>();
                string listType = data.SelectToken("listType")?.Value<string>();

                ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>()
                {
                    Entities = new List<EntranceRequest>(),
                    Search = new SearchInfo(),
                    PagingInfo = new PagingInfo(page, _PAGESIZE, 0, order)
                };

                model.Search.CompanyCode = _COMPANY_CODE;
                model.Search.ConsultWay = consultType;
                model.Search.Name = search;
                model.Search.TeacherId = null;

                if (listType == "_O" || listType == "S_O")
                {
                    EntranceRequestRepository.Instance.GetListData(model);
                }
                else
                {
                    if(_identity.ConsultId == "admin" || _identity.ConsultId=="future")                        
                    {
                        EntranceRequestRepository.Instance.EntranceRequestSelectPaging_Teacher(0, model);
                    }
                    else
                    {
                        EntranceRequestRepository.Instance.EntranceRequestSelectPaging_Teacher(int.Parse(_identity.TeacherIdx), model);
                    }                    
                }

                return Json(new
                {
                    ok = true,
                    data = model
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    ok = true,
                }, JsonRequestBehavior.AllowGet);
            }
        }
        

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult EntranceRequestChageSchedule(int requestIdx, int timeIdx, string schType)
        {            
            int result = EntranceRequestRepository.Instance.EntranceRequestChageSchedule(requestIdx, timeIdx, schType);
            return Json(new
            {
                jsonMsg = result
            });
        }

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult GetAbleDate(string way, string schType = "0")
        {
            if(schType == "0")
            {
                schType = _identity.SchoolType;
            }
            string ableDate = ConsultRepository.Instance.GetScheduleDate(_COMPANY_CODE, schType);
            return Json(new
            {
                jsonMsg = ableDate
            });
        }

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public JsonResult GetAbleTime(string date, string way, string schType = "0")
        {
            if (schType == "0")
            {
                schType = _identity.SchoolType;
            }
            List<ScheduleTime> rVal = ConsultRepository.Instance.GetScheduleTime(_COMPANY_CODE, date, schType);

            var jsMaster = new JavaScriptSerializer();
            string jsonMaster = jsMaster.Serialize(rVal);
            return Json(new
            {
                jsonMsg = jsonMaster
            });
        }

        [HttpPost]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public JsonResult GetData(string searchType, int memberIdx)
        {

            switch (searchType.ToUpper())
            {
                case "DEFAULT_INFO_P":
                    {
                        DefaultInfo rVal = MyPageRepository.Instance.GetDefaultInfo(memberIdx, "P", true);
                        var jsMaster = new JavaScriptSerializer();
                        string jsonMaster = jsMaster.Serialize(rVal);
                        return Json(new
                        {
                            jsonMsg = jsonMaster
                        });
                    }
                case "DEFAULT_INFO_S":
                    {
                        DefaultInfo rVal = MyPageRepository.Instance.GetDefaultInfo(memberIdx, "S", true);
                        var jsMaster = new JavaScriptSerializer();
                        string jsonMaster = jsMaster.Serialize(rVal);
                        return Json(new
                        {
                            jsonMsg = jsonMaster
                        });

                        
                    }
                case "NESIN":
                    {
                        Nesin rVal = MyPageRepository.Instance.GetNesin(memberIdx);
                        var jsMaster = new JavaScriptSerializer();
                        string jsonMaster = jsMaster.Serialize(rVal);
                        return Json(new
                        {
                            jsonMsg = jsonMaster
                        });

                    }
                case "MOI":
                    {
                        List<JsonSat> lstJsonScore = new List<JsonSat>();
                        List<Sat> lstSat = MyPageRepository.Instance.GetSat(memberIdx.ToString());
                        if (lstSat == null)
                        {
                            return Json(new
                            {
                                jsonMsg = string.Empty
                            });
                        }
                        else
                        {
                            foreach (Sat sat in lstSat)
                            {
                                SatItem Kor = new SatItem
                                {
                                    PartName = sat.PartNameKor,
                                    PartCode = sat.PartCodeKor,
                                    Gra = sat.GraKor.ToString(),
                                    Std = sat.StdKor.ToString(),
                                    Per = sat.PerKor.ToString(),
                                };
                                SatItem Eng = new SatItem
                                {
                                    PartName = sat.PartNameEng,
                                    PartCode = sat.PartCodeEng,
                                    Gra = sat.GraEng.ToString(),
                                    Std = "-",
                                    Per = "-"
                                };
                                SatItem Mat = new SatItem
                                {
                                    PartName = sat.PartNameMat,
                                    PartCode = sat.PartCodeMat,
                                    Gra = sat.GraMat.ToString(),
                                    Std = sat.StdMat.ToString(),
                                    Per = sat.PerMat.ToString()
                                };
                                SatItem Tam1 = new SatItem
                                {
                                    PartName = sat.PartNameTam1,
                                    PartCode = sat.PartCodeTam1,
                                    Gra = sat.GraTam1.ToString(),
                                    Std = sat.StdTam1.ToString(),
                                    Per = sat.PerTam1.ToString()
                                };
                                SatItem Tam2 = new SatItem
                                {
                                    PartName = sat.PartNameTam2,
                                    PartCode = sat.PartCodeTam2,
                                    Gra = sat.GraTam2.ToString(),
                                    Std = sat.StdTam2.ToString(),
                                    Per = sat.PerTam2.ToString()
                                };
                                SatItem His = new SatItem
                                {
                                    PartName = sat.PartNameHis,
                                    PartCode = sat.PartCodeHis,
                                    Gra = sat.GraHis.ToString(),
                                    Std = "-",
                                    Per = "-"
                                };
                                SatItem Lang2 = new SatItem
                                {
                                    PartName = sat.PartNameLang2,
                                    PartCode = sat.PartCodeLang2,
                                    Gra = sat.GraLang2.ToString(),
                                    Std = sat.StdLang2.ToString(),
                                    Per = sat.PerLang2.ToString(),
                                };
                                JsonSat jsonScore = new JsonSat
                                {
                                    SchoolYear = sat.SchoolYear,
                                    TestYear = sat.TestYear,
                                    TestDate = sat.TestDate,
                                    GroupKey = sat.GroupKey,
                                    Kor = Kor,
                                    Mat = Mat,
                                    Eng = Eng,
                                    Tam1 = Tam1,
                                    Tam2 = Tam2,
                                    His = His,
                                    Lang2 = Lang2,
                                };
                                lstJsonScore.Add(jsonScore);
                            }
                            var jsMaster = new JavaScriptSerializer();
                            string jsonMaster = jsMaster.Serialize(lstJsonScore);

                            return Json(new
                            {
                                jsonMsg = jsonMaster
                            });
                        }
                    }
                case "STUDENT_RECORD":
                    {
                        List<Files> rVal = MyPageRepository.Instance.GetFiles(memberIdx.ToString());
                        var jsMaster = new JavaScriptSerializer();
                        string jsonMaster = jsMaster.Serialize(rVal);
                        return Json(new
                        {
                            jsonMsg = jsonMaster
                        });

                    }
            }

            return Json(new
            {
                jsonMsg = "error"
            });
        }

        private string GetUploadFolder()
        {
            string folder = "/Uploads/";
            //folder = String.Format("data/");            
            return Server.MapPath(folder);
        }

        [Authorize]
        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult Download(string fileId)
        {
            string _fullName = Path.Combine(string.Format(GetUploadFolder() + "/{0}", fileId));
            FileInfo ofile = new FileInfo(_fullName);
            if (ofile.Exists)
            {
                try
                {
                    string _fileName = MyPageRepository.Instance.GetFileSaveName(fileId);
                    string sContentType = string.Empty;
                    if (Request.UserAgent.IndexOf("MSIE") >= 0)
                    {
                        //IE 5.0인 경우.
                        if (Request.UserAgent.IndexOf("MSIE 5.0") >= 0)
                        {
                            sContentType = "application/x-msdownload";
                        }
                        //IE 5.0이 아닌 경우.
                        else
                        {
                            sContentType = "Application/Octet-Stream";
                        }
                    }
                    else
                    {
                        //Netscape등 기타 브라우저인 경우.
                        sContentType = "application/unknown";
                    }
                    //Response.ContentEncoding = System.Text.Encoding.Default; 
                    Response.Clear();
                    Response.ClearHeaders();
                    Response.ClearContent();
                    //확인해봐야겠굼
                    Response.HeaderEncoding = Encoding.GetEncoding("utf-8");
                    //HttpContext.Current.Response.HeaderEncoding = Encoding.UTF8;
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(_fileName));
                    //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + "a.zip");
                    Response.AddHeader("Content-Length", ofile.Length.ToString());
                    Response.ContentType = sContentType;

                    Response.TransmitFile(_fullName);

                    Response.Flush();
                    Response.End();
                }
                catch (ThreadAbortException ex)
                {
                    //fLog.Error(LogManager.WriteLog(ex));
                }
                catch (Exception ex)
                {
                    //fLog.Error(LogManager.WriteLog(ex, "server_nm=" + server_nm + "save_nm=" + save_nm));
                }
            }
            //파일이 없을경우
            else
            {
                //ClientScript.RegisterStartupScript(this.GetType(), "", string.Format("alert('{0}')", HttpContext.GetGlobalResourceObject("Resource", "aspx_cs_content2", culture)), true);
            }

            return View();
        }
        #endregion


        #region Private

        private string SetNavigation(string w)
        {
            switch (w)
            {
                case "I": return "INTERVIEW";
                case "V": return "VIDEO";
                case "O": return "ONLINE";
                default: return "INTERVIEW";
            }
        }

        private string SetTitle(string w)
        {
            switch (w)
            {
                case "I": return "대면컨설팅";
                case "V": return "화상(비대면)컨설팅";
                case "O": return "온라인컨설팅";
                default: return "화상(비대면)컨설팅";
            }
        }

        #endregion


        #region | Teacher |
        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult List(string way = "I", int page = 1, SearchInfo searchInfo = null)
        {
            ViewBag.ConsultWay = way;
            ViewBag.Page = page;
            ViewBag.Title = SetTitle(way);
            ViewBag.Navigation = SetNavigation(way);

            return View();
        }
        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult Reply(string w, int requestIdx = 0)
        {
            Reply reply = new Reply();
            if (_identity.Roll == "CONSULT")
            {
                if (_identity.ConsultId == "admin" || _identity.ConsultId == "future")
                {
                    reply = ConsultRepository.Instance.GetReply(requestIdx, string.Empty);
                }
                else
                {
                    reply = ConsultRepository.Instance.GetReply(requestIdx, _identity.ConsultId);
                }
            }
            if (w == "o")
            {
                ViewBag.Title = "상담입력";
            }
            else
            {
                ViewBag.Title = "상담보고서 작성";
            }
            ViewBag.ConsultWay = w;
            ViewBag.Navigation = SetNavigation(w);
            return View(reply);
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult ExcelDownLoad(string s, string e)
        {
            string teacheridx = _identity.TeacherIdx;
            if(_identity.ConsultId == "admin")
            {
                teacheridx = "0";                
            }
            CSO6 cs06 = MyPageRepository.Instance.GetCS06_ForExcel(teacheridx, s,e);

            DataTable dtNesin = new DataTable();
            dtNesin.Columns.Add("stu_id");
            dtNesin.Columns.Add("stu_use_year");
            dtNesin.Columns.Add("kor_grade");
            dtNesin.Columns.Add("math_grade");
            dtNesin.Columns.Add("eng_grade");
            dtNesin.Columns.Add("soc_grade");
            dtNesin.Columns.Add("sci_grade");


            //DataTable dtMoi = new DataTable();
            //dtMoi.Columns.Add("stu_id");
            //dtMoi.Columns.Add("test_date");
            //dtMoi.Columns.Add("unit_code");

            //dtMoi.Columns.Add("kor_code");
            //dtMoi.Columns.Add("ori_kor");
            //dtMoi.Columns.Add("std_kor");
            //dtMoi.Columns.Add("per_kor");
            //dtMoi.Columns.Add("gra_kor");

            //dtMoi.Columns.Add("mat_code");
            //dtMoi.Columns.Add("mat_check");
            //dtMoi.Columns.Add("mat_name");
            //dtMoi.Columns.Add("ori_mat");
            //dtMoi.Columns.Add("std_mat");
            //dtMoi.Columns.Add("per_mat");
            //dtMoi.Columns.Add("gra_mat");


            List<string> lstColumnName = GetFieldNameList<CS06_SCORE>();
            DataTable dtMoi = new DataTable();
            foreach (string colunmName in lstColumnName)
            {
                dtMoi.Columns.Add(colunmName);
            }


            lstColumnName = GetFieldNameList<CS06_STU_LIST>();
            DataTable dtStu = new DataTable();
            foreach(string colunmName in lstColumnName)
            {
                dtStu.Columns.Add(colunmName);
            }

           
            foreach (CS06_NESIN nesin in cs06.CS06_NESINs)
            {
                DataRow dr = dtNesin.NewRow();
                dr["stu_id"] = nesin.stu_id;
                dr["stu_use_year"] = nesin.stu_use_year;
                dr["kor_grade"] = nesin.kor_grade;
                dr["math_grade"] = nesin.math_grade;
                dr["eng_grade"] = nesin.eng_grade;
                dr["soc_grade"] = nesin.soc_grade;
                dr["sci_grade"] = nesin.sci_grade;

                dtNesin.Rows.Add(dr);
            }

            foreach (CS06_SCORE score in cs06.CS06_SCOREs)
            {
                DataRow dr = dtMoi.NewRow();
                dr["stu_id"] = score.stu_id;
                dr["test_date"] = score.test_date;
                dr["unit_code"] = score.unit_code;
                dr["kor_code"] = score.kor_code;
                dr["ori_kor"] = score.ori_kor;
                dr["std_kor"] = score.std_kor;
                dr["per_kor"] = score.per_kor;
                dr["gra_kor"] = score.gra_kor;
                dr["mat_code"] = score.mat_code;
                dr["mat_check"] = score.mat_check;
                dr["mat_name"] = score.mat_name;
                dr["ori_mat"] = score.ori_mat;
                dr["std_mat"] = score.std_mat;
                dr["per_mat"] = score.per_mat;
                dr["gra_mat"] = score.gra_mat;
                dr["eng_code"] = score.eng_code;

                dr["ori_eng"] = score.ori_eng;
                dr["std_eng"] = score.std_eng;
                dr["per_eng"] = score.per_eng;
                dr["gra_eng"] = score.gra_eng;
                dr["sech_check"] = score.sech_check;
                dr["sech1_code"] = score.sech1_code;
                dr["sech1_name"] = score.sech1_name;
                dr["ori_sech1"] = score.ori_sech1;
                dr["std_sech1"] = score.std_sech1;
                dr["per_sech1"] = score.per_sech1;
                dr["gra_sech1"] = score.gra_sech1;
                dr["sech2_code"] = score.sech2_code;
                dr["sech2_name"] = score.sech2_name;
                dr["ori_sech2"] = score.ori_sech2;
                dr["std_sech2"] = score.std_sech2;
                dr["per_sech2"] = score.per_sech2;
                dr["gra_sech2"] = score.gra_sech2;
                dr["sech3_code"] = score.sech3_code;
                dr["sech3_name"] = score.sech3_name;
                dr["ori_sech3"] = score.ori_sech3;
                dr["std_sech3"] = score.std_sech3;
                dr["per_sech3"] = score.per_sech3;
                dr["gra_sech3"] = score.gra_sech3;
                dr["sech4_code"] = score.sech4_code;
                dr["sech4_name"] = score.per_sech4;
                dr["per_sech4"] = score.std_sech4;
                dr["gra_sech4"] = score.gra_sech4;
                dr["reg_date"] = score.reg_date;
                dr["upd_date"] = score.upd_date;
                dr["upd_cnt"] = score.upd_cnt;
                dr["lang2_code"] = score.lang2_code;
                dr["lang2_name"] = score.lang2_name;
                dr["ori_lang2"] = score.ori_lang2;
                dr["gra_sech4"] = score.gra_sech4;
                dr["std_lang2"] = score.std_lang2;
                dr["per_lang2"] = score.per_lang2;
                dr["gra_lang2"] = score.gra_lang2;
                dr["sta_lang2"] = score.sta_lang2;
                dr["lang2_name"] = score.lang2_name;
                dr["stu_use_year"] = score.stu_use_year;
                dr["sco_hack"] = score.sco_hack;
                dr["result_type"] = score.result_type;
                
                dtMoi.Rows.Add(dr);
            }

            foreach (CS06_STU_LIST score in cs06.CS06_STU_LISTs)
            {
                DataRow dr = dtStu.NewRow();
                dr["stu_id"] = score.stu_id;
                dr["stu_name"] = score.stu_name;
                dr["cur_hak"] = score.cur_hak;
                dr["cur_ban"] = score.cur_ban;

                dtStu.Rows.Add(dr);
            }

           DataTable dtAnswer_p = ConvertExcelData(cs06.Answer_P);
           foreach(ApplyUniversity item in cs06.UniverSityItem_P)
           {
                try
                {
                    dtAnswer_p.Select("stu_id = '" + item.MemberIdx.ToString()+"'")[0][item.Ranking.ToString() + "순위_대학"] = item.UniversityName;
                }
                catch(Exception ex)
                {

                }
            }

            DataTable dtAnswer_s = ConvertExcelData(cs06.Answer_S);
            foreach (ApplyUniversity item in cs06.UniverSityItem_S)
            {
                try
                {
                    dtAnswer_s.Select("stu_id = '" + item.MemberIdx.ToString() + "'")[0][item.Ranking.ToString() + "순위_대학"] = item.UniversityName;
                }
                catch (Exception ex)
                {

                }
            }

            DataSet sourceSet = new DataSet();
            dtStu.TableName = "CS06_STU_LIST";
            dtNesin.TableName = "CS06_NESIN";
            dtMoi.TableName = "CS06_MOI";
            dtAnswer_p.TableName = "기초조사서(부모)";
            dtAnswer_s.TableName = "기초조사서(학생)";
            sourceSet.Tables.Add(dtStu);
            sourceSet.Tables.Add(dtNesin);
            sourceSet.Tables.Add(dtMoi);
            sourceSet.Tables.Add(dtAnswer_p);
            sourceSet.Tables.Add(dtAnswer_s);

            Random r = new Random();
            string fileName = string.Format("TongUser_"+DateTime.Now.Year + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day+"_"+r.Next(1,1000).ToString()+".xls");
            //ExcelHelper.CreateExcelDocument(sourceSet, string.Format(@"D:\Workplace\Geumcheon\Geumcheon.Web.Consult\Uploads\Excel\{0}", fileName), 1, 1);
            //return Redirect("http://localhost:12401/Uploads/Excel/" + fileName);

            ExcelHelper.CreateExcelDocument(sourceSet, string.Format(@"D:\IIS\GEUMCHEON.WEB.CONSULT\Uploads\Excel\{0}", fileName), 1, 1);
            return Redirect("http://geumcheon.eduplan.co.kr/Uploads/Excel/" + fileName);
        }
        public static DataTable ConvertExcelData(List<AnswerItem>lstAnswer)
        {
            DataTable dtAnswer = new DataTable();

            dtAnswer.Columns.Add("stu_id");
            dtAnswer.Columns.Add("학습_제일어렵거나힘든과목");
            dtAnswer.Columns.Add("학습_위의과목공부가힘든이유");
            dtAnswer.Columns.Add("학습_그외의과목에서어려움");
            dtAnswer.Columns.Add("진학_진학방면으로고민되는도움받고싶은부분");
            dtAnswer.Columns.Add("진로_앞으로어떤갖고싶은직업");
            dtAnswer.Columns.Add("진로_위의직업을갖고싶은이유");
            dtAnswer.Columns.Add("진로_진로에대한고민");
            dtAnswer.Columns.Add("생활_생활하면서고민되는부분");
            dtAnswer.Columns.Add("생활_고치고싶은습관");
            dtAnswer.Columns.Add("기타_현재다니고있는학원_과목기타사항");
            dtAnswer.Columns.Add("기타_궁금하거나상담받고싶은사항");

            dtAnswer.Columns.Add("1순위_대학");
            dtAnswer.Columns.Add("2순위_대학");
            dtAnswer.Columns.Add("3순위_대학");


            dtAnswer.Columns.Add("금천구컨설팅상담유무");
            dtAnswer.Columns.Add("희망컨설턴트");

            dtAnswer.Columns.Add("[중등]특목등희망학교준비");
            dtAnswer.Columns.Add("[중등]특목등희망학교에대한진학상담경험");

            dtAnswer.Columns.Add("[중등]1순위_고등");
            dtAnswer.Columns.Add("[중등]2순위_고등");
            dtAnswer.Columns.Add("[중등]3순위_고등");


            var lstMemnerIdx_P = from p in lstAnswer
                                 group p by p.MemberIdx into g
                                 select new { MemberIdx = g.Key };
            foreach (var idx in lstMemnerIdx_P)
            {
                DataRow dr = dtAnswer.NewRow();
                dr["stu_id"] = idx.MemberIdx;
                foreach (var item in lstAnswer.Where(x => x.MemberIdx.Equals(idx.MemberIdx)).ToList())
                {
                    switch (item.ItemName)
                    {
                        case "s_a1":
                            {
                                dr["학습_제일어렵거나힘든과목"] = item.Answer;
                                break;
                            }
                        case "s_a2":
                            {
                                dr["학습_위의과목공부가힘든이유"] = item.Answer;
                                break;
                            }
                        case "s_a3":
                            {
                                dr["학습_그외의과목에서어려움"] = item.Answer;
                                break;
                            }
                        case "j_a1":
                            {
                                dr["진학_진학방면으로고민되는도움받고싶은부분"] = item.Answer;
                                break;
                            }
                        case "c_a1":
                            {
                                dr["진로_앞으로어떤갖고싶은직업"] = item.Answer;
                                break;
                            }
                        case "c_a2":
                            {
                                dr["진로_위의직업을갖고싶은이유"] = item.Answer;
                                break;
                            }
                        case "c_a3":
                            {
                                dr["진로_진로에대한고민"] = item.Answer;
                                break;
                            }
                        case "l_a1":
                            {
                                dr["생활_생활하면서고민되는부분"] = item.Answer;
                                break;
                            }
                        case "l_a2":
                            {
                                dr["생활_고치고싶은습관"] = item.Answer;
                                break;
                            }
                        case "e_a1":
                            {
                                dr["기타_현재다니고있는학원_과목기타사항"] = item.Answer;
                                break;
                            }
                        case "e_a2":
                            {
                                dr["기타_궁금하거나상담받고싶은사항"] = item.Answer;
                                break;
                            }
                        case "r_a1":
                            {
                                dr["금천구컨설팅상담유무"] = item.Answer;
                                break;
                            }
                        case "r_a2":
                            {
                                dr["희망컨설턴트"] = item.Answer;
                                break;
                            }
                        case "m_a1":
                            {
                                dr["[중등]특목등희망학교준비"] = item.Answer;
                                break;
                            }
                        case "m_a2":
                            {
                                dr["[중등]특목등희망학교에대한진학상담경험"] = item.Answer;
                                break;
                            }
                        case "h_a1":
                            {
                                dr["[중등]1순위_고등"] = item.Answer;
                                break;
                            }
                        case "h_a2":
                            {
                                dr["[중등]2순위_고등"] = item.Answer;
                                break;
                            }
                        case "h_a3":
                            {
                                dr["[중등]3순위_고등"] = item.Answer;
                                break;
                            }
                    }

                }
                dtAnswer.Rows.Add(dr);
            }

            return dtAnswer;
        }
        public static List<string> GetFieldNameList<T>()
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));

            List<string> list = new List<string>(props.Count);

            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                list.Add(prop.Name);
            }

            return list;
        }
        #endregion
    }
}