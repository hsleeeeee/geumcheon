﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Geumcheon.Web.Consult.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult err(string status="0")
        {
            if (status == "0")
            {
                ViewBag.ErrMessage = "시스템에 예기지 않은 문제가 발생했습니다";
            }
            else if(status == "401")
            {
                ViewBag.ErrMessage = "잘못된 접근입니다.";
            }
            else
            {
                ViewBag.ErrMessage = status;
            }
            return View();
        }
        
    }
}