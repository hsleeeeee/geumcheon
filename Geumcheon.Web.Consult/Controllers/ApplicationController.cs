﻿using Geumcheon.Web.Consult.Models;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Geumcheon.Web.Consult.Controllers
{
    public class ApplicationController : Controller
    {
        // GET: Application
        public ActionResult Index()
        {
            if (Request.UserAgent.ToLower().IndexOf("iphone") > -1)
            {
                return Redirect("https://itunes.apple.com/kr/app/id1511879785");
            }
            else if (Request.UserAgent.ToLower().IndexOf("android") > -1)
            {
                return Redirect("https://play.google.com/store/apps/details?id=com.FuturePlan.GeumcheonConsult");
            }
            return View();
        }

        public ActionResult Info()
        {
            if (Request.UserAgent.ToLower().IndexOf("iphone") > -1)
            {
                return Redirect("https://itunes.apple.com/kr/app/id1511879785");
            }
            else if (Request.UserAgent.ToLower().IndexOf("android") > -1)
            {
                return Redirect("https://play.google.com/store/apps/details?id=com.FuturePlan.GeumcheonConsult");
            }
            return View();
        }

        public ActionResult Info_iframe()
        {
            return View();
        }
    }
}