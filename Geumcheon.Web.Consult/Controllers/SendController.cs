﻿using Geumcheon.Web.Consult.App_Start;
using Geumcheon.Web.Consult.Models;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Geumcheon.Web.Consult.Controllers
{
    public class SendController : Controller
    {
        static string _COMPANY_CODE = "0001";
        static int _PAGESIZE = 15;
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }

        [FPAuthorize(Roles = "GEUMCHEON")]
        public ActionResult SendMessage()
        {
            ViewBag.Navigation = "NOTISEND";

            return View();
        }

        // 프로그램 리스트 가져오기
        [FPAuthorize(Roles = "GEUMCHEON")]
        [HttpGet]
        public JsonResult GetProgramList(string way = "V", int page = 1)
        {
            if (way == "D")
            {
                ViewModelList<Member, SearchBase> model = new ViewModelList<Member, SearchBase>
                {
                    Entities = new List<Member>(),
                    Search = new SearchBase(),
                    PagingInfo = new PagingInfo(page, _PAGESIZE, 0)
                };

                MemberRepository.Instance.MemberSelectPaging(model);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else if (way == "V" || way == "I" || way == "O")
            {
                ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>
                {
                    Entities = new List<EntranceRequest>(),
                    Search = new SearchInfo
                    {
                        CompanyCode = _COMPANY_CODE,
                        ConsultWay = way.ToLower(),
                        TeacherId = _identity.ConsultId.ToLower() == "admin" ? null : _identity.ConsultId
                    },
                    PagingInfo = new PagingInfo(page, _PAGESIZE, 0)
                };
                
                ConsultRepository.Instance.EntranceRequestSelectPaging(model);

                return Json(model,JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (_identity.ConsultId.ToLower() == "admin")
                {
                    var ret = PresentationRepository.Instance.PresentationSelectList(way);

                    foreach (var item in ret)
                    {
                        int cnt = PresentationRepository.Instance.PresentationRequestSelect(item.Idx);
                        item.BufferCount = item.BufferCount - cnt;
                    }

                    return Json(new
                    {
                        Entities = ret,
                        PagingInfo = new PagingInfo(page, _PAGESIZE, ret.Count)
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "잘못 된 접근입니다." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // 해당 프로그램에 신청 또는 속해있는 사용자 정보 가져오기
        [FPAuthorize(Roles = "GEUMCHEON")]
        [HttpGet]
        public JsonResult GetProgramMemberList(string way = "V", string id = "", int idx = 0)
        {
            if (way == "V" || way == "I" || way == "O")
            {
                var member = MemberRepository.Instance.MemberSelect(id);
                if (member.FirstOrDefault() != null)
                {
                    member.FirstOrDefault().ReceiveNotification = MobileRepository.Instance.MobileSettingSelect(id).FirstOrDefault()?.ReceiveNotification ?? false;
                }

                return Json(new
                {
                    Entities = member.Select(x => new { x.Idx, x.ID, x.Name, x.Hp, x.ReceiveNotification })
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (_identity.ConsultId.ToLower() == "admin")
                {
                    var ret = PresentationRepository.Instance.PresentationRequestSelectForSendInfo(idx);

                    return Json(new
                    {
                        Entities = ret.Select(x => new { x.Idx, x.ID, x.Name, x.Hp, x.ReceiveNotification })
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { error = "잘못 된 접근입니다." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // SMS 발송
        [FPAuthorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public JsonResult SetSendMessage()
        {
            JObject model = JObject.Parse(new StreamReader(Request.InputStream).ReadToEnd());

            try
            {
                if (MobilePushSend(model["target"]?.Value<string>(), model["title"]?.Value<string>(), model["body"]?.Value<string>(), model["extra"]?.Value<string>()))
                {
                    return Json(new { success = "OK" });
                }
                else
                {
                    return Json(new { error = "푸시 발송에 실패하였습니다." });
                }
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.ToString() });
            }
        }

        private bool MobilePushSend(string userID, string title, string body, string exdata)
        {
            var notificationSetting = MobileRepository.Instance.MobileSettingSelect(userID);

            TimeSpan currentTime = DateTime.Now.TimeOfDay;

            bool pushSuccess = false;
            string errorMsg = string.Empty;
            JObject jResult = new JObject();

            List<string> devices_Android = new List<string>();
            List<string> devices_iPhone = new List<string>();

            foreach (var login in notificationSetting)
            {
                if (login.ReceiveNotification && (!login.ExceptionTime || (currentTime <= login.ExceptionTimeStart || currentTime >= login.ExceptionTimeEnd)))
                {
                    if (login.DeviceType == "A")
                    {
                        devices_Android.Add(login.DeviceToken);
                    }
                    else if (login.DeviceType == "I")
                    {
                        devices_iPhone.Add(login.DeviceToken);
                    }
                }

            }

            if (devices_Android.Count == 0 && devices_iPhone.Count == 0)
            {
                pushSuccess = true;
                errorMsg = "로그인 된 디바이스 기록이 없습니다.";
            }

            var serializer = new JavaScriptSerializer();

            // 안드로이드 발송
            if (devices_Android.Count > 0)
            {
                try
                {
                    int curLength = 0;

                    while (curLength < devices_Android.Count)
                    {
                        List<string> get_ids;

                        if (devices_Android.Count - (curLength + 1000) > 0)
                        {
                            get_ids = devices_Android.GetRange(curLength, curLength + 1000);
                            curLength += 1000;
                        }
                        else
                        {
                            get_ids = devices_Android.GetRange(curLength, devices_Android.Count);
                            curLength = devices_Android.Count;
                        }

                        string data = string.Empty;
                        var jsonData = new
                        {
                            registration_ids = get_ids.ToArray(),
                            notification = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                            },
                            data = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                            }
                        };
                        //data = serializer.Serialize(jsonData);

                        data = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);

                        if (!string.IsNullOrEmpty(data))
                        {
                            string result = PushSend(data);
                            if (result == "OK")
                            {
                                pushSuccess = true;
                            }
                            errorMsg = result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorMsg = ex.ToString();
                }
            }


            // 아이폰 발송            
            if (devices_iPhone.Count > 0)
            {
                try
                {
                    int curLength = 0;

                    while (curLength < devices_iPhone.Count)
                    {
                        List<string> get_ids;

                        if (devices_iPhone.Count - (curLength + 1000) > 0)
                        {
                            get_ids = devices_iPhone.GetRange(curLength, curLength + 1000);
                            curLength += 1000;
                        }
                        else
                        {
                            get_ids = devices_iPhone.GetRange(curLength, devices_iPhone.Count);
                            curLength = devices_iPhone.Count;
                        }

                        string data = string.Empty;
                        var jsonData = new
                        {
                            registration_ids = get_ids.ToArray(),
                            notification = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                                sound = "default",
                                vibrate = "true"
                            },
                            data = new
                            {
                                data = exdata
                            }
                        };
                        data = serializer.Serialize(jsonData);

                        if (!string.IsNullOrEmpty(data))
                        {
                            string result = PushSend(data);
                            if (result == "OK")
                            {
                                pushSuccess = true;
                            }
                            errorMsg = result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorMsg = ex.ToString();
                }
            }

            try
            {
                MobileRepository.Instance.MobilePushLogInsert(new Core.Entity.MobilePushLog
                {
                    ID = userID,
                    Title = title,
                    Body = body,
                });
            }
            catch 
            {
                
            }

            return pushSuccess;
        }

        private string PushSend(string data)
        {
            try
            {
                var applicationID = ConfigurationManager.AppSettings["PushAppID"];
                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                WebRequest request = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                request.Method = "post";
                request.ContentType = "application/json";
                request.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                request.ContentLength = byteArray.Length;

                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(byteArray, 0, byteArray.Length);

                    using (Stream dataStreamResponse = request.GetResponse().GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            JObject push = JObject.Parse(tReader.ReadToEnd());

                            if (push.SelectToken("success").Value<bool>())
                            {
                                return "OK";
                            }
                            else
                            {
                                JToken jArray = push.SelectToken("results");

                                if (jArray.FirstOrDefault() != null)
                                {
                                    return jArray.FirstOrDefault().SelectToken("error")?.Value<string>();
                                }
                                else
                                {
                                    return string.Empty;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}