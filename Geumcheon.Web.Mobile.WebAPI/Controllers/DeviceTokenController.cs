﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class DeviceTokenController : BaseController
    {
        [Authorize(Roles = "GEUMCHEON")]
        [HttpPut]
        public HttpResponseMessage Put(JObject value)
        {
            try
            {
                string token = value.SelectToken("token")?.Value<string>() ?? string.Empty;
                string type = value.SelectToken("type")?.Value<string>() ?? string.Empty;
                string refreshtoken = value.SelectToken("refreshtoken")?.Value<string>() ?? string.Empty;

                MobileRepository.Instance.MobileLoginTokenUpsert(new MobileLogin
                {
                    DeviceToken = token,
                    DeviceType = type,
                    ID = GetClaimRole("Parent") ? GetClaims("parentid") : GetClaims("id"),
                }, refreshtoken);

                return ResultMessageJson(true);
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false, ex.ToString());
            }
        }
    }
}
