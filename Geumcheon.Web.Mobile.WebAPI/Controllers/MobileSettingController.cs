﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class MobileSettingController : BaseController
    {
        [Authorize(Roles = "GEUMCHEON")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                var item = MobileRepository.Instance.MobileSettingSelect(GetClaims("id")).FirstOrDefault();

                if (item == null || string.IsNullOrEmpty(item.ID))
                {
                    return ResultMessageJson(new {
                        ReceiveNotification = true,
                        ExceptionTime = false,
                        ExceptionTimeStart = TimeSpan.FromTicks(0),
                        ExceptionTimeEnd = TimeSpan.FromHours(8)
                    });
                }
                else
                {
                    return ResultMessageJson(item);
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }


        [Authorize(Roles = "GEUMCHEON")]
        [HttpPut]
        public HttpResponseMessage Put(JObject model)
        {
            try
            {
                bool receiveNotification = model.SelectToken("receivenotification").Value<bool>();
                bool exceptionTime = model.SelectToken("exceptiontime").Value<bool>();
                TimeSpan exceptionTimeStart = new TimeSpan();
                TimeSpan exceptionTimeEnd = new TimeSpan();
                TimeSpan.TryParse(model.SelectToken("exceptiontimestart").Value<string>(), out exceptionTimeStart);
                TimeSpan.TryParse(model.SelectToken("exceptiontimeend").Value<string>(), out exceptionTimeEnd);

                if (MobileRepository.Instance.MobileSettingUpsert(new MobileSetting
                {
                    ID = GetClaims("id"),
                    ReceiveNotification = receiveNotification,
                    ExceptionTime = exceptionTime,
                    ExceptionTimeStart = exceptionTimeStart,
                    ExceptionTimeEnd = exceptionTimeEnd
                }) > 0)
                {
                    return ResultMessageJson(true);
                }
                else
                {
                    return ResultMessageJson(false, "업데이트 중 오류가 발생하였습니다.");
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
            
        }
    }
}
