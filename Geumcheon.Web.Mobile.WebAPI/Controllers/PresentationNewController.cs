﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class PresentationNewController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage Get(string type, string way = "E", int pidx = 0)
        {
            try
            {
                if (type == "list")
                {
                    var ret = PresentationRepository.Instance.PresentationSelectList(way);

                    return ResultMessageJson(new
                    {
                        Entities = ret
                    });
                }
                else if (type == "request")
                {
                    int midx = 0;
                    int.TryParse(GetClaims("memberIdx"), out midx);
                    List<PresentationRequest> ret;

                    if (midx == 0)
                    {
                        var item = PresentationRepository.Instance.PresentationSelectItem(pidx);
                        ret = new List<PresentationRequest>();
                        ret.Add(new PresentationRequest
                        {
                            PtRequestIdx = 0,
                            Name = "",
                            HP = "",
                            Email = "",
                            Memo = "",
                            AccountType = "S",
                        });
                    }
                    else
                    {
                        ret = PresentationRepository.Instance.PresentationRequestSelectByPtIdx(midx, pidx, 0);
                    }

                    return ResultMessageJson(new
                    {
                        Entities = ret.Select(x => new { x.Name, x.PtRequestIdx, x.HP, x.Email, x.AccountType, x.Memo })
                    });
                }
                else
                {
                    return ResultMessageJson(false, "잘못 된 접근입니다.");
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }

        [Authorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public HttpResponseMessage Post(string way, int page = 1)
        {
            try
            {
                var ret = PresentationRepository.Instance.PresentationRequestSelectList(int.Parse(GetClaims("memberIdx")), way);
                //ret.Select(c => { c.CreateDate = c.CreateDate.ToShortDateString(); return c; }).ToList();

                return ResultMessageJson(ret);
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }

        [AllowAnonymous]
        [HttpPut]
        public HttpResponseMessage Put(JObject model)
        {
            try
            {
                int ptidx = model["ptidx"].Value<int>();
                string memo = model["memo"].Value<string>();
                int memberIdx = -1;
                int.TryParse(GetClaims("memberIdx"), out memberIdx);

                List<string> rVal = new List<string>();

                foreach (var child in model["requestuserinfo"])
                {
                    if (!string.IsNullOrEmpty(child["Name"]?.Value<string>()))
                    {
                        PresentationRequest entity = new PresentationRequest
                        {
                            MemberIdx = memberIdx < 0 ? 0 : memberIdx,
                            PtIdx = ptidx,
                            PtRequestIdx = child["PtRequestIdx"].Value<int>(),
                            Name = child["Name"]?.Value<string>() ?? "",
                            HP = child["HP"]?.Value<string>() ?? "",
                            Email = child["Email"]?.Value<string>() ?? "",
                            AccountType = child["AccountType"]?.Value<string>() ?? "",
                            Memo = memo
                        };

                        rVal.Add(PresentationRepository.Instance.PresentationRequestModifyAnonymouse(entity));
                    }
                    else
                    {
                        rVal.Add("");
                    }
                }

                return ResultMessageJson(new
                {
                    state = rVal
                });

                //PresentationRequest pr = new PresentationRequest();

                //int ptidx = model[""]


                //if (model.ContainsKey("request"))
                //{
                //    pr = JsonConvert.DeserializeObject<PresentationRequest>(model.SelectToken("request").ToString());
                //}
                //else
                //{
                //    pr = JsonConvert.DeserializeObject<PresentationRequest>(model.ToString());
                //}

                //pr.MemberIdx = int.Parse(GetClaims("memberIdx"));

                ////Member member = new Member
                ////{
                ////    ID = GetClaims("id"),
                ////    Name = string.IsNullOrEmpty(pr.Name) ? null : pr.Name,
                ////    Hp = string.IsNullOrEmpty(pr.HP) ? null : pr.HP,
                ////    Email = string.IsNullOrEmpty(pr.Email) ? null : pr.Eamil,
                ////    School = string.IsNullOrEmpty(pr.School) ? null : pr.School,
                ////    Grade = pr.Grade,
                ////    IsUse = true,
                ////};

                ////int idx = MemberRepository.Instance.MemberUpdate(member);

                //string statestr = PresentationRepository.Instance.PresentationRequestModify(pr);
                //int retidx = -1;

                //int.TryParse(statestr.Split('/')[0], out retidx);
                //statestr = statestr.Split('/')[1];

                //if (retidx == 0 || statestr == "F")
                //{
                //    return ResultMessageJson(false, "신청 및 수정 중 오류가 발생하였습니다.");
                //}
                //else if (statestr == "D" && retidx == 99)
                //{
                //    return ResultMessageJson(false, "이미 신청 되었습니다.");
                //}
                //else {
                //    return ResultMessageJson(true, statestr == "W" ? "예비 신청 되었습니다." : (pr.PtIdx == 0 ? "신청되었습니다." : "수정되었습니다."));
                //}
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }
    }
}
