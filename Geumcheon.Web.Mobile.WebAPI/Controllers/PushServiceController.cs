﻿using Geumcheon.Web.Core.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class PushServiceController : BaseController
    {
        [HttpPost]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> Post(JObject model)
        {
            string userID = model.SelectToken("userid")?.Value<string>() ?? string.Empty;
            string title = model.SelectToken("title")?.Value<string>() ?? string.Empty;
            string body = model.SelectToken("body")?.Value<string>() ?? string.Empty;
            string exdata = model.SelectToken("data")?.Value<string>() ?? string.Empty;

            var notificationSetting = MobileRepository.Instance.MobileSettingSelect(userID);

            TimeSpan currentTime = DateTime.Now.TimeOfDay;

            bool pushSuccess = false;
            string errorMsg = string.Empty;
            JObject jResult = new JObject();

            List<string> devices_Android = new List<string>();
            List<string> devices_iPhone = new List<string>();

            foreach (var login in notificationSetting)
            {
                if (login.ReceiveNotification && (!login.ExceptionTime || (currentTime <= login.ExceptionTimeStart || currentTime >= login.ExceptionTimeEnd)))
                {
                    if (login.DeviceType == "A")
                    {
                        devices_Android.Add(login.DeviceToken);
                    }
                    else if (login.DeviceType == "I")
                    {
                        devices_iPhone.Add(login.DeviceToken);
                    }
                }

            }

            if (devices_Android.Count == 0 && devices_iPhone.Count == 0)
            {
                pushSuccess = true;
                errorMsg = "로그인 된 디바이스 기록이 없습니다.";
            }

            var serializer = new JavaScriptSerializer();

            // 안드로이드 발송
            if (devices_Android.Count > 0)
            {
                try
                {
                    int curLength = 0;

                    while (curLength < devices_Android.Count)
                    {
                        List<string> get_ids;

                        if (devices_Android.Count - (curLength + 1000) > 0)
                        {
                            get_ids = devices_Android.GetRange(curLength, curLength + 1000);
                            curLength += 1000;
                        }
                        else
                        {
                            get_ids = devices_Android.GetRange(curLength, devices_Android.Count);
                            curLength = devices_Android.Count;
                        }

                        string data = string.Empty;
                        var jsonData = new
                        {
                            registration_ids = get_ids.ToArray(),
                            notification = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                            },
                            data = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                            }
                        };
                        //data = serializer.Serialize(jsonData);

                        data = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);

                        if (!string.IsNullOrEmpty(data))
                        {
                            string result = PushSend(data);
                            if (result == "OK")
                            {
                                pushSuccess = true;
                            }
                            else if (!string.IsNullOrEmpty(result))
                            {
                                SetExceptionInfo(new Exception(result));
                            }
                            errorMsg = result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    SetExceptionInfo(ex);
                    errorMsg = ex.ToString();
                }
            }


            // 아이폰 발송            
            if (devices_iPhone.Count > 0)
            {
                try
                {
                    int curLength = 0;

                    while (curLength < devices_iPhone.Count)
                    {
                        List<string> get_ids;

                        if (devices_iPhone.Count - (curLength + 1000) > 0)
                        {
                            get_ids = devices_iPhone.GetRange(curLength, curLength + 1000);
                            curLength += 1000;
                        }
                        else
                        {
                            get_ids = devices_iPhone.GetRange(curLength, devices_iPhone.Count);
                            curLength = devices_iPhone.Count;
                        }

                        string data = string.Empty;
                        var jsonData = new
                        {
                            registration_ids = get_ids.ToArray(),
                            notification = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                                sound = "default",
                                vibrate = "true"
                            },
                            data = new
                            {
                                data = exdata
                            }
                        };
                        data = serializer.Serialize(jsonData);

                        if (!string.IsNullOrEmpty(data))
                        {
                            string result = PushSend(data);
                            if (result == "OK")
                            {
                                pushSuccess = true;
                            }
                            else if (!string.IsNullOrEmpty(result))
                            {
                                SetExceptionInfo(new Exception(result));
                            }
                            errorMsg = result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    SetExceptionInfo(ex);
                    errorMsg = ex.ToString();
                }
            }

            try
            {
                MobileRepository.Instance.MobilePushLogInsert(new Core.Entity.MobilePushLog
                {
                    ID = userID,
                    Title = title,
                    Body = body,
                });
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
            }

            return ResultMessageJson(new
            {
                success = pushSuccess,
                message = errorMsg
            });
        }

        private string PushSend(string data)
        {
            try
            {
                var applicationID = ConfigurationManager.AppSettings["ApplicationID"];
                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                WebRequest request = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                request.Method = "post";
                request.ContentType = "application/json";
                request.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                request.ContentLength = byteArray.Length;

                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(byteArray, 0, byteArray.Length);

                    using (Stream dataStreamResponse = request.GetResponse().GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            JObject push = JObject.Parse(tReader.ReadToEnd());

                            if (push.SelectToken("success").Value<bool>())
                            {
                                return "OK";
                            }
                            else
                            {
                                JToken jArray = push.SelectToken("results");

                                if (jArray.FirstOrDefault() != null)
                                {
                                    return jArray.FirstOrDefault().SelectToken("error")?.Value<string>();
                                }
                                else
                                {
                                    return string.Empty;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return string.Empty;
            }
        }
    }
}