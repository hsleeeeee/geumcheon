﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class AccountController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage Get(string id)
        {
            try
            {
                if (MemberRepository.Instance.MemberIDCheck(HttpUtility.UrlDecode(id)) == null)
                {
                    return ResultMessageJson(true, "사용 가능한 ID 입니다.");
                }
                else
                {
                    return ResultMessageJson(false, "이미 사용 중인 ID 입니다.");
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }

        [Authorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public HttpResponseMessage Post(JObject model)
        {
            try
            {
                if (model == null)
                {
                    var member = MemberRepository.Instance.MemberSelectItem(GetClaims("id"));

                    return ResultMessageJson(new
                    {
                        member.Name,
                        member.Gender,
                        member.Hp,
                        member.Hp_P,
                        member.Hp_S,
                        member.Email,
                        member.School,
                        member.Grade,
                        member.Address,
                        member.SchoolType,
                        Univ = member.univ
                    });
                }
                else
                {
                    var hope = model.SelectToken("hope");

                    if (hope != null)
                    {
                        var member = MemberRepository.Instance.MemberSelectItem(GetClaims("id"));

                        DataTable univ = new DataTable();
                        univ.Columns.Add(new DataColumn("ApplyUnivIdx", typeof(int)));
                        univ.Columns.Add(new DataColumn("Ranking", typeof(int)));
                        univ.Columns.Add(new DataColumn("UniversityName", typeof(string)));
                        univ.Columns.Add(new DataColumn("StypeRem", typeof(string)));
                        univ.Columns.Add(new DataColumn("MajorName", typeof(string)));

                        foreach (var item in hope)
                        {
                            int unividx = 0;
                            int.TryParse(item.SelectToken("ApplyUnivIdx")?.Value<string>(), out unividx);

                            DataRow row = univ.NewRow();

                            row["ApplyUnivIdx"] = unividx;
                            row["Ranking"] = item.SelectToken("Ranking").Value<int>();
                            row["UniversityName"] = item.SelectToken("UniversityName").Value<string>();
                            row["StypeRem"] = item.SelectToken("StypeRem").Value<string>();
                            row["MajorName"] = item.SelectToken("MajorName").Value<string>();
                            univ.Rows.Add(row);
                            row = null;
                        }

                        if (univ.Rows.Count > 0)
                        {
                            MyPageRepository.Instance.SetApplyUniversity(new ApplyUniversity
                            {
                                MemberIdx = member.Idx,
                                WriterType = GetClaims("Roll") == "PARENT" ? "P" : "S",
                                tbUnivesity = univ
                            });
                        }

                        return ResultMessageJson(true);
                    }
                    else
                    {
                        string deviceid = model.SelectToken("deviceid")?.Value<string>() ?? string.Empty;
                        string phonenumber = model.SelectToken("phonenumber")?.Value<string>() ?? string.Empty;
                        string code = model.SelectToken("code")?.Value<string>() ?? string.Empty;

                        string ret = MobileRepository.Instance.MobileSMSAuth("V", new MobileSMSAuth
                        {
                            DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                            PhoneNumber = phonenumber,
                            Code = code
                        });

                        if (!string.IsNullOrEmpty(ret))
                        {
                            Member member = new Member
                            {
                                ID = GetClaims("id"),
                                Password = model.SelectToken("pwd")?.Value<string>(),
                                Name = model.SelectToken("name")?.Value<string>(),
                                Hp = phonenumber,
                                Email = model.SelectToken("email")?.Value<string>(),
                                School = model.SelectToken("school")?.Value<string>(),
                                Grade = model.SelectToken("grade")?.Value<int>() ?? 1,
                                Gender = model.SelectToken("gender")?.Value<int>() ?? 1,
                                Address = model.SelectToken("address")?.Value<string>(),
                                IsUse = true,
                                Hp_P = model.SelectToken("hp_p")?.Value<string>(),
                                Hp_S = model.SelectToken("hp_s")?.Value<string>(),
                                SchoolType = model.SelectToken("schooltype").Value<string>()
                            };

                            if (string.IsNullOrEmpty(member.Password))
                            {
                                member.Password = null;
                            }

                            int idx = MemberRepository.Instance.MemberUpdate(member);

                            if (idx > 0)
                            {
                                MobileRepository.Instance.MobileSMSAuth("D", new MobileSMSAuth
                                {
                                    DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                                    PhoneNumber = phonenumber,
                                    Code = code
                                });

                                return ResultMessageJson(true);
                            }
                            else
                            {
                                return ResultMessageJson(false, "업데이트 중 오류가 발생하였습니다.");
                            }
                        }
                        else
                        {
                            MobileRepository.Instance.MobileSMSAuth("D", new MobileSMSAuth
                            {
                                DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                                PhoneNumber = phonenumber,
                                Code = code
                            });
                            return ResultMessageJson(false, "인증 정보가 만료되었습니다. 처음부터 다시 시도해주시기 바랍니다.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
            
        }

        [AllowAnonymous]
        [HttpPut]
        public HttpResponseMessage Put(JObject model)
        {
            string phonenumber = model.SelectToken("hp")?.Value<string>() ?? string.Empty;
            string deviceid = model.SelectToken("deviceid")?.Value<string>() ?? string.Empty;
            string code = model.SelectToken("code")?.Value<string>() ?? string.Empty;

            string ret = MobileRepository.Instance.MobileSMSAuth("V", new MobileSMSAuth
            {
                DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                PhoneNumber = phonenumber,
                Code = code,
            });

            if (!string.IsNullOrEmpty(ret))
            {
                int joinCase = 0;
                int.TryParse(model.SelectToken("joincase")?.Value<string>(), out joinCase);

                Member member = new Member
                {
                    ID = model.SelectToken("id").Value<string>(),
                    Password = model.SelectToken("pwd").Value<string>(),
                    Name = model.SelectToken("name").Value<string>(),
                    Hp = phonenumber,
                    Email = model.SelectToken("email").Value<string>(),
                    School = model.SelectToken("school").Value<string>(),
                    Grade = model.SelectToken("grade").Value<int>(),
                    Gender = model.SelectToken("gender").Value<int>(),
                    AccountType = model.SelectToken("type").Value<string>(),
                    Address = model.SelectToken("area").Value<string>(),
                    JoinCase = joinCase,
                    JoinCaseEtc = model.SelectToken("joincaseetc")?.Value<string>(),
                    Hp_P = model.SelectToken("hp_p")?.Value<string>(),
                    Hp_S = model.SelectToken("hp_p")?.Value<string>(),
                    SchoolType = model.SelectToken("schooltype").Value<string>()
                };

                int idx = MemberRepository.Instance.MemberInsert(member);

                if (idx > 0)
                {
                    var hope = model.SelectToken("hope");

                    DataTable univ = new DataTable();
                    univ.Columns.Add(new DataColumn("ApplyUnivIdx", typeof(int)));
                    univ.Columns.Add(new DataColumn("Ranking", typeof(int)));
                    univ.Columns.Add(new DataColumn("UniversityName", typeof(string)));
                    univ.Columns.Add(new DataColumn("StypeRem", typeof(string)));
                    univ.Columns.Add(new DataColumn("MajorName", typeof(string)));

                    foreach (var item in hope)
                    {
                        DataRow row = univ.NewRow();

                        row["ApplyUnivIdx"] = 0;

                        row["Ranking"] = item.SelectToken("rank").Value<int>();
                        row["UniversityName"] = item.SelectToken("univ").Value<string>();
                        row["StypeRem"] = item.SelectToken("mode").Value<string>();
                        row["MajorName"] = item.SelectToken("department").Value<string>();
                        univ.Rows.Add(row);
                        row = null;
                    }

                    if (univ.Rows.Count > 0)
                    {
                        MyPageRepository.Instance.SetApplyUniversity(new ApplyUniversity
                        {
                            MemberIdx = idx,
                            WriterType = member.AccountType,
                            tbUnivesity = univ
                        });
                    }

                    MobileRepository.Instance.MobileSMSAuth("D", new MobileSMSAuth
                    {
                        DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                        PhoneNumber = phonenumber,
                        Code = code
                    });

                    return ResultMessageJson(true);
                }
                else
                {
                    return ResultMessageJson(false, "계정 생성 중 오류가 발생하였습니다.");
                }
            }
            else
            {
                MobileRepository.Instance.MobileSMSAuth("D", new MobileSMSAuth
                {
                    DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                    PhoneNumber = phonenumber,
                    Code = code
                });

                return ResultMessageJson(false, "인증 정보가 만료되었습니다. 처음부터 다시 시도해주시기 바랍니다."); return ResultMessageJson(false, "정보가 일치하지 않습니다.");
            }
        }
    }
}
