﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class BaseController : ApiController
    {
        public static int PAGESIZE = 15;
        public static string COMPANYCODE = "0001";

        [NonAction]
        public string GetClaims(string name)
        {
            try
            {
                var identity = User.Identity as ClaimsIdentity;

                return identity.FindFirst(name).Value;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [NonAction]
        public bool GetClaimRole(string name)
        {
            try
            {
                var identity = User.Identity as ClaimsIdentity;

                return identity.FindAll(ClaimTypes.Role).Any(x => x.Value == name);

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // 에러 데이터 삽입
        [NonAction]
        public string SetExceptionInfo(Exception ex)
        {
            // to-do 에러 처리

            //ICommonRepository service = System.Web.Mvc.DependencyResolver.Current.GetService(typeof(CommonRepository)) as ICommonRepository;

            ////filterContext.ExceptionHandled = true;
            //SystemErrorLog entity = new SystemErrorLog();
            //entity.Type = "Error";
            //entity.ErrorMessage = string.IsNullOrEmpty(ex.Message) ? string.Empty : ex.Message;
            //entity.ErrorContents = string.IsNullOrEmpty(ex.StackTrace) ? string.Empty : ex.Message;
            //entity.ErrorPage = string.Format("[{0}] {1}", HttpContext.Current.Request.HttpMethod, HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath);
            //entity.ErrorIp = HttpContext.Current.Request.UserHostAddress;

            //service.SystemErrorInsert(entity);

            return ex.Message;
        }

        // 성공, 실패 반환
        [NonAction]
        public HttpResponseMessage ResultMessageJson(bool success, string msg = "에러가 발생하였습니다.")
        {
            if (success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { result = "ok" }, Configuration.Formatters.JsonFormatter);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { error = msg }, Configuration.Formatters.JsonFormatter);
            }
        }

        // Json 데이터 반환
        [NonAction]
        public HttpResponseMessage ResultMessageJson(object json)
        {
            return Request.CreateResponse(HttpStatusCode.OK, json, Configuration.Formatters.JsonFormatter);
        }

        // 권한 없음 반환
        [NonAction]
        public HttpResponseMessage ResultMessageUnAuthorize()
        {
            return Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        [NonAction]
        public string RndNumber(int digit = 999999)
        {
            Random random = new Random();

            return random.Next(digit).ToString(string.Format("D{0}", digit.ToString().Length));
        }

        [NonAction]
        public string SendSMSCode(string code, string sender)
        {
            try
            {
                string smsId = ConfigurationManager.AppSettings.Get("SMSId");
                string smsKey = ConfigurationManager.AppSettings.Get("SMSKey");
                string smsSender = ConfigurationManager.AppSettings.Get("SMSSender");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://apis.aligo.in/send/");
                HttpWebResponse response = null;
                string postData = string.Format("user_id={0}&key={1}&sender={2}&receiver={3}&msg=인증번호 [{4}] (5분간 유효합니다.)", smsId, smsKey, smsSender, sender, code);
                byte[] data = Encoding.UTF8.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();

                    response = (HttpWebResponse)request.GetResponse();
                }

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return string.Empty;
                }
                else
                {
                    return response.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ex.ToString();
            }
        }
    }
}
