﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class ScheduleController : BaseController
    {
        [Authorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public HttpResponseMessage Post(JObject model)
        {
            string type = model.SelectToken("type")?.Value<string>();

            if (type == "date")
            {
                string way = model.SelectToken("way")?.Value<string>();

                string ableDate = ConsultRepository.Instance.GetScheduleDate(COMPANYCODE, GetClaims("schooltype"));

                return ResultMessageJson(new
                {
                    entities = (ableDate ?? "").Split(',')
                });
            }
            else if (type == "time")
            {
                string way = model.SelectToken("way")?.Value<string>();
                string date = model.SelectToken("date")?.Value<string>();

                List<ScheduleTime> rVal = ConsultRepository.Instance.GetScheduleTime(COMPANYCODE, date, GetClaims("schooltype"));

                return ResultMessageJson(new
                {
                    entities = rVal
                });
            }
            else
            {
                return ResultMessageJson(false, "잘못 된 요청입니다.");
            }
        }
    }
}