﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class AnonymousController : BaseController
    {
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage Post(JObject model)
        {
            try
            {
                if ((model.SelectToken("type")?.Value<string>() ?? string.Empty).Equals("smsrequest"))
                {
                    string phonenumber = model.SelectToken("phonenumber")?.Value<string>() ?? string.Empty;
                    string deviceid = Guid.NewGuid().ToString();
                    string smsCode = RndNumber();

                    string rtn = MobileRepository.Instance.MobileSMSAuth("C", new MobileSMSAuth
                    {
                        DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                        PhoneNumber = phonenumber,
                        Code = smsCode
                    });

                    if (!string.IsNullOrEmpty(rtn))
                    {
                        string rtnsms = SendSMSCode(smsCode, phonenumber);
                        if (string.IsNullOrEmpty(rtnsms))
                        {
                            return ResultMessageJson(new { deviceid = deviceid });
                        }
                        else
                        {
                            return ResultMessageJson(false, "SMS 발송에 문제가 발생하였습니다.");
                        }
                    }
                    else
                    {
                        return ResultMessageJson(false, "문제가 발생하여 다시 시도해 주십시오.");
                    }
                }
                else if ((model.SelectToken("type")?.Value<string>() ?? string.Empty).Equals("smscert"))
                {
                    string phonenumber = model.SelectToken("phonenumber")?.Value<string>() ?? string.Empty;
                    string deviceid = model.SelectToken("deviceid")?.Value<string>() ?? string.Empty;
                    string code = model.SelectToken("code")?.Value<string>() ?? string.Empty;

                    string ret = MobileRepository.Instance.MobileSMSAuth("V", new MobileSMSAuth
                    {
                        DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                        PhoneNumber = phonenumber,
                        Code = code,
                    });

                    if (!string.IsNullOrEmpty(ret))
                    {
                        return ResultMessageJson(new { deviceid = deviceid });
                    }
                    else
                    {
                        return ResultMessageJson(false, "정보가 일치하지 않습니다.");
                    }
                }
                // 아이디 찾기
                else if ((model.SelectToken("type")?.Value<string>() ?? string.Empty).Equals("id"))
                {
                    string displayname = model.SelectToken("displayname")?.Value<string>() ?? string.Empty;
                    string phonenumber = model.SelectToken("phonenumber")?.Value<string>().Trim() ?? string.Empty;

                    Member member = MemberRepository.Instance.MemberFind(new Member
                    {
                        Name = displayname,
                        Hp = phonenumber,
                        ID = ""
                    });

                    if (member != null)
                    {
                        return ResultMessageJson(new { ID = member.ID });
                    }
                    else
                    {
                        return ResultMessageJson(false, "일치하는 정보가 없습니다.");
                    }
                }
                // 비번 찾기 (SMS 인증번호 발송 요청)
                else if ((model.SelectToken("type")?.Value<string>() ?? string.Empty).Equals("pwdrequest"))
                {
                    string id = model.SelectToken("id")?.Value<string>() ?? string.Empty;
                    string displayname = model.SelectToken("displayname")?.Value<string>() ?? string.Empty;
                    string phonenumber = model.SelectToken("phonenumber")?.Value<string>() ?? string.Empty;

                    Member member = MemberRepository.Instance.MemberFind(new Member
                    {
                        Name = displayname,
                        Hp = phonenumber,
                        ID = id
                    });


                    if (member != null)
                    {
                        if (!member.IsUse || member.IsDelete)
                        {
                            return ResultMessageJson(false, "사용할 수 없는 계정입니다.");
                        }
                        else
                        {
                            string smsCode = RndNumber();
                            string deviceid = Guid.NewGuid().ToString();

                            string rtn = MobileRepository.Instance.MobileSMSAuth("C", new MobileSMSAuth
                            {
                                DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                                PhoneNumber = phonenumber,
                                Code = smsCode
                            });

                            if (!string.IsNullOrEmpty(rtn))
                            {
                                string rtnsms = SendSMSCode(smsCode, phonenumber);
                                if (string.IsNullOrEmpty(rtnsms))
                                {
                                    return ResultMessageJson(new { deviceid = deviceid });
                                }
                                else
                                {
                                    return ResultMessageJson(false, "SMS 발송에 실패하였습니다.");
                                }
                            }
                            else
                            {
                                return ResultMessageJson(false, "문제가 발생하여 다시 시도해 주십시오.");
                            }
                        }
                    }
                    else
                    {
                        return ResultMessageJson(false, "정보가 일치하지 않습니다.");
                    }
                }
                // 비번 찾기 (SMS 인증번호 확인)
                else if ((model.SelectToken("type")?.Value<string>() ?? string.Empty).Equals("pwdvalidate"))
                {
                    string deviceid = model.SelectToken("deviceid")?.Value<string>() ?? string.Empty;
                    string phonenumber = model.SelectToken("phonenumber")?.Value<string>() ?? string.Empty;
                    string code = model.SelectToken("code")?.Value<string>() ?? string.Empty;

                    string ret = MobileRepository.Instance.MobileSMSAuth("V", new MobileSMSAuth
                    {
                        DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                        PhoneNumber = phonenumber,
                        Code = code,
                    });

                    if (!string.IsNullOrEmpty(ret))
                    {
                        return ResultMessageJson(new { deviceid = deviceid });
                    }
                    else
                    {
                        return ResultMessageJson(false, "정보가 일치하지 않습니다.");
                    }
                }
                // 비번 찾기 (비번 변경)
                else if ((model.SelectToken("type")?.Value<string>() ?? string.Empty).Equals("pwdchange"))
                {
                    string deviceid = model.SelectToken("deviceid")?.Value<string>() ?? string.Empty;
                    string phonenumber = model.SelectToken("phonenumber")?.Value<string>() ?? string.Empty;
                    string code = model.SelectToken("code")?.Value<string>() ?? string.Empty;
                    string password = model.SelectToken("password")?.Value<string>() ?? string.Empty;
                    string id = model.SelectToken("id")?.Value<string>() ?? string.Empty;

                    string ret = MobileRepository.Instance.MobileSMSAuth("V", new MobileSMSAuth
                    {
                        DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                        PhoneNumber = phonenumber,
                        Code = code
                    });

                    if (!string.IsNullOrEmpty(ret))
                    {
                        if (MemberRepository.Instance.MemberUpdate(new Member
                        {
                            ID = id,
                            Password = password,
                            IsUse = true,
                            Grade = 0,
                        }) > 0)
                        {
                            MobileRepository.Instance.MobileSMSAuth("D", new MobileSMSAuth
                            {
                                DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                                PhoneNumber = phonenumber,
                                Code = code
                            });
                            return ResultMessageJson(true);
                        }
                        else
                        {
                            return ResultMessageJson(false, "비밀번호 변경에 실패하였습니다.");
                        }
                    }
                    else
                    {
                        MobileRepository.Instance.MobileSMSAuth("D", new MobileSMSAuth
                        {
                            DeviceToken = string.Format("{0}|{1}", deviceid, phonenumber),
                            PhoneNumber = phonenumber,
                            Code = code
                        });
                        return ResultMessageJson(false, "인증 정보가 만료되었습니다. 처음부터 다시 시도해주시기 바랍니다.");
                    }
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }

            return ResultMessageUnAuthorize();
        }
    }
}
