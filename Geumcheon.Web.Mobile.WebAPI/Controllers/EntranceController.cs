﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class EntranceController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage Get(int requestIdx, string way = null, int entranceIdx = 0)
        {
            try
            {
                if (GetClaims("Roll") == "CONSULT")
                {
                    var item = EntranceRequestRepository.Instance.EntranceRequestSelectItem(requestIdx);
                    var member = MemberRepository.Instance.MemberSelectItem(item.member.ID);
                    var entrance = EntranceRequestRepository.Instance.EntranceInfoSelect(item.EIdx);
                    item.DisplayInputType = entrance?.DisplayInputType;
                    if (item.TeacherIdx == MemberRepository.Instance.MemberIDCheck(GetClaims("id"))?.Idx.ToString())
                    {
                        return ResultMessageJson(new
                        {
                            Member = new
                            {
                                member.Name,
                                member.Hp,
                                member.Email,
                                member.School,
                                member.Grade,
                                member.univ
                            },
                            item.Schedule,
                            Univ = member.univ,
                            Consult = new
                            {
                                item.ConsultWay,
                                item.StudentName,
                                item.StudentGender,
                                item.AreaName,
                                item.Title,
                                item.ScoreHtml,
                                item.Progress,
                            },
                            item.Reply.ReplyNote,
                            item.DisplayInputType
                        });
                    }
                    else
                    {
                        return ResultMessageJson(false, "잘못 된 접근입니다.");
                    }
                }
                else
                {
                    if (entranceIdx == 0 && requestIdx == 0) {
                        var ret = EntranceRequestRepository.Instance.EntranceSelectList(way);

                        return ResultMessageJson(new
                        {
                            Entities = ret
                        });
                    }
                    else if (requestIdx == 0)
                    {
                        int bufferCount = EntranceRequestRepository.Instance.EntranceSelect(way, entranceIdx);

                        return ResultMessageJson(new
                        {
                            BufferCount = bufferCount
                        });
                    }
                    else {
                        var member = MemberRepository.Instance.MemberSelectItem(GetClaims("id"));
                        var item = EntranceRequestRepository.Instance.EntranceRequestSelectItem(requestIdx);

                        if (item.member.ID == GetClaims("id"))
                        {
                            return ResultMessageJson(new
                            {
                                Member = new
                                {
                                    member.Name,
                                    member.Hp,
                                    member.Email,
                                    member.School,
                                    member.Grade,
                                    member.univ
                                },
                                item.Schedule,
                                Univ = member.univ,
                                Consult = new
                                {
                                    item.ConsultWay,
                                    item.StudentName,
                                    item.StudentGender,
                                    item.AreaName,
                                    item.Title,
                                    item.ScoreHtml,
                                    item.Progress,
                                },
                                ReplyNote = (item.Reply != null && item.ConsultWay == "O") ? item.Reply.ReplyNote : "",
                                StarRating = (item.Reply != null && item.ConsultWay == "O") ? item.Reply.Satisfaction ?? -1 : -1,
                                item.NeedValue1,
                                item.NeedValue2
                            });
                        }
                        else
                        {
                            return ResultMessageJson(false, "잘못 된 접근입니다.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }


        [Authorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public HttpResponseMessage Post(SearchInfo searchInfo, string way = "I", int page = 1)
        {
            try
            {
              if (string.IsNullOrEmpty(searchInfo.CompanyCode))
                {
                    searchInfo.CompanyCode = COMPANYCODE;
                }

                ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>()
                {
                    Entities = new List<EntranceRequest>(),
                    Search = searchInfo,
                    PagingInfo = new PagingInfo(page, PAGESIZE, 0, searchInfo.OrderType)
                };

                EntranceRequestRepository.Instance.EntranceRequestSelectPaging(MemberRepository.Instance.MemberIDCheck(GetClaims("id")).Idx, model);
                return ResultMessageJson(new
                {
                    Entities = model.Entities.Select(x => new { x.RowIdx, x.RequestIdx, x.EIdx, x.ConsultWay, x.ConsultWayName, x.Title, x.Progress, x.StudentName, x.CreateDate, x.DeadlineDate, x.ConfirmDate, x.Schedule, x.Re_Note, x.NeedValue1, x.NeedValue2, x.DisplayInputType }).ToList(),
                    model.PagingInfo,
                });
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
            
        }

        [Authorize(Roles = "GEUMCHEON")]
        [HttpPut]
        public HttpResponseMessage Put(JObject model)
        {
            try
            {
                int requestIdx, scheduleTimeIdx = 0;
                int.TryParse(model.SelectToken("consulttime")?.Value<string>(), out scheduleTimeIdx);
                int.TryParse(model.SelectToken("requestidx").Value<string>(), out requestIdx);

                if (model["type"]?.Value<string>() == "V")
                {
                    List<ScheduleTime> rVal = ConsultRepository.Instance.GetScheduleTime(COMPANYCODE, model.SelectToken("consultdate")?.Value<string>(), GetClaims("schooltype"));
                    if (rVal != null && requestIdx == 0 && (rVal.Where(x => x.ConsultTimeIdx == scheduleTimeIdx).FirstOrDefault()?.Cnt ?? -1) < 1)
                    {
                        return ResultMessageJson(false, "해당 희망 날짜에 좌석이 없습니다.");
                    }
                }

                string studentName = model.SelectToken("studentname").Value<string>();
                string areaName = model.SelectToken("areaname").Value<string>();

                Member member = new Member
                {
                    ID = GetClaims("id"),
                    Name = model.SelectToken("name")?.Value<string>(),
                    Hp = model.SelectToken("hp")?.Value<string>(),
                    Email = model.SelectToken("email")?.Value<string>(),
                    School = model.SelectToken("school")?.Value<string>(),
                    Grade = model.SelectToken("grade")?.Value<int>() ?? 1,
                    Gender = null,
                    Address = areaName,
                    IsUse = true,
                    Hp_P = model.SelectToken("hp_p")?.Value<string>(),
                    Hp_S = model.SelectToken("hp_s")?.Value<string>()
                };

                int idx = MemberRepository.Instance.MemberUpdate(member);

                var hope = model.SelectToken("hope");
                if (hope != null)
                {
                    DataTable univ = new DataTable();
                    univ.Columns.Add(new DataColumn("ApplyUnivIdx", typeof(int)));
                    univ.Columns.Add(new DataColumn("Ranking", typeof(int)));
                    univ.Columns.Add(new DataColumn("UniversityName", typeof(string)));
                    univ.Columns.Add(new DataColumn("StypeRem", typeof(string)));
                    univ.Columns.Add(new DataColumn("MajorName", typeof(string)));

                    foreach (var item in hope)
                    {
                        int unividx = 0;
                        int.TryParse(item.SelectToken("ApplyUnivIdx")?.Value<string>(), out unividx);

                        DataRow row = univ.NewRow();

                        row["ApplyUnivIdx"] = unividx;
                        row["Ranking"] = item.SelectToken("Ranking").Value<int>();
                        row["UniversityName"] = item.SelectToken("UniversityName").Value<string>();
                        row["StypeRem"] = item.SelectToken("StypeRem").Value<string>();
                        row["MajorName"] = item.SelectToken("MajorName").Value<string>();
                        univ.Rows.Add(row);
                        row = null;
                    }

                    if (univ.Rows.Count > 0)
                    {
                        MyPageRepository.Instance.SetApplyUniversity(new ApplyUniversity
                        {
                            MemberIdx = idx,
                            WriterType = GetClaims("Roll") == "PARENT" ? "P" : "S",
                            tbUnivesity = univ
                        });
                    }
                }

                EntranceRequest app = new EntranceRequest
                {
                    RequestIdx = requestIdx,
                    MemberIdx = int.Parse(GetClaims("memberIdx")),
                    CompanyCode = COMPANYCODE,
                    Progress = "R",
                    ConsultWay = model.SelectToken("type")?.Value<string>() ?? "I",
                    Title = model.SelectToken("title")?.Value<string>(),
                    ScoreHtml = model.SelectToken("score")?.Value<string>(),
                    StudentName = model.SelectToken("studentname")?.Value<string>(),
                    AreaName = model.SelectToken("areaname")?.Value<string>(),
                    Schedule = new Schedule
                    {
                        ConsultTimeIdx = scheduleTimeIdx
                    },
                    EIdx = model.SelectToken("eidx").Value<int>(),
                    Re_Note = model.SelectToken("renote").Value<string>(),
                    NeedValue1 = model.SelectToken("needvalue1")?.Value<string>(),
                    NeedValue2 = model.SelectToken("needvalue2")?.Value<string>()
                };

                string statestr = EntranceRequestRepository.Instance.EntranceRequestUpsert(app, GetClaims("schooltype"));
                int retidx = -1;

                int.TryParse(statestr.Split('/')[0], out retidx);
                statestr = statestr.Split('/')[1];

                if (retidx < 0)
                {
                    if (requestIdx == 0)
                    {
                        return ResultMessageJson(false, "좌석 예약에 실패하였습니다.");
                    }
                    else
                    {
                        return ResultMessageJson(true, "수정 되었지만 해당 희망 날짜에 좌석이 없어 변경하지 못하였습니다.");
                    }
                }
                else
                {
                    return ResultMessageJson(true, statestr == "W" ? "예비 신청 되었습니다." : (requestIdx == 0 ? "신청되었습니다." : "수정되었습니다."));
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }
    }
}
