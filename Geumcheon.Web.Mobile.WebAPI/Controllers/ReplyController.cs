﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class ReplyController : BaseController
    {
        [Authorize(Roles = "GEUMCHEON")]
        [HttpPut]
        public HttpResponseMessage Put(JObject model)
        {
            try
            {
                Reply reply = JsonConvert.DeserializeObject<Reply>(model.ToString());

                if (reply != null)
                {
                    ConsultRepository.Instance.UpdateReply(reply);

                    return ResultMessageJson(true);
                }
                else
                {
                    return ResultMessageJson(false);
                }

            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
            
        }
    }
}
