﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class MyPageController : BaseController
    {
        [Authorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public HttpResponseMessage Post(SearchInfo searchInfo, string type = "consult", int page = 1)
        {
            try
            {
                if (string.IsNullOrEmpty(searchInfo.CompanyCode))
                {
                    searchInfo.CompanyCode = COMPANYCODE;
                }

                if (type == "consult")
                {
                    ViewModelList<EntranceRequest, SearchInfo> model = new ViewModelList<EntranceRequest, SearchInfo>()
                    {
                        Entities = new List<EntranceRequest>(),
                        Search = searchInfo,
                        PagingInfo = new PagingInfo(page, PAGESIZE, 0, searchInfo.OrderType)
                    };

                    EntranceRequestRepository.Instance.EntranceRequestSelectPaging(int.Parse(GetClaims("memberIdx")), model);

                    return ResultMessageJson(new
                    {
                        Entities = model.Entities.Select(x => new { x.RowIdx, x.ConsultWay, x.RequestIdx, x.Title, x.StudentName, x.Progress, x.CreateDate, x.ConfirmDate, x.Schedule }).ToList(),
                        model.PagingInfo,
                    });
                }
                else if (type == "presentation")
                {
                    ViewModelList<PresentationRequest, SearchInfo> model = new ViewModelList<PresentationRequest, SearchInfo>()
                    {
                        Entities = new List<PresentationRequest>(),
                        Search = searchInfo,
                        PagingInfo = new PagingInfo(page, PAGESIZE, 0, searchInfo.OrderType)
                    };

                    model.Entities = PresentationRepository.Instance.PresentationRequestSelectList(int.Parse(GetClaims("memberIdx")), null);
                    model.PagingInfo.TotalCount = model.Entities.Count;

                    return ResultMessageJson(new
                    {
                        Entities = model.Entities.Skip((page - 1) * PAGESIZE).Take(PAGESIZE),
                        model.PagingInfo,
                    });
                }
                else
                {
                    return ResultMessageJson(false, "잘못 된 접근입니다.");
                }

                
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
            
        }
    }
}
