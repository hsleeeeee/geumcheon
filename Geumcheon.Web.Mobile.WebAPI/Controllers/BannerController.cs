﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class BannerController : BaseController
    {
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            try
            {
                List<MobileBanner> banners = MobileRepository.Instance.MobileBannerSelect();

                return ResultMessageJson(new
                {
                    banners = banners.Select(x => new { x.Title, x.Body, x.Link, x.LinkTitle, x.IsUse, x.Color1, x.Color2, x.NavIdx })
                });
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }
    }
}
