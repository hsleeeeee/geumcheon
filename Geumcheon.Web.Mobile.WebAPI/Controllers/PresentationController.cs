﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Geumcheon.Web.Mobile.WebAPI.Controllers
{
    public class PresentationController : BaseController
    {
        [Authorize(Roles = "GEUMCHEON")]
        [HttpGet]
        public HttpResponseMessage Get(string way)
        {
            try
            {
                var member = MemberRepository.Instance.MemberSelectItem(GetClaims("id"));
                var ret = PresentationRepository.Instance.PresentationSelectList(way);

                if (ret != null)
                {
                    return ResultMessageJson(new
                    {
                        member.Name,
                        member.Gender,
                        member.Hp,
                        member.Email,
                        member.School,
                        member.Grade,
                        member.Address,
                        Entities = ret
                    });
                }
                else
                {
                    return ResultMessageJson(false, "잘못 된 접근입니다.");
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }

        [Authorize(Roles = "GEUMCHEON")]
        [HttpPost]
        public HttpResponseMessage Post(string way, int page = 1)
        {
            try
            {
                var ret = PresentationRepository.Instance.PresentationRequestSelectList(int.Parse(GetClaims("memberIdx")), way);
                //ret.Select(c => { c.CreateDate = c.CreateDate.ToShortDateString(); return c; }).ToList();

                return ResultMessageJson(ret);
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }

        [Authorize(Roles = "GEUMCHEON")]
        [HttpPut]
        public HttpResponseMessage Put(JObject model)
        {
            try
            {
                PresentationRequest pr = new PresentationRequest();

                if (model.ContainsKey("request"))
                {
                    pr = JsonConvert.DeserializeObject<PresentationRequest>(model.SelectToken("request").ToString());
                }
                else
                {
                    pr = JsonConvert.DeserializeObject<PresentationRequest>(model.ToString());
                }

                pr.MemberIdx = int.Parse(GetClaims("memberIdx"));

                //Member member = new Member
                //{
                //    ID = GetClaims("id"),
                //    Name = string.IsNullOrEmpty(pr.Name) ? null : pr.Name,
                //    Hp = string.IsNullOrEmpty(pr.HP) ? null : pr.HP,
                //    Email = string.IsNullOrEmpty(pr.Email) ? null : pr.Eamil,
                //    School = string.IsNullOrEmpty(pr.School) ? null : pr.School,
                //    Grade = pr.Grade,
                //    IsUse = true,
                //};

                //int idx = MemberRepository.Instance.MemberUpdate(member);

                string statestr = PresentationRepository.Instance.PresentationRequestModify(pr);
                int retidx = -1;

                int.TryParse(statestr.Split('/')[0], out retidx);
                statestr = statestr.Split('/')[1];

                if (retidx == 0 || statestr == "F")
                {
                    return ResultMessageJson(false, "신청 및 수정 중 오류가 발생하였습니다.");
                }
                else if (statestr == "D" && retidx == 99)
                {
                    return ResultMessageJson(false, "이미 신청 되었습니다.");
                }
                else {
                    return ResultMessageJson(true, statestr == "W" ? "예비 신청 되었습니다." : (pr.PtIdx == 0 ? "신청되었습니다." : "수정되었습니다."));
                }
            }
            catch (Exception ex)
            {
                SetExceptionInfo(ex);
                return ResultMessageJson(false);
            }
        }
    }
}
