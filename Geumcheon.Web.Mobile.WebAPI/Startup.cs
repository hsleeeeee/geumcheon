﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Geumcheon.Web.Mobile.WebAPI.Startup))]
namespace Geumcheon.Web.Mobile.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
        }
    }
}