﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Geumcheon.Web.Mobile.WebAPI
{
    public class CustomRefreshTokenProvider : IAuthenticationTokenProvider
    {
        //private static ConcurrentDictionary<string, AuthenticationTicket> _refreshTokens = new ConcurrentDictionary<string, AuthenticationTicket>();
        //private Dictionary<string, string> tokenDic = new Dictionary<string, string>();

        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            string refreshToken = CreateRefreshToken();
            string token = context.Ticket.Identity.Claims.Where(x => x.Type == "devicetoken").Select(c => c.Value).SingleOrDefault();
            string dtype = context.Ticket.Identity.Claims.Where(x => x.Type == "devicetype").Select(c => c.Value).SingleOrDefault();
            string refreshKey = context.SerializeTicket();

            MobileRepository.Instance.MobileLoginUpsert(new MobileLogin
            {
                DeviceToken = token,
                DeviceType = dtype,
                RefreshToken = refreshToken,
                RefreshKey = refreshKey
            });

            //tokenDic.Add(refreshToken, refreshKey);

            context.SetToken(refreshToken);
        }

        //public override Task CreateAsync(AuthenticationTokenCreateContext context)
        //{
        //    context.SetToken(CreateRefreshToken(context.Ticket));
        //}

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {

            MobileLogin model = MobileRepository.Instance.MobileLoginRefreshToken(context.Token);


            if (model != null)
            {
                var data = await context.Request.ReadFormAsync();

                context.DeserializeTicket(model.RefreshKey);
            }
            else
            {
                Console.WriteLine(context.Token);
            }
        }

        //public override Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        //{
        //    AuthenticationTicket ticket;
        //    if (_refreshTokens.TryRemove(context.Token, out ticket))
        //    {
        //        var newticket = new AuthenticationTicket(ticket.Identity, new AuthenticationProperties());
        //        context.SetTicket(newticket);
        //    }
        //}

        private string CreateRefreshToken()
        {
            var randomNumber = new byte[32];
            string refreshToken = string.Empty;

            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                refreshToken = Convert.ToBase64String(randomNumber);
            }

            return refreshToken;
        }
    }
}