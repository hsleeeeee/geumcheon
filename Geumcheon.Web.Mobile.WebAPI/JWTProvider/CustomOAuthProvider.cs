﻿using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.Repository;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Geumcheon.Web.Mobile.WebAPI
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var data = Task.Run(async () => await context.Request.ReadFormAsync());
            data.Wait();

            string deviceType = data.Result.Where(x => x.Key == "dtype").Select(x => x.Value).FirstOrDefault()?[0];
            string deviceToken = data.Result.Where(x => x.Key == "token").Select(x => x.Value).FirstOrDefault()?[0];

            Member member = MemberRepository.Instance.MemberLogin(context.UserName, context.Password);

            if (member != null)
            {
                if (!member.IsUse || member.IsDelete)
                {
                    context.SetError("invalid_grant", "The user is not used");
                    context.Rejected();
                }
                else
                {
                    MobileRepository.Instance.MobileLoginUpsert(new MobileLogin
                    {
                        DeviceToken = deviceToken,
                        DeviceType = deviceType,
                        ID = member.ID,
                        RefreshToken = string.Empty,
                        LoginDate = DateTime.Now
                    });

                    ClaimsIdentity identity = new ClaimsIdentity("JWT");
                    identity.AddClaim(new Claim("devicetoken", deviceToken));
                    identity.AddClaim(new Claim("devicetype", deviceType));
                    identity.AddClaim(new Claim("id", member.ID));
                    identity.AddClaim(new Claim("memberIdx", member.Idx.ToString()));
                    identity.AddClaim(new Claim("name", member.Name));
                    identity.AddClaim(new Claim("school", member.School));
                    identity.AddClaim(new Claim("schooltype", member.SchoolType));

                    if (member.AccountType == "S")
                    {
                        identity.AddClaim(new Claim("roll", "STUDENT"));
                    }
                    else if (member.AccountType == "P")
                    {
                        identity.AddClaim(new Claim("roll", "PARENT"));
                    }
                    identity.AddClaim(new Claim(ClaimTypes.Role, "GEUMCHEON"));
                    var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                    ticket.Properties.AllowRefresh = true;
                    context.Validated(ticket);
                }
            }
            else
            {
                //string teacherIdx = ConsultRepository.Instance.LoginTeacher(companyCode, context.UserName, context.Password);


                //if (!string.IsNullOrEmpty(teacherIdx))
                //{
                //    MobileRepository.Instance.MobileLoginUpsert(new MobileLogin{
                //        DeviceToken = deviceToken,
                //        DeviceType = deviceType,
                //        ID = context.UserName,
                //        RefreshToken = string.Empty,
                //        LoginDate = DateTime.Now
                //    });

                //    ClaimsIdentity identity = new ClaimsIdentity("JWT");

                //    identity.AddClaim(new Claim("id", context.UserName));
                //    identity.AddClaim(new Claim("roll", "CONSULT"));
                //    identity.AddClaim(new Claim(ClaimTypes.Role, "GEUMCHEON"));

                //    var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                //    ticket.Properties.AllowRefresh = true;
                //    context.Validated(ticket);
                //}
                //else
                //{
                    context.SetError("invalid_grant", "The user name or password is incorrect");
                    context.Rejected();
                //}
            }
        }

        public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            var identity = context.Ticket.Identity as ClaimsIdentity;

            try
            {
                string id = identity.FindFirst("id").Value;
                Member member = MemberRepository.Instance.MemberIDCheck(id);

                if (member != null)
                {
                    if (!member.IsUse || member.IsDelete)
                    {
                        context.SetError("invalid_grant", "The user is not used");
                        context.Rejected();
                    }
                    else
                    {
                        // 사용자 정보 업데이트
                        identity.RemoveClaim(identity.FindFirst("name"));
                        identity.RemoveClaim(identity.FindFirst("school"));
                        identity.RemoveClaim(identity.FindFirst("schooltype"));
                        //identity.RemoveClaim(identity.FindFirst("schoolname"));
                        identity.AddClaim(new Claim("name", member.Name));
                        identity.AddClaim(new Claim("school", member.School));
                        identity.AddClaim(new Claim("schooltype", member.SchoolType));
                        //identity.AddClaim(new Claim("gradeinfo", string.Format("{0}|{1:D2}|{2:D2}", student.cur_hak, student.cur_ban, student.cur_bun)));

                        var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                        ticket.Properties.AllowRefresh = true;
                        context.Validated(ticket);
                    }
                }
                else
                {
                    context.Rejected();
                }
            }
            catch (Exception ex)
            {
                context.Rejected();
            }
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }
    }
}