﻿using Microsoft.Owin.Security;
using System;
using System.Configuration;

namespace Geumcheon.Web.Mobile.WebAPI
{
    public class CustomRefreshJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private static readonly byte[] _secret = Convert.FromBase64String(ConfigurationManager.AppSettings["secret"]);
        private readonly string _issuer;

        public CustomRefreshJwtFormat(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            return string.Empty;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            Console.WriteLine(protectedText);

            return null;
        }
    }
}