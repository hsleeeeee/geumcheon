﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Reply
    {
        public int ApplicationIdx { get; set; }
        public int RequestIdx { get; set; }
        public int TeacherIdx { get; set; }
        public string Note { get; set; }
        public string ReplyNote { get; set; }
        public string Progress { get; set; }
        public int? Satisfaction { get; set; }


        public string ProgressName
        {
            get
            {
                string rVal = string.Empty;
                switch (Progress)
                {
                    case "R":
                        {
                            rVal = "대기중";
                            break;
                        }
                    case "I":
                        {
                            rVal = "작성중";
                            break;
                        }
                    case "C":
                        {
                            rVal = "상담완료";
                            break;
                        }
                }
                return rVal;
            }
        }
    }
}
