﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Schedule
    {
        public int ScheduleIdx { get; set; }
        public int ApplicaionIdx { get; set; }
        public int ConsultTimeIdx { get; set; }

        public string ConsultDate { get; set; }
        public string ConsultTime { get; set; }
    }

    public class SheduleTable
    {
        public List<ScheduleDate> lstScheduleDate { get; set; }
        public List<ScheduleTime> lstScheduleTime { get; set; }
    }
}
