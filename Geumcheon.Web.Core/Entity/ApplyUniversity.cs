﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class ApplyUniversity
    {
        public int ApplyUnivIdx { get; set; }
        public int MemberIdx { get; set; }
        public int Ranking { get; set; }
        public string UniversityName { get; set; }
        public string StypeRem { get; set; }
        public string MajorName { get; set; }
        public string WriterType { get; set; }

        public DataTable tbUnivesity { get; set; }
    }
}
