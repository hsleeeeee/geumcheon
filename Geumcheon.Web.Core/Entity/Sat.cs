﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class JsonUnivs
    {
        public string univ { get; set; }
        public double l { get; set; }
        public double h { get; set; }
        public double o { get; set; }
        public double c { get; set; }

        public double startScore { get; set; }
        public double endScore { get; set; }
        public double maxPer { get; set; }
        public double minPer { get; set; }
        public string univCode { get; set; }
        public string univName { get; set; }
        public string stypeRem { get; set; }
        public string inviteMem { get; set; }
        public string s2Rate1 { get; set; }
        public string hakGrade5Gap { get; set; } //입시결과/평균
        public string hakgrade1Gap { get; set; } //입시결과/최저
        public string applyHakGrade { get; set; } //지원가능등급
        public string applyHakScore1 { get; set; }//배치점수
        public string majorName { get; set; }
        public string majorCode { get; set; }

        public string finalCalcIdx { get; set; }
        public string masterIdx { get; set; }


    }

    public class JsonAimNsk
    {
        public string kor { get; set; }
        public string math { get; set; }
        public string eng { get; set; }
        public string soc { get; set; }
        public string sic { get; set; }
    }

    public class JsonAimSat
    {
        public string kor { get; set; }
        public string math { get; set; }
        public string mathType { get; set; }
        public string eng { get; set; }
        public string tam { get; set; }
        public string tamType { get; set; }
    }

    public class JsonSchools
    {
        public string schoolName { get; set; }
        public string schoolCode { get; set; }
    }

    public class JsonNst
    {
        public string ItemId { get; set; }
        public string SchoolYear { get; set; }
        public string SCode { get; set; }
        public string SName { get; set; }
        public string CCode { get; set; }
        public string CName { get; set; }

        public Nst Nst_1 { get; set; }
        public Nst Nst_2 { get; set; }

    }

    public class Nst
    {
        public string Unit { get; set; }
        public string Won { get; set; }
        public string Acc { get; set; }
        public string Avg { get; set; }
        public string Grade { get; set; }
        public string Std { get; set; }
        public string RegAmt { get; set; }
    }

    public class JsonSat
    {
        public string SchoolYear { get; set; }
        public string TestYear { get; set; }
        public string TestDate { get; set; }
        public string GroupKey { get; set; }
        public string Tam { get; set; }

        public SatItem Kor { get; set; }
        public SatItem Mat { get; set; }
        public SatItem Eng { get; set; }
        public SatItem His { get; set; }
        public SatItem Tam1 { get; set; }
        public SatItem Tam2 { get; set; }
        public SatItem Lang2 { get; set; }
    }

    public class SatItem
    {
        public string PartName { get; set; }
        public string PartCode { get; set; }
        public string Won { get; set; }
        public string Per { get; set; }
        public string Std { get; set; }
        public string Gra { get; set; }
    }

    public class Sat
    {
        public string ItemId { get; set; }
        public string TestYear { get; set; }
        public string TestDate { get; set; }
        public string SchoolYear { get; set; }
        public string GroupKey { get; set; }

        public string PartNameKor { get; set; }
        public string PartCodeKor { get; set; }
        public double StdKor { get; set; }
        public double PerKor { get; set; }
        public double GraKor { get; set; }
        public double WonKor { get; set; }


        public string PartNameMat { get; set; }
        public string PartCodeMat { get; set; }
        public double StdMat { get; set; }
        public double PerMat { get; set; }
        public double GraMat { get; set; }
        public double WonMat { get; set; }

        public string PartNameEng { get; set; }
        public string PartCodeEng { get; set; }
        public double StdEng { get; set; }
        public double PerEng { get; set; }
        public double GraEng { get; set; }
        public double WonEng { get; set; }

        public string PartNameTam { get; set; }
        public string PartCodeTam { get; set; }

        public string PartNameTam1 { get; set; }
        public string PartCodeTam1 { get; set; }
        public double StdTam1 { get; set; }
        public double PerTam1 { get; set; }
        public double GraTam1 { get; set; }
        public double WonTam1 { get; set; }

        public string PartNameTam2 { get; set; }
        public string PartCodeTam2 { get; set; }
        public double StdTam2 { get; set; }
        public double PerTam2 { get; set; }
        public double GraTam2 { get; set; }
        public double WonTam2 { get; set; }

        public string PartNameHis { get; set; }
        public string PartCodeHis { get; set; }
        public double StdHis { get; set; }
        public double PerHis { get; set; }
        public double GraHis { get; set; }
        public double WonHis { get; set; }

        public string PartNameLang2 { get; set; }
        public string PartCodeLang2 { get; set; }
        public double StdLang2 { get; set; }
        public double PerLang2 { get; set; }
        public double GraLang2 { get; set; }
        public double WonLang2 { get; set; }
    }

    
}
