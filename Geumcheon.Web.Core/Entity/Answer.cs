﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Core.Entity
{
    public class Answer
    {
        public int AnswerIdx { get; set; }
        public int MemberIdx { get; set; }
        public string AnswerJson { get; set; }
        public string WriterType { get; set; }

    }
}