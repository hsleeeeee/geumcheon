﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public partial class MobileBanner
    {
        [Key]
        public long Idx { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Body { get; set; }

        [StringLength(500)]
        public string Link { get; set; }

        [StringLength(20)]
        public string LinkTitle { get; set; }

        public bool IsUse { get; set; }

        public string Color1 { get; set; }

        public string Color2 { get; set; }

        public int NavIdx { get; set; }

        public int OrderIdx { get; set; }
    }
}
