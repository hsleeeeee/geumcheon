﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Entrance
    {
        DateTime ConsultDateStart { get; set; }
        DateTime ConsultDateEnd { get; set; }
        DateTime RequestDataStart { get; set; }
        DateTime RequestDataEnd { get; set; }
        public int EntranceIdx { get; set; }
        public string ConsultWay { get; set; }
        public string Title { get; set; }
        public string ConsultDt { get; set; }
        public string RequestDt { get; set; }

        public string DisplayInputType { get; set; } 
    }
    public class EntranceRequest
    {
        public int RequestIdx { get; set; }
        public int EIdx { get; set; }
        public int MemberIdx { get; set; }
        public string CompanyCode { get; set; }
        public string Progress { get; set; }
        public string ConsultWay { get; set; }
        public string Title { get; set; }
        public string StudentName { get; set; }
        public int StudentGender { get; set; }
        public string AreaName { get; set; }
        public string ScoreHtml { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public string ConfirmDate { get; set; }
        public string DeadlineDate { get; set; }
        public string Re_Note { get; set; }

        public string NeedValue1 { get; set; }
        public string NeedValue2 { get; set; }

        public string TeacherIdx { get; set; }
        public string TeacherName { get; set; }
        public int RowIdx { get; set; }
        public string ConsultWayName
        {
            get
            {
                string rVal = string.Empty;
                switch (ConsultWay)
                {
                    case "I": //Interview
                        {
                            rVal = "대면";
                            break;
                        }
                    case "V": //Video
                        {
                            rVal = "화상";
                            break;
                        }
                    case "O": //Online
                        {
                            rVal = "온라인";
                            break;
                        }
                }

                return rVal;
            }

        }

        public string ProgressName
        {
            get
            {
                string rVal = string.Empty;
                switch (Progress)
                {
                    case "R":
                        {
                            rVal = "승인대기";
                            break;
                        }
                    case "I":
                        {
                            rVal = "신청승인";
                            break;
                        }
                    case "C":
                        {
                            rVal = "상담완료";
                            break;
                        }
                    case "W":
                        {
                            rVal = "예약대기";
                            break;
                        }
                }
                return rVal;
            }
        }

        public Schedule Schedule { get; set; }

        public Reply Reply { get; set; }

        public Member member { get; set; }

        public string DisplayInputType { get; set; }
    }
}

