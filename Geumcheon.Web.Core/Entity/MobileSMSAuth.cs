﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public partial class MobileSMSAuth
    {
        [Key]
        [StringLength(255)]
        public string DeviceToken { get; set; }

        [StringLength(12)]
        public string PhoneNumber { get; set; }

        [StringLength(6)]
        public string Code { get; set; }

        public System.DateTime ExpireDate { get; set; }
    }
}
