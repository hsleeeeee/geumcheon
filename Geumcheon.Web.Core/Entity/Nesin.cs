﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Nesin
    {
        public int stu_id { get; set; }
        public double kor_grade { get; set; }
        public double math_grade { get; set; }
        public double eng_grade { get; set; }
        public double soc_grade { get; set; }
        public double sci_grade { get; set; }
        public double avrg { get; set; }
        public string scoreJson { get; set; }
    }


    public class ScorePer
    {
        public string GroupKey { get; set; }
        public string TestDate { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Date { get; set; }
        public string CssCode { get; set; }
        public string PartCode { get; set; }
        public string PartName { get; set; }
        public int Std { get; set; }
        public int Per { get; set; }
        public int Ori { get; set; }
        public int Grd { get; set; }
        public int IsReal { get; set; }
    }
}
