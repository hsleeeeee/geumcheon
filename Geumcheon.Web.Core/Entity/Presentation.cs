﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Presentation
    {
        public int Idx { get; set; }
        public string PresentationType { get; set; }
        public string PresentationName { get; set; }
        public string Address { get; set; }
        public string AddressURL { get; set; }
        public DateTime RequestStartDate { get; set; }
        public DateTime RequestEndDate { get; set; }
        public string PresentationDate { get; set; }
        public string PresentationDate_DP { get; set; }
        public int MaxCount { get; set; }
        public int BufferCount { get; set; }
        public string DownFile { get; set; }
    }

    public class PresentationRequest
    {
        public int RowIDx { get; set; }
        public int PtRequestIdx { get; set; }
        public int PtIdx { get; set; }
        public int MemberIdx { get; set; }
        public string State { get; set; }
        public string Name { get; set; }
        public string HP { get; set; }
        public string Eamil { get; set; }
        public string Email { get; set; }
        public string Area { get; set; }
        public string School { get; set; }
        public int Grade { get; set; }
        public string Memo { get; set; }
        public string PresentationDate_DP { get; set; }
        public string AccountType { get; set; }
        public DateTime CreateDate { get; set; }

        
        public string Address { get; set; }
        public string PresentationType { get; set; }
        public string PresentationName { get; set; }
        public string PresentationDate { get; set; }

        public string PresentationTypeDP
        {
            get
            {
                string rVal = string.Empty;
                switch(PresentationType)
                {
                    case "U":
                        {
                            rVal = "대입설명회";
                            break;
                        }
                    case "E":
                        {
                            rVal = "입학사정관특강";
                            break;
                        }
                    case "A":
                        {
                            rVal = "학부모아카데미";
                            break;
                        }
                }
                return rVal;
            }
        }



        public string StateDP
        {
            get
            {
                string rVal = string.Empty;
                switch (State)
                {
                    case "R":
                        {
                            rVal = "신청";
                            break;
                        }
                    case "I":
                        {
                            rVal = "신청승인";
                            break;
                        }
                    case "C":
                        {
                            rVal = "상담완료";
                            break;
                        }
                    case "W":
                        {
                            rVal = "예약";
                            break;
                        }
                }

                return rVal;
            }
        }
        public string jCreateDate
        {
            get
            {
                return CreateDate.ToString();
            }
        }
    }


}
