﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Compay
    {
        public string CompayCode { get; set; }
        public string CompayName { get; set; }
    }
}
