﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class ScheduleTime
    {
        public int ConsultTimeIdx { get; set; }
        public int ConsultDateIdx { get; set; }
        public string ConsultTime { get; set; }
        public int Cnt { get; set; }
    }
}
