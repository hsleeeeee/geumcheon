﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Member
    {
        public int Idx { get; set; }

        public string ID { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Hp { get; set; }

        public string Hp_S { get; set; }

        public string Hp_P { get; set; }

        public int? Gender { get; set; }

        public string School { get; set; }

        public string SchoolType { get; set; }

        public string SchoolTypeName
        {            
            get
            {
                return SchoolType == "M" ? "중등" : "고등";
            }
        }

        public int Grade { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public string AccountType { get; set; }

        public string Address { get; set; }

        public bool IsUse { get; set; }

        public bool IsDelete { get; set; }

        public List<ApplyUniversity> univ { get; set; }

        public int JoinCase { get; set; }

        public string JoinCaseEtc { get; set; }

        public bool ReceiveNotification { get; set; }

        public List<EntranceRequest> EntranceRequestItem { get; set; }
    }
}
