﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Teacher
    {
        public int TeacherIdx { get; set; }
        public string LoginId { get; set; }
        public string TeacherName { get; set; }
        public int IsOnlineConsult { get; set; }
    }
}
