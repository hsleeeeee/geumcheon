﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Core.Entity
{
    public class MobilePushLog
    {
        public long idx { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime SendTime { get; set; }
    }
}