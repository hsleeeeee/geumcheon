﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Core.Entity
{
    public class MobileSetting
    {
        
        public string ID { get; set; }
        public bool ReceiveNotification { get; set; }
        public bool ExceptionTime { get; set; }
        public TimeSpan ExceptionTimeStart { get; set; }
        public TimeSpan ExceptionTimeEnd { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }

        public string ConsultWay { get; set; }
    }
}