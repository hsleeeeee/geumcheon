﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class Files
    {
        public string MemberIdx { get; set; }
                
        public string FileType { get; set; }

        public string FileID { get; set; }

        public string FileName { get; set; }

        public string FileSize { get; set; }

        public string FilePath { get; set; }

        public string SchCode { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? DeleteDate { get; set; }


    }
}
