﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Geumcheon.Web.Core.Entity
{
    public class MobileLogin
    {
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string ID { get; set; }
        public string RefreshKey { get; set; }
        public string RefreshToken { get; set; }
        public DateTime LoginDate { get; set; }
    }
}