﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class ScheduleDate
    {
        public int ConsultDateIdx { get; set; }
        public string ConsultDate { get; set; }
    }
}
