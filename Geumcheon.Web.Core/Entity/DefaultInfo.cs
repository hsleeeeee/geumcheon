﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class DefaultInfo
    {
        public List<ApplyUniversity> ApplyUniversityItem { get; set; }
        public Answer Answer { get; set; }

        public DataTable tbUnivesity { get; set; }

    }
}
