﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Entity
{
    public class CSO6
    {
        public List<CS06_NESIN> CS06_NESINs { get; set; }
        public List<CS06_SCORE> CS06_SCOREs { get; set; }
        public List<CS06_STU_LIST> CS06_STU_LISTs { get; set; }

        public List<AnswerItem> Answer_S { get; set; }
        public List<AnswerItem> Answer_P { get; set; }

        public List<ApplyUniversity> UniverSityItem_S { get; set; }
        public List<ApplyUniversity> UniverSityItem_P { get; set; }
    }

    public class CS06_NESIN
    {
        public string stu_id { get; set; }
        public string stu_use_year { get; set; }
        public string kor_grade { get; set; }
        public string math_grade { get; set; }
        public string eng_grade { get; set; }
        public string soc_grade { get; set; }
        public string sci_grade { get; set; }        
    }

    public class CS06_SCORE
    {
        public string stu_id { get; set; }
        public string test_date { get; set; }
        public string unit_code { get; set; }

        public string kor_code { get; set; }
        public string ori_kor { get; set; }
        public string std_kor { get; set; }
        public string per_kor { get; set; }
        public string gra_kor { get; set; }

        public string mat_code { get; set; }
        public string mat_check { get; set; }
        public string mat_name { get; set; }
        public string ori_mat { get; set; }
        public string std_mat { get; set; }
        public string per_mat { get; set; }
        public string gra_mat { get; set; }

        public string eng_code { get; set; }
        public string ori_eng { get; set; }
        public string std_eng { get; set; }
        public string per_eng { get; set; }
        public string gra_eng { get; set; }
        public string sech_check { get; set; }
        public string sech1_code { get; set; }
        public string sech1_name { get; set; }
        public string ori_sech1 { get; set; }
        public string std_sech1 { get; set; }
        public string per_sech1 { get; set; }
        public string gra_sech1 { get; set; }
        public string sech2_code { get; set; }
        public string sech2_name { get; set; }
        public string ori_sech2 { get; set; }
        public string std_sech2 { get; set; }
        public string per_sech2 { get; set; }
        public string gra_sech2 { get; set; }
        public string sech3_code { get; set; }
        public string sech3_name { get; set; }
        public string ori_sech3 { get; set; }
        public string std_sech3 { get; set; }
        public string per_sech3 { get; set; }
        public string gra_sech3 { get; set; }
        public string sech4_code { get; set; }
        public string sech4_name { get; set; }
        public string ori_sech4 { get; set; }
        public string std_sech4 { get; set; }
        public string per_sech4 { get; set; }
        public string gra_sech4 { get; set; }
        public string reg_date { get; set; }
        public string upd_date { get; set; }
        public string upd_cnt { get; set; }
        public string lang2_code { get; set; }
        public string lang2_name { get; set; }
        public string ori_lang2 { get; set; }
        public string std_lang2 { get; set; }
        public string per_lang2 { get; set; }
        public string gra_lang2 { get; set; }
        public string sta_lang2 { get; set; }
        public string stu_use_year { get; set; }
        public string sco_hack { get; set; }
        public string result_type { get; set; }


    }

    public class CS06_STU_LIST
    {
        public string stu_id { get; set; }
        public string stu_name { get; set;}
        public string cur_hak { get; set; }
        public string cur_ban { get; set; }

    }

    public class AnswerItem
    {
        public string ItemNo { get; set; }
        public string ItemName { get; set; }
        public string MemberIdx { get; set; }
        public string WriterType { get; set; }
        public string Answer { get; set; }
    }
}


                                                                                              
