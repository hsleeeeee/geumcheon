﻿using Dapper;
using Geumcheon.Web.Core.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Repository
{
    public class PresentationRepository : BaseRepository
    {
        #region Singleton Instance

        private static readonly Lazy<PresentationRepository> _instance = new Lazy<PresentationRepository>(() => new PresentationRepository());

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static PresentationRepository Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #endregion

        public string PresentationRequestModify(PresentationRequest entity)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@PtRequestIdx", entity.PtRequestIdx);
                parameters.Add("@PtIdx", entity.PtIdx);
                parameters.Add("@MemberIdx", entity.MemberIdx);
                parameters.Add("@Name", entity.Name);
                parameters.Add("@HP", entity.HP);
                parameters.Add("@Email", entity.Email);
                parameters.Add("@Area", entity.Area);
                parameters.Add("@School", entity.School);
                parameters.Add("@Grade", entity.Grade);
                parameters.Add("@Memo", entity.Memo);                
                parameters.Add("@Result", dbType: System.Data.DbType.String, size: 20, direction: ParameterDirection.Output);
                dbConnection.ExecuteScalar("dbo.sp_Consult_Presentation_Request_Modify", parameters, commandType: CommandType.StoredProcedure);
                string rVal = parameters.Get<string>("@Result");

                dbConnection.Close();

                return rVal;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public string PresentationRequestModifyAnonymouse(PresentationRequest entity)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@PtRequestIdx", entity.PtRequestIdx);
                parameters.Add("@PtIdx", entity.PtIdx);
                parameters.Add("@MemberIdx", entity.MemberIdx);
                parameters.Add("@Name", entity.Name);
                parameters.Add("@HP", entity.HP);
                parameters.Add("@Email", entity.Email);
                parameters.Add("@Memo", entity.Memo);
                parameters.Add("@AccountType", entity.AccountType);
                parameters.Add("@Result", dbType: System.Data.DbType.String, size: 20, direction: ParameterDirection.Output);
                dbConnection.ExecuteScalar("dbo.sp_Consult_Presentation_Request_Modify_Anoymouse", parameters, commandType: CommandType.StoredProcedure);
                string rVal = parameters.Get<string>("@Result");

                dbConnection.Close();

                return rVal;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int PresentationRequestSelect(int ptIdx)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@PtIdx", ptIdx);
                int ret = dbConnection.Query<int>("dbo.sp_Consult_Presentation_Request_Select", parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                dbConnection.Close();

                return ret;
            }
            catch
            {
                return -1;
            }
        }

        public Presentation PresentationSelectItem(int ptIdx)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PtIdx", ptIdx);
                var ret = dbConnection.Query<Presentation>("dbo.sp_Consult_Presentation_Select", parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                dbConnection.Close();

                return ret;
            }
            catch
            {
                return null;
            }
           
        }

        public List<Presentation> PresentationSelectList(string way)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@Way", way);
                var ret = dbConnection.Query<Presentation>("dbo.sp_Consult_Presentation_SelectList", parameters, commandType: CommandType.StoredProcedure);

                dbConnection.Close();

                return ret.ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<PresentationRequest> PresentationRequestSelectList(int mIdx, string way)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@MIdx", mIdx);
                parameters.Add("@Way", way);
                var ret = dbConnection.Query<PresentationRequest>("dbo.sp_Consult_Presentation_Request_SelectList", parameters, commandType: CommandType.StoredProcedure);

                dbConnection.Close();

                return ret.ToList();
            }
            catch
            {
                return null;
            }
        }

        public List<PresentationRequest> PresentationRequestSelectByPtIdx(int mIdx, int ptIdx, int ptRequestIdx = 0)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@MIdx", mIdx);
                parameters.Add("@PtIdx", ptIdx);
                parameters.Add("@PtRequestIdx", ptRequestIdx);
                var ret = dbConnection.Query<PresentationRequest>("dbo.sp_Consult_Presentation_Request_Select_By_PtIdx", parameters, commandType: CommandType.StoredProcedure).ToList();

                dbConnection.Close();

                return ret;
            }
            catch
            {
                return null;
            }
        }

        public List<Member> PresentationRequestSelectForSendInfo(int ptIdx)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@PtIdx", ptIdx);
                var ret = dbConnection.Query<Member>("dbo.sp_Consult_Presentation_Request_Select_For_SendInfo", parameters, commandType: CommandType.StoredProcedure);
                dbConnection.Close();
                return ret.ToList();
            }
            catch
            {
                return null;
            }
        }


        public int PresentationRequestDelete(int idx)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@PtRequestIdx", idx);

                var ret = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Presentation_Request_Delete", parameters, commandType: CommandType.StoredProcedure);

                dbConnection.Close();

                return 1;
            }
            catch
            {
                return -1;
            }
        }

        public Presentation CheckBatchDown()
        {
            try
            {

                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                var item = dbConnection.Query<Presentation>("dbo.sp_Batch_Down", parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();


                dbConnection.Close();

                return item;
            }
            catch (Exception ex)
            {
                return null;
            }           
        }

        public int CHECK_POPUP()
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                dbConnection.ExecuteScalar("dbo.SP_CHECK_POPUP", parameters, commandType: CommandType.StoredProcedure);
                int rVal = parameters.Get<int>("@Result");

                dbConnection.Close();

                return rVal;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
