﻿using Dapper;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Geumcheon.Web.Core.Repository
{
    public class ManagementRepository:BaseRepository
    {
        #region Singleton Instance

        private static readonly Lazy<ManagementRepository> _instance = new Lazy<ManagementRepository>(() => new ManagementRepository());

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static ManagementRepository Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #endregion


        public SheduleTable GetScheduleTimeAdmin(string companyCode, string consultDate, string consultWay)
        {
            try
            {
                SheduleTable schedule = new SheduleTable();
                var parameters = new DynamicParameters();
                parameters.Add("@CompanyCode", companyCode);
                parameters.Add("@ConsultDate", consultDate);

                var result = dbConnection.QueryMultiple("dbo.sp_Consult_Schedule_Time_For_Admin", parameters, commandType: CommandType.StoredProcedure);

                schedule.lstScheduleDate =  result.Read<ScheduleDate>().ToList();
                schedule.lstScheduleTime = result.Read<ScheduleTime>().ToList();

                return schedule;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }
    }
}
