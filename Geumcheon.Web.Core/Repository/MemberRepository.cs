﻿using Dapper;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Geumcheon.Web.Core.Repository
{
    public class MemberRepository : BaseRepository
    {
        #region Singleton Instance

        private static readonly Lazy<MemberRepository> _instance = new Lazy<MemberRepository>(() => new MemberRepository());

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static MemberRepository Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #endregion

        /// <summary>
        /// Member 조회
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Member> MemberSelect(string id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);
            var result = dbConnection.Query<Member>("dbo.sp_Consult_Member_Select", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result.ToList();
        }

        /// <summary>
        /// ID 조회
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Member MemberSelectItem(string id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);
            var result = dbConnection.QueryMultiple("dbo.sp_Consult_Member_SelectItem", parameters, commandType: CommandType.StoredProcedure);

            try
            {
                var rtn = result.Read<Member>().FirstOrDefault();
                rtn.univ = result.Read<ApplyUniversity>().ToList();
               
                return rtn;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnection.Close();
            }
        }


        /// <summary>
        /// ID 조회
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Member MemberSelectItemForConsult(string id, string requestIdx)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);
            parameters.Add("@RequestIdx", requestIdx);
            var result = dbConnection.QueryMultiple("dbo.sp_Consult_Member_SelectItem_For_Consult", parameters, commandType: CommandType.StoredProcedure);

            try
            {
                var rtn = result.Read<Member>().FirstOrDefault();
                rtn.univ = result.Read<ApplyUniversity>().ToList();
                rtn.EntranceRequestItem = result.Read<EntranceRequest>().ToList();

                return rtn;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnection.Close();
            }
        }

        public Member MemberIDCheck(string id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);
            var result = dbConnection.Query<Member>("dbo.SP_Consult_IDCheck", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result.FirstOrDefault();
        }

        /// <summary>
        /// 로그인
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        public Member MemberLogin(string id, string pwd)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);
            parameters.Add("@PWD", pwd);
            var result = dbConnection.Query<Member>("dbo.sp_Consult_Member_Login", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result.FirstOrDefault();
        }

        /// <summary>
        /// Member 페이지 조회
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<Member> MemberSelectPaging(ViewModelList<Member, SearchBase> model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@PageIndex", model.PagingInfo.CurrentPage);
            parameters.Add("@PageSize", model.PagingInfo.PageSize);
            parameters.Add("@SearchField", model.Search.SearchField);
            parameters.Add("@SearchText", model.Search.SearchText);
            parameters.Add("@TotalCount", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
            var result = dbConnection.Query<Member>("dbo.sp_Consult_Member_SelectPaging", parameters, commandType: CommandType.StoredProcedure);
            model.Entities = result.ToList();
            model.PagingInfo.TotalCount = parameters.Get<int>("TotalCount");

            dbConnection.Close();

            return result.ToList();
        }

        /// <summary>
        ///  Member 추가
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int MemberInsert(Member model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@Name", model.Name);
            parameters.Add("@Email", model.Email);
            parameters.Add("@Hp", model.Hp);
            parameters.Add("@School", model.School);
            parameters.Add("@Grade", model.Grade);
            parameters.Add("@Password", model.Password);
            parameters.Add("@AccountType", model.AccountType);
            parameters.Add("@ID", model.ID);
            parameters.Add("@Gender", model.Gender == 0 ? null : model.Gender);
            parameters.Add("@Address", model.Address);
            parameters.Add("@JoinCase", model.JoinCase);
            parameters.Add("@JoinCaseEtc", model.JoinCaseEtc);
            parameters.Add("@Hp_S", model.Hp_S);
            parameters.Add("@Hp_P", model.Hp_P);
            parameters.Add("@SchoolType", model.SchoolType);
            parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
            var result = dbConnection.Execute("dbo.sp_Consult_Member_Insert", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return parameters.Get<int>("Result");
        }

        /// <summary>
        /// Member 수정
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int MemberUpdate(Member model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", model.ID);
            parameters.Add("@Name", model.Name);
            parameters.Add("@Email", model.Email);
            parameters.Add("@Hp", model.Hp);
            parameters.Add("@Gender", model.Gender == 0 ? null : model.Gender);
            parameters.Add("@School", model.School);
            parameters.Add("@Grade", model.Grade);
            parameters.Add("@Password", model.Password);
            parameters.Add("@AccountType", model.AccountType);
            parameters.Add("@IsUse", model.IsUse);
            parameters.Add("@Address", model.Address);
            parameters.Add("@Hp_S", model.Hp_S);
            parameters.Add("@Hp_P", model.Hp_P);
            parameters.Add("@SchoolType", model.SchoolType);
            var result = dbConnection.Execute("dbo.sp_Consult_Member_Update", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result;
        }

        /// <summary>
        /// Member 삭제
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int MemberDelete(string id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);

            var result = dbConnection.Execute("dbo.sp_Consult_Member_Delete", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result;
        }

        /// <summary>
        /// 아이디 또는 패스워드 찾기
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Member MemberFind(Member model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", model.ID);
            parameters.Add("@HP", model.Hp);
            parameters.Add("@Name", model.Name);

            var result = dbConnection.Query<Member>("dbo.sp_Consult_Member_Find", parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
            dbConnection.Close();

            return result;
        }
    }
}
