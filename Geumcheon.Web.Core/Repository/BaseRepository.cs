﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Repository
{
    public class BaseRepository
    {
        protected readonly IDbConnection dbConnection = null;
        protected readonly string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        public BaseRepository()
        {
            dbConnection = new SqlConnection(connectionString);
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        }

        public BaseRepository(string connStr)
        {
            dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[connStr].ConnectionString);
            connectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString;
        }


        public void Dispose()
        {
            if (dbConnection != null)
            {
                dbConnection.Dispose();
            }
        }
    }
}
