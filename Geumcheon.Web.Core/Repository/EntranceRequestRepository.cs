﻿using Dapper;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Geumcheon.Web.Core.Repository
{
    public class EntranceRequestRepository :BaseRepository
    {
        #region Singleton Instance

        private static readonly Lazy<EntranceRequestRepository> _instance = new Lazy<EntranceRequestRepository>(() => new EntranceRequestRepository());

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static EntranceRequestRepository Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #endregion

        public string EntranceRequestUpsert(EntranceRequest entity, string schoolType)
        {
            int requestIdx = -1;
            string rValEntranceRequest = string.Empty;
            string progress = string.Empty;
            var parameters = new DynamicParameters();
            parameters.Add("@RequestIdx", entity.RequestIdx);
            parameters.Add("@MemberIdx", entity.MemberIdx);
            parameters.Add("@CompanyCode", entity.CompanyCode);
            parameters.Add("@Progress", entity.Progress);
            parameters.Add("@ConsultWay", entity.ConsultWay);
            parameters.Add("@Title", entity.Title);
            parameters.Add("@StudentName", entity.StudentName);
            parameters.Add("@StudentGender", entity.StudentGender);
            parameters.Add("@AreaName", entity.AreaName);
            parameters.Add("@EntranceIdx", entity.EIdx);
            parameters.Add("@ScoreHtml", entity.ScoreHtml);
            parameters.Add("@Re_Note", entity.Re_Note);
            parameters.Add("@NeedValue1", entity.NeedValue1);
            parameters.Add("@NeedValue2", entity.NeedValue2);
            parameters.Add("@Result", dbType: System.Data.DbType.String, size: 20, direction: ParameterDirection.Output);

            rValEntranceRequest = dbConnection.ExecuteScalar<string>("dbo.sp_Consult_Entrance_Request_Modify", parameters, commandType: CommandType.StoredProcedure);
            int ret = Convert.ToInt32(rValEntranceRequest.Split('/')[0]);

            rValEntranceRequest = parameters.Get<string>("@Result");

            if (entity.ConsultWay == "V")
            {
                requestIdx = Convert.ToInt32(rValEntranceRequest.Split('/')[0]);
                progress = rValEntranceRequest.Split('/')[1];
                if (ret > 0 && (entity.ConsultWay.Equals("I") || entity.ConsultWay.Equals("V")))
                {
                    parameters = new DynamicParameters();
                    parameters.Add("@RequestIdx", requestIdx);
                    parameters.Add("@ConsultTimeIdx", entity.Schedule.ConsultTimeIdx);
                    parameters.Add("@SchoolType", schoolType);
                    parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                    ret = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Schedule_Insert", parameters, commandType: CommandType.StoredProcedure);
                    ret = parameters.Get<int>("@Result");

                    if (ret < 0)
                    {
                        EntranceRequestRemove(requestIdx);
                    }
                }

                var parameters4 = new DynamicParameters();
                parameters4.Add("@CompanyCode", entity.CompanyCode);
                parameters4.Add("@RequestIdx", requestIdx);
                parameters4.Add("@ConsultWay", entity.ConsultWay);
                parameters4.Add("@ConsultTimeIdx", entity.Schedule.ConsultTimeIdx);
                parameters4.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                int ret2 = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Reply_Teacher_Auto_Insert", parameters4, commandType: CommandType.StoredProcedure);
                ret2 = parameters4.Get<int>("@Result");
                dbConnection.Close();
                if (ret2 > 0 && ret > 0)
                {
                    return requestIdx.ToString() + "/" + progress;
                }
                else
                {
                    return ret < 0 ? ret.ToString() + "/" + progress : ret2 + "/" + progress;
                }
            }
            else
            {
                return rValEntranceRequest;
            }        
 
    }
    public int EntranceRequestRemove(int idx)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@RequestIdx", idx);

            parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
            int ret = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Entrance_Request_Remove", parameters, commandType: CommandType.StoredProcedure);

            dbConnection.Close();
            return ret;
        }

    public int EntranceRequestChageSchedule(int requestIdx, int timeIdx, string schoolType)
    {
        var parameters = new DynamicParameters();
        parameters.Add("@RequestIdx", requestIdx);
        parameters.Add("@ConsultTimeIdx", timeIdx);
        parameters.Add("@SchoolType", schoolType);
        parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
        int ret = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Schedule_Insert", parameters, commandType: CommandType.StoredProcedure);
        ret = parameters.Get<int>("@Result");

        //if (ret < 0)
        //{
        //    EntranceRequestRemove(requestIdx);
        //}

        return ret;
    }

    public bool EntranceRequestSelectPaging(int idx, ViewModelList<EntranceRequest, SearchInfo> model)
        {
            try
            {
                List<EntranceRequest> lstApplication = new List<EntranceRequest>();
                var parameters = new DynamicParameters();
                parameters.Add("@MemberIdx", idx);
                parameters.Add("@CompanyCode", model.Search.CompanyCode);
                parameters.Add("@ConsultWay", model.Search.ConsultWay);
                parameters.Add("@Name", model.Search.Name);
                parameters.Add("@TeacherId", model.Search.TeacherId);
                parameters.Add("@currentPage", model.PagingInfo.CurrentPage);
                parameters.Add("@pageSize", model.PagingInfo.PageSize);
                parameters.Add("@orderBy", model.PagingInfo.OrderType);

                parameters.Add("@totalCount", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                //lstApplication = dbConnection.Query<Application>("dbo.sp_Consult_Select_By_ConsultType", parameters, commandType: CommandType.StoredProcedure).ToList();
                //return lstApplication;

                var rVal = dbConnection.Query<EntranceRequest, Member, Schedule, EntranceRequest>("dbo.sp_Consult_Select_By_MyConsultWay"
                , (ca, mem, csd) =>
                {

                    ca.member = mem;
                    ca.Schedule = csd;
                    return ca;
                }
                , param: parameters
                , commandType: CommandType.StoredProcedure
                , splitOn: "Name,ConsultDateIdx").ToList();
                model.Entities = rVal;
                model.PagingInfo.TotalCount = parameters.Get<int>("@totalCount");

                dbConnection.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public bool EntranceRequestSelectPaging_Teacher(int idx, ViewModelList<EntranceRequest, SearchInfo> model)
        {
            try
            {
                List<EntranceRequest> lstApplication = new List<EntranceRequest>();
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherIdx", idx);
                parameters.Add("@CompanyCode", model.Search.CompanyCode);
                parameters.Add("@ConsultWay", model.Search.ConsultWay);
                parameters.Add("@Name", model.Search.Name);
                parameters.Add("@TeacherId", model.Search.TeacherId);
                parameters.Add("@currentPage", model.PagingInfo.CurrentPage);
                parameters.Add("@pageSize", model.PagingInfo.PageSize);
                parameters.Add("@orderBy", model.PagingInfo.OrderType);

                parameters.Add("@totalCount", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                //lstApplication = dbConnection.Query<Application>("dbo.sp_Consult_Select_By_ConsultType", parameters, commandType: CommandType.StoredProcedure).ToList();
                //return lstApplication;

                var rVal = dbConnection.Query<EntranceRequest, Member, Schedule, EntranceRequest>("dbo.sp_Consult_Select_By_Teacher"
                , (ca, mem, csd) =>
                {

                    ca.member = mem;
                    ca.Schedule = csd;
                    return ca;
                }
                , param: parameters
                , commandType: CommandType.StoredProcedure
                , splitOn: "Idx,ConsultDateIdx").ToList();
                model.Entities = rVal;
                model.PagingInfo.TotalCount = parameters.Get<int>("@totalCount");

                dbConnection.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public EntranceRequest EntranceRequestSelectItem(int idx)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@RequestIdx", idx);

            var rVal = dbConnection.Query<EntranceRequest, Member, Schedule, Reply, EntranceRequest>("dbo.sp_Consult_Select_By_EntranceIdx"
                , (er, m, s, r) =>
                {
                    er.member = m;
                    er.Schedule = s;
                    er.Reply = r;
                    return er;
                }
                , param: parameters
                , commandType: CommandType.StoredProcedure
                , splitOn: "Idx,ConsultTimeIdx,ReplyNote");
            dbConnection.Close();

            return rVal.FirstOrDefault();
        }

        public int EntranceSelect(string consultWay, int eidx)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@ConsultWay", consultWay);
                parameters.Add("@EntranceIdx", eidx);

                var rVal = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Entrance_Select"

                    , param: parameters
                    , commandType: CommandType.StoredProcedure);
                dbConnection.Close();

                return rVal;
            }
            catch (Exception ex)
            {
                return -1;
            }
            finally
            {
                dbConnection.Close();
            }
        }


        #region 20200504 
        public bool GetListData(ViewModelList<EntranceRequest, SearchInfo> model)
        {
            try
            {
                List<EntranceRequest> lstApplication = new List<EntranceRequest>();
                var parameters = new DynamicParameters();
                
                parameters.Add("@currentPage", model.PagingInfo.CurrentPage);
                parameters.Add("@pageSize", model.PagingInfo.PageSize);
                parameters.Add("@orderBy", model.PagingInfo.OrderType);

                parameters.Add("@totalCount", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                //lstApplication = dbConnection.Query<Application>("dbo.sp_Consult_Select_By_ConsultType", parameters, commandType: CommandType.StoredProcedure).ToList();
                //return lstApplication;

                var rVal = dbConnection.Query<EntranceRequest, Member, Schedule, EntranceRequest>("dbo.sp_Consult_Select"
                , (ca, mem, csd) =>
                {
                    ca.member = mem;
                    ca.Schedule = csd;
                    return ca;
                }
                , param: parameters
                , commandType: CommandType.StoredProcedure
                , splitOn: "Name,ConsultDateIdx").ToList();
                model.Entities = rVal;
                model.PagingInfo.TotalCount = parameters.Get<int>("@totalCount");

                dbConnection.Close();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int RemoveEntranceRequest(int requestIdx)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@RequestIdx", requestIdx);                
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);

                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Entrance_Request_Remove", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }
        #endregion

        public Entrance EntranceInfoSelect(int entranceidx)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@EntranceIdx", entranceidx);

                var result = dbConnection.Query<Entrance>("dbo.sp_Consult_Entrance_Info_Select", parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();                

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnection.Close();
            }
        }

        public List<Entrance> EntranceSelectList(string way)
        {
            try
            {
                string rValEntranceRequest = string.Empty;
                string progress = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@Way", way);
                var ret = dbConnection.Query<Entrance>("dbo.sp_Consult_Entrance_SelectList", parameters, commandType: CommandType.StoredProcedure);

                return ret.ToList();
            }
            catch
            {
                return null;
            }
            finally
            {
                dbConnection.Close();
            }
        }
    }
}
