﻿using Dapper;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Geumcheon.Web.Core.Repository
{
    public class MyPageRepository :BaseRepository
    {
        #region Singleton Instance

        private static readonly Lazy<MyPageRepository> _instance = new Lazy<MyPageRepository>(() => new MyPageRepository());

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static MyPageRepository Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #endregion

        public int setDefaultInfo(DefaultInfo model)
        {
            try
            {
                int rVal = 0;
                var parameters = new DynamicParameters();

                parameters.Add("@MemberIdx", model.Answer.MemberIdx);
                parameters.Add("@WriterType", model.Answer.WriterType);
                parameters.Add("@AnswerJson", model.Answer.AnswerJson);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);

                dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Answer_Modify", parameters, commandType: CommandType.StoredProcedure);

                rVal = parameters.Get<int>("@Result");

                if (rVal > 0)
                {
                    var parameters2 = new DynamicParameters();

                    parameters2.Add("@MemberIdx", model.Answer.MemberIdx);
                    parameters2.Add("@WriterType", model.Answer.WriterType);
                    parameters2.Add("@tbUniversity", model.tbUnivesity.AsTableValuedParameter());
                    parameters2.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);

                    dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Apply_University_Modify", parameters2, commandType: CommandType.StoredProcedure);

                    rVal =  parameters.Get<int>("@Result");
                }

                return rVal;
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int SetAnswer(Answer model)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@MemberIdx", model.MemberIdx);
                parameters.Add("@WriterType", model.WriterType);
                parameters.Add("@AnswerJson", model.AnswerJson);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);

                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Answer_Modify", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int SetApplyUniversity(ApplyUniversity model)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@MemberIdx", model.MemberIdx);
                parameters.Add("@WriterType", model.WriterType);
                parameters.Add("@tbUniversity", model.tbUnivesity.AsTableValuedParameter());
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);

                int result = dbConnection.ExecuteScalar<int>("dbo.sp_Consult_Apply_University_Modify", parameters, commandType: CommandType.StoredProcedure);

                return parameters.Get<int>("@Result");
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public DefaultInfo GetDefaultInfo(int memberIdx, string writerType, bool isUnivItem = true)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@MemberIdx",memberIdx);
                parameters.Add("@WriterType", writerType);
                parameters.Add("@IsUnivItem", isUnivItem);

                var result = dbConnection.QueryMultiple("dbo.sp_Consult_Answer_Select", parameters, commandType: CommandType.StoredProcedure);

                DefaultInfo defaultInfo = new DefaultInfo();


                defaultInfo.Answer = result.Read<Answer>().SingleOrDefault();
                defaultInfo.ApplyUniversityItem = result.Read<ApplyUniversity>().ToList();


                return defaultInfo;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }


        public int SetNesin(Nesin model)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@stu_id", model.stu_id);
                parameters.Add("@kor_grade", model.kor_grade);
                parameters.Add("@math_grade", model.math_grade);
                parameters.Add("@eng_grade", model.eng_grade);
                parameters.Add("@soc_grade", model.soc_grade);
                parameters.Add("@sci_grade", model.sci_grade);
                parameters.Add("@scoreJson", model.scoreJson);
                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                dbConnection.ExecuteScalar<int>("dbo.sp_CS06_NESIN_Modify", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("@Result");
                
            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }            
        }

        public Nesin GetNesin(int stuId)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@stu_id", stuId);

                var result = dbConnection.Query<Nesin>("dbo.sp_CS06_NESIN_Select", parameters, commandType: CommandType.StoredProcedure).SingleOrDefault(); 
                return result ;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public List<ScorePer>GetScorePer_PartCode(string groupKey, int grade, string cssCode)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupKey", groupKey);
                parameters.Add("@Grade", grade);
                parameters.Add("@CssCode", cssCode);

                var result = dbConnection.Query<ScorePer>("dbo.sp_CS06_SCORE_PER_Select_PartCode", parameters, commandType: CommandType.StoredProcedure).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public ScorePer GetScorePer_Score(string groupKey, string cssCode, int std)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@GroupKey", groupKey);
                parameters.Add("@CssCode", cssCode);
                parameters.Add("@Std", std);

                var result = dbConnection.Query<ScorePer>("dbo.sp_CS06_SCORE_PER_Select_Score", parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public List<ScorePer> GetScorePer_TestDate(int grade)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Grade", grade);


                var result = dbConnection.Query<ScorePer>("dbo.sp_CS06_SCORE_PER_Select_TestDate", parameters, commandType: CommandType.StoredProcedure).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }



        public List<Sat> GetSat(string stuId, string grade=null, string groupKey=null)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@StuId", stuId);
                parameters.Add("@Grade", grade);
                parameters.Add("@GroupKey", groupKey);
                var result = dbConnection.Query<Sat>("dbo.sp_CS06_SCORE_Select", parameters, commandType: CommandType.StoredProcedure).ToList();
                return result;
            }
            catch
            {
                dbConnection.Close();
                return null;
            }
        }


        public SatItem ConverStdToPer(string groupKey, string partCode, string grade, string std)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@GroupKey", groupKey);
                parameters.Add("@PartCode", partCode);
                parameters.Add("@Grade", grade);
                parameters.Add("@Std", std);

                var result = dbConnection.Query<SatItem>("dbo.sp_CS06_SCORE_PER_Select_Score", parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return result;
            }
            catch
            {
                dbConnection.Close();
                return null;
            }
        }

        public SatItem ConverOriToStd(string groupKey, string partCode, string grade, string ori)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@GroupKey", groupKey);
                parameters.Add("@PartCode", partCode);
                parameters.Add("@Grade", grade);
                parameters.Add("@Ori", ori);

                var result = dbConnection.Query<SatItem>("dbo.sp_CS06_SCORE_PER_Select_Score_By_Ori", parameters, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return result;
            }
            catch
            {
                dbConnection.Close();
                return null;
            }
        }


        public int SetScore(Sat sat, int grade, int stuid, string groupKey)
        {
            try
            {
                var parameters = new DynamicParameters();

                parameters.Add("@sco_hack", grade);
                parameters.Add("@stu_id", stuid);
                parameters.Add("@test_date", groupKey);

                parameters.Add("@kor_code", "001");
                parameters.Add("@std_kor", sat.StdKor);
                parameters.Add("@per_kor", sat.PerKor);
                parameters.Add("@gra_kor", sat.GraKor);
                parameters.Add("@ori_kor", sat.WonKor);

                parameters.Add("@mat_code", sat.PartCodeMat);
                parameters.Add("@std_mat", sat.StdMat);
                parameters.Add("@per_mat", sat.PerMat);
                parameters.Add("@gra_mat", sat.GraMat);
                parameters.Add("@ori_mat", sat.WonMat);

                parameters.Add("@eng_code", sat.PartCodeEng);
                parameters.Add("@std_eng", 0);
                parameters.Add("@per_eng", 0);
                parameters.Add("@gra_eng", sat.GraEng);
                parameters.Add("@ori_eng", sat.WonEng);

                parameters.Add("@sech1_code", sat.PartCodeTam1);
                parameters.Add("@std_sech1", sat.StdTam1);
                parameters.Add("@per_sech1", sat.PerTam1);
                parameters.Add("@gra_sech1", sat.GraTam1);
                parameters.Add("@ori_sech1", sat.WonTam1);

                parameters.Add("@sech2_code", sat.PartCodeTam2);
                parameters.Add("@std_sech2", sat.StdTam2);
                parameters.Add("@per_sech2", sat.PerTam2);
                parameters.Add("@gra_sech2", sat.GraTam2);
                parameters.Add("@ori_sech2", sat.WonTam2);

                parameters.Add("@sech4_code", sat.PartCodeHis);
                parameters.Add("@std_sech4", 0);
                parameters.Add("@per_sech4", 0);
                parameters.Add("@gra_sech4", sat.GraHis);
                parameters.Add("@ori_sech4", sat.WonHis);

                parameters.Add("@lang2_code", sat.PartCodeLang2);
                parameters.Add("@std_lang2", sat.StdLang2);
                parameters.Add("@per_lang2", sat.PerLang2);
                parameters.Add("@gra_lang2", sat.GraLang2);
                parameters.Add("@ori_lang2", sat.WonLang2);



                parameters.Add("@Result", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
                dbConnection.ExecuteScalar<int>("dbo.sp_CS06_SCORE_Modify", parameters, commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("@Result");

            }
            catch (Exception ex)
            {
                return -1;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public string GetMoiInfo(int stuId)
        {
            try
            {
                string rVal = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@StuId", stuId);                

               rVal = dbConnection.ExecuteScalar<string>("dbo.sp_CS06_SCORE_Select_Info", parameters, commandType: CommandType.StoredProcedure);
                return rVal;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }

        public int FilesUpsert(Files entity)
        {
            int rVal = 0;
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MemberIdx", entity.MemberIdx);
                parameters.Add("@FileType", entity.FileType);
                parameters.Add("@FileID", entity.FileID);
                parameters.Add("@FileName", entity.FileName);
                parameters.Add("@FileSize", entity.FileSize);
                parameters.Add("@Result", rVal, DbType.Int32, ParameterDirection.Output);


                dbConnection.Execute("sp_Consult_File_Insert", param: parameters, commandType: CommandType.StoredProcedure);
                rVal = parameters.Get<int>("@Result");
                return rVal;
            }
            catch (Exception ex)
            {
                return rVal;
            }
            finally
            {
                dbConnection.Close();
            }
        }

        
        public string GetFileSaveName(string fileId)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@FileId", fileId);

                string fileName = dbConnection.ExecuteScalar<string>("sp_Consult_File_Select_By_FileId", param: parameters, commandType: CommandType.StoredProcedure);
                return fileName;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnection.Close();
            }
        }

        public CSO6 GetCS06_ForExcel(string teacherIdx, string s, string e)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("@TeacherIdx", teacherIdx);
                parameters.Add("@StartDate", s);
                parameters.Add("@EndDate", e);

                var result = dbConnection.QueryMultiple("dbo.sp_CS06_SCORE_Select_For_Tong", parameters, commandType: CommandType.StoredProcedure);

                CSO6 cs06 = new CSO6();

                cs06.CS06_SCOREs = result.Read<CS06_SCORE>().ToList();
                cs06.CS06_NESINs = result.Read<CS06_NESIN>().ToList();
                cs06.CS06_STU_LISTs = result.Read<CS06_STU_LIST>().ToList();
                cs06.Answer_P = result.Read<AnswerItem>().ToList();
                cs06.Answer_S = result.Read<AnswerItem>().ToList();

                cs06.UniverSityItem_P = result.Read<ApplyUniversity>().ToList();
                cs06.UniverSityItem_S = result.Read<ApplyUniversity>().ToList();


                return cs06;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }


        public string GetReportLinkCode(string orgIdx, string memberIdx)
        {
            try
            {
                string rVal = string.Empty;
                var parameters = new DynamicParameters();
                parameters.Add("@OrgIdx", orgIdx);
                parameters.Add("@MemberIdx", memberIdx);

                rVal = dbConnection.ExecuteScalar<string>("dbo.sp_Consult_SmartTest_Select", parameters, commandType: CommandType.StoredProcedure);
                return rVal;
            }
            catch (Exception ex)
            {
                return null;
            }

            finally
            {
                dbConnection.Close();
            }
        }
        public int FileInsert(List<Files> insertList, Files updateList)
        {
            int rVal = 0;
            try
            {
                DynamicParameters parameters2 = new DynamicParameters();
                parameters2.Add("@MemberIdx", updateList.MemberIdx);                
                parameters2.Add("@fileID", updateList.FileID);                
                parameters2.Add("@Result", rVal, DbType.Int32, ParameterDirection.Output);
                dbConnection.Execute("dbo.sp_Consult_File_Delete"
                        , parameters2
                        , commandType: CommandType.StoredProcedure
                    );

                foreach (var file in insertList)
                {
                    DynamicParameters parameters = new DynamicParameters();

                    parameters.Add("@MemberIdx", file.MemberIdx);
                    parameters.Add("@FileType", file.FileType);
                    parameters.Add("@FileID", file.FileID);
                    parameters.Add("@FileName", file.FileName);
                    parameters.Add("@FileSize", file.FileSize);                    
                    parameters.Add("@Result", rVal, DbType.Int32, ParameterDirection.Output);

                    dbConnection.Execute("dbo.sp_Consult_File_Insert"
                            , parameters
                            , commandType: CommandType.StoredProcedure
                        );

                    rVal =  parameters.Get<int>("@Result");
                }

                return rVal;
            }
            catch (Exception e)
            {
                return -1;

            }
            finally
            {
                dbConnection.Close();
            }
        }

        public List<Files> GetFiles(string memberIdx)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MemberIdx", memberIdx);

                var rVal = dbConnection.Query<Files>("dbo.sp_Consult_File_Select", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                return rVal;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnection.Close();
            }
        }


        public string GetConsultSmartCoaching(string memberIdx)
        {
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@MemberIdx", memberIdx);

                string fileName = dbConnection.ExecuteScalar<string>("sp_Consult_SmartCoaching_Select", param: parameters, commandType: CommandType.StoredProcedure);
                return fileName;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                dbConnection.Close();
            }
        }

    }
    
}
