﻿using Dapper;
using Geumcheon.Web.Core.Entity;
using Geumcheon.Web.Core.ViewModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.Repository
{
    public class MobileRepository :BaseRepository
    {
        #region Singleton Instance

        private static readonly Lazy<MobileRepository> _instance = new Lazy<MobileRepository>(() => new MobileRepository());

        /// <summary>
        /// Sington Instance
        /// </summary>
        public static MobileRepository Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        #endregion

        /// <summary>
        /// 모바일 로그인 정보 갱신
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int MobileLoginUpsert(MobileLogin model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@DeviceToken", model.DeviceToken);
            parameters.Add("@DeviceType", model.DeviceType);
            parameters.Add("@ID", model.ID);
            parameters.Add("@RefreshToken", model.RefreshToken);
            parameters.Add("@Refreshkey", model.RefreshKey);            
            var result = dbConnection.Execute("dbo.SP_Consult_M_Login_Upsert", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result;
        }

        /// <summary>
        /// 모바일 로그인 인증 체크
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public MobileLogin MobileLoginRefreshToken(string token)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@RefreshToken", token);
            var result = dbConnection.Query<MobileLogin>("dbo.SP_Consult_M_Select_RefreshToken", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result.FirstOrDefault();
        }

        /// <summary>
        /// 모바일 설정 갱신
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int MobileSettingUpsert(MobileSetting model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", model.ID);
            parameters.Add("@ReceiveNotification", model.ReceiveNotification);
            parameters.Add("@ExceptionTime", model.ExceptionTime);
            parameters.Add("@ExceptionTimeStart", model.ExceptionTimeStart);
            parameters.Add("@ExceptionTimeEnd", model.ExceptionTimeEnd);
            var result = dbConnection.Execute("dbo.SP_Consult_M_Setting_Upsert", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result;
        }

        /// <summary>
        /// 모바일 설정 가져오기
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<MobileSetting> MobileSettingSelect(string id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", id);
            var result = dbConnection.Query<MobileSetting>("dbo.SP_Consult_M_Setting_Select", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result.ToList();
        }

        /// <summary>
        ///  푸시 발송 내역 입력
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int MobilePushLogInsert(MobilePushLog model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ID", model.ID);
            parameters.Add("@Name", model.Name);
            parameters.Add("@Title", model.Title);
            parameters.Add("@Body", model.Body);
            var result = dbConnection.Execute("dbo.SP_Consult_M_PushLog_Insert", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result;
        }

        /// <summary>
        /// 푸시 발송 내역 아이템 확인
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        public MobilePushLog MobilePushLogSelectItem(long idx)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@idx", idx);
            var result = dbConnection.Query<MobilePushLog>("dbo.SP_Consult_M_PushLog_SelectItem", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result.FirstOrDefault();
        }

        /// <summary>
        /// 푸시 발송 내역 조회
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<MobilePushLog> MobilePushLogList(ViewModelList<MobilePushLog, SearchBase> model)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@PageIndex", model.PagingInfo.CurrentPage);
            parameters.Add("@PageSize", model.PagingInfo.PageSize);
            parameters.Add("@SearchField", model.Search.SearchField);
            parameters.Add("@SearchText", model.Search.SearchText);
            parameters.Add("@TotalCount", dbType: System.Data.DbType.Int32, direction: ParameterDirection.Output);
            var result = dbConnection.Query<MobilePushLog>("dbo.SP_Consult_M_PushLog_SelectItem", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return result.ToList();
        }

        /// <summary>
        /// SMS 발송
        /// </summary>
        /// <param name="authType"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public string MobileSMSAuth(string authType, MobileSMSAuth model)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@type", authType);
            parameters.Add("@deviceid", model.DeviceToken);
            parameters.Add("@phonenumber", model.PhoneNumber);
            parameters.Add("@smscode", model.Code);
            parameters.Add("@rtnValue", dbType: DbType.String, direction: ParameterDirection.ReturnValue);

            var ret = dbConnection.QueryFirstOrDefault<string>("dbo.SP_Consult_M_SMSAUTH", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return ret;
        }

        public int MobileLoginTokenUpsert(MobileLogin model, string newToken)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@DeviceToken", model.DeviceToken);
            parameters.Add("@DeviceType", model.DeviceType);
            parameters.Add("@userid", model.ID);
            parameters.Add("@RefreshToken", newToken);

            var ret = dbConnection.Execute("dbo.SP_Consult_M_SMSAUTH", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return ret;
        }

        public bool MobilePushSend(int requestIdx, string title = null, string body = null)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@Idx", requestIdx);

            var ret = dbConnection.Query<MobileSetting>("dbo.SP_Consult_M_Setting_Select_RequestIdx", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            TimeSpan currentTime = DateTime.Now.TimeOfDay;

            bool pushSuccess = false;
            string errorMsg = string.Empty;

            List<string> devices_Android = new List<string>();
            List<string> devices_iPhone = new List<string>();

            foreach (var login in ret)
            {
                if (login.ReceiveNotification && (!login.ExceptionTime || (currentTime <= login.ExceptionTimeStart || currentTime >= login.ExceptionTimeEnd)))
                {
                    if (login.DeviceType == "A")
                    {
                        devices_Android.Add(login.DeviceToken);
                    }
                    else if (login.DeviceType == "I")
                    {
                        devices_iPhone.Add(login.DeviceToken);
                    }
                }

            }

            if (devices_Android.Count == 0 && devices_iPhone.Count == 0)
            {
                return true;
            }

            string exdata = string.Format("{0};{1}", (ret.FirstOrDefault()?.ConsultWay.ToUpper() == "O" ? "online" : ret.FirstOrDefault()?.ConsultWay.ToUpper() == "V" ? "video" : "interview"), requestIdx);
            if (string.IsNullOrEmpty(title))
            {
                if (ret.FirstOrDefault()?.ConsultWay.ToUpper() == "O")
                {
                    title = "온라인 상담 알림";
                }
                else if (ret.FirstOrDefault()?.ConsultWay.ToUpper() == "I")
                {
                    title = "대면 상담 알림";
                }
                else if (ret.FirstOrDefault()?.ConsultWay.ToUpper() == "V")
                {
                    title = "화상(비대면) 상담 알림";
                }
            }
            if (string.IsNullOrEmpty(body))
            {
                if (ret.FirstOrDefault()?.ConsultWay.ToUpper() == "O")
                {
                    body = "온라인 상담 답변이 완료되었습니다.";
                }
                else if (ret.FirstOrDefault()?.ConsultWay.ToUpper() == "I")
                {
                    body = "대면 상담 보고서 내용이 완료되었습니다.";
                }
                else if (ret.FirstOrDefault()?.ConsultWay.ToUpper() == "V")
                {
                    body = "화상(비대면) 상담 보고서 내용이 완료되었습니다.";
                }
            }

            // 안드로이드 발송
            if (devices_Android.Count > 0)
            {
                try
                {
                    int curLength = 0;

                    while (curLength < devices_Android.Count)
                    {
                        List<string> get_ids;

                        if (devices_Android.Count - (curLength + 1000) > 0)
                        {
                            get_ids = devices_Android.GetRange(curLength, curLength + 1000);
                            curLength += 1000;
                        }
                        else
                        {
                            get_ids = devices_Android.GetRange(curLength, devices_Android.Count);
                            curLength = devices_Android.Count;
                        }

                        string data = string.Empty;
                        var jsonData = new
                        {
                            registration_ids = get_ids.ToArray(),
                            notification = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                            },
                            data = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                            }
                        };
                        //data = serializer.Serialize(jsonData);

                        data = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);

                        if (!string.IsNullOrEmpty(data))
                        {
                            string result = PushSend(data);
                            if (result == "OK")
                            {
                                pushSuccess = true;
                            }
                            errorMsg = result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorMsg = ex.ToString();
                }
            }


            // 아이폰 발송            
            if (devices_iPhone.Count > 0)
            {
                try
                {
                    int curLength = 0;

                    while (curLength < devices_iPhone.Count)
                    {
                        List<string> get_ids;

                        if (devices_iPhone.Count - (curLength + 1000) > 0)
                        {
                            get_ids = devices_iPhone.GetRange(curLength, curLength + 1000);
                            curLength += 1000;
                        }
                        else
                        {
                            get_ids = devices_iPhone.GetRange(curLength, devices_iPhone.Count);
                            curLength = devices_iPhone.Count;
                        }

                        string data = string.Empty;
                        var jsonData = new
                        {
                            registration_ids = get_ids.ToArray(),
                            notification = new
                            {
                                title = title,
                                body = body,
                                data = exdata,
                                sound = "default",
                                vibrate = "true"
                            },
                            data = new
                            {
                                data = exdata
                            }
                        };

                        data = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);

                        if (!string.IsNullOrEmpty(data))
                        {
                            string result = PushSend(data);
                            if (result == "OK")
                            {
                                pushSuccess = true;
                            }
                            errorMsg = result;
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorMsg = ex.ToString();
                }
            }
            
            try
            {
                MobileRepository.Instance.MobilePushLogInsert(new Core.Entity.MobilePushLog
                {
                    ID = ret.FirstOrDefault()?.ID,
                    Title = title,
                    Body = body,
                });
            }
            catch { }

            return pushSuccess;
        }

        // 푸시 발송
        private string PushSend(string data)
        {
            try
            {
                var applicationID = ConfigurationManager.AppSettings["PushAppID"] ?? "AAAA_7rMnGw:APA91bE0NrEc_d7Vasp_fFoCQTyArE7kg6MKfcwNJZ2iQ25WIF-amS214xce9SVsq-4vP1CZNihi_oYSAr8W3KO3UoKb0GkZWJVi-Fz9HrXDlKgRsQpmumonbMh6zhdSIAV7p6Kl6GE5";
                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                WebRequest request = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                request.Method = "post";
                request.ContentType = "application/json";
                request.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                request.ContentLength = byteArray.Length;

                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(byteArray, 0, byteArray.Length);

                    using (Stream dataStreamResponse = request.GetResponse().GetResponseStream())
                    {
                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                        {
                            JObject push = JObject.Parse(tReader.ReadToEnd());

                            if (push.SelectToken("success").Value<bool>())
                            {
                                return "OK";
                            }
                            else
                            {
                                JToken jArray = push.SelectToken("results");

                                if (jArray.FirstOrDefault() != null)
                                {
                                    return jArray.FirstOrDefault().SelectToken("error")?.Value<string>();
                                }
                                else
                                {
                                    return string.Empty;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public List<MobileBanner> MobileBannerSelect(int user = 1)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@user", user);

            var ret = dbConnection.Query<MobileBanner>("SP_Consult_M_BANNER_SELECT", parameters, commandType: CommandType.StoredProcedure);
            dbConnection.Close();

            return ret.ToList();
        }
    }
}
