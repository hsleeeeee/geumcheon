﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geumcheon.Web.Core.ViewModel
{
    public class ViewModelList<T, SearchT>
    {

        public List<T> Entities { get; set; }
        public SearchT Search { get; set; }
        public PagingInfo PagingInfo { get; set; }

        //public List<KeyValuePair<string, object>> KeyValuePair { get; set;}
    }


    public class SearchBase
    {
        public string SearchField { get; set; }
        public string SearchText { get; set; }
    }
    public class SearchInfo :SearchBase
    {
        public string SearchType { get; set; }
        public string Name { get; set; }
        public string TeacherId { get; set; }
        public string CompanyCode { get; set; }
        public string ConsultWay { get; set; }
        public string OrderType { get; set; }
    }
    public class PagingInfo
    {
        public int TotalCount { get; set; }
        public int PageSize { get; set; }
        public long TotalItems { get; set; }
        public int CurrentPage { get; set; }
        public string OrderType { get; set; }
        //public int TotalPages
        //{
        //    get
        //    {
        //        var totalPage = (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage);

        //        return totalPage > 0 ? totalPage : 1;
        //    }
        //}

        public PagingInfo()
        {
            //ItemsPerPage = 40;
        }

        public PagingInfo(int currentPage, int pageSize, int totalItems, string orderType = null)
        {
            CurrentPage = currentPage;
            PageSize = pageSize;
            TotalItems = totalItems;
            OrderType = orderType;
        }
    }
}
