import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, IconButton, AppBar, Toolbar, Typography, DialogActions, DialogContent } from '@material-ui/core';
import { DialogAppbar } from '../components/parts';
import { OpenModal, CloseModal } from '../helper/appfunction';

class TermsDialog extends Component {
    constructor(props) {
        super(props);

        this.userinfo = JSON.parse(localStorage.getItem('user'));

        this.state = {
            terms: [],
        }
    }

    handleDialogClose = (value) => {
        CloseModal();
        this.props.onClose(value);
    }

    handleEnter = () => {
        let data = [];

        if (this.props.item.Body != null) {
            this.props.item.Body.split("<strong>").forEach(function (item) {
                if (item.indexOf("</strong>") > -1) {
                    const content = item.split("</strong>");
                    data.push({
                        Title: content[0].replace(/<(\/br|em|\/em|p|\/p|br)([^>]*)>/ig, ""),
                        Body: content[1].replace(/<(\/br|em|\/em|p|\/p|br)([^>]*)>/ig, "").trim(),
                    });
                }
            });
        }

        this.setState({
            terms: data,
        });

        OpenModal(this.handleDialogClose);
    }

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => this.handleDialogClose(true)}
                    fullScreen
                    elevation={0}
                    style={{ paddingTop: "0px" }}
                    onEnter={this.handleEnter}
                >
                    <div>
                        <DialogAppbar title="이용약관" onClose={this.handleDialogClose} />
                    </div>
                    <DialogContent style={{ padding: '56px 12px 8px 12px' }}>
                        {this.props.item == "private" ? (
                            <div>
                                <Typography variant="body2" color="primary" style={{ marginTop: '16px', marginBottom: '16px', fontSize: '16px', fontWeight: '600' }}>개인정보 수집 동의</Typography>
                                <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                    1. 수집 및 제공 받는자의 이용목적 : 1:1 입시상담 
                                </Typography>
                                <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                    2. 제공하는 항목 : 입시 상담을 위해 작성한 모든 내역 (성명, 핸드폰번호, 학교, 학년, 지역, 상담내용)
                                </Typography>
                                <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                    3. 제공받는 자 : 금천구 온라인 진학상담실 민간위탁운영 퓨쳐플랜
                                </Typography>
                                <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                    4. 보유 및 이용기간 : 상담 종료 후 1년
                                </Typography>
                                <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                    5. 동의를 거부할 수 있으며, 동의 거부 시 해당 서비스를 제공 받을 수 없습니다.
                                </Typography>
                            </div>
                        ) : (
                            <div>
                                    <Typography variant="body2" color="primary" style={{ marginTop: '16px', marginBottom: '16px', fontSize: '16px', fontWeight: '600'  }}>개인정보 제3자 제공 동의</Typography>
                                        <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                            1. 수집 및 제공 받는자의 이용목적 : 1:1 입시상담
                                </Typography>
                                        <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                            2. 제공하는 항목 : 입시 상담을 위해 작성한 모든 내역 (성명, 핸드폰번호, 학교, 학년, 지역, 상담내용)
                                </Typography>
                                        <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                            3. 제공받는 자 : 금천구 온라인 진학상담실 민간위탁운영 퓨쳐플랜
                                </Typography>
                                        <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                            4. 보유 및 이용기간 : 상담 종료 후 1년
                                </Typography>
                                        <Typography variant="body2" style={{ wordBreak: 'keep-all', fontSize: '16px', lineHeight: '26px', margin: '8px 0px' }}>
                                            5. 동의를 거부할 수 있으며, 동의 거부 시 해당 서비스를 제공 받을 수 없습니다. 
                                </Typography>                                    
                            </div>
                        )
                    }
                    </DialogContent>
                </Dialog>
            </div >
        );
    }
}

TermsDialog.propTypes = {
    open: PropTypes.bool,
    item: PropTypes.string,
    onClose: PropTypes.func,
};

TermsDialog.defaultProps = {
    open: false,
    item: "private",
    onClose: () => { console.error("close function is not defined"); },
};

export default TermsDialog
