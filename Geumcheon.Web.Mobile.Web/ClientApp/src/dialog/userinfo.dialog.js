import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, Grid, Container, DialogTitle, DialogContent, DialogActions, Box, Switch, Checkbox, FormControlLabel, Radio, Select, Typography, Divider } from '@material-ui/core';
import { CheckCircleOutline } from '@material-ui/icons';

import { FPTextField, FPTextFieldButton, DialogAppbar, FPTimePicker } from '../components/parts';
import { OpenModal, CloseModal, GetUserInfo, GetUserInfoRole } from '../helper/appfunction';
import { compose } from '@material-ui/system';

export class UserInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            gender: 0,
            phoneNumber: "",
            email: "",
            school: "",
            grade: 0,
            address: "",
            password: "",
            passwordValidate: "",
            hp_p: "",
            hp_s: "",
            schooltype: "H",

            smsauthState: false,
            deviceID: "",
            smsCode: "",
            phoneNumberDisabled: false,
            SmsDisabled: false
        }
    }

    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.type == "checkbox" ? e.target.checked : e.target.value;
        this.setState(nextState);
    }

    handleGenderRadio = (value) => {
        this.setState({
            gender: value
        });
    }

    // SMS 코드 요청
    requestSMSCode = () => {
        this.props.onBackDrop(true, "요청 중...");
        this.props.onRequest('api/anonymous/', 'post', {
            type: 'smsrequest',
            phonenumber: this.state.phoneNumber,
        }, true).then((result) => {
            if (result.error == null) {
                this.props.onAlert({
                    message: "SMS가 전송되었습니다."
                });
                this.setState({
                    deviceID: result.deviceid,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // SMS 코드 확인
    requestSMSCodeValidate = () => {
        this.props.onBackDrop(true, "확인 중...");
        this.props.onRequest('api/anonymous/', 'post', {
            type: 'smscert',
            deviceid: this.state.deviceID,
            phonenumber: this.state.phoneNumber,
            code: this.state.smsCode
        }, true).then((result) => {
            if (result.error == null && result.deviceid == this.state.deviceID) {
                this.props.onAlert({
                    message: '인증 완료 되었습니다.'
                });
                this.setState({
                    smsauthState: true,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // 적용 및 닫기 or 취소
    handleDialogClose = (value) => {
        if (value) {
            const emailRegex = /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
            const phoneRegex = /^01(?:0|1|[6-9])[.-]?(\d{3}|\d{4})[.-]?(\d{4})$/;

            if (this.state.password != this.state.passwordValidate) {
                this.props.onAlert({
                    message: "비밀번호가 일치하지 않습니다.",
                });
                return;
            }
            else if (this.state.name == "") {
                this.props.onAlert({
                    message: "이름을 입력해 주세요.",
                });
                return;
            }
            else if (this.state.email == "") {
                this.props.onAlert({
                    message: "이메일 주소를 입력해 주세요.",
                });
                return;
            }
            else if (this.state.email != null && !emailRegex.test(this.state.email)) {
                this.props.onAlert({
                    message: "잘못 된 Email 주소 입니다.",
                });
                return;
            }
            else if (this.state.school == "") {
                this.props.onAlert({
                    message: "학교를 입력해 주세요.",
                });
                return;
            }
            else if (this.state.address == "") {
                this.props.onAlert({
                    message: "거주지역을 입력해 주세요.",
                });
                return;
            }

            this.props.onBackDrop(true, "저장 중...");
            this.props.onRequest('api/Account', 'post', {
                pwd: this.state.password,
                name: this.state.name,
                email: this.state.email,
                school: this.state.school,
                grade: this.state.grade,
                gender: this.state.gender,
                address: this.state.address,
                deviceid: this.state.deviceID,
                phonenumber: this.state.phoneNumber,
                code: this.state.smsCode,
                hp_p: GetUserInfoRole("STUDENT") ? this.state.hp_p : null,
                hp_s: GetUserInfoRole("PARENT") ? this.state.hp_s : null,
                schooltype: this.state.schooltype
            }).then((result) => {
                if (result.error == null) {
                    this.props.onAlert({
                        message: "저장되었습니다.",
                    }, function () { CloseModal(); this.props.onClose(); }.bind(this, CloseModal));
                }
                else {
                    this.props.onAlert({
                        message: result.error,
                    });
                }
            });
        }
        else {
            CloseModal();
            this.props.onClose(value);
        }
    }

    // 다이얼로그 열때
    handleDialogEnter = () => {
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/Account', 'post').then((result) => {

            const nextState = {};
            if (result.error == null) {
                nextState.name = result.Name;
                nextState.gender = result.Gender;
                nextState.email = result.Email;
                nextState.school = result.School;
                nextState.grade = result.Grade;
                nextState.address = result.Address;
                nextState.hp_p = result.Hp_P;
                nextState.hp_s = result.Hp_S;
                nextState.schooltype = result.SchoolType;
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }

            this.setState(nextState);
        });



        OpenModal(this.handleDialogClose);
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleDialogClose}
                fullScreen
                onEnter={this.handleDialogEnter}>
                <DialogAppbar title="정보수정" onClose={this.handleDialogClose} />
                <DialogTitle style={{ display: "none" }} >정보수정</DialogTitle>
                <DialogContent style={{ padding: "0px" }}>
                    <Container component="main" maxWidth="xl">
                        <Box p={1}>
                            핸드폰 인증 후 수정할 수 있습니다.
                            </Box>
                        <Box border={1}>
                            <FPTextFieldButton
                                fullWidth
                                name="phoneNumber"
                                label="핸드폰 번호 (숫자만 입력)"
                                id="phoneNumber"
                                autoComplete="phoneNumber"
                                onChange={this.handleChange}
                                value={this.state.phoneNumber}
                                onClick={this.requestSMSCode}
                                buttonName="전송"
                                disabled={this.state.phoneNumberDisabled}
                            />
                        </Box>
                        {this.state.deviceID != '' ? (
                            <Box border={1} borderTop={0} >
                                <FPTextFieldButton
                                    fullWidth
                                    name="smsCode"
                                    label="인증번호 6자리"
                                    id="smsCode"
                                    onChange={this.handleChange}
                                    value={this.state.smsCode}
                                    onClick={this.requestSMSCodeValidate}
                                    buttonName="확인"
                                    disabled={this.state.SmsDisabled}
                                />
                            </Box>
                        ) : null}
                    </Container>

                    <Divider style={{ margin: "16px 0px" }} />

                    <Container component="main" maxWidth="xl">
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                성별
                            </Box>
                            <Box border={1} pl={1.5} pr={1.5} pt={0.5} pb={0.5} bgcolor="#fff" borderColor="#dadada" flexGrow={1}>
                                <FormControlLabel value={0} control={<Radio checked={this.state.gender == 0} onChange={() => this.handleGenderRadio(0)} disabled={!this.state.smsauthState} />} label="미선택" style={{ display: "none" }} />
                                <FormControlLabel value={1} control={<Radio checked={this.state.gender == 1} onChange={() => this.handleGenderRadio(1)} disabled={!this.state.smsauthState} />} label="남자" />
                                <FormControlLabel value={2} control={<Radio checked={this.state.gender == 2} onChange={() => this.handleGenderRadio(2)} disabled={!this.state.smsauthState} />} label="여자" />
                            </Box>
                        </Box>
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                아이디
                            </Box>
                            <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 82px)" }}>
                                <FPTextField name="id" label="" value={GetUserInfo("id")} disabled={true} />
                            </Box>
                        </Box>
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                비밀번호
                            </Box>
                            <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                                <FPTextField name="password" label="" value={this.state.password} onChange={this.handleChange} type="password" disabled={!this.state.smsauthState} />
                            </Box>
                        </Box>
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep" }} >
                                비밀번호 확인
                            </Box>
                            <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                                <FPTextField name="passwordValidate" label="" value={this.state.passwordValidate} onChange={this.handleChange} type="password" disabled={!this.state.smsauthState} />
                            </Box>
                        </Box>
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                이름
                            </Box>
                            <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                                <FPTextField name="name" label="" value={this.state.name} onChange={this.handleChange} disabled={!this.state.smsauthState} />
                            </Box>
                        </Box>
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                이메일
                            </Box>
                            <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                                <FPTextField name="email" label="" value={this.state.email} onChange={this.handleChange} disabled={!this.state.smsauthState} />
                            </Box>
                        </Box>
                        {
                            GetUserInfoRole() == "PARENT" ? (<Box display="flex" alignItems="center" >
                                <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                                    학생 핸드폰
                    </Box>
                                <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                                    <FPTextField name="hp_s" label="" value={this.state.hp_s} onChange={this.handleChange} disabled={!this.state.smsauthState} />
                                </Box>
                            </Box>) : (<Box display="flex" alignItems="center" >
                                <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                                    학부모 핸드폰
                    </Box>
                                <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                                    <FPTextField name="hp_p" label="" value={this.state.hp_p} onChange={this.handleChange} disabled={!this.state.smsauthState} />
                                </Box>
                            </Box>)
                        }
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                거주지역
                            </Box>
                            <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                                <FPTextField name="address" label="" value={this.state.address} onChange={this.handleChange} disabled={!this.state.smsauthState} />
                            </Box>
                        </Box>
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                학교
                            </Box>
                            <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 156px)" }}>
                                <FPTextField name="school" label="" value={this.state.school} onChange={this.handleChange} disabled={!this.state.smsauthState} />
                            </Box>
                            <Box bgcolor="#fff" border={1} borderLeft={0} borderTop={0} width={74} borderColor="#dadada">
                                <Select native fullWidth value={this.state.grade} onChange={this.handleChange} name="grade" style={{ paddingRight: "12px" }} disabled={!this.state.smsauthState} >
                                    <option value="1">1학년</option>
                                    <option value="2">2학년</option>
                                    <option value="3">3학년</option>
                                </Select>
                            </Box>
                        </Box>
                        <Box display="flex" alignItems="center" >
                            <Box width={80} fontWeight={600} textAlign="center" >
                                학교구분
                            </Box>
                            <Box border={1} borderTop={0} pl={1.5} pr={1.5} pt={0.5} pb={0.5} bgcolor="#fff" borderColor="#dadada" flexGrow={1}>
                                <FormControlLabel value="H" control={<Radio checked={this.state.schooltype == "H"} onChange={this.handleChange} name="schooltype" value="H" disabled={!this.state.smsauthState} />} label="고등학교" />
                                <FormControlLabel value="M" control={<Radio checked={this.state.schooltype == "M"} onChange={this.handleChange} name="schooltype" value="M" disabled={!this.state.smsauthState} />} label="중학교" />
                            </Box>
                        </Box>
                    </Container>
                </DialogContent>
                <DialogActions style={{ padding: "0px 0px 16px 0px" }}>
                    <Container component="div" maxWidth="xl">
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                    취소
                                    </Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                    적용
                                    </Button>
                            </Grid>
                        </Grid>
                    </Container>
                </DialogActions>
            </Dialog>
        );
    }
}
UserInfo.propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onAlert: PropTypes.func,
};

UserInfo.defaultProps = {
    open: false,
    onClose: () => { console.error("close function is not defined"); },
    onRequest: () => { console.error("reqeust function is not defined"); },
    onAlert: () => { console.info("alert function is not defined"); },
};

export class NotiInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ReceiveNotification: true,
            ExceptionTime: false,
            ExceptionTimeStart: '00:00',
            ExceptionTimeEnd: '08:00',

            // 타임피커
            timePickerDialog: false,
            timePickerName: '',
            timePickerItem: '00:00',

            // 알림 다이얼로그
            alertDialog: false,
            alertMessage: '',
        }
    }

    // 창 닫기
    handleDialogClose = (value) => {
        if (value) {

            // 값 유효성 체크
            let isTimeInit = false;
            let startTimeHour = this.state.ExceptionTimeStart.split(":")[0];
            let startTimeMinute = parseInt(this.state.ExceptionTimeStart.split(":")[1]);
            let endTimeHour = this.state.ExceptionTimeEnd.split(":")[0];
            let endTimeMinute = parseInt(this.state.ExceptionTimeEnd.split(":")[1]);
            startTimeHour = parseInt(startTimeHour);
            endTimeHour = parseInt(endTimeHour);



            if ((startTimeHour > endTimeHour) || ((startTimeHour == endTimeHour) && (startTimeMinute > endTimeMinute))) {
                //if (this.state.ExceptionTime) {
                this.props.onAlert({
                    message: "알림 차단 시작 시간은 종료 시간 보다 이전 시간이어야 합니다."
                });
                return;
                //}
                //else {
                //    isTimeInit = true;
                //}
            }

            this.props.onBackDrop(true, "저장 중...");
            this.props.onRequest('api/MobileSetting', 'put', {
                receivenotification: this.state.ReceiveNotification,
                exceptiontime: this.state.ExceptionTime,
                exceptiontimestart: isTimeInit ? "00:00:00" : this.state.ExceptionTimeStart,
                exceptiontimeend: isTimeInit ? "08:00:00" : this.state.ExceptionTimeEnd,
            }).then((result) => {
                if (result.error == null) {
                    this.props.onAlert({
                        message: "저장되었습니다.",
                    }, function () { CloseModal(); this.props.onClose(true); }.bind(this, CloseModal));
                }
                else {
                    this.props.onAlert({
                        message: result.error,
                    });
                }
            })
        }
        else {
            CloseModal();
            this.props.onClose(value);
        }
    }

    // input 내용 변경시
    handleChange = (e) => {
        console.log(e.target.type);
        let nextState = {};
        nextState[e.target.name] = e.target.type == "checkbox" ? e.target.checked : e.target.value;
        this.setState(nextState);
    }

    // 타임 피커
    handleTimePicker = (name) => {
        this.setState({
            timePickerDialog: true,
            timePickerName: name,
            timePickerItem: this.state[name]
        });
    }

    // 타임피커 완료
    handleTimePickerClose = (value, name) => {
        let nextState = {};
        nextState.timePickerDialog = false;

        if (value) {
            let clock = parseInt(value.clock)
            let hour = parseInt(value.hour);
            hour = ((clock == 1 ? 12 : 0) + hour);
            hour = hour == 24 ? 0 : hour;
            let minute = parseInt(value.minute);
            nextState[name] = (hour < 10 ? "0" : "") + hour.toString() + ":" + (minute < 10 ? "0" : "") + value.minute.toString() + ":00";
        }

        this.setState(nextState);
    }

    // 다이얼로그 열때
    handleDialogEnter = () => {
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/MobileSetting', 'get').then((result) => {
            if (result.error == null) {
                this.setState({
                    ReceiveNotification: result.ReceiveNotification,
                    ExceptionTime: result.ExceptionTime,
                    ExceptionTimeStart: result.ExceptionTimeStart,
                    ExceptionTimeEnd: result.ExceptionTimeEnd,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error,
                });
            }
        });
        OpenModal(this.handleDialogClose);
    }

    convertTimeString = (value) => {
        let timeStr = value.split(":");
        let hour = parseInt(timeStr[0]) == 0 ? 24 : parseInt(timeStr[0]);
        let clock = "오전 ";
        if (hour > 12) {
            clock = "오후 ";
            hour = hour - 12;
        }

        return clock + (hour < 10 ? "0" : "") + hour.toString() + ":" + timeStr[1];
    }

    convertTicktoTime = (value) => {
        let hour = value.getHours();
        let minute = value.getMinutes();

    }

    render() {
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={() => this.handleDialogClose(true)}
                    fullScreen
                    maxWidth="xl"
                    elevation={0}
                    onEnter={this.handleDialogEnter}
                >
                    <DialogAppbar title="알림설정" onClose={this.handleDialogClose} />
                    <DialogTitle style={{ display: 'none' }} >알림설정</DialogTitle>
                    <DialogContent style={{ padding: '0px' }}>
                        <Box p={1} display="flex" borderBottom={1}>
                            <Box p={1} pt={1.5} flexGrow={1} >
                                알림 끄기 / 받기
                            </Box>
                            <Box pr={1}>
                                <Switch
                                    name="ReceiveNotification"
                                    checked={this.state.ReceiveNotification}
                                    onChange={this.handleChange}
                                    color="primary"
                                />
                            </Box>
                        </Box>

                        <Box p={2} pb={1}>
                            알림 차단시간 설정
                        </Box>
                        <Box display="flex">
                            <Box pl={4} pt={1.5} flexGrow={1} color="#9A9A9A" fontSize={14} fontWeight="fontWeightRegular">
                                해당 시간에 알림을 받지 않습니다.
                            </Box>
                            <Box pr={2}>
                                <Checkbox style={{ padding: '0px' }}
                                    name="ExceptionTime"
                                    onChange={this.handleChange}
                                    checkedIcon={<CheckCircleOutline style={{ fill: "#FF7C27" }} />}
                                    icon={<CheckCircleOutline style={{ fill: "#9A9A9A" }} />}
                                    checked={this.state.ExceptionTime}
                                />
                            </Box>
                        </Box>
                        <Box pb={2} borderBottom={1} fontSize={15} fontWeight="fontWeightRegular">
                            <Container component="div" maxWidth="xl">
                                <Grid container spacing={1}>
                                    <Grid item xs={6}>
                                        <Box p={2} display="flex" border={1} bgcolor="#fff" onClick={() => this.handleTimePicker("ExceptionTimeStart")}>
                                            <Box flexGrow={1} color="#FF7C27">
                                                시작
                                            </Box>
                                            <Box color="#9A9A9A">
                                                {this.convertTimeString(this.state.ExceptionTimeStart)}
                                            </Box>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Box p={2} display="flex" border={1} bgcolor="#fff" onClick={() => this.handleTimePicker("ExceptionTimeEnd")}>
                                            <Box flexGrow={1} color="#FF7C27">
                                                종료
                                            </Box>
                                            <Box color="#9A9A9A">
                                                {this.convertTimeString(this.state.ExceptionTimeEnd)}
                                            </Box>
                                        </Box>
                                    </Grid>
                                </Grid>
                            </Container>
                        </Box>
                        <Box p={1.5} pl={4} fontSize={14} color="#9A9A9A" fontWeight="fontWeightRegular">
                            알림 수신 여부를 설정할 수 있습니다.
                        </Box>
                    </DialogContent>
                    <DialogActions style={{ padding: '0px 0px 16px 0px' }}>
                        <Container component="div" maxWidth="xl">
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                        취소
                                    </Button>
                                </Grid>
                                <Grid item xs={6}>
                                    <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                        적용
                                    </Button>
                                </Grid>
                            </Grid>
                        </Container>
                    </DialogActions>
                    <FPTimePicker open={this.state.timePickerDialog} name={this.state.timePickerName} item={this.state.timePickerItem} onClose={this.handleTimePickerClose} />
                </Dialog>
            </div >
        );
    }
}

NotiInfo.propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onAlert: PropTypes.func,
};

NotiInfo.defaultProps = {
    open: false,
    onClose: () => { console.error("close function is not defined"); },
    onRequest: () => { console.error("reqeust function is not defined"); },
    onAlert: () => { console.info("alert function is not defined"); },
};

export class UniversityInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            gender: 0,
            grade: 0,
            univ: [],
        }
    }

    handleChangeArray = (index, e) => {
        const tempuniv = [...this.state.univ];

        tempuniv[index][e.target.name] = e.target.value;

        console.log(tempuniv);

        this.setState({
            univ: tempuniv
        });
    }

    // 적용 및 닫기 or 취소
    handleDialogClose = (value) => {
        if (value) {
            let needuniv = false;
            this.state.univ.forEach((item) => {
                if (item.UniversityName != "") {
                    needuniv = true;
                }
            });

            if (!needuniv) {
                this.props.onAlert({
                    message: "희망학교는 1개 이상 입력하세요."
                });
                return;
            }

            this.props.onBackDrop(true, "저장 중...");
            this.props.onRequest('api/Account', 'post', {
                hope: this.state.univ
            }).then((result) => {
                if (result.error == null) {
                    this.props.onAlert({
                        message: "저장되었습니다.",
                    }, function () { CloseModal(); this.props.onClose(); }.bind(this, CloseModal));
                }
                else {
                    this.props.onAlert({
                        message: result.error,
                    });
                }
            });
        }
        else {
            CloseModal();
            this.props.onClose(value);
        }
    }

    // 다이얼로그 열때
    handleDialogEnter = () => {
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/Account', 'post').then((result) => {

            const nextState = {};

            if (result.error == null) {
                nextState.gender = result.Gender;
                nextState.grade = result.Grade;

                const tempuniv = [];
                for (let i = 0; i < 3; i++) {
                    tempuniv.push({
                        ApplyUnivIdx: 0,
                        Ranking: i + 1,
                        UniversityName: "",
                        StypeRem: "",
                        MajorName: ""
                    });
                }

                result.Univ.forEach((item) => {
                    tempuniv[item.Ranking - 1] = item;
                });

                nextState.univ = tempuniv;
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }

            this.setState(nextState);
        });

        OpenModal(this.handleDialogClose);
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={() => this.handleDialogClose(true)}
                fullScreen
                maxWidth="xl"
                elevation={0}
                onEnter={this.handleDialogEnter}
            >
                <DialogAppbar title="희망대학 수정" onClose={this.handleDialogClose} />
                <DialogTitle style={{ display: 'none' }} >알림설정</DialogTitle>
                <DialogContent style={{ padding: '0px' }}>
                    <Container component="main" maxWidth="xl" >
                        <Typography variant="subtitle1" gutterBottom style={{ color: "#ff7704" }} >
                            *희망학교는 1개이상 입력하세요.
                        </Typography>
                    </Container>


                    {this.state.univ.map((item, index) => (
                        <div key={index}>
                            <Box p={2} borderBottom={1} fontWeight={600} >
                                {index + 1}지망
                            </Box>
                            <Box p={2} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                                <Box border={1} flexGrow={1} color="#dadada">
                                    <FPTextField name="UniversityName" label="" value={this.state.univ[index].UniversityName} onChange={this.handleChangeArray.bind(this, index)} />
                                </Box>
                                <Box pl={2}>
                                    대학
                                </Box>
                            </Box>
                            <Box p={2} pt={0} pb={0} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                                <Box border={1} flexGrow={1} color="#dadada">
                                    <FPTextField name="StypeRem" label="" value={this.state.univ[index].StypeRem} onChange={this.handleChangeArray.bind(this, index)} />
                                </Box>
                                <Box pl={2}>
                                    전형
                                </Box>
                            </Box>
                            <Box p={2} bgcolor="#fff" display="flex" borderBottom={1} fontSize={15} alignItems="center" >
                                <Box border={1} flexGrow={1} color="#dadada">
                                    <FPTextField name="MajorName" label="" value={this.state.univ[index].MajorName} onChange={this.handleChangeArray.bind(this, index)} />
                                </Box>
                                <Box pl={2}>
                                    학과
                                </Box>
                            </Box>
                        </div>
                    ))}
                </DialogContent>
                <DialogActions style={{ padding: '0px 0px 16px 0px' }}>
                    <Container component="div" maxWidth="xl">
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                    취소
                                    </Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                    적용
                                    </Button>
                            </Grid>
                        </Grid>
                    </Container>
                </DialogActions>
            </Dialog>
        );
    }
}

UniversityInfo.propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onAlert: PropTypes.func,
};

UniversityInfo.defaultProps = {
    open: false,
    onClose: () => { console.error("close function is not defined"); },
    onRequest: () => { console.error("reqeust function is not defined"); },
    onAlert: () => { console.info("alert function is not defined"); },
};


export class PhoneInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            gender: 0,
            grade: 0,
            phoneNumber: "",

            smsauthState: false,
            deviceID: "",
            smsCode: "",
            phoneNumberDisabled: false,
            SmsDisabled: false
        }
    }

    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.type == "checkbox" ? e.target.checked : e.target.value;
        this.setState(nextState);
    }

    // SMS 코드 요청
    requestSMSCode = () => {
        this.props.onBackDrop(true, "요청 중...");
        this.props.onRequest('api/anonymous/', 'post', {
            type: 'smsrequest',
            phonenumber: this.state.phoneNumber,
        }, true).then((result) => {
            if (result.error == null) {
                this.props.onAlert({
                    message: "SMS가 전송되었습니다."
                });
                this.setState({
                    deviceID: result.deviceid,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // SMS 코드 확인
    requestSMSCodeValidate = () => {
        this.props.onBackDrop(true, "확인 중...");
        this.props.onRequest('api/anonymous/', 'post', {
            type: 'smscert',
            deviceid: this.state.deviceID,
            phonenumber: this.state.phoneNumber,
            code: this.state.smsCode
        }, true).then((result) => {
            if (result.error == null && result.deviceid == this.state.deviceID) {
                this.props.onAlert({
                    message: '인증 완료 되었습니다.'
                });
                this.setState({
                    smsauthState: true,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // 적용 및 닫기 or 취소
    handleDialogClose = (value) => {
        if (value) {
            const emailRegex = /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
            const phoneRegex = /^01(?:0|1|[6-9])[.-]?(\d{3}|\d{4})[.-]?(\d{4})$/;

            if (!this.state.smsauthState) {
                this.props.onAlert({
                    message: "핸드폰 번호를 변경하려면 인증이 필요합니다."
                });
                return;
            }

            this.props.onBackDrop(true, "저장 중...");
            this.props.onRequest('api/Account', 'post', {
                hp: this.state.phone,
                gender: this.state.gender,
                grade: this.state.grade,
                deviceid: this.state.deviceID,
                phonenumber: this.state.phoneNumber,
                code: this.state.smsCode
            }).then((result) => {
                if (result.error == null) {
                    this.props.onAlert({
                        message: "저장되었습니다.",
                    }, function () { CloseModal(); this.props.onClose(); }.bind(this, CloseModal));
                }
                else {
                    this.props.onAlert({
                        message: result.error,
                    });
                }
            });
        }
        else {
            CloseModal();
            this.props.onClose(value);
        }
    }

    // 다이얼로그 열때
    handleDialogEnter = () => {
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/Account', 'post').then((result) => {

            const nextState = {};

            if (result.error == null) {
                nextState.gender = result.Gender;
                nextState.grade = result.Grade;
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }

            this.setState(nextState);
        });

        OpenModal(this.handleDialogClose);
    }

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={() => this.handleDialogClose(true)}
                fullScreen
                maxWidth="xl"
                elevation={0}
                onEnter={this.handleDialogEnter}
            >
                <DialogAppbar title="핸드폰 번호 수정" onClose={this.handleDialogClose} />
                <DialogTitle style={{ display: 'none' }} >알림설정</DialogTitle>
                <DialogContent style={{ padding: '0px' }}>
                    <Container component="main" maxWidth="xl" >
                        <Box p={1}>
                            핸드폰 번호 수정
                        </Box>

                        <Box border={1}>
                            <FPTextFieldButton
                                fullWidth
                                name="phoneNumber"
                                label="핸드폰 번호 (숫자만 입력)"
                                id="phoneNumber"
                                autoComplete="phoneNumber"
                                onChange={this.handleChange}
                                value={this.state.phoneNumber}
                                onClick={this.requestSMSCode}
                                buttonName="전송"
                                disabled={this.state.phoneNumberDisabled}
                            />
                        </Box>
                        {this.state.deviceID != '' ? (
                            <Box border={1} borderTop={0} >
                                <FPTextFieldButton
                                    fullWidth
                                    name="smsCode"
                                    label="인증번호 6자리"
                                    id="smsCode"
                                    onChange={this.handleChange}
                                    value={this.state.smsCode}
                                    onClick={this.requestSMSCodeValidate}
                                    buttonName="확인"
                                    disabled={this.state.SmsDisabled}
                                />
                            </Box>
                        ) : null}
                    </Container>
                </DialogContent>
                <DialogActions style={{ padding: '0px 0px 16px 0px' }}>
                    <Container component="div" maxWidth="xl">
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                    취소
                                    </Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                    적용
                                    </Button>
                            </Grid>
                        </Grid>
                    </Container>
                </DialogActions>
            </Dialog>
        );
    }
}

PhoneInfo.propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onAlert: PropTypes.func,
};

PhoneInfo.defaultProps = {
    open: false,
    onClose: () => { console.error("close function is not defined"); },
    onRequest: () => { console.error("reqeust function is not defined"); },
    onAlert: () => { console.info("alert function is not defined"); },
};
