import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, Grid, Container, Box,  } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

import { DialogAppbar, FPTextField, FPTextFieldButton } from '../components/parts';
import { OpenModal, CloseModal, callNativeFunction } from '../helper/appfunction';
import { logoImg, logoImg2 } from '../helper/appstring';
import RegisterDialog from './register.dialog';


class LoginDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: '',
            password: '',
            open: '',
        };

        window.LoginWeb = function (token, type, version, osversion) {

            if (osversion != null) {
                localStorage.setItem("os", JSON.stringify({
                    os: type,
                    version: osversion,
                }));
            }

            this.funcLogin(token, type, version);
        }.bind(this);
    };

    handleDialogClose = () => {
        try {
            if (JSON.parse(localStorage.getItem("os")).os == "I") {
                document.getElementById("root").style.display = "";
            }
        }
        catch (ex) { }
        CloseModal();
        this.props.onClose();
    };

    handleDialogEnter = () => {
        try {
            if (JSON.parse(localStorage.getItem("os")).os == "I") {
                document.getElementById("root").style.display = "none";
            }
        }
        catch (ex) { }
        OpenModal(this.handleDialogClose);
        this.setState({
            userName: "",
            password: "",
        });
    };

    // 로그인 디바이스 정보 얻기
    handleLogin = (e) => {
        if (callNativeFunction("loginWeb") == 0) {
            this.funcLogin("pc", "p", "0.0.0.1");
        }
    };

    // 아이디 찾기
    handleFindID = () => {
        this.setState({
            open: "ID"
        });
    };

    // 비밀번호 찾기
    handleFindPWD = () => {
        this.setState({
            open: "PWD"
        });
    };

    // 회원 가입
    handleRegister = () => {
        this.setState({
            open: "Join"
        });
    };

    // 아이디, 비번 입력 시
    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    };
    // enter 입력
    handleKeyPress = (e) => {
        if (e.charCode === 13) {
            this.handleLogin();
        }
    };

    handleChildDialogClose = (value) => {
        var nextState = { open: '' };

        if (value) {
            nextState.userName = value;
        }

        this.setState(nextState);
    }

    // 로그인
    funcLogin = (token, type, version) => {
        let schcode = this.state.schoolCode;
        let id = this.state.userName;
        let pw = this.state.password;

        localStorage.setItem("version", version);

        this.props.onLogin(id, pw, token, type).then(
            (success) => {
                if (!success) {
                    this.props.onAlert({
                        title: "로그인",
                        message: "아이디 또는 비밀번호가 잘못 되었습니다."
                    });

                    this.setState({
                        password: ''
                    });
                }
                else {
                    this.handleDialogClose();
                }
            }
        );
    };

    render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleDialogClose}
                fullScreen
                onEnter={this.handleDialogEnter}>
                <div style={{ backgroundImage: "url(" + logoImg + ")", backgroundRepeat: "no-repeat", backgroundPosition: "bottom", backgroundSize: "320px", marginBottom: "32px", height: "100%" }}>
                    <DialogAppbar title="로그인" onClose={this.handleDialogClose} />
                    <Container component="main" maxWidth="xl">
                        <Box alignItems="center" style={{ textAlign: 'center' }} pt={4} pb={4}>
                            <img src={logoImg2} style={{ width: "80%", maxWidth: "380px" }} />
                        </Box>
                        <Box mb={5} pb={2} bgcolor="#fafafa">
                            <Box border={1} mb={1.5}>
                                <FPTextField required={true} label="아이디를 입력하세요." name="userName" onChange={this.handleChange} onKeyPress={this.handleKeyPress} autoFocus={true} value={this.state.userName} />
                            </Box>
                            <Box border={1} mb={2}>
                                <FPTextField required={true} name="password" label="비밀번호를 입력하세요." type="password" onChange={this.handleChange} onKeyPress={this.handleKeyPress} value={this.state.password} />
                            </Box>

                            <Box mb={2}>
                                <Button type="button" fullWidth variant="contained" color="primary" onClick={this.handleLogin}>
                                    로그인
                            </Button>
                            </Box>

                            <Grid container justify="center" alignItems="center">
                                <Box onClick={this.handleFindID} color="#9A9A9A" fontSize={14}>
                                    아이디 찾기
                            </Box>
                                <Box style={{ padding: '0px 8px' }} color="#9A9A9A" fontSize={14}>
                                    |
                            </Box>
                                <Box onClick={this.handleFindPWD} color="#9A9A9A" fontSize={14}>
                                    비밀번호 찾기
                            </Box>
                                <Box style={{ padding: '0px 8px' }} color="#9A9A9A" fontSize={14}>
                                    |
                            </Box>
                                <Box onClick={this.handleRegister} color="#9A9A9A" fontSize={14}>
                                    회원 가입
                            </Box>
                            </Grid>
                        </Box>
                    </Container>
                    
                </div>
                {this.state.open == "Join" ? (<RegisterDialog open={true} onAlert={this.props.onAlert} onClose={this.handleChildDialogClose} onBackDrop={this.props.onBackDrop} onRequest={this.props.onRequest} />) : null}
                {this.state.open == "ID" ? (<FindidDialog open={true} onAlert={this.props.onAlert} onClose={this.handleChildDialogClose} onBackDrop={this.props.onBackDrop} onRequest={this.props.onRequest} on />) : null}
                {this.state.open == "PWD" ? (<FindpwdDialog open={true} onAlert={this.props.onAlert} onClose={this.handleChildDialogClose} onBackDrop={this.props.onBackDrop} onRequest={this.props.onRequest} />) : null}  
            </Dialog>
        );
    };
}

LoginDialog.propTypes = {
    open: PropTypes.bool,
    onAlert: PropTypes.func,
    onClose: PropTypes.func,
    onLogin: PropTypes.func,
    onBackDrop: PropTypes.func,
    onRequest: PropTypes.func,
};

LoginDialog.defaultProps = {
    open: false,
    onAlert: () => { console.info("alert function is not defined"); },
    onClose: () => { console.info("close function is not defined"); },
    onLogin: () => { console.info("login function is not defined"); },
    onBackDrop: () => { console.info("backdrop function is not defined"); },
    onRequest: () => { console.info("request function is not defined"); }
};

export default LoginDialog





class FindidDialog extends Component {
    state = {
        displayName: '',
        phoneNumber: '',
        userName: '',

        findID: false,
    }

    findIDWebService = () => {
        this.props.onBackDrop(true, "조회 중...");
        return this.props.onRequest('api/anonymous/', 'post', {
            type: 'id',
            displayname: this.state.displayName,
            phonenumber: this.state.phoneNumber
        }, true).then((result) => {
            if (result.error == null) {
                this.setState({
                    userName: result.ID,
                    findID: true
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    handleDialogClose = () => {
        CloseModal();
        this.setState({
            displayName: '',
            phoneNumber: '',
            userName: '',
            findID: false
        })
        this.props.onClose(this.state.userName);
    }

    handleDialogEnter = () => {
        OpenModal(this.handleDialogClose);
    }


    render() {
        const findIDRequest = (
            <Container component="main" maxWidth="xl" style={{ marginTop: '20px' }}>
                <Typography variant="subtitle1" gutterBottom >
                    이름
                </Typography>
                <Box mb={2} border={1}>
                    <FPTextField
                        fullWidth
                        id="displayName"
                        label="이름을 입력하세요."
                        name="displayName"
                        autoComplete="displayName"
                        onChange={this.handleChange}
                        value={this.state.displayName}
                    />
                </Box>
                <Typography variant="subtitle1" gutterBottom >
                    휴대폰번호
                </Typography>
                <Box mb={2} border={1}>
                    <FPTextField
                        fullWidth
                        name="phoneNumber"
                        label="휴대폰 번호를 입력하세요."
                        id="phoneNumber"
                        autoComplete="phoneNumber"
                        onChange={this.handleChange}
                        value={this.state.phoneNumber}
                    />
                </Box>
                <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={this.findIDWebService}
                >
                    확인
                </Button>
            </Container>
        );

        const findIDResponse = (
            <Container component="main" maxWidth="xl" style={{ marginTop: '20px' }}>
                <Typography variant="subtitle1" gutterBottom >
                    입력하신 정보와 일치하는 아이디 입니다.
                </Typography>
                <Box mb={2} border={1}>
                    <FPTextField
                        fullWidth
                        id="userName"
                        label="아이디"
                        name="userName"
                        autoComplete="userName"
                        value={this.state.userName}
                        inputProps={{
                            readOnly: true,
                        }}
                    />
                </Box>
                <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={this.handleDialogClose}
                >
                    로그인 페이지로 이동
                </Button>
            </Container>
        );

        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleDialogClose}
                fullScreen
                onEnter={this.handleDialogEnter}
            >
                <div>
                    <DialogAppbar title="아이디 찾기" onClose={this.handleDialogClose} />
                    {this.state.findID ? findIDResponse : findIDRequest}
                </div>
            </Dialog>
        );
    }
}


FindidDialog.propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
};

FindidDialog.defaultProps = {
    open: false,
    onClose: () => { console.error("close function is not defined"); },
    onRequest: () => { console.error("reqeust function is not defined"); },
    onBackDrop: () => { console.log("backdrop function is not defined"); },
    onAlert: () => { console.info("alert function is not defined"); },
};



class FindpwdDialog extends Component {
    state = {
        displayName: '',    // 사용자 이름
        phoneNumber: '',    // 사용자 핸드폰 번호
        userName: '',       // 사용자 아이디
        deviceID: '',       // 임시 디바이스 ID
        smsCode: '',        // SMS 인증 코드
        password: '',       // 암호
        passwordValidate: '',   // 암호 체크
        progState: 0        // 현재 진행 단계 (0: 인증, 1: 비번설정, 3: 완료)
    }

    // SMS 코드 발송 요청
    requestSMSCode = () => {
        this.props.onBackDrop(true, "요청 중...");
        return this.props.onRequest('api/anonymous/', 'post', {
            type: 'pwdrequest',
            id: this.state.userName,
            displayname: this.state.displayName,
            phonenumber: this.state.phoneNumber
        }, true).then((result) => {
            if (result.error == null) {
                var nextState = {
                    deviceID: result.deviceid,
                };

                this.setState(nextState);

                this.props.onAlert({
                    message: "SMS가 전송 되었습니다."
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // SMS 코드 확인
    requestSMSCodeValidate = () => {
        this.props.onBackDrop(true, "확인 중...");
        return this.props.onRequest('api/anonymous/', 'post', {
            type: 'pwdvalidate',
            deviceid: this.state.deviceID,
            phonenumber: this.state.phoneNumber,
            code: this.state.smsCode
        }, true).then((result) => {
            if (result.error == null) {
                if (this.state.deviceID == result.deviceid) {
                    this.props.onAlert({
                        message: '인증되었습니다.'
                    }, function () { this.setState({ progState: 1 }); }.bind(this));
                }
            }
            else {
                this.props.onAlert({
                    message: result.error
                });

                this.setState({
                    smsCode: ''
                });
            }
        });
    }

    // 암호 변경
    requestChangePWD = () => {
        // 암호 유효성 체크 

        //if (!/^[a-zA-Z0-9]{6,12}$/.test(this.state.password)) {
        //    this.setState({
        //        alertDialog: true,
        //        alertMessage: "비밀번호는 6~12자리, 영문과 숫자가 조합되어야 합니다.",
        //    });
        //    return;
        //}
        //else if (this.state.password.search(/[0-9]/g) < 0 || this.state.password.search(/[a-z]/ig) < 0) {
        //    this.setState({
        //        alertDialog: true,
        //        alertMessage: "비밀번호는 6~12자리, 영문과 숫자가 조합되어야 합니다.",
        //    });
        //    return;
        //}
        // to-do

        this.props.onBackDrop(true, "저장 중...");
        return this.props.onRequest('api/anonymous/', 'post', {
            type: 'pwdchange',
            deviceid: this.state.deviceID,
            phonenumber: this.state.phoneNumber,
            code: this.state.smsCode,
            password: this.state.password,
            id: this.state.userName
        }, true).then((result) => {
            if (result.error == null) {
                if (result.result == "ok") {
                    this.props.onAlert({
                        message: '수정되었습니다.',
                    }, this.handleDialogClose);
                }
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    handleDialogClose = () => {
        CloseModal();
        // 창 종료 시 초기화
        this.setState({
            displayName: '',    // 사용자 이름
            phoneNumber: '',    // 사용자 핸드폰 번호
            userName: '',       // 사용자 아이디
            deviceID: '',       // 임시 디바이스 ID
            smsCode: '',        // SMS 인증 코드
            password: '',       // 암호
            passwordValidate: '',   // 암호 확인
            alertDialog: false, // 알림창 표시
            alertMessage: '',  // 알림창 내용
            progState: 0        // 현재 진행 단계
        });
        this.props.onClose();
    }

    handleDialogEnter = () => {
        OpenModal(this.handleDialogClose);
    }

    render() {
        const findPWDRequest = (
            <Container component="main" maxWidth="xl" style={{ marginTop: '20px' }} >
                <Typography variant="subtitle1" gutterBottom>
                    아이디
                </Typography>
                <Box mb={2} border={1} >
                    <FPTextField
                        fullWidth
                        id="userName"
                        label="아이디를 입력하세요."
                        name="userName"
                        autoComplete="userName"
                        onChange={this.handleChange}
                        value={this.state.userName}
                    />
                </Box>
                <Typography variant="subtitle1" gutterBottom>
                    이름
                </Typography>
                <Box mb={2} border={1}>
                    <FPTextField
                        fullWidth
                        id="displayName"
                        label="이름을 입력하세요."
                        name="displayName"
                        autoComplete="displayName"
                        onChange={this.handleChange}
                        value={this.state.displayName}
                    />
                </Box>
                <Typography variant="subtitle1" gutterBottom>
                    입력하신 번호로 인증코드를 전송합니다.
                </Typography>
                <Box border={1}>
                    <FPTextFieldButton
                        fullWidth
                        name="phoneNumber"
                        label="핸드폰 번호 (숫자만 입력)"
                        id="phoneNumber"
                        autoComplete="phoneNumber"
                        onChange={this.handleChange}
                        value={this.state.phoneNumber}
                        onClick={this.requestSMSCode}
                        buttonName="전송"
                    />
                </Box>
                {this.state.deviceID != '' ? (
                    <Box border={1} borderTop={0}>
                        <FPTextFieldButton
                            fullWidth
                            name="smsCode"
                            label="인증 번호 6자리"
                            id="smsCode"
                            onChange={this.handleChange}
                            value={this.state.smsCode}
                            onClick={this.requestSMSCodeValidate}
                            countTick={this.state.smsTimeCount}
                            buttonName="확인"
                        />
                    </Box>
                ) : null}

            </Container>
        );

        const findPWDResponse = (
            <Container component="main" maxWidth="xl" style={{ marginTop: '20px' }}>
                <Typography variant="subtitle1" gutterBottom>
                    새로운 비밀번호
                </Typography>
                <Box mb={2} border={1}>
                    <FPTextField
                        fullWidth
                        id="password"
                        label=""
                        name="password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        type="password"
                    />
                </Box>
                <Typography variant="subtitle1" gutterBottom >
                    비밀번호 확인
                </Typography>
                <Box mb={2} border={1}>
                    <FPTextField
                        fullWidth
                        id="passwordValidate"
                        label=""
                        name="passwordValidate"
                        value={this.state.passwordValidate}
                        onChange={this.handleChange}
                        type="password"
                    />
                </Box>
                <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={this.requestChangePWD}
                >
                    확인
                </Button>
            </Container>
        );

        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleDialogClose}
                fullScreen
                onEnter={this.handleDialogEnter}
            >
                <div>
                    <DialogAppbar title="비밀번호 찾기" onClose={this.handleDialogClose} />
                    {this.state.progState === 0 ? findPWDRequest : findPWDResponse}
                </div>
            </Dialog>
        );
    }
}


FindpwdDialog.propTypes = {
    open: PropTypes.bool,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onAlert: PropTypes.func,
};

FindpwdDialog.defaultProps = {
    open: false,
    onClose: () => { console.error("close function is not defined"); },
    onRequest: () => { console.error("reqeust function is not defined"); },
    onAlert: () => { console.info("alert function is not defined"); },
};

