import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, IconButton, Typography, Box, Button, Container, Grid, Tabs, Tab, FormControlLabel, Checkbox, MenuItem, Card, CardMedia, Select, RadioGroup, Radio, Dialog } from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';
import { CheckCircleOutline, ArrowBackIos } from '@material-ui/icons';


import { OpenModal, CloseModal, callNativeFunction } from '../helper/appfunction';
import { FPTextField, FPTextFieldButton, DialogAppbar } from '../components/parts';
import TermsDialog from './terms.dialog'


// 커스텀 탭 디자인
const FPTabs = withStyles({
    indicator: {
        height: "3px",
        backgroundColor: "#ff7704"
    }
})(Tabs);

const FPTab = withStyles(theme => ({
    selected: {
        color: '#333333',
        fontWeight: '500',
    }
}))(props => <Tab disableRipple {...props} />);

// 탭 패널
class TabPanel extends Component {
    render() {
        const { children, value, index, ...other } = this.props;

        return (
            <div style={{ marginTop: '20px', marginBottom: '16px' }}>
                {value === index && <Box>{children}</Box>}
            </div>
        );
    }
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

export class RegisterDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            phoneNumber: '',    // 사용자 핸드폰 번호
            smsCode: '',        // SMS 인증 코드
            displayName: '',    // 사용자 이름
            email: '',          // 사용자 이메일
            userName: '',       // 사용자 아이디
            password: '',       // 암호
            passwordValidate: '',   // 암호 확인
            deviceID: '',       // 임시 디바이스 ID
            school: '',         // 학교
            grade: 1,           // 학년
            accountType: 'S',   // 회원유형
            gender: 0,         // 성별
            areaName: '',       // 거주지역
            joinCase: 0,        // 알게된 경로
            joinCaseEtc: "",    // 알게된 경로 기타
            hp_p: "",
            hp_s: "",
            schooltype: "H",

            univ1: '',
            mode1: '',
            department1: '',
            univ2: '',
            mode2: '',
            department2: '',
            univ3: '',
            mode3: '',
            department3: '',

            privateAgree1: false,   // 개인정보 수집 동의
            privateAgree2: false,   // 개인정보 제3자 제공 동의

            tabState: 0,         // 탭 상태
            smsauthState: false,    // 본인 인증 상태
            idDuplicateState: false, // ID 중복체크

            terms: "private",   // 약관 정보
        }

        this.dialogRef = React.createRef();

        this.joinCaseArray = [{
            name: "금천구청 홈페이지", value: 1
        }, {
            name: "금천구 소식지", value: 2
        }, {
            name: "학교 소개 및 가정통신문", value: 4
        }, {
            name: "배치 자료집", value: 8
        }, {
            name: "현수막", value: 16
        }, {
            name: "전단지 또는 포스터", value: 32
        }, {
            name: "지인 소개", value: 64
        }];
    };


    a11yProps = (index) => {
        return {
            id: `register-tab-${index}`,
            'aria-controls': `register-tabpanel-${index}`,
        };
    }

    // SMS 코드 요청
    requestSMSCode = () => {
        this.props.onBackDrop(true, "요청 중...");
        this.props.onRequest('api/anonymous/', 'post', {
            type: 'smsrequest',
            phonenumber: this.state.phoneNumber,
        }, true).then((result) => {
            if (result.error == null) {
                this.props.onAlert({
                    message: "SMS가 전송되었습니다."
                });
                this.setState({
                    deviceID: result.deviceid,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // SMS 코드 확인
    requestSMSCodeValidate = () => {
        this.props.onBackDrop(true, "확인 중...");
        this.props.onRequest('api/anonymous/', 'post', {
            type: 'smscert',
            deviceid: this.state.deviceID,
            phonenumber: this.state.phoneNumber,
            code: this.state.smsCode
        }, true).then((result) => {
            if (result.error == null && result.deviceid == this.state.deviceID) {
                this.props.onAlert({
                    message: '인증 완료 되었습니다.'
                });
                this.setState({
                    smsauthState: true,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // ID 중복 체크
    requestIDDuplicate = () => {
        if (!/^[a-zA-Z0-9]{4,30}$/.test(this.state.userName)) {
            this.props.onAlert({
                message: "ID는 4자리 이상의 영문 및 영문과 숫자만 사용가능합니다.",
            });
            return;
        }

        this.props.onBackDrop(true, "확인 중...");
        this.props.onRequest('api/account?id=' + this.state.userName, 'get', null, true).then((result) => {
            if (result.error == null) {
                this.props.onAlert({
                    message: '사용할 수 있는 ID입니다.'
                });
                this.setState({
                    idDuplicateState: true,
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // 계정 생성 요청
    requestAccountCreate = () => {
        const univ = [];
        if (this.state.univ1.trim() != "" || this.state.mode1.trim() != "" || this.state.department1.trim() != "") {
            univ.push({
                rank: 1,
                univ: this.state.univ1,
                mode: this.state.mode1,
                department: this.state.department1
            });
        }
        if (this.state.univ2.trim() != "" || this.state.mode2.trim() != "" || this.state.department2.trim() != "") {
            univ.push({
                rank: 2,
                univ: this.state.univ2,
                mode: this.state.mode2,
                department: this.state.department2
            });
        }
        if (this.state.univ3.trim() != "" || this.state.mode3.trim() != "" || this.state.department3.trim() != "") {
            univ.push({
                rank: 3,
                univ: this.state.univ3,
                mode: this.state.mode3,
                department: this.state.department3
            });
        }

        this.props.onBackDrop(true, "생성 중...");
        this.props.onRequest('api/account/', 'put', {
            deviceid: this.state.deviceID,
            code: this.state.smsCode,
            hp: this.state.phoneNumber,

            id: this.state.userName,
            email: this.state.email,
            pwd: this.state.password,
            name: this.state.displayName,
            school: this.state.school,
            grade: this.state.grade,
            type: this.state.accountType,
            area: this.state.areaName,
            gender: this.state.gender,
            hope: univ,
            joincase: this.state.joinCase,
            joincaseetc: this.state.joinCaseEtc,
            hp_p: this.state.accountType == "S" ? this.state.hp_p : null,
            hp_s: this.state.accountType == "P" ? this.state.hp_s : null,
            schooltype: this.state.schooltype
        }, true).then((result) => {
            if (result.error == null) {
                this.setState((prevState) => ({
                    tabState: prevState.tabState + 1
                }))
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    // TextField 입력 시 이벤트
    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.type == "checkbox" ? e.target.checked : e.target.value;
        this.setState(nextState);
    }

    // 약관 팝업 열기
    handleTermsOpen = (value) => {
        this.setState({
            termsDialog: true,
            terms: value
        });
    }

    // 약관 팝업 닫기
    handleTermsClose = () => {
        this.setState({
            termsDialog: false
        });
    }

    // 탭 컨텐츠 스와이프 이벤트
    handleSwipeChangeIndex = (idx, fromIndex) => {
        console.log("swipe", idx, this.el, this);
        //this.setState({
        //    tabState: idx
        //});

        const children = Array.from(this.el.containerNode.children);
        children[idx].scrollTop = 0;
    };

    // 다음 스탭
    nextTab = (idx) => {
        const emailRegex = /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
        const phoneRegex = /^01(?:0|1|[6-9])[.-]?(\d{3}|\d{4})[.-]?(\d{4})$/;
        // 정보 입력 체크
        if (this.state.tabState == 0) {
            if (!this.state.smsauthState) {
                this.props.onAlert({
                    message: "핸드폰 인증이 필요합니다."
                });
                return;
            }
            else if (!emailRegex.test(this.state.email)) {
                this.props.onAlert({
                    message: "잘못 된 Email 주소 입니다."
                });
                return;
            }
            else if (!/^[a-zA-Z0-9]{4,30}$/.test(this.state.userName)) {
                this.props.onAlert({
                    message: "ID는 4자리 이상의 영문 및 영문과 숫자만 사용가능합니다.",
                });
                return;
            }
            else if (!this.state.idDuplicateState) {
                this.props.onAlert({
                    message: "ID 중복 확인이 필요합니다."
                });
                return;
            }
            else if (this.state.password != this.state.passwordValidate || this.state.password == "") {
                this.props.onAlert({
                    message: "비밀번호가 일치하지 않습니다."
                });
                return;
            }
            else if (this.state.displayName == "") {
                this.props.onAlert({
                    message: "이름을 입력해 주세요."
                });
                return;
            }
            else if (this.state.areaName == "") {
                this.props.onAlert({
                    message: "거주지역을 입력해 주세요."
                });
                return;
            }
            else if (this.state.school == "") {
                this.props.onAlert({
                    message: "학교를 입력해 주세요."
                });
                return;
            }
            else if (this.state.schooltype == "") {
                this.props.onAlert({
                    message: "학교 구분(중학교 또는 고등학교)를 선택해 주세요."
                });
                return;
            }
            else if (!this.state.privateAgree1) {
                this.props.onAlert({
                    message: "개인정보 수집 동의에 동의해주세요."
                });
                return;
            }
            else if (!this.state.privateAgree2) {
                this.props.onAlert({
                    message: "개인정보 제3자 제공 동의에 동의해주세요."
                });
                return;
            }
            else if (this.state.joinCase == 0) {
                this.props.onAlert({
                    message: "신청경로를 선택해 주세요."
                });
                return;
            }

            this.setState({
                tabState: idx + 1,
            });
        }
        else if (this.state.tabState == 1) {

            if (this.state.univ1.trim() == "" && this.state.univ2.trim() == "" && this.state.univ3.trim() == "") {
                this.props.onAlert({
                    message: "희망학교는 1개 이상 입력하세요."
                });
                return;
            }

            this.requestAccountCreate();
            return;
        }

        this.setState({
            tabState: idx + 1,
        });

        // 상단 스크롤
        const children = Array.from(this.dialogRef.current.children);
        children[2].children[0].scrollTop = 0;
    }

    handleGenderRadio = (value) => {
        this.setState({
            gender: value
        });
    }

    handleChangeJoinCase = (value) => {
        let joinValue = this.state.joinCase;

        if (joinValue & value) {
            joinValue -= value;
        }
        else {
            joinValue += value;
        }

        this.setState({
            joinCase: joinValue
        });
    };

    handleChangeJoinEtc = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        nextState.joinCase = this.state.joinCase;

        if (e.target.value == "") {
            if (nextState.joinCase & 128) {
                nextState.joinCase -= 128;
            }
        }
        else {
            if (!(nextState.joinCase & 128)) {
                nextState.joinCase += 128;
            }
        }

        this.setState(nextState);

    }

    handleDialogClose = () => {
        CloseModal();
        this.props.onClose();
    };

    handleDialogEnter = () => {
        OpenModal(this.handleDialogClose);
    };

    render() {
        // 본인 정보 입력
        const tabstate1 = (
            <Container component="main" maxWidth="xl" >


                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        회원유형
                    </Box>
                    <Box bgcolor="#fff" border={1} borderColor="#dadada" flexGrow={1}>
                        <Select native fullWidth value={this.state.accountType} onChange={this.handleChange} name="accountType" >
                            <option value="S">학생</option>
                            <option value="P">학부모</option>
                        </Select>
                    </Box>
                </Box>

                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        성별
                    </Box>
                    <Box border={1} borderTop={0} pl={1.5} pr={1.5} pt={0.5} pb={0.5} bgcolor="#fff" borderColor="#dadada" flexGrow={1}>
                        <FormControlLabel value={0} control={<Radio checked={this.state.gender == 0} onChange={() => this.handleGenderRadio(0)} />} label="미선택" style={{ display: "none" }} />
                        <FormControlLabel value={1} control={<Radio checked={this.state.gender == 1} onChange={() => this.handleGenderRadio(1)} />} label="남자" />
                        <FormControlLabel value={2} control={<Radio checked={this.state.gender == 2} onChange={() => this.handleGenderRadio(2)} />} label="여자" />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        아이디
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 82px)" }}>
                        <FPTextFieldButton name="userName" label="" value={this.state.userName} onChange={this.handleChange} buttonName="중복확인" onClick={this.requestIDDuplicate} />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        비밀번호
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                        <FPTextField name="password" label="" value={this.state.password} onChange={this.handleChange} type="password" />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                        비밀번호 확인
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                        <FPTextField name="passwordValidate" label="" value={this.state.passwordValidate} onChange={this.handleChange} type="password" />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        이름
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                        <FPTextField name="displayName" label="" value={this.state.displayName} onChange={this.handleChange} />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        핸드폰
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 82px)" }}>
                        <FPTextFieldButton name="phoneNumber" label="핸드폰 번호(숫자만 입력)" value={this.state.phoneNumber} onChange={this.handleChange} buttonName="전송" onClick={this.requestSMSCode} />
                    </Box>
                </Box>
                {this.state.deviceID != "" ? (
                    <Box display="flex" alignItems="center" >
                        <Box width={80} fontWeight={600} textAlign="center" >
                            인증번호
                    </Box>
                        <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 82px)" }}>
                            <FPTextFieldButton name="smsCode" label="인증번호 6자리" value={this.state.smsCode} onChange={this.handleChange} buttonName="확인" onClick={this.requestSMSCodeValidate} />
                        </Box>
                    </Box>
                ) : null}
                {
                    this.state.accountType == "P" ? (<Box display="flex" alignItems="center" >
                        <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                            학생 핸드폰
                    </Box>
                        <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                            <FPTextField name="hp_s" label="" value={this.state.hp_s} onChange={this.handleChange} />
                        </Box>
                    </Box>) : (<Box display="flex" alignItems="center" >
                        <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                            학부모 핸드폰
                    </Box>
                        <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                            <FPTextField name="hp_p" label="" value={this.state.hp_p} onChange={this.handleChange} />
                        </Box>
                    </Box>)
                }


                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        이메일
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                        <FPTextField name="email" label="" value={this.state.email} onChange={this.handleChange} />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        거주지역
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1}>
                        <FPTextField name="areaName" label="" value={this.state.areaName} onChange={this.handleChange} />
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        학교
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 156px)" }}>
                        <FPTextField name="school" label="" value={this.state.school} onChange={this.handleChange} />
                    </Box>
                    <Box bgcolor="#fff" border={1} borderLeft={0} borderTop={0} borderColor="#dadada">
                        <Select native fullWidth value={this.state.grade} onChange={this.handleChange} name="grade" style={{ paddingRight: "12px" }} >
                            <option value="1">1학년</option>
                            <option value="2">2학년</option>
                            <option value="3">3학년</option>
                        </Select>
                    </Box>
                </Box>
                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        학교구분
                            </Box>
                    <Box border={1} borderTop={0} pl={1.5} pr={1.5} pt={0.5} pb={0.5} bgcolor="#fff" borderColor="#dadada" flexGrow={1}>
                        <FormControlLabel value="H" control={<Radio checked={this.state.schooltype == "H"} onChange={this.handleChange} name="schooltype" value="H" />} label="고등학교" />
                        <FormControlLabel value="M" control={<Radio checked={this.state.schooltype == "M"} onChange={this.handleChange} name="schooltype" value="M" />} label="중학교" />
                    </Box>
                </Box>

                <Box display="flex" alignItems="center" >
                    <Box width={80} fontWeight={600} textAlign="center" >
                        신청경로
                    </Box>
                    <Box bgcolor="#fff" border={1} borderTop={0} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 82px)" }}>
                        <Box p={1}>
                            본 프로그램을 알게 된 경로는?
                        </Box>
                        <Box>
                            {this.joinCaseArray.map((item, index) => (
                                <FormControlLabel key={index} value="end" label={item.name} labelPlacement="end" style={{ color: '#010101', minWidth: "120px", margin: "0px" }} control={<Checkbox checked={(this.state.joinCase & item.value) != 0} onChange={() => this.handleChangeJoinCase(item.value)} name="" color="primary" />} />
                            ))}
                        </Box>
                        <Box borderTop={1} borderColor="#dadada">
                            <FPTextField name="joinCaseEtc" label="기타" value={this.state.joinCaseEtc} onChange={this.handleChangeJoinEtc} />
                        </Box>
                    </Box>
                </Box>

                <Box display="flex" pt={1} alignItems="center" >
                    <Box flexGrow={1}>
                        <FormControlLabel value={this.state.privateAgree1} label="[필수] 개인정보 수집" labelPlacement="end" style={{ paddingLeft: '12px', color: this.state.privateAgree1 ? "#ff7704" : "#9A9A9A" }} control={
                            <div>
                                <CheckCircleOutline style={{ padding: "8px 8px 0px 8px", fill: this.state.privateAgree1 ? "#ff7704" : "#9A9A9A" }} />
                                <Box display="none">
                                    <Checkbox checked={this.state.privateAgree1} onChange={this.handleChange} name="privateAgree1" />
                                </Box>
                            </div>
                        } />
                    </Box>
                    <Box p={1} pt={1.5} onClick={() => this.handleTermsOpen("private")} color="#FF7C27">
                        [보기]
                    </Box>
                </Box>

                <Box display="flex" pt={1} mb={2} alignItems="center" >
                    <Box flexGrow={1}>
                        <FormControlLabel value={this.state.privateAgree2} label="[필수] 개인정보 제3자 제공" labelPlacement="end" style={{ paddingLeft: '12px', color: this.state.privateAgree2 ? "#ff7704" : "#9A9A9A" }} control={
                            <div>
                                <CheckCircleOutline style={{ padding: "8px 8px 0px 8px", fill: this.state.privateAgree2 ? "#ff7704" : "#9A9A9A" }} />
                                <Box display="none">
                                    <Checkbox checked={this.state.privateAgree2} onChange={this.handleChange} name="privateAgree2" />
                                </Box>
                            </div>
                        } />
                    </Box>
                    <Box p={1} pt={1.5} onClick={() => this.handleTermsOpen("offer")} color="#FF7C27">
                        [보기]
                    </Box>
                </Box>

                <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={() => this.nextTab(this.state.tabState)}
                >
                    다음
                </Button>

            </Container>
        );

        // 정보입력
        const tabstate2 = (
            <div>
                <Container component="main" maxWidth="xl" >
                    <Typography variant="subtitle1" gutterBottom style={{ color: "#ff7704" }} >
                        *희망학교는 1개이상 입력하세요.
                    </Typography>
                </Container>
                <Box p={2} borderBottom={1} fontWeight={600} >
                    1지망
                </Box>
                <Box p={2} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="univ1" label="" value={this.state.univ1} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        대학
                    </Box>
                </Box>
                <Box p={2} pt={0} pb={0} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="mode1" label="" value={this.state.mode1} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        전형
                    </Box>
                </Box>
                <Box p={2} bgcolor="#fff" display="flex" borderBottom={1} fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="department1" label="" value={this.state.department1} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        학과
                    </Box>
                </Box>

                <Box p={2} borderBottom={1} mt={1} fontWeight={600}>
                    2지망
                </Box>
                <Box p={2} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="univ2" label="" value={this.state.univ2} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        대학
                    </Box>
                </Box>
                <Box p={2} pt={0} pb={0} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="mode2" label="" value={this.state.mode2} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        전형
                    </Box>
                </Box>
                <Box p={2} bgcolor="#fff" display="flex" borderBottom={1} fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="department2" label="" value={this.state.department2} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        학과
                    </Box>
                </Box>

                <Box p={2} borderBottom={1} mt={1} fontWeight={600}>
                    3지망
                </Box>
                <Box p={2} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="univ3" label="" value={this.state.univ3} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        대학
                    </Box>
                </Box>
                <Box p={2} pt={0} pb={0} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="mode3" label="" value={this.state.mode3} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        전형
                    </Box>
                </Box>
                <Box p={2} bgcolor="#fff" display="flex" borderBottom={1} fontSize={15} alignItems="center" >
                    <Box border={1} flexGrow={1} color="#dadada">
                        <FPTextField name="department3" label="" value={this.state.department3} onChange={this.handleChange} />
                    </Box>
                    <Box pl={2}>
                        학과
                    </Box>
                </Box>

                <Container component="main" maxWidth="xl" >
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        onClick={() => this.nextTab(this.state.tabState)}
                        style={{ marginTop: "16px" }}
                    >
                        다음
                    </Button>
                </Container>
            </div>
        );

        // 가입완료
        const tabstate3 = (
            <div>
                <Container component="main" maxWidth="xl" >
                    <Box mb={2.5}>
                        <Typography variant="subtitle1" align="center">
                            축하합니다.
                       </Typography>
                        <Typography variant="subtitle1" align="center">
                            가입이 완료되었습니다.
                    </Typography>
                    </Box>
                    <Box border={1} borderBottom={0} borderColor="#DBDBDB" bgcolor="#fff" p={1.5}>
                        <Grid container spacing={0}>
                            <Box width={80}>
                                <Typography variant="subtitle2">
                                    이름
                                </Typography>
                            </Box>
                            <Box>
                                <Typography variant="subtitle1">
                                    {this.state.displayName}
                                </Typography>
                            </Box>
                        </Grid>
                    </Box>
                    <Box border={1} borderBottom={0} borderColor="#DBDBDB" bgcolor="#fff" p={1.5}>
                        <Grid container spacing={0}>
                            <Box width={80}>
                                <Typography variant="subtitle2">
                                    아이디
                                </Typography>
                            </Box>
                            <Box>
                                <Typography variant="subtitle1">
                                    {this.state.userName}
                                </Typography>
                            </Box>
                        </Grid>
                    </Box>
                    <Box border={1} borderBottom={0} borderColor="#DBDBDB" bgcolor="#fff" p={1.5}>
                        <Grid container spacing={0}>
                            <Box width={80}>
                                <Typography variant="subtitle2">
                                    이메일
                                </Typography>
                            </Box>
                            <Box>
                                <Typography variant="subtitle1">
                                    {this.state.email}
                                </Typography>
                            </Box>
                        </Grid>
                    </Box>
                    <Box border={1} borderColor="#DBDBDB" bgcolor="#fff" p={1.5} mb={2.5}>
                        <Grid container spacing={0}>
                            <Box width={80}>
                                <Typography variant="subtitle2">
                                    핸드폰
                                </Typography>
                            </Box>
                            <Box>
                                <Typography variant="subtitle1">
                                    {this.state.phoneNumber}
                                </Typography>
                            </Box>
                        </Grid>
                    </Box>

                    <div>
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={this.handleDialogClose}
                        >
                            확인
                        </Button>
                    </div>
                </Container>
            </div>
        );

        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleDialogClose}
                fullScreen
                onEnter={this.handleDialogEnter}
                disabled={true}
                ref={this.dialogRef}
            >
                <div>
                    <DialogAppbar title="회원가입" onClose={this.handleDialogClose} />
                    <FPTabs value={this.state.tabState} onChange={this.handleTabChangeIndex} aria-label="simple tabs example" variant="fullWidth" >
                        <FPTab label="정보입력" {...this.a11yProps(0)} disabled={this.state.tabState != 0} />
                        <FPTab label="희망학교" {...this.a11yProps(1)} disabled={this.state.tabState != 1} />
                        <FPTab label="가입완료" {...this.a11yProps(2)} disabled={this.state.tabState != 2} />
                    </FPTabs>
                    <SwipeableViews
                        disabled={true}
                        index={this.state.tabState}
                    >
                        <TabPanel value={this.state.tabState} index={0} >
                            {tabstate1}
                        </TabPanel>
                        <TabPanel value={this.state.tabState} index={1} >
                            {tabstate2}
                        </TabPanel>
                        <TabPanel value={this.state.tabState} index={2} >
                            {tabstate3}
                        </TabPanel>
                    </SwipeableViews>

                    <TermsDialog open={this.state.termsDialog} onClose={this.handleTermsClose} item={this.state.terms} />
                </div>
            </Dialog>
        )
    };
};

RegisterDialog.propTypes = {
    open: PropTypes.bool,
    onAlert: PropTypes.func,
    onClose: PropTypes.func,
    onBackDrop: PropTypes.func,
};

RegisterDialog.defaultProps = {
    open: false,
    onAlert: () => { console.info("alert function is not defined"); },
    onClose: () => { console.info("close function is not defined"); },
    onBackDrop: () => { console.info("backdrop function is not defined"); },
};

export default RegisterDialog