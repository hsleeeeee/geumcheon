import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, Grid, DialogTitle, DialogContent, DialogActions, Container, Backdrop, Tabs, Tab, Box, Select, Typography, FormControlLabel, Checkbox } from '@material-ui/core';
import { EventAvailable, CheckCircleOutline } from '@material-ui/icons';
import SwipeableViews from 'react-swipeable-views';
import CalendarComp from 'react-calendar';

import { FPTextField, FPTextFieldButton, DialogAppbar, FPTimePicker, FPTextArea } from '../components/parts';
import { OpenModal, CloseModal, GetUserInfo, ajaxWebAPI } from '../helper/appfunction';

// 커스텀 탭 디자인
const FPTabs = withStyles({
    indicator: {
        height: "3px",
        backgroundColor: "#ff7704"
    }
})(Tabs);

const FPTab = withStyles(theme => ({
    selected: {
        color: '#333333',
        fontWeight: '500',
    }
}))(props => <Tab disableRipple {...props} />);

// 탭 패널
class TabPanel extends Component {
    render() {
        const { children, value, index, ...other } = this.props;

        return (
            <div style={{ marginTop: '20px', marginBottom: '16px' }}>
                {value === index && <Box onClick={() => this.props.onTab(index)} > { children }</Box>}
            </div>
        );
    }
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
    onTab: PropTypes.func
};

class RequestDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tabState: 0,

            timeIdx: 0,
            consultTime: [],
            calendarPiekcerDialog: false,
            calendarDate: new Date(),

            name: "",
            studentName: "",
            phone: "",
            email: "",
            school: "",
            grade: 0,
            address: "",
            title: "",
            univ: [],
            progress: "R",
            renote: "",

            score: new Array(17),
            activity: "",
            content: "",

            reply: "",
            starRating: -1,

            needvalue1: "",
            needvalue2: ""
        }

        this.ratingString = ["매우 도움이 되었다.","조금 도움이 되었다.","보통이다.","조금 도움이 되지 않았다.","매우 도움이 되지 않았다."];
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.noti) {
            console.log("onNoti");
            this.funcInNotification(nextProps.item.RequestIdx);
            this.props.onNoti();
        }
    }

    // 탭 컨텐츠 스와이프 이벤트
    handleSwipeChangeIndex = (idx) => {
        this.setState({
            tabState: idx
        });
    };

    handleTabClick = (event, newValue) => {
        this.setState({
            tabState: newValue
        });
    };

    a11yProps = (index) => {
        return {
            id: `register-tab-${index}`,
            'aria-controls': `register-tabpanel-${index}`,
        };
    }

    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.type == "checkbox" ? e.target.checked : e.target.value;
        this.setState(nextState);
    }

    handleChangeArray = (index, e) => {
        const tempuniv = [...this.state.univ];

        tempuniv[index][e.target.name] = e.target.value;

        this.setState({
            univ: tempuniv
        });
    }

    handleChangeScore = (index, e) => {
        const nextState = [...this.state.score];
        nextState[index] = e.target.value;

        this.setState({
            score: nextState
        });
    }

    handleChangeTextArea = (name, value) => {
        const nextState = {};
        nextState[name] = value;
        this.setState(nextState);
    }

    // 일자 선택
    handleDateChange = (value) => {
        this.setState({
            calendarDate: value
        });
    }

    handleCalendarPicker = () => {
        if (this.props.mode == "detail") {
            return;
        }
        else {
            this.setState({
                calendarPiekcerDialog: true,
            });
        }
    }

    // 캘린더 선택 또는 백드롭 클릭시 닫기
    handleCalendarPickerClose = (value) => {
        this.setState({
            calendarPiekcerDialog: false
        });
    }

    // 달력 일 클릭시
    handleDayClick = (date) => {
        this.props.onBackDrop(true, "확인 중...");
        this.props.onRequest('api/Schedule', 'post', {
            type: "time",
            way: this.props.way,
            date: date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
        }).then((result) => {
            console.log(result);
            if (result.error == null) {
                this.setState({
                    consultTime: result.entities || [],
                    calendarDate: date,
                    calendarPiekcerDialog: false,
                    timeIdx: (result.entities[0] || { ConsultTimeIdx: 0 }).ConsultTimeIdx
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    handleCalendarToString = () => {
        let year = this.state.calendarDate.getFullYear();
        let month = this.state.calendarDate.getMonth() + 1;
        let day = this.state.calendarDate.getDate();
        let week = "월";
        switch (this.state.calendarDate.getDate()) {
            case 0: week = "일"; break;
            case 1: week = "월"; break;
            case 2: week = "화"; break;
            case 3: week = "수"; break;
            case 4: week = "목"; break;
            case 5: week = "금"; break;
            case 6: week = "토"; break;
        }

        return year.toString() + "-" + (month < 10 ? "0" : "") + month.toString() + "-" + (day < 10 ? "0" : "") + day.toString() + "(" + week + ")";
    }

    handletileDisabled = ({ date, view }) => {
        if (view == "month") {
            const compareDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
            if (this.props.date.some(item => compareDate === item)) {
                return false;
            }
            else {
                return true;
            }
        }
    }

    convertRequestString = () => {
        if (this.props.mode == "request") {
            if (this.props.way == "O") {
                if (this.state.tabState == 0 || this.state.tabState == 1) {
                    return "다음";
                }
                else {
                    return "신청";
                }
            }
            else {
                if (this.state.tabState == 0) {
                    return "다음";
                }
                else {
                    return "신청";
                }
            }
        }
        else {
            return "신청";
        }
    }

    handleDialogModify = () => {
        if (this.state.progress != "R" && this.state.progress != "W") {
            this.props.onAlert({
                message: "담당 선생님 배정 후에는 수정할 수 없습니다."
            });
        }
        else {
            this.props.onClose("modify");
        }
    }

    handleDialogClose = (value) => {
        if (value) {
            
            if (this.state.title.trim() == "") {
                this.props.onAlert({
                    message: "제목을 입력해 주세요."
                });
                this.setState({
                    tabState: 0
                });
                return;
            }
            else if (this.state.school == "") {
                this.props.onAlert({
                    message: "학교를 입력해 주세요."
                });
                this.setState({
                    tabState: 0
                });
                return;
            }
            else if (this.state.address == "") {
                this.props.onAlert({
                    message: "거주지역을 입력해 주세요."
                });
                this.setState({
                    tabState: 0
                });
                return;
            }
            else if (this.props.way == "V" && this.state.timeIdx < 1) {
                this.props.onAlert({
                    message: "상담 희망 시간을 선택해 주세요."
                });
                this.setState({
                    tabState: 0
                });
                return;
            }

            if (this.props.mode == "request" && this.state.tabState == 0) {
                this.setState({
                    tabState: 1
                });
                return
            }
            if (this.state.tabState == 1) {
                let needuniv = false;
                this.state.univ.forEach((item) => {
                    if (item.UniversityName != "") {
                        needuniv = true;
                    }
                });

                if (!needuniv) {
                    this.props.onAlert({
                        message: "희망학교는 1개 이상 입력하세요."
                    });
                    this.setState({
                        tabState: 1
                    });
                    return;
                }

                if (this.props.way == "O" && this.props.mode == "request") {
                    this.setState({
                        tabState: 2
                    });
                    return
                }
            }
            else if (this.state.tabState == 2) {
                if (this.props.way == "O" && this.state.activity == "") {
                    this.props.onAlert({
                        message: "본인에게 의미 있었던 활동을 적어주세요."
                    });
                    this.setState({
                        tabState: 2
                    });
                    return;
                }
                else if (this.props.way == "O" && this.state.content == "") {
                    this.props.onAlert({
                        message: "본 상담을 통해 해결하고 싶거나 궁금한 점을 적어주세요."
                    });
                    this.setState({
                        tabState: 2
                    });
                    return;
                }
            }
            

            this.props.onBackDrop(true, "저장 중...");
            this.props.onRequest('api/entrance', 'put', {
                pwd: this.state.password,
                school: this.state.school,
                grade: this.state.grade,
                address: this.state.address,
                hope: this.state.univ,


                requestidx: this.props.item.RequestIdx,
                consulttime: this.state.timeIdx,
                consultdate: this.state.calendarDate.getFullYear() + "-" + (this.state.calendarDate.getMonth() + 1) + "-" + this.state.calendarDate.getDate(),
                type: this.props.way,
                title: this.state.title,
                score: this.props.way == "O" ? JSON.stringify([...this.state.score, this.state.activity, this.state.content]) : "[]",
                studentname: this.state.studentName == "" ? this.state.name : this.state.studentName,
                areaname: this.state.address,
                eidx: this.props.eidx,
                renote: this.state.renote,
                needvalue1: this.state.needvalue1,
                needvalue2: this.state.needvalue2
            }).then((result) => {
                if (result.error == null) {
                    this.props.onAlert({
                        message: "저장되었습니다.",
                    }, function () { CloseModal(); this.props.onClose(true); }.bind(this, CloseModal));
                }
                else {
                    this.props.onAlert({
                        message: result.error,
                    });
                }
            });
        }
        else {
            if (this.props.mode != "detail") {
                this.props.onAlert({
                    mode: "yesorno",
                    message: "저장하지 않은 내용은 저장되지 않습니다. 닫으시겠습니까?"
                }, function (bind, value) {
                        if (value) {
                        CloseModal();
                        this.props.onClose(false);
                        }
                    }.bind(this, CloseModal));
            }
            else {
                CloseModal();
                this.props.onClose(false);
            }
        }
    }

    handleRatingClick = (value) => {
        ajaxWebAPI("api/Reply", "put", {
            RequestIdx: this.props.item.RequestIdx,
            Satisfaction: value
        });

        this.setState({
            starRating: value
        });
    }

    funcInNotification = (idx) => {
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/Entrance?requestIdx=' + idx, 'get').then((result) => {
            const nextState = {};
            if (result.error == null) {
                nextState.name = result.Member.Name || "";
                nextState.email = result.Member.Email || "";
                nextState.school = result.Member.School || "";
                nextState.grade = result.Member.Grade || "";
                nextState.address = result.Consult.AreaName || "";
                nextState.phone = result.Member.Hp || "";
                nextState.studentName = result.Consult.StudentName || "";
                nextState.title = result.Consult.Title || "";
                nextState.score = JSON.parse(result.Consult.ScoreHtml);
                nextState.activity = nextState.score[17] || "";
                nextState.content = nextState.score[18] || "";
                nextState.reply = result.ReplyNote || "";
                nextState.progress = result.Consult.Progress;
                nextState.needvalue1 = result.NeedValue1;
                nextState.needvalue2 = result.NeedValue2;

                if (this.props.way != "O") {
                    nextState.timeIdx = result.Schedule.ConsultTimeIdx;
                    nextState.time = result.Schedule.ConsultTime;
                    nextState.date = result.Schedule.ConsultDate;

                    let calDate = new Date();
                    calDate.setFullYear(nextState.date.split('-')[0]);
                    calDate.setMonth(parseInt(nextState.date.split('-')[1]) - 1);
                    calDate.setDate(nextState.date.split('-')[2]);

                    nextState.calendarDate = calDate;
                }

                const tempuniv = [];
                for (let i = 0; i < 3; i++) {
                    tempuniv.push({
                        ApplyUnivIdx: 0,
                        Ranking: i + 1,
                        UniversityName: "",
                        StypeRem: "",
                        MajorName: ""
                    });
                }

                result.Univ.forEach((item) => {
                    tempuniv[item.Ranking - 1] = item;
                });

                nextState.univ = tempuniv;
            }
            else {
                this.props.onAlert({
                    message: result.error
                }, function () { this.handleDialogClose(false) }.bind(this));
            }

            this.setState(nextState);

            if (this.props.way != "O") {
                this.props.onRequest('api/Schedule', 'post', {
                    type: "time",
                    way: this.props.way,
                    date: nextState.date
                }).then((result) => {
                    console.log(result);
                    if (result.error == null) {
                        this.setState({
                            consultTime: result.entities || [],
                            calendarPiekcerDialog: false,
                            timeIdx: (result.entities[0] || { ConsultTimeIdx: 0 }).ConsultTimeIdx
                        });
                    }
                    else {
                        this.props.onAlert({
                            message: result.error
                        });
                    }
                });
            }
        });
    }

    handleDialogEnter = () => {
        this.props.onBackDrop(true, "불러오는 중...");

        if (this.props.mode == "request") {
            this.props.onRequest('api/Account', 'post').then((result) => {
                const nextState = {};
                if (result.error == null) {
                    nextState.name = result.Name || "";
                    nextState.email = result.Email || "";
                    nextState.school = result.School || "";
                    nextState.grade = result.Grade || "";
                    nextState.address = result.Address || "";
                    nextState.phone = result.Hp || "";

                    const tempuniv = [];
                    for (let i = 0; i < 3; i++) {
                        tempuniv.push({
                            ApplyUnivIdx: 0,
                            Ranking: i + 1,
                            UniversityName: "",
                            StypeRem: "",
                            MajorName: ""
                        });
                    }

                    result.Univ.forEach((item) => {
                        tempuniv[item.Ranking - 1] = item;
                    });

                    nextState.univ = tempuniv;
                }
                else {
                    this.props.onAlert({
                        message: result.error
                    });
                }

                this.setState(nextState);
            });
        }
        else {
            ajaxWebAPI('api/Entrance?requestIdx=' + this.props.item.RequestIdx, 'get').then((result) => {
                this.props.onBackDrop(false);
                const nextState = {};
                if (result.error == null) {
                    nextState.name = result.Member.Name || "";
                    nextState.email = result.Member.Email || "";
                    nextState.school = result.Member.School || "";
                    nextState.grade = result.Member.Grade || "";
                    nextState.address = result.Consult.AreaName || "";
                    nextState.phone = result.Member.Hp || "";
                    nextState.studentName = result.Consult.StudentName || "";
                    nextState.title = result.Consult.Title || "";
                    nextState.score = JSON.parse(result.Consult.ScoreHtml);
                    nextState.activity = nextState.score[17] || "";
                    nextState.content = nextState.score[18] || "";
                    nextState.reply = result.ReplyNote || "";
                    nextState.progress = result.Consult.Progress;
                    nextState.starRating = result.StarRating;
                    nextState.renote = result.Re_Note;
                    nextState.needvalue1 = result.NeedValue1;
                    nextState.needvalue2 = result.NeedValue2;

                    if (this.props.way == "V") {
                        nextState.timeIdx = result.Schedule.ConsultTimeIdx;
                        nextState.time = result.Schedule.ConsultTime;
                        nextState.date = result.Schedule.ConsultDate;

                        let calDate = new Date();
                        calDate.setFullYear(nextState.date.split('-')[0]);
                        calDate.setMonth(parseInt(nextState.date.split('-')[1]) - 1);
                        calDate.setDate(nextState.date.split('-')[2]);

                        nextState.calendarDate = calDate;
                    }

                    const tempuniv = [];
                    for (let i = 0; i < 3; i++) {
                        tempuniv.push({
                            ApplyUnivIdx: 0,
                            Ranking: i + 1,
                            UniversityName: "",
                            StypeRem: "",
                            MajorName: ""
                        });
                    }

                    result.Univ.forEach((item) => {
                        tempuniv[item.Ranking - 1] = item;
                    });

                    nextState.univ = tempuniv;
                }
                else {
                    this.props.onAlert({
                        message: result.error
                    }, function () { this.handleDialogClose(false) }.bind(this));
                }

                this.setState(nextState);

                if (this.props.way == "V") {
                    this.props.onRequest('api/Schedule', 'post', {
                        type: "time",
                        way: this.props.way,
                        date: nextState.date
                    }).then((result) => {
                        console.log(result);
                        if (result.error == null) {
                            this.setState({
                                consultTime: result.entities || [],
                                calendarPiekcerDialog: false,
                                timeIdx: (result.entities[0] || { ConsultTimeIdx: 0 }).ConsultTimeIdx
                            });
                        }
                        else {
                            this.props.onAlert({
                                message: result.error
                            });
                        }
                    });
                }
            });
        }

        OpenModal(this.handleDialogClose);
    }

    render() {
        const tabstate1 = (
            <div style={{ height: this.state.tabState == 0 ? "auto" : "0px", minHeight: "calc(100vh - 185px)" }}>
                <Container component="main" maxWidth="xl" style={{ padding: "16px" }}>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} >
                        <Box width={80} fontWeight={600} textAlign="center" >
                            본인 이름
                        </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                            <FPTextField name="name" label="" value={this.state.name} onChange={this.handleChange} disabled={true} />
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                            참가학생 이름
                            </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1} >
                            <FPTextField name="studentName" label="미입력시 본인 이름" value={this.state.studentName} onChange={this.handleChange} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={80} fontWeight={600} textAlign="center">
                            핸드폰
                            </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                            <FPTextField name="phone" label="" value={this.state.phone} onChange={this.handleChange} disabled={true} />
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={80} fontWeight={600} textAlign="center" >
                            이메일
                            </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                            <FPTextField name="email" label="" value={this.state.email} onChange={this.handleChange} disabled={true} />
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={80} fontWeight={600} textAlign="center" >
                            거주지역
                            </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                            <FPTextField name="address" label={this.props.mode == "detail" ? "" : "거주지역을 입력해주세요."} value={this.state.address} onChange={this.handleChange} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0} >
                        <Box width={80} fontWeight={600} textAlign="center">
                            학교
                            </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 156px)" }}>
                            <FPTextField name="school" label={this.props.mode == "detail" ? "" : "학교를 입력해주세요."} value={this.state.school} onChange={this.handleChange} disabled={this.props.mode == "detail"} />
                        </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderTop={0} width={74} borderColor="#dadada">
                            <Select native fullWidth value={this.state.grade} onChange={this.handleChange} name="grade" style={{ paddingRight: "12px" }} disabled={this.props.mode == "detail"} >
                                <option value="1">1학년</option>
                                <option value="2">2학년</option>
                                <option value="3">3학년</option>
                            </Select>
                        </Box>
                    </Box>
                    {
                        (this.props.way == "O" || this.props.way == "I") ? null : (
                            <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                                <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }}>
                                    상담 희망일
                                </Box>
                                <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                                    {this.props.mode == "detail" ? (<Box p={1} pt={1.5} pb={1.5}>{this.state.date}  {this.state.time}</Box>) : (
                                        <div>
                                            <Box p={1} onClick={this.handleCalendarPicker} display="flex" alignItems="center" >
                                                {this.handleCalendarToString()}
                                                <EventAvailable style={{ padding: "4px 8px 4px 4px" }} />
                                            </Box>
                                            <Box borderTop={1} borderColor="#dadada">
                                                <Select native fullWidth value={this.state.timeIdx} onChange={this.handleChange} name="timeIdx" style={{ paddingRight: "12px" }} disabled={this.props.mode == "detail"} >
                                                    {
                                                        this.state.consultTime.length == 0 ? (<option value="0">날짜를 선택해 주세요.</option>) :
                                                            this.state.consultTime.map(function (item, index) {
                                                                return <option key={index} value={item.ConsultTimeIdx}>{item.ConsultTime + "(남은좌석 : " + item.Cnt + ")"}</option>
                                                            })
                                                    }
                                                </Select>
                                            </Box>
                                        </div>
                                    )}
                                </Box>
                            </Box>)
                    }
                    
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={80} fontWeight={600} textAlign="center">
                            제목
                        </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                            <FPTextField name="title" label={this.props.mode == "detail" ? "" : "제목을 입력해주세요."} value={this.state.title} onChange={this.handleChange} disabled={this.props.mode == "detail"}  />
                        </Box>
                    </Box>

                    {this.props.displayInputType == "J" ? (
                        <div>
                            <Box bgcolor="#F7F7F7">
                                <Box p={2} fontWeight={600} borderLeft={1} borderRight={1} >
                                    입력1. 수시 지원 학교/학과/전형을 적어주세요.
                                </Box>
                                <Box bgcolor="#fff" fontSize={15} alignItems="center" >
                                    <Box border={1}>
                                        <FPTextArea placeholder="" name="needvalue1" value={this.state.needvalue1} onChange={this.handleChangeTextArea} rows={6} fullWidth disabled={this.props.mode == "detail"} />
                                    </Box>
                                </Box>
                            </Box>
                            <Box bgcolor="#F7F7F7">
                                <Box p={2} fontWeight={600} borderLeft={1} borderRight={1} >
                                    입력2. 면접이나 논술 응시여부 결정이 시급하다면, 학교/학과/전형/면접,논술일정을 적어주세요.
                                </Box>
                                <Box bgcolor="#fff" fontSize={15} alignItems="center" >
                                    <Box border={1}>
                                        <FPTextArea placeholder="" name="needvalue2" value={this.state.needvalue2} onChange={this.handleChangeTextArea} rows={6} fullWidth disabled={this.props.mode == "detail"} />
                                    </Box>
                                </Box>
                            </Box>
                        </div>
                    ) : this.props.way == "I" || this.props.way == "V" ? (
                            <Box bgcolor="#F7F7F7">
                                <Box p={2} fontWeight={600} borderLeft={1} borderRight={1} >
                                    재상담자 필수입력
                            </Box>
                                <Box bgcolor="#fff" fontSize={15} alignItems="center" >
                                    <Box border={1}>
                                        <FPTextArea placeholder="재상담 신청일 시 입력해 주세요." name="renote" value={this.state.renote} onChange={this.handleChangeTextArea} rows={6} fullWidth disabled={this.props.mode == "detail"} />
                                    </Box>
                                </Box>
                            </Box>) : null}

                    

                    <Box color="red" p={1} style={{ wordBreak: "keep-all" }}>
                        * 개인정보는 [설정] 화면에서 본인 인증 후 수정할 수 있습니다.
                    </Box>
                </Container>
            </div>
        );

        const tabstate2 = (
            <div style={{ height: this.state.tabState == 1 ? "auto" : "0px", minHeight: "calc(100vh - 185px)" }}>
                <Container component="main" maxWidth="xl" style={{ padding:" 16px 16px 0px 16px"}} >
                    <Typography variant="subtitle1" gutterBottom style={{ color: "#ff7704" }} >
                        *희망학교는 1개이상 입력하세요.
                    </Typography>
                </Container>


                {this.state.univ.map((item, index) => (
                    <div key={index}>
                        <Box p={2} borderBottom={1} fontWeight={600}  >
                            {index + 1}지망
                            </Box>
                        <Box p={2}>
                            <Box bgcolor="#fff" display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1}>
                                <Box width={64} textAlign="center" fontWeight={600}>
                                    대학
                                </Box>
                                <Box borderLeft={1} flexGrow={1} color="#dadada">
                                    <FPTextField name="UniversityName" label={this.props.mode == "detail" ? "" : "희망대학"} value={this.state.univ[index].UniversityName} onChange={this.handleChangeArray.bind(this, index)} disabled={this.props.mode == "detail"} />
                                </Box>
                            </Box>
                            <Box pt={0} pb={0} bgcolor="#fff" display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                                <Box width={64} fontWeight={600} textAlign="center">
                                    전형
                                </Box>
                                <Box borderLeft={1} flexGrow={1} color="#dadada">
                                    <FPTextField name="StypeRem" label={this.props.mode == "detail" ? "" : "희망전형"} value={this.state.univ[index].StypeRem} onChange={this.handleChangeArray.bind(this, index)} disabled={this.props.mode == "detail"} />
                                </Box>
                            
                            </Box>
                            <Box bgcolor="#fff" display="flex" borderBottom={1} fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0} >
                                <Box width={64} fontWeight={600} textAlign="center">
                                    학과
                                </Box>
                                <Box borderLeft={1} flexGrow={1} color="#dadada">
                                    <FPTextField name="MajorName" label={this.props.mode == "detail" ? "" : "희망학과"} value={this.state.univ[index].MajorName} onChange={this.handleChangeArray.bind(this, index)} disabled={this.props.mode == "detail"} />
                                </Box>
                            </Box>
                        </Box>
                    </div>
                ))}
            </div>
        );

        const tabstate3 = (
            <div style={{ height: this.state.tabState == 2 ? "auto" : "0px", minHeight: "calc(100vh - 185px)" }}>
                <Box p={2} borderBottom={1} fontWeight={600} >
                    내신 성적(석차등급 평균을 입력하세요)
                </Box>
                <Box padding={2}>
                    <Box bgcolor="#fff" display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} >
                        <Box width={64} textAlign="center">
                            전과목
                        </Box>
                        <Box borderLeft={1} flexGrow={1} color="#dadada">
                            <FPTextField name="score" label={this.props.mode == "detail" ? "" : "석차등급 평균"} value={this.state.score[1] || ""} onChange={this.handleChangeScore.bind(this, 1)} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" pt={0} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            국어
                        </Box>
                        <Box borderLeft={1} flexGrow={1} color="#dadada">
                            <FPTextField name="score" label={this.props.mode == "detail" ? "" : "석차등급 평균"} value={this.state.score[2] || ""} onChange={this.handleChangeScore.bind(this, 2)} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" pt={0} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            수학
                        </Box>
                        <Box borderLeft={1} flexGrow={1} color="#dadada">
                            <FPTextField name="score" label={this.props.mode == "detail" ? "" : "석차등급 평균"} value={this.state.score[3] || ""} onChange={this.handleChangeScore.bind(this, 3)} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" pt={0} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            영어
                        </Box>
                        <Box borderLeft={1} flexGrow={1} color="#dadada">
                            <FPTextField name="score" label={this.props.mode == "detail" ? "" : "석차등급 평균"} value={this.state.score[4] || ""} onChange={this.handleChangeScore.bind(this, 4)} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" pt={0} pb={0} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0} >
                        <Box width={64} textAlign="center">
                            사회
                        </Box>
                        <Box borderLeft={1} flexGrow={1} color="#dadada">
                            <FPTextField name="score" label={this.props.mode == "detail" ? "" : "석차등급 평균"} value={this.state.score[5] || ""} onChange={this.handleChangeScore.bind(this, 5)} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" display="flex" borderBottom={1} fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            과학
                        </Box>
                        <Box borderLeft={1} flexGrow={1} color="#dadada">
                            <FPTextField name="score" label={this.props.mode == "detail" ? "" : "석차등급 평균"} value={this.state.score[6] || ""} onChange={this.handleChangeScore.bind(this, 6)} disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>

                </Box>


                <Box p={2} borderBottom={1} fontWeight={600} >
                    모의고사 성적
                </Box>
                <Box p={2}>
                    <Box bgcolor="#fff" display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1}>
                        <Box width={64} textAlign="center">
                            국어
                        </Box>
                        <Box display="flex" style={{ width: "calc(100% - 64px)" }}>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "백분위"} value={this.state.score[7] || ""} onChange={this.handleChangeScore.bind(this, 7)} disabled={this.props.mode == "detail"} />
                            </Box>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "등급"} value={this.state.score[8] || ""} onChange={this.handleChangeScore.bind(this, 8)} disabled={this.props.mode == "detail"} />
                            </Box>
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" pt={0} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            수학
                        </Box>
                        <Box display="flex" style={{ width: "calc(100% - 64px)" }}>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "백분위"} value={this.state.score[9] || ""} onChange={this.handleChangeScore.bind(this, 9)} disabled={this.props.mode == "detail"} />
                            </Box>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "등급"} value={this.state.score[10] || ""} onChange={this.handleChangeScore.bind(this, 10)} disabled={this.props.mode == "detail"} />
                            </Box>
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" pt={0} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            영어
                        </Box>
                        <Box display="flex" style={{ width: "calc(100% - 64px)" }}>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "백분위"} value={this.state.score[11] || ""} onChange={this.handleChangeScore.bind(this, 11)} disabled={this.props.mode == "detail"} />
                            </Box>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "등급"} value={this.state.score[12] || ""} onChange={this.handleChangeScore.bind(this, 12)} disabled={this.props.mode == "detail"} />
                            </Box>
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" pt={0} pb={0} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            탐구1
                        </Box>
                        <Box display="flex" style={{ width: "calc(100% - 64px)" }}>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "백분위"} value={this.state.score[13] || ""} onChange={this.handleChangeScore.bind(this, 13)} disabled={this.props.mode == "detail"} />
                            </Box>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "등급"} value={this.state.score[14] || ""} onChange={this.handleChangeScore.bind(this, 14)} disabled={this.props.mode == "detail"} />
                            </Box>
                        </Box>
                    </Box>
                    <Box bgcolor="#fff" borderBottom={1} display="flex" fontSize={15} alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={64} textAlign="center">
                            탐구2
                        </Box>
                        <Box display="flex" style={{ width: "calc(100% - 64px)" }}>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "백분위"} value={this.state.score[15] || ""} onChange={this.handleChangeScore.bind(this, 15)} disabled={this.props.mode == "detail"} />
                            </Box>
                            <Box borderLeft={1} flexGrow={1} color="#dadada">
                                <FPTextField name="score" label={this.props.mode == "detail" ? "" : "등급"} value={this.state.score[16] || ""} onChange={this.handleChangeScore.bind(this, 16)} disabled={this.props.mode == "detail"} />
                            </Box>
                        </Box>
                    </Box>
                </Box>
                    
                <Box p={2} borderBottom={1} fontWeight={600} >
                    본인에게 의미 있었던 활동 3가지를 적어주세요
                </Box>
                <Box p={2} bgcolor="#fff" borderBottom={1} fontSize={15} alignItems="center" >
                    <Box border={1}>
                        <FPTextArea placeholder="" name="activity" value={this.state.activity} onChange={this.handleChangeTextArea} rows={6} fullWidth disabled={this.props.mode == "detail"} />
                    </Box>
                </Box>

                <Box p={2} borderBottom={1} fontWeight={600} >
                    본 상담을 통해 해결하고 싶거나 궁금한 점을 적어주세요 (진로, 진학, 학습, 생활면에서 최대한 상세히, 필수)
                </Box>
                <Box p={2} bgcolor="#fff" borderBottom={1} fontSize={15} alignItems="center" mb={4}>
                    <Box border={1}>
                        <FPTextArea placeholder="" name="content" value={this.state.content} onChange={this.handleChangeTextArea} rows={6} fullWidth disabled={this.props.mode == "detail"} />
                    </Box>
                </Box>
            </div>
        );

        const tabstate4 = (
            <div style={{ height: this.state.tabState == (this.props.way == "O" ? 3 : 2) ? "auto" : "0px", minHeight: "calc(100vh - 185px)" }}>
                <Box p={2} borderBottom={1} fontWeight={600} >
                    {this.props.way == "O" ? "상담내용" : "상담보고서"}
                </Box>
                <Box p={2} bgcolor="#fff" borderBottom={1} fontSize={15} alignItems="center" >
                    <Box border={1}>
                        <div dangerouslySetInnerHTML={{ __html: this.state.reply }} ></div>
                    </Box>
                </Box>
                <Box p={2} borderBottom={1} fontWeight={600} >
                    만족도설문 (선택시 저장 됩니다.)
                </Box>
                <Box p={2} bgcolor="#fff" borderBottom={1} fontSize={15} alignItems="center" >
                    <Box border={1}>
                        {[...Array(5)].map((_, index) => (
                            <Box pl={2} pr={2} key={index}>
                                <FormControlLabel control={<Checkbox checked={this.state.starRating == (index + 1)} checkedIcon={<CheckCircleOutline style={{ color: "#FF7C27" }} />} icon={<CheckCircleOutline style={{ color: "#9A9A9A" }} />} onChange={() => this.handleRatingClick((index + 1))} />} label={<Box>{this.ratingString[index]}</Box>} />
                            </Box>
                        ))}
                    </Box>
                </Box>
            </div>
        );

        return (
            <div>
                <Dialog 
                    open={this.props.open}
                    onClose={this.handleDialogClose}
                    fullScreen
                    maxWidth="xl"
                    elevation={0}
                    onEnter={this.handleDialogEnter}
                >
                    <Box display={this.state.calendarPiekcerDialog ? "block" : "none"} style={{ position: 'fixed', left: '0px', zIndex: '1100' }} >
                        <Backdrop open={this.state.calendarPiekcerDialog} onClick={this.handleCalendarPickerClose} />
                        <CalendarComp
                            value={this.state.calendarDate}
                            view="month"
                            calendarType="US"
                            tileDisabled={this.handletileDisabled.bind(this)}
                            minDate={new Date(new Date().setDate(new Date().getDate() + 13))}
                            onChange={this.handleDayClick}
                        />
                    </Box>
                    <DialogAppbar title={this.props.title} onClose={this.handleDialogClose} />
                    <DialogTitle>
                        <FPTabs value={this.state.tabState} aria-label="simple tabs example" variant="fullWidth" style={{ margin: "-12px" }} onChange={this.handleTabClick} >
                            <FPTab label="신청정보" {...this.a11yProps(0)} />
                            <FPTab label="희망대학" {...this.a11yProps(1)} />
                            {this.props.way == "O" ? (<FPTab label="성적입력" {...this.a11yProps(2)} />) : null}
                            {this.state.progress == "C" && this.props.way == "O" ? (<FPTab label="결과" {...this.a11yProps(3)} />) : null}
                        </FPTabs>
                    </DialogTitle>
                    <DialogContent style={{ padding: "0px" }}>
                        {this.props.way == "O" && this.state.progress == "C" ? (
                        <SwipeableViews enableMouseEvents
                            index={this.state.tabState}
                            onChangeIndex={this.handleSwipeChangeIndex}
                        >
                                {tabstate1}
                                {tabstate2}
                                {tabstate3}
                                {tabstate4}
                        </SwipeableViews>
                        ) : this.props.way == "O" ? (<SwipeableViews enableMouseEvents
                            index={this.state.tabState}
                            onChangeIndex={this.handleSwipeChangeIndex}
                        >
                                {tabstate1}
                                {tabstate2}
                                {tabstate3}
                            </SwipeableViews>) : (
                        <SwipeableViews enableMouseEvents
                            index={this.state.tabState}
                            onChangeIndex={this.handleSwipeChangeIndex}
                        >
                            {tabstate1}
                            {tabstate2}
                        </SwipeableViews>)}
                    </DialogContent>
                    <DialogActions style={{ padding: "0px 0px 16px 0px" }}>
                        <Container component="div" maxWidth="xl">
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                        닫기
                                    </Button>
                                </Grid>
                                {(this.props.mode == "request" || this.props.mode == "modify") ? (
                                    <Grid item xs={6}>
                                        <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                            {this.convertRequestString()}
                                    </Button>
                                    </Grid>) : null
                                }
                                {this.props.mode == "detail" ? (
                                    <Grid item xs={6}>
                                        <Button fullWidth autoFocus onClick={this.handleDialogModify} color={(this.state.progress != "R" && this.state.progress != "W") ? "secondary" : "primary"} variant="contained">
                                            수정
                                    </Button>
                                    </Grid>
                                ) : null}
                            </Grid>
                        </Container>
                    </DialogActions>
                </Dialog>
            </div >
        );
    }
}

RequestDialog.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string,
    item: PropTypes.object,
    date: PropTypes.array,
    mode: PropTypes.string,
    way: PropTypes.string,
    noti: PropTypes.bool,
    eidx: PropTypes.number,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
    onNoti: PropTypes.func,
};

RequestDialog.defaultProps = {
    open: false,
    title: "신청",
    item: {},
    mode: 'detail',
    date: [],
    way: "",
    noti: false,
    eidx: 0,
    onClose: () => { console.error("close function is not defined"); },
    onBackDrop: () => { console.log("backdrop function is not defined."); },
    onAlert: () => { console.log("alert function is not defined."); },
    onRequest: () => { console.log("request function is not defined."); },
    onNoti: () => { console.log("noti function is not defined."); },
};

export default RequestDialog
