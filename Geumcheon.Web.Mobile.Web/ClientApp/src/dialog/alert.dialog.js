import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, Grid, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { OpenModal, CloseModal } from '../helper/appfunction';

const styles = theme => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
        paddingTop: "0px",
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const FPDialog = withStyles(theme => ({
    paper: {
        boxShadow: 'none',
        borderRadius: '0px'
    }
}))(Dialog);

const FPDialogTitle = withStyles(styles)(props => {
    const { children, classes, onClose, ...other } = props;
    return (
        <DialogTitle disableTypography className={classes.root} {...other}>
            <Typography align="center" variant="h6">{children}</Typography>
        </DialogTitle>
    );
});

const FPDialogContent = withStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        paddingTop: theme.spacing(1),
        textAlign: "center"

    },
}))(DialogContent);

const FPDialogActions = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing(0),
    },
}))(DialogActions);

class AlertDialog extends Component {
    handleDialogClose = (value) => {
        CloseModal();
        this.props.onClose(value, this.props.type);
    }

    handleDialogEnter = () => {        
        OpenModal(this.handleDialogClose);
    }

    render() {
        return (
            <div>
                <FPDialog 
                    open={this.props.open}
                    onClose={() => this.handleDialogClose(false)}
                    fullWidth
                    maxWidth="xs"
                    elevation={0}
                    onEnter={this.handleDialogEnter}
                >
                    <FPDialogTitle>
                        {this.props.data.title || ""}
                    </FPDialogTitle>
                    <FPDialogContent>
                        <Typography gutterBottom style={{ whiteSpace: this.props.data.preline ? "pre-line" : "inherit", textAlign: this.props.data.align == "left" ? "left" : "center" }}>
                            {this.props.data.message || "확인"}
                        </Typography>
                    </FPDialogContent>
                    <FPDialogActions>
                        {this.props.data.mode == "yesorno" ? (<Grid container spacing={0}>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                    아니오
                                </Button>
                            </Grid>
                            <Grid item xs={6}>
                                <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                    예
                                </Button>
                            </Grid>
                            
                        </Grid>) : (
                            <Button fullWidth autoFocus onClick={ () => this.handleDialogClose(true)} color="primary" variant="contained">
                            확인
                        </Button>
                    )}
                    </FPDialogActions>
                </FPDialog>
            </div >
        );
    }
}

AlertDialog.propTypes = {
    open: PropTypes.bool,
    data: PropTypes.object,
    onClose: PropTypes.func,
};

AlertDialog.defaultProps = {
    open: false,
    data: {},
    onClose: () => { console.error("close function is not defined"); },
};

export default AlertDialog
