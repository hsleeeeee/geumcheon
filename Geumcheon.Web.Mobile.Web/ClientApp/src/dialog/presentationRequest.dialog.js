import React, { Component, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, Grid, DialogTitle, DialogContent, DialogActions, Container, FormControlLabel, Tabs, Tab, Box, Select, Radio, Divider } from '@material-ui/core';
import { EventAvailable } from '@material-ui/icons';
import SwipeableViews from 'react-swipeable-views';
import CalendarComp from 'react-calendar';

import { FPTextField, FPTextFieldButton, DialogAppbar, FPTimePicker, FPTextArea } from '../components/parts';
import { OpenModal, CloseModal, GetUserInfo } from '../helper/appfunction';

// 커스텀 탭 디자인
const FPTabs = withStyles({
    indicator: {
        height: "3px",
        backgroundColor: "#ff7704"
    }
})(Tabs);

const FPTab = withStyles(theme => ({
    selected: {
        color: '#333333',
        fontWeight: '500',
    }
}))(props => <Tab disableRipple {...props} />);

// 탭 패널
class TabPanel extends Component {
    render() {
        const { children, value, index, ...other } = this.props;

        return (
            <div style={{ marginTop: '20px', marginBottom: '16px' }}>
                {value === index && <Box onClick={() => this.props.onTab(index)} > {children}</Box>}
            </div>
        );
    }
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
    onTab: PropTypes.func
};

function RequestUser(props) {
    const [name, setName] = useState(props.name);
    const [hp, setHP] = useState(props.hp);
    const [email, setEmail] = useState(props.email);
    const [usertype, setUserType] = useState(props.usertype);
    useEffect(() => {
        setName(props.name);
        setHP(props.hp);
        setEmail(props.email);
        setUserType(props.usertype);
    }, [props])

    const handleChange = () => {
        props.onChange({
            PtRequestIdx: props.requestIdx,
            Name: name,
            HP: hp,
            Email: email,
            AccountType: usertype
        }, props.index);
    }

    return (
        <div>
            <Box border={1} borderTop={0} bgcolor="#C9C9C9" textAlign="center" p={1} fontWeight={600}>
                신청자{props.index + 1}
            </Box>
            <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                <Box width={80} fontWeight={600} textAlign="center" >
                    성명
                </Box>
                <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                    <FPTextField name="name" label="" value={name} onChange={e => setName(e.target.value)} disabled={props.mode == "detail"} onBlur={handleChange} />
                </Box>
            </Box>
            <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                <Box width={80} fontWeight={600} textAlign="center" >
                    핸드폰
                </Box>
                <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                    <FPTextField name="hp" label="핸드폰 번호(숫자만 입력)" value={hp} onChange={e => setHP(e.target.value)} disabled={props.mode == "detail"} onBlur={handleChange} />
                </Box>
            </Box>
            <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                <Box width={80} fontWeight={600} textAlign="center" >
                    구분
                </Box>
                <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                    <Select native fullWidth value={usertype} onChange={e => setUserType(e.target.value)} name="usertype" style={{ paddingRight: "12px" }} disabled={props.mode == "detail"} onBlur={handleChange} >
                        <option value="S">학생</option>
                        <option value="P">학부모</option>
                        <option value="G">일반</option>
                    </Select>
                </Box>
            </Box>
        </div>
    );
}




class PresentationRequestDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            phone: "",
            email: "",
            school: "",
            grade: 0,
            address: "",
            progress: "R",
            content: "",
            requestType: [],
            requestSelect: 0,

            requestUserInfos: [{
                PtRequestIdx: 0,
                Name: "",
                HP: "",
                Email: "",
                AccountType: "S"
            }],
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.noti) {
            console.log("onNoti");
            this.funcInNotification(nextProps.item.RequestIdx);
            this.props.onNoti();
        }
    }

    // 탭 컨텐츠 스와이프 이벤트
    handleSwipeChangeIndex = (idx) => {
        this.setState({
            tabState: idx
        });
    };

    handleTabClick = (event, newValue) => {
        this.setState({
            tabState: newValue
        });
    };

    a11yProps = (index) => {
        return {
            id: `register-tab-${index}`,
            'aria-controls': `register-tabpanel-${index}`,
        };
    }

    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.type == "checkbox" ? e.target.checked : e.target.value;
        this.setState(nextState);
    }

    handleUserInfoChange = (e, index) => {
        console.log(e, index);

        const curUserInfo = [...this.state.requestUserInfos];
        curUserInfo[index] = e;

        this.setState({
            requestUserInfos: curUserInfo
        });
    }

    handleChangeTextArea = (name, value) => {
        const nextState = {};
        nextState[name] = value;
        this.setState(nextState);
    }

    handleDialogModify = () => {
        if (this.state.progress != "R" && this.state.progress != "W") {
            this.props.onAlert({
                message: "참석 확정 후에는 수정할 수 없습니다."
            });
        }
        else {
            this.props.onClose("modify");
        }
    }

    handleDialogClose = (value) => {
        if (value) {
            let nouser = true;
            let invalidUser = false;
            const phoneRegex = /^01(?:0|1|[6-9])[.-]?(\d{3}|\d{4})[.-]?(\d{4})$/;
            const emailRegex = /^(([^<>()\[\].,;:\s@"]+(\.[^<>()\[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

            this.state.requestUserInfos.forEach((item, index) => {
                if (!invalidUser) {
                    if (item.HP != "" && !phoneRegex.test(item.HP)) {
                        invalidUser = true;
                        this.props.onAlert({
                            message: "신청자" + (index + 1).toString() + "의 핸드폰 번호를 확인해 주세요."
                        });
                    }
                    else if (item.Name != "" && item.HP != "") {
                        nouser = false;
                    }
                    else if (item.Name == "" && item.HP == "") {
                    }
                    else {
                        invalidUser = true;
                        this.props.onAlert({
                            message: "신청자" + (index + 1).toString() + "의 이름, 핸드폰을 입력해주세요."
                        });
                    }
                }
            });

            if (invalidUser) {
                return;
            }
            else if (nouser) {
                this.props.onAlert({
                    message: "1명 이상의 신청자 정보를 입려해 주세요."
                });
                return;
            }

            const auth = GetUserInfo("id") == null ? true : false;


            this.props.onBackDrop(true, "저장 중...");
            this.props.onRequest('api/PresentationNew', 'put', {
                ptidx: this.props.mode == "request" ? this.props.item.Idx : this.props.item.PtIdx,
                memo: this.state.content,
                requestuserinfo: this.state.requestUserInfos
            }, auth).then((result) => {
                if (result.error == null) {
                    const duplicatePhone = [];
                    result.state.forEach((item, index) => {
                        if (item == "99/D") {
                            duplicatePhone.push(this.state.requestUserInfos[index].HP);
                        }
                    });

                    let alertMsg = "설명회 신청을 완료하였습니다.";
                    if (GetUserInfo("id") != "") {
                        alertMsg += "\n신청내역은 나의활동>신청 및 상담내역에서 확인하실수 있습니다.";
                    }
                    if (duplicatePhone.length > 0) {
                        alertMsg += "\n신청자 중 " + duplicatePhone.join(", ") + "는 이미 신청된 핸드폰 번호 입니다.";
                    }

                    this.props.onAlert({
                        message: alertMsg,
                        preline: true,
                        align: "left",
                    }, function () { CloseModal(); this.props.onClose(true); }.bind(this, CloseModal));
                }
                else {
                    this.props.onAlert({
                        message: result.error,
                    });
                }
            });
        }
        else {
            if (this.props.mode != "detail") {
                this.props.onAlert({
                    mode: "yesorno",
                    message: "저장하지 않은 내용은 저장되지 않습니다. 닫으시겠습니까?"
                }, function (bind, value) {
                    if (value) {
                        CloseModal();
                        this.props.onClose(false);
                    }
                }.bind(this, CloseModal));
            }
            else {
                CloseModal();
                this.props.onClose(false);
            }
        }
    }

    handleRequestRadio = (value) => {
        this.setState({
            requestSelect: value
        });
    }

    handleAddRequestUser = () => {
        if (this.state.requestUserInfos.length < 5) {
            const curUserInfos = [...this.state.requestUserInfos];
            curUserInfos.push({
                PtRequestIdx: 0,
                Name: "",
                HP: "",
                Email: "",
                AccountType: "S"
            });

            this.setState({
                requestUserInfos: curUserInfos
            });
        }
    }

    funcInNotification = (idx) => {
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/Entrance?requestIdx=' + idx, 'get').then((result) => {
            const nextState = {};
            if (result.error == null) {
                nextState.name = result.Member.Name || "";
                nextState.email = result.Member.Email || "";
                nextState.school = result.Member.School || "";
                nextState.grade = result.Member.Grade || "";
                nextState.address = result.Consult.AreaName || "";
                nextState.phone = result.Member.Hp || "";
                nextState.studentName = result.Consult.StudentName || "";
                nextState.title = result.Consult.Title || "";
                nextState.score = JSON.parse(result.Consult.ScoreHtml);
                nextState.activity = nextState.score[17] || "";
                nextState.content = nextState.score[18] || "";
                nextState.reply = result.ReplyNote || "";
                nextState.progress = result.Consult.Progress;

                if (this.props.way != "O") {
                    nextState.timeIdx = result.Schedule.ConsultTimeIdx;
                    nextState.time = result.Schedule.ConsultTime;
                    nextState.date = result.Schedule.ConsultDate;

                    let calDate = new Date();
                    calDate.setFullYear(nextState.date.split('-')[0]);
                    calDate.setMonth(parseInt(nextState.date.split('-')[1]) - 1);
                    calDate.setDate(nextState.date.split('-')[2]);

                    nextState.calendarDate = calDate;
                }

                const tempuniv = [];
                for (let i = 0; i < 3; i++) {
                    tempuniv.push({
                        ApplyUnivIdx: 0,
                        Ranking: i + 1,
                        UniversityName: "",
                        StypeRem: "",
                        MajorName: ""
                    });
                }

                result.Univ.forEach((item) => {
                    tempuniv[item.Ranking - 1] = item;
                });

                nextState.univ = tempuniv;
            }
            else {
                this.props.onAlert({
                    message: result.error
                }, function () { this.handleDialogClose(false) }.bind(this));
            }

            this.setState(nextState);

            if (this.props.way != "O") {
                this.props.onRequest('api/Schedule', 'post', {
                    type: "time",
                    way: this.props.way,
                    date: nextState.date
                }).then((result) => {
                    console.log(result);
                    if (result.error == null) {
                        this.setState({
                            consultTime: result.entities || [],
                            calendarPiekcerDialog: false,
                        });
                    }
                    else {
                        this.props.onAlert({
                            message: result.error
                        });
                    }
                });
            }
        });
    }

    handleDialogEnter = () => {
        const auth = GetUserInfo("id") == null ? true : false;
        if (this.props.mode == "request") {
            this.props.onBackDrop(true, "불러오는 중...");
            this.props.onRequest('api/PresentationNew?type=request&pidx=' + this.props.item.Idx, 'get', null, auth).then((result) => {
                console.log(result);

                if (result.Entities.length > 0) {
                    this.setState({
                        requestUserInfos: [...result.Entities],
                        content: result.Entities[0].Memo
                    });
                }

                //const nextState = {};
                //if (result.error == null) {
                //    nextState.name = result.Name || "";
                //    nextState.email = result.Email || "";
                //    nextState.school = result.School || "";
                //    nextState.grade = result.Grade || "";
                //    nextState.address = result.Address || "";
                //    nextState.phone = result.Hp || "";
                //    nextState.requestType = [];

                //    result.Entities.forEach((item, index) => {
                //        nextState.requestType.push({
                //            type: item.Idx,
                //            name: item.PresentationName + "(" + (this.props.way == "E" ? (index + 1) + "회차" : item.PresentationDate) + ")"
                //        });
                //    });

                //    console.log(nextState);

                //}
                //else {
                //    this.props.onAlert({
                //        message: result.error
                //    });
                //}

                //this.setState(nextState);
            });
        }
        else {
            this.props.onBackDrop(true, "불러오는 중...");
            this.props.onRequest('api/PresentationNew?type=request&pidx=' + this.props.item.PtIdx, 'get', null, auth).then((result) => {
                if (result.Entities.length > 0) {
                    this.setState({
                        requestUserInfos: [...result.Entities],
                        content: result.Entities[0].Memo
                    });
                }
            });

            //this.setState({
            //    name: this.props.item.Name || "",
            //    email: this.props.item.Email || "",
            //    school: this.props.item.School || "",
            //    grade: this.props.item.Grade || "",
            //    address: this.props.item.Area || "",
            //    phone : this.props.item.HP || "",
            //    requestType: [{ type: this.props.item.PtIdx || "", name: this.props.item.PresentationName || "", },],
            //    requestSelect: 0,
            //    content: this.props.item.Memo
            //});
        }



        OpenModal(this.handleDialogClose);
    }

    render() {
        const tabstate1 = (
            <div style={{ height: this.state.tabState == 0 ? "auto" : "0px", minHeight: "calc(100vh - 185px)" }}>
                <Container component="main" maxWidth="xl" style={{ padding: "16px" }}>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} >
                        <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                            행사명
                        </Box>
                        <Box p={1} bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 98px)" }} >
                            {this.props.item.PresentationName}
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0} >
                        <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                            행사일정
                        </Box>
                        <Box p={1} bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 98px)" }} >
                            {this.props.item.PresentationDate_DP || "safsdafsadf"}
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0} >
                        <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                            행사장소
                        </Box>
                        <Box p={1} bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1} style={{ maxWidth: "calc(100% - 98px)" }} >
                            {this.props.item.Address || "블라블라블라블라블라블라블라블라블라블라블라블라블라블라블라블라블라블라블라블라"}
                        </Box>
                    </Box>
                    <Box display="flex" alignItems="center" bgcolor="#F7F7F7" border={1} borderTop={0}>
                        <Box width={80} fontWeight={600} textAlign="center" style={{ wordBreak: "keep-all" }} >
                            {this.props.way == "A" ? "학부모 아카데미에서 다뤘으면 하는 내용" : "설명회에서 다뤘으면 하는 내용"}
                        </Box>
                        <Box bgcolor="#fff" borderLeft={1} borderColor="#dadada" flexGrow={1}>
                            <FPTextArea placeholder="내용 입력" name="content" value={this.state.content} onChange={this.handleChangeTextArea} rows={6} fullWidth disabled={this.props.mode == "detail"} />
                        </Box>
                    </Box>
                    {
                        this.state.requestUserInfos.map((item, index) => (
                            <RequestUser key={index} name={item.Name} requestIdx={item.PtRequestIdx || 0} hp={item.HP} email={item.Email} usertype={item.AccountType} onChange={this.handleUserInfoChange} index={index} mode={this.props.mode} />
                        ))}
                    {this.props.mode == "detail" ? null : (<Box p={1} border={1} borderTop={0}>
                        <Button fullWidth color={this.state.requestUserInfos.length >= 5 ? "secondary" : "primary"} variant="contained" onClick={this.handleAddRequestUser}>동행인 추가</Button>
                        <Box pt={1} color="red" style={{ wordBreak: "keep-all" }}>
                            * 한 번 신청시 동행인 최대 5명까지 신청 가능합니다. <br />
                            * 신청자 확인, 안내를 위해 휴대폰, 이메일 주소가 필요하니 반드시 적어주세요.
                        </Box>
                    </Box>)}

                </Container>
            </div>
        );

        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.handleDialogClose}
                    fullScreen
                    maxWidth="xl"
                    elevation={0}
                    onEnter={this.handleDialogEnter}
                >
                    <DialogAppbar title={this.props.title} onClose={this.handleDialogClose} />
                    <DialogTitle style={{ display: "none" }}>
                    </DialogTitle>
                    <DialogContent style={{ padding: "0px" }}>
                        <Divider />
                        {tabstate1}
                    </DialogContent>
                    <DialogActions style={{ padding: "0px 0px 16px 0px" }}>
                        <Container component="div" maxWidth="xl">
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                        닫기
                                    </Button>
                                </Grid>
                                {(this.props.mode == "request" || this.props.mode == "modify") ? (
                                    <Grid item xs={6}>
                                        <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                            신청
                                    </Button>
                                    </Grid>) : null
                                }
                                {this.props.mode == "detail" ? (
                                    <Grid item xs={6}>
                                        <Button fullWidth autoFocus onClick={this.handleDialogModify} color={(this.state.progress != "R" && this.state.progress != "W") ? "secondary" : "primary"} variant="contained">
                                            수정
                                    </Button>
                                    </Grid>
                                ) : null}
                            </Grid>
                        </Container>
                    </DialogActions>
                </Dialog>
            </div >
        );
    }
}

PresentationRequestDialog.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string,
    item: PropTypes.object,
    mode: PropTypes.string,
    way: PropTypes.string,
    noti: PropTypes.bool,
    onClose: PropTypes.func,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
    onNoti: PropTypes.func,
};

PresentationRequestDialog.defaultProps = {
    open: false,
    title: "신청",
    item: {},
    mode: 'detail',
    way: "",
    noti: false,
    onClose: () => { console.error("close function is not defined"); },
    onBackDrop: () => { console.log("backdrop function is not defined."); },
    onAlert: () => { console.log("alert function is not defined."); },
    onRequest: () => { console.log("request function is not defined."); },
    onNoti: () => { console.log("noti function is not defined."); },
};

export default PresentationRequestDialog
