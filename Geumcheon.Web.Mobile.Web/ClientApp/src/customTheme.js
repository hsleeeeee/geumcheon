import { createMuiTheme } from '@material-ui/core/styles'
import { fontSize, spacing } from '@material-ui/system';

export default createMuiTheme({
    typography: {
        fontFamily: "Nanum Gothic",
    },
    overrides: {
        // 상단 헤더
        MuiAppBar: {
            colorPrimary: {
                color: "#010101",
                backgroundColor: "#fafafa",
            },
        },
        MuiToolbar: {
            root: {
                display: "inline",
                textAlign: "center",
            },
        },
        // 텍스트
        MuiTypography: {
            h6: {
                fontWeight: "bold",
                padding: "12px",
            },
            subtitle1: {
                fontWeight: "bold",
            },
            subtitle2: {
                color: "#9A9A9A",
                fontSize: "1rem",
                fontWeight: "400",
            },
            body2: {
                color: "#9A9A9A",
                fontSize: "0.8rem",
            },
            colorPrimary: {
                color: "#FF7C27",
            },
        },
        // 입력 텍스트 박스 관련
        MuiInputBase: {
            root: {
                borderRadius: "0px",
                padding: "9px",
                backgroundColor: "#fff"
            },
            multiline: {
                padding: "9px",
            },
        },
        MuiOutlinedInput: {
            root: {
                borderRadius: "0px",
            },
            notchedOutline: {
                border: "0px",
            },
        },
        // 버튼
        MuiButton: {
            root: {
                fontSize: "1.0rem",
            },
            containedPrimary: {
                backgroundColor: "#2f96d4",
                '&:hover': {
                    backgroundColor: "#2f96d4",
                    '@media (hover: none)': {
                        backgroundColor: "#2f96d4",
                    },
                },
                '&:focus': {
                    backgroundColor: "#2f96d4",
                },
            },
            containedSecondary: {
                backgroundColor: "#B2B2B2",
                '&:hover': {
                    backgroundColor: "#B2B2B2",
                    '@media (hover: none)': {
                        backgroundColor: "#B2B2B2",
                    },
                },
                '&:focus': {
                    backgroundColor: "#B2B2B2",
                },
            },
            contained: {
                boxShadow: "none",
                '&:hover': {
                    '@media (hover: none)': {
                        boxShadow: "none",
                    },
                },
                padding: "12px",
            },
            fullWidth: {
                borderRadius: "0px",
            },
        },
        // 다이얼로그
        MuiDialog: {
            root: {
                paddingTop: "56px",
                '@media (min-width: 600px)': {
                    paddingTop: "64px",
                },
            },
            paper: {
                backgroundColor: "#fafafa",
            },
        },
        MuiDialogTitle: {
            root: {
                padding: "0px"
            },
        },
        // 폼컨트롤
        MuiFormControl: {
            marginNormal: {
                marginTop: "0px",
                marginBottom: "0px",
            },
        },
        // Paper
        MuiPaper: {
            root: {
                backgroundColor: "#fafafa",
            }
        },
        // Tabs 컨트롤
        MuiTabs: {
            root: {
                minHeight: "10px",
            },
            flexContainer: {
                borderBottom: '3px solid #EEEEEE',
            },
        },
        MuiTab: {
            root: {
                fontSize: "1rem",
                paddingBottom: "0px",
                minHeight: "40px",
            },
        },
        // 리스트
        MuiListItemIcon: {
            root: {
                minWidth: "40px",
            },
        },
        // 셀렉트박스
        MuiInput: {
            underline: {
                '&:after': {
                    borderBottom: "0px",
                },
                '&:before': {
                    borderBottom: "0px",
                },
                '&:hover:not(.Mui-disabled):before': {
                    borderBottom: "0px",
                    '@media (hover:none)': {
                        borderBottom: "0px !important",
                    },
                },
            },
        },
        // Grid 관련
        MuiGrid: {
            "spacing-xs-1": {
                marginTop: "0px",
                marginBottom: "0px",
            },
        },
        // Fab 관련
        MuiFab: {
            primary: {
                backgroundColor: "#2f96d4",
                '&:hover': {
                    backgroundColor: "#2f96d4",
                    '@media (hover: none)': {
                        backgroundColor: "#2f96d4",
                    },
                },
                '&:focus': {
                    backgroundColor: "#2f96d4",
                },
            },
        },
        // 스위치 관련
        MuiSwitch: {
            colorPrimary: {
                "&.Mui-checked": {
                    color: "#FF7C27",
                },
                "&.Mui-checked + .MuiSwitch-track": {
                    backgroundColor: "#FF7C27",
                },
            },
        },
        // 메뉴
        MuiMenu: {
            paper: {
                width: "100%",
            },
        },
        // 바텀 네비게이션
        MuiBottomNavigation: {
            root: {
                backgroundColor: "#fafafa",
            },
        },
        MuiBottomNavigationAction: {
            root: {
                minWidth: "60px",
            },
        },
        MuiInputLabel: {
            outlined: {
                "&.MuiInputLabel-shrink": {
                    transform: 'translate(14px, 0px) scale(0.75)'
                },
            },
        },
        // 체크박스
        MuiCheckbox: {
            colorPrimary: {
                "&.Mui-checked": {
                    color: "#FF7C27",
                },
            },
        },
        MuiSelect: {
            select: {
                "&.MuiSelect-select": {
                    paddingRight: "12px"
                },
            },
        },
        MuiRadio: {
            colorSecondary: {
                "&.Mui-checked": {
                    color: "#ff7704"
                },
            },
        },
        MuiLink: {
            underlineHover: {
                textDecoration: "underline",
                display:"block"
            },
        },
    },
});

