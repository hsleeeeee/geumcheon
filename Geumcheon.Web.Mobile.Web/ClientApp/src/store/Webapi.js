import * as types from '../actions/ActionTypes';

const initialState = { login: 'WEBAPI_REQUEST' };

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case types.WEBAPI_REQUEST:
            return {
                ...state,
                result: {
                    status: 'WAITING'
                }
            };
        case types.WEBAPI_RESPONSE_OK:
            return {
                ...state,
                result: {
                    status: 'SUCCESS',
                    data: action.data
                }
            };
        case types.WEBAPI_RESPONSE_FAILED:
            return {
                ...state,
                result: {
                    status: 'FAILURE',
                    data: action.data
                }
            };
        case types.AUTH_LOGIN:
            return {
                ...state,
                result: {
                    status: 'WAITING'
                }
            };
        case types.AUTH_LOGIN_SUCCESS:
            return {
                ...state,
                result: {
                    status: 'SUCCESS'
                }
            };
        case types.AUTH_LOGIN_FAILURE:
            return {
                ...state,
                result: {
                    status: 'FAILURE'
                }
            };
    }

    return state;
};

