import * as types from '../actions/ActionTypes';

const initialState = {
    result: false,
};

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case types.OPENLOGIN:
            return {
                ...state,
                result: true
            };
        case types.CLOSELOGIN:
            return {
                ...state,
                result: false,
            };
    }

    return state;
};

