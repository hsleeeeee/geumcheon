import * as types from '../actions/ActionTypes';

const initialState = {
    result: {
        backDrop: 'RESUME',
        msg: '',
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case types.BACKDROPWAIT:
            return {
                ...state,
                result: {
                    backDrop: 'WAIT',
                    msg: action.msg,
                }
            };
        case types.BACKDROPRESUME:
            return {
                ...state,
                result: {
                    backDrop: 'RESUME',
                }
            };
    }

    return state;
};

