import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import * as NavSelectMenu from './NavSelectMenu';
import * as BackDropState from './BackDropState';
import * as Webapi from './Webapi';
import * as AlertGlobal from './AlertGlobal';
import * as PopupLogin from './PopupLogin';

export default function configureStore(history, initialState) {
  const reducers = {
      navSelect: NavSelectMenu.reducer,
      backDrop: BackDropState.reducer,
      webapiData: Webapi.reducer,
      alertData: AlertGlobal.reducer,
      isLogin: PopupLogin.reducer,
  };

  const middleware = [
    thunk,
    routerMiddleware(history)
  ];

  // In development, use the browser's Redux dev tools extension if installed
  const enhancers = [];
  const isDevelopment = process.env.NODE_ENV === 'development';
  if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
    enhancers.push(window.devToolsExtension());
  }

  const rootReducer = combineReducers({
    ...reducers,
    routing: routerReducer
  });

  return createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware), ...enhancers)
  );
}
