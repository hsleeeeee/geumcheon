import * as types from '../actions/ActionTypes';

const initialState = {
    result: {
        selectMenu: [99]
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case types.SELECTNAVMENU:
            return {
                ...state,
                result: {
                    selectMenu: action.data
                }
            };
    }

    return state;
};

