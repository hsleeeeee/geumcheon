import * as types from '../actions/ActionTypes';

const initialState = {
    result: {
        open: false,
        data: {},
    }
};

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case types.ALERT_OPEN:
            return {
                ...state,
                result: {
                    open: true,
                    data: action.data,
                }
            };
        case types.ALERT_CLOSE:
            return {
                ...state,
                result: {
                    open: false,
                }
            };
    }

    return state;
};

