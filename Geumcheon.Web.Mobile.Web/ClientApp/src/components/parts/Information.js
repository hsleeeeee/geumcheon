import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Box, List, ListItem, ListItemText, Link, Typography } from '@material-ui/core';
import { Info, Place, DateRange, Schedule, CollectionsBookmark, Help, Accessibility, Create } from '@material-ui/icons';

import { callNativeFunction } from '../../helper/appfunction';

const FPlistItemText = withStyles(theme => ({
    secondary: {
        fontSize: "14px",
        color: "rgba(0, 0, 0, 0.75)"
    },
    primary: {
        fontSize: "14px",
        color: "red"
    }
}))(ListItemText);

export class InfoConsult extends Component {
    handleGoURL = (link, e) => {
        e.preventDefault();

        

        if (callNativeFunction("goLocationBrowser", link) == 0) {
            window.open(link);
        }
    }

    render() {
        const interview = (
            <div style={{ height: this.props.visible ? "auto" : "0px", minHeight: "calc(100vh - 163px)", marginTop: "43px" }}>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Info style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            주의사항
                        </Box>
                        <Box>
                            <List dense={true}>
                                {this.props.buffer ? (
                                    <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                        <FPlistItemText primary="* 모집인원 수가 초과되었을 경우, '대기신청'으로 신청 가능하며, 취소인원이 있을 때 담당자가 연락을 드립니다." />
                                    </ListItem>) : null}
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="본 서비스는 금천구청 고교 재학생(또는 졸업생)과 금천구 주민들에게만 제공되는 서비스입니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 참가학생이 여럿인 경우, 학생 1명에 1아이디로 로그인해서 신청해주세요." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 8px 0px 8px" }}>
                                    <FPlistItemText secondary="특성화고 전형, 고른기회전형은 전문적인 상담이 어렵습니다. 사전에 참고해주시기 바랍니다.(예체능은 불가)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 8px 0px 8px" }}>
                                    <FPlistItemText primary="본 행사는 코로나19로 인해 화상컨설팅으로 변경되었습니다." />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Place style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담장소
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="Zoom프로그램 활용(비대면컨설팅)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <DateRange style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            신청기간
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 정시컨설팅" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 10월 15일(목) ~ 12월 15일(화)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Schedule style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담일정
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 정시컨설팅" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 12월 26일(토)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff" mb={8}>
                    <Box>
                        <CollectionsBookmark style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담 전 준비서류
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="‘신청’버튼을 클릭하여 상담신청 후, PC에서 http://geumcheon.eduplan.co.kr 로그인 후 상단의 <나의활동>의 <컨설팅 기초자료 입력>에서 모든 항목을 입력해야 정식으로 접수가 완료됩니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 10일 전까지 기초자료를 입력하지 않았거나 누락의 경우, 예비자 학생에게 컨설팅 기회가 자동 양도되므로 주의해 주세요." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="모바일에서는 기초자료 입력을 지원하지 않습니다" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
            </div>
        );

        const video = (
            <div style={{ height: this.props.visible ? "auto" : "0px", minHeight: "calc(100vh - 163px)", marginTop: "43px" }}>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Info style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            주의사항
                        </Box>
                        <Box>
                            <List dense={true}>
                                {this.props.buffer ? (
                                    <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                        <FPlistItemText primary="* 모집인원 수가 초과되었을 경우, '대기신청'으로 신청 가능하며, 취소인원이 있을 때 담당자가 연락을 드립니다." />
                                    </ListItem>) : null}
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="본 서비스는 금천구청 고교 재학생(또는 졸업생)과 금천구 주민들에게만 제공되는 서비스입니다." />
                                </ListItem>
                            </List>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 참가학생이 여럿인 경우, 학생 1명에 1아이디로 로그인해서 신청해주세요." />
                                </ListItem>
                            </List>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="특성화고 전형, 고른기회전형은 전문적인 상담이 어렵습니다. 사전에 참고해주시기 바랍니다.(예체능은 불가)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>


                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Place style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담장소
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="화상컨설팅 (* 대면없이 ZOOM 프로그램 활용)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>


                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <DateRange style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            신청기간
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 종합컨설팅(진로, 학습, 진학 종합 로드맵)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 4월 27일(월) ~ 11월 18일(수)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>

                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Schedule style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담일정
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 종합컨설팅(진로, 학습, 진학 종합 로드맵)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 5월 18일(월) ~ 11월 30일(월)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>



                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <CollectionsBookmark style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담 전 준비서류
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="‘신청’버튼을 클릭하여 상담신청 후, PC에서 http://geumcheon.eduplan.co.kr 로그인 후 상단의 <나의활동>의 <컨설팅 기초자료 입력>에서 모든 항목을 입력해야 정식으로 접수가 완료됩니다." />
                                </ListItem>
                            </List>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 10일 전까지 기초자료를 입력하지 않았거나 누락의 경우, 예비자 학생에게 컨설팅 기회가 자동 양도되므로 주의해 주세요." />
                                </ListItem>
                            </List>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="모바일에서는 기초자료 입력을 지원하지 않습니다." />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>


                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff" mb={8}>
                    <Box>
                        <Help style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담방법
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 신청 후, 3일 이내에 담당자가 확인 연락을 드립니다.(컨설팅 일정, 사전 자료제출, 컨설팅 진행안내)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="정확한 컨설팅을 위해 사전자료 제출은 필수입니다(자료제출은 5월 6일부터/ 기초조사서, 성적입력, 학생부, 심리검사 등)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="(기한 내 사전자료 미제출(또는 누락)의 경우, 컨설팅은 자동 취소되며 예비자 학생에게 권한이 이양됩니다.)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 시간 전에 미리 화상컨설팅 준비를 마치고, 약속된 컨설팅 시간에 컨설팅을 시작합니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 시간은 1인 50분이며, 다음 학생의 컨설팅 진행 관계로 초과상담은 불가하므로, 화상컨설팅 설치 및 준비에 차질 없게 해주세요." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="(시간에 늦게 시작된 경우, 해당 잔여시간 까지만 상담이 가능하며, 잔여시간 이후 접속이 된 경우는 상담이 자동 취소됩니다)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="상담을 통해 수집된 개인정보 및 성적, 상담내용 등 모든 정보는 일절 유출되지 않으며, 철저한 비밀이 보장을 약속 드리니 부담없이 상담해주세요." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="진학 및 학습관련 질문 외, 심리상담이나 생활관련 또는 기타의 경우 답변드릴 수 없으니, 이 점 참고해주세요." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary={
                                        <React.Fragment>
                                            <span style={{ color: "red", fontSize: "16px" }}>
                                                1:1 화상(비대면) 입시컨설팅 ZOOM 설치방법 및 프로그램 다운로드
                                        </span>
                                            <Link style={{ fontSize: "1rem" }} href="#" onClick={this.handleGoURL.bind(this, "http://geumcheon.eduplan.co.kr/%EA%B8%88%EC%B2%9C%EA%B5%AC_%ED%99%94%EC%83%81_%EB%8C%80%EB%A9%B4_%EC%BB%A8%EC%84%A4%ED%8C%85_%EB%A7%A4%EB%89%B4%EC%96%BC_%EC%98%88%EC%95%BD%EC%9E%90%EC%9A%A9.pdf")} >
                                                [예약자용]※ZOOM설치방법 PDF 파일 다운로드
                                        </Link>
                                            <Link style={{ fontSize: "1rem" }} href="#" onClick={this.handleGoURL.bind(this, "https://apps.apple.com/kr/app/zoom-for-intune/id1462818858")} >
                                                [ZOOM 프로그램 다운로드 iOS]
                                        </Link>
                                            <Link style={{ fontSize: "1rem" }} href="#" onClick={this.handleGoURL.bind(this, "https://play.google.com/store/apps/details?id=us.zoom.videomeetings4intune")} >
                                                [ZOOM 프로그램 다운로드 Android]
                                        </Link>
                                        </React.Fragment>
                                    } />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
            </div>
        );

        const online = (
            <div style={{ height: this.props.visible ? "auto" : "0px", minHeight: "calc(100vh - 163px)", marginTop: "43px" }}>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Info style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            주의사항
                        </Box>
                        <Box>
                            <List dense={true}>
                                {this.props.buffer ? (
                                    <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                        <FPlistItemText primary="* 모집인원 수가 초과되었을 경우, '대기신청'으로 신청 가능하며, 취소인원이 있을 때 담당자가 연락을 드립니다." />
                                    </ListItem>) : null}
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="본 서비스는 금천구청 고교 재학생(또는 졸업생)과 금천구 주민들에게만 제공되는 서비스입니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="특성화고 전형, 고른기회전형은 전문적인 상담이 어렵습니다. 사전에 참고해주시기 바랍니다.(예체능은 불가)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="컨설팅 참가학생이 여럿인 경우, 학생 1명에 1아이디로 로그인해서 신청해주세요." />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <DateRange style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            신청기간
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 5월 1일(금) ~ 2020년 12월 15일(화)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff" mb={8}>
                    <Box>
                        <Schedule style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            상담일정
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 5월 1일(금) ~ 2020년 12월 18일(수)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
            </div>
        );

        switch (this.props.way) {
            case "I":
                return interview;
            case "V":
                return video;
            case "O":
                return online;
            default: return (<div></div>);
        }
    }
};
InfoConsult.propTypes = {
    way: PropTypes.string,
    visible: PropTypes.bool,
};
InfoConsult.defaultProps = {
    way: "O",
    visible: true,
};

export class InfoPresentation extends Component {
    handleGoURL = (link, e) => {
        e.preventDefault();

        const curDate = new Date();
        if (curDate < new Date("2020-10-07 22:30:01")) {
            if (callNativeFunction("goLocationBrowser", "http://geumcheon.eduplan.co.kr/Resources/초중등_학부모가_반드시_알아야_할_고입과_대입.pdf") == 0) {
                window.open("http://geumcheon.eduplan.co.kr/Resources/초중등_학부모가_반드시_알아야_할_고입과_대입.pdf");
            }
        }
        else if (curDate > new Date("2020-10-08 15:59:59") && curDate < new Date("2020-10-08 22:30:01")) {
            if (callNativeFunction("goLocationBrowser", "http://geumcheon.eduplan.co.kr/Resources/최상위권_대학들이_원하는_학생부_관리방법.pdf") == 0) {
                window.open("http://geumcheon.eduplan.co.kr/Resources/최상위권_대학들이_원하는_학생부_관리방법.pdf");
            }
        }
        else if (curDate > new Date("2020-10-14 17:59:59") && curDate < new Date("2020-10-14 22:30:01")) {
            if (callNativeFunction("goLocationBrowser", "http://geumcheon.eduplan.co.kr/mobilewebservice/Resources/변화하는_대입제도의_이해와_백전백승_전략.pdf") == 0) {
                window.open("http://geumcheon.eduplan.co.kr/mobilewebservice/Resources/변화하는_대입제도의_이해와_백전백승_전략.pdf");
            }
        }
        else if (curDate > new Date("2020-10-15 17:59:59") && curDate < new Date("2020-10-15 22:30:01")) {
            if (callNativeFunction("goLocationBrowser", "http://geumcheon.eduplan.co.kr/mobilewebservice/Resources/학생부종합전형_대비_자소서_작성과_면접_전략의_실제.pdf") == 0) {
                window.open("http://geumcheon.eduplan.co.kr/mobilewebservice/Resources/학생부종합전형_대비_자소서_작성과_면접_전략의_실제.pdf");
            }
        }
        else {
            this.props.onAlert({
                message: "강의용 교재는 해당 강의 시작 및 종료 전후 1시간 동안 다운로드 가능합니다."
            });
        }
    }

    render() {
        const ontime = (
            <div style={{ height: this.props.visible ? "auto" : "0px", minHeight: "calc(100vh - 163px)", marginTop: "43px" }}>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Info style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            안내사항
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2021. 대입의 이슈와 변화, 수시 및 정시 지원전략, 합격 배치점 및 사례" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<신청> 프로그램 신청 기간에만 신청이 가능합니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<예비> 모집인원이 초과되면 ‘예비대기자’로 신청이 가능하며, 취소자 발생 시에 담당자가 연락을 드립니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<마감> 신청 기간이 지나면 마감됩니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="* 문의 : 금천구청 교육지원과(02-2627-2822), 담당자 직통(070-5125-8615)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText primary="코로나19 상황에 따라 온라인으로 운영될 수 있습니다." />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Create style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            진행
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 정시 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="온라인 (유튜브 스트리밍)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Accessibility style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            대상
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 정시 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="희망 학생 및 학부모 (선착순 300명)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <DateRange style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            신청기간
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 정시 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현재 ~ 2020년 12월 11일(금)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Schedule style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            행사일정
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 정시 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 12월 12일(토) 14:00 ~ 16:00" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText primary="*코로나로 변경 될 수 있습니다." />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff" mb={8}>
                    <Box>
                        <Help style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            행사내용
                        </Box>
                        <Box>
                            <Box p={1} fontSize={18} fontWeight="bold" color="#357190">[정시 지원전략 1-2부]</Box>
                            <Box pl={1} pr={1} fontWeight="bold" >- 주제 : 2021. 정시지원 전략 중심 내용</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 8px" }}>
                                <ListItem>
                                    <FPlistItemText secondary="1. 2021. 정시의 다양한 변수" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="2021.정시 전형의 주요 변화 분석" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="합불사례로 보는 정시 합격선에 영향을 주는 변수" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="모집인원 규모 변화와 합격선" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="경쟁률 변화가 합격선에 미치는 영향" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="기타 심리적 요소 VS 합격선" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="2. 수도권 주요대학 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="합불사례로 보는 최상위권 대학 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="합불사례로 보는 중상위권 대학 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="합불사례로 보는 중위권 대학 지원전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="합불사례로 보는 경기. 인천권 지원전략" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="3. 마무리" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="모의지원과 배치지원 참고표" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="모의지원의 허와 실" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >- 연사 : 김창묵</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현. 경신고 진학부장" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="대교협 상담교사 대상 직무연수 전문강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울시 교육청 대학진학지도지원단 대입직무연수 강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울진학지도협의회 및 전국진학지도협의회 상담국 이사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="EBSi 상담위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="교육청 설명회 및 컨설팅 (대구교육청, 부산교육청, 강원교육청, 광주교육청, 전분교육청, 제주교육청)" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >- 연사 : 이규복</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현. ㈜퓨쳐플랜 법인이사, (사)청소년드림토피아 이사장 교육청 지정 연수기관 강사 및 진로진학상담1급 정교사 교수요원 (전남교육청, 태안교육청, 서울시교육청, 대구교육청, 경기교육청 외)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="전. 한국진로학습연구소 대표이사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="유니포타임즈 진학진로 전문컬럼니스트" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="이슈투데이 입시전략 전문컬럼니스트" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="한국교육개발원 진로진학 전문위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="씨스쿨 교육전략연구소 소장 EBS수능방송 입시전략 자문위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="강남구청수능방송 입시전략 자문위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="중앙교육진흥연구소 입시분석 실장" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="고려대학교 교육학 석사(교육정보학)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="모의고사 및 수능 분석/전국 배치참고표 개발" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="*강의(진로진학교사 직무연수 및 교장연수-경기도교육청, 전라남도교육청, 서울시교육청, 경북칠곡교육청, 천안교육청, 전남교육청 외/중앙일보 다빈치센터 학부모 교실/전국고교 초청 설명회/신세계 백화점/현대 백화점(삼성)외)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="*주요연구 (모의고사 및 수능 분석/전국 배치참고표 개발, 현재 진로진학 컨설팅 및 시스템 개발, 컨설팅 프로그램 진로진학가이드 통 분석/개발, 한국교육개발원 진로진학 서비스 개발, GLP AP 진단검사 개발(퓨처북), 진로진학정보 시스템 구축 및 운영(교육개발원,강남구청등), EBS 인터넷 대입 종합서비스 구축, 대학진학종합 시스템 구축 및 운영(대교협), 모의고사 문제은행 시스템 구축(중앙교육), 심리검사 분석설계 및 개발 (중앙교육) 외" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
            </div>
        );

        const admission = (
            <div style={{ height: this.props.visible ? "auto" : "0px", minHeight: "calc(100vh - 163px)", marginTop: "43px" }}>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Info style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            안내사항
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울 및 수도권 대학 현직 입학사정관으로부터 직접 듣는 합격전략 및 자소서 면접 사례" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<신청> 프로그램 신청 기간에만 신청이 가능합니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<예비> 모집인원이 초과되면 ‘예비대기자’로 신청이 가능하며, 취소자 발생 시에 담당자가 연락을 드립니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<마감> 신청 기간이 지나면 마감됩니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="* 문의 : 금천구청 교육지원과(02-2627-2822), 담당자 직통(070-5125-8615)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText primary="입학사정관특강 3일 연속 참여시, 1:1 입시컨설팅 우선 참여 특혜를 드립니다." />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Accessibility style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            대상
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="희망 학생 및 학부모 (선착순 50명 * 3회)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <DateRange style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            신청기간
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 6월 3일(수) ~ 2020년 7월 29일(수)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Schedule style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            행사일정
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 7월 28일(화) 16:50 ~ 19:00" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 7월 29일(수) 16:50 ~ 18:30" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 7월 30일(목) 16:50 ~ 18:40" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText primary="*코로나로 변경 될 수 있습니다." />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff" mb={8}>
                    <Box>
                        <Help style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            행사내용
                        </Box>
                        <Box>
                            <Box pl={1} pr={1} fontWeight="bold" color="red" >- 입장시 유의사항</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="행사장에서는 KF94이상 마스크 착용 (비말차단용 마스크 불가)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="출입자 명부 작성(사전 신청자만 출입 가능)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="행사 당일 발열체크, 37.8도 이하만 입장가능" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >1. 2021. 대학별 대입전형 주요사항</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="수시 주요전형 변경 포인트와 특징" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="정시 주요전형 변경 포인트와 특징" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >2. 대학별 인재상</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="Best와 Worst 학생부 비교" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="학생부 종합전형 사례로 보는 인재상" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="면접관을 사로잡는 자소서와 면접 사례" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >*현장 질의응답*</Box>
                        </Box>
                    </Box>
                </Box>
            </div>
        );

        const academy = (
            <div style={{ height: this.props.visible ? "auto" : "0px", minHeight: "calc(100vh - 163px)", marginTop: "43px" }}>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Place style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            안내사항
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="초중고 학부모가 반드시 일아야할 대입 및 고입, 대입 합격 사례, 학생부 관리 및 자소서 작성 등" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<신청> 프로그램 신청 기간에만 신청이 가능합니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<예비> 모집인원이 초과되면 ‘예비대기자’로 신청이 가능하며, 취소자 발생 시에 담당자가 연락을 드립니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="<마감> 신청 기간이 지나면 마감됩니다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="* 문의 : 금천구청 교육지원과(02-2627-2822), 담당자 직통(070-5125-8615)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText primary="* 온라인 실시간 유튜브 (코로나로 인해 비대면 교육)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText primary="* 사전 신청하신 분만 초대하여 입장 가능하니, 반드시 사전 신청 부탁드립니다. (선착순 주의)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Accessibility style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            대상
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="희망 학부모 (선착순 40명 * 3개반)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <DateRange style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            신청기간
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 초·중등반" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현재 ~ 2020년 10월 6일(화)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 공통(초·중·고)반" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현재 ~ 2020년 10월 7일(수)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 고등반" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현재 ~ 2020년 10월 14(수)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Schedule style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            행사일정
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 초·중등반" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 10월 7일(수) 19:00 ~ 21:30" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 공통(초·중·고)반" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 10월 8일(목) 19:00 ~ 21:30" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="- 고등반" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="2020년 10월 14(수) ~ 15일(목) 19:00 ~ 21:30" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <Create style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            운영 방식
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="온라인 실시간 유튜브 (코로나로 인해 비대면 교육)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff">
                    <Box>
                        <CollectionsBookmark style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            강의용 교재
                        </Box>
                        <Box>
                            <List dense={true}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary={
                                        <React.Fragment>
                                            <span style={{ fontSize: "16px" }}>
                                                - 강의용 교재는 해당 강의 시작 1시간전부터 종료 1시간 후까지 다운로드 가능합니다.
                                        </span>
                                            <Link style={{ fontSize: "1rem" }} href="#" onClick={this.handleGoURL.bind(this, "")} >
                                                [강의용 교재 다운로드]
                                        </Link>
                                        </React.Fragment>
                                    } />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
                <Box p={2} display="flex" borderBottom={1} bgcolor="#fff" mb={8}>
                    <Box>
                        <Help style={{ color: "#2f96d4" }} />
                    </Box>
                    <Box flexGrow={1}>
                        <Box pl={1} fontSize={18} fontWeight="bold" color="#2f96d4">
                            행사내용
                        </Box>
                        <Box>
                            <Box p={1} fontSize={18} fontWeight="bold" color="#357190">[초·중등 반]</Box>
                            <Box pl={1} pr={1} fontWeight="bold" >- 주제 : 초중등 학부모가 반드시 알아야 할 고입‧ 대입</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem>
                                    <FPlistItemText secondary="1. 중 ‧ 고등학교 교육과정에 대한 이해" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="교육과정을 알아야 대입에 성공한다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="고등학교 성적표를 알면 전략이 보인다." />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="2. 초중등 학부모가 알아야할 대입의 기초" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="대학입시 용어와 기본 이해" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="관심대학 전형별 특징과 합격전략" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="3. 고교유형별 현명한 대입 전략" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="고교유형별 특징과 강약점" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="특목고, 자사고 폐지와 향후 전망" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="고교 선택이 대학 진학을 결정한다" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="우리아이 성향에 맞는 대입전형 찾아보기" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="3. 성공적인 대입을 위한 준비" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="자유학기제를 잘 활용하는 아이, 대입에도 성공한다." />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="성공적인 대입을 위해 지금부터는 이렇게!" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >- 연사 : 윤상형</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현. 영동고등학교 진학부장" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울시교육청 대학진학지도 지원단" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="한국 대학교육협의회 대입상담센터 대표강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="tbs, ‘기적의 TV, 상담 받고 대학 가자’ 진행 및 출연/EBS 라디오, ‘행복한 교육세상’ 패널 출연" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울특별시교육청 학부모 대상 입시 설명회 강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울특별시교육청 교사 대상 입시 설명회 강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울특별시교육쳥 교사 대상 진학지도 관련 직무연수 강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울특별시교육청 중등진학지도연구회 직무연수 강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="성북구청, 마포구청, 강북구청 광주 남구청 학부모 대상 입시 설명회 강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="대구광역시 교육청 교사 대상 진학지도 관련 직무연수 강사" />
                                </ListItem>
                            </List>
                            <Box p={1} fontSize={18} fontWeight="bold" color="#357190">[공통(초·중·고) 반]</Box>
                            <Box pl={1} pr={1} fontWeight="bold" >- 주제 : 최상위권 대학들이 원하는 학교생활기록부 관리 방법</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 8px" }}>
                                <ListItem>
                                    <FPlistItemText secondary="1. 학생부 평가 방법 실제(Worst vs Best 케이스 분석)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="학생부 종합전형 달라지는 점(현 중2~고2) - 블라인드 평가를 중심으로" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="학생부 학업역량 영역 평가 사례 분석" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="학생부 인성영역 평가 사례 분석" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="학생부 발전가능성 영역 사례 분석" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="유사도 검증시 불이익 사례 분석(고려대 방식)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="교과 vs 종합 vs 논술전형 합격률 비교" />
                                </ListItem>

                                <ListItem>
                                    <FPlistItemText secondary="2. 경쟁력 있는 학생부 만들기 실습" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="고등학교 학생부 로드맵 짜기 - 학교 교육계획서 분석하기" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="전공적합성을 높이는 선택과목 선택" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="학업역량과 전공적합성 모두 잡는 대회 선택" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="전공적합성을 높이는 동아리 선택" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="전공적합성을 높이는 진로활동 선택" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="전공과 연관된 봉사활동 선택" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >- 연사 : 이규복</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현. ㈜퓨쳐플랜 법인이사, (사)청소년드림토피아 이사장 교육청 지정 연수기관 강사 및 진로진학상담1급 정교사 교수요원 (전남교육청, 태안교육청, 서울시교육청, 대구교육청, 경기교육청 외)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="전. 한국진로학습연구소 대표이사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="유니포타임즈 진학진로 전문컬럼니스트" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="한국교육개발원 진로진학 전문위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="씨스쿨 교육전략연구소 소장 EBS수능방송 입시전략 자문위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="강남구청수능방송 입시전략 자문위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="중앙교육진흥연구소 입시분석 실장" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="고려대학교 교육학 석사(교육정보학)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="모의고사 및 수능 분석/전국 배치참고표 개발" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="*강의(진로진학교사 직무연수 및 교장연수-경기도교육청, 전라남도교육청, 서울시교육청, 경북칠곡교육청, 천안교육청, 전남교육청 외/중앙일보 다빈치센터 학부모 교실/전국고교 초청 설명회/신세계 백화점/현대 백화점(삼성)외)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="*주요연구 (모의고사 및 수능 분석/전국 배치참고표 개발, 현재 진로진학 컨설팅 및 시스템 개발, 컨설팅 프로그램 진로진학가이드 통 분석/개발, 한국교육개발원 진로진학 서비스 개발, GLP AP 진단검사 개발(퓨처북), 진로진학정보 시스템 구축 및 운영(교육개발원,강남구청등), EBS 인터넷 대입 종합서비스 구축, 대학진학종합 시스템 구축 및 운영(대교협), 모의고사 문제은행 시스템 구축(중앙교육), 심리검사 분석설계 및 개발 (중앙교육) 외" />
                                </ListItem>
                            </List>
                            <Box p={1} fontSize={18} fontWeight="bold" color="#357190">[고등 반, 1-2회기]</Box>
                            <Box pl={1} pr={1} fontWeight="bold" >- 주제 : 변화하는 대입제도의 이해와 백전백승 전략</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 8px" }}>
                                <ListItem>
                                    <FPlistItemText secondary="1. 교육 환경의 변화와 대응" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="학령인구의 감소, 학생부 기재의 변화, 수능제도의 변화, 입시제도의 변화" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="2. 교육과정의 변화와 대응" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="2015교육과정에 맞는 교과목 선택, 학점제의 이해와 대비" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="3. 전형별 이해와 대응마무리" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="학생부 위주(교과/종합), 논술위주, 실기/실적 위주, 수능위주" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="4. 사회변화와 자녀의 진로진학 지도" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="사회변화, 인식변화에 따른 자녀의 진로진학 지도 포인트" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >- 연사 : 박문수</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현. 청원여고 교무부장" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="TBS 상담받고 대학가자 MC 및 전문위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="한국 대학교육 협의회 대표강사, 상담교사, 파견교사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="전. 청원여고 진학부장, 연구부장" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울시교육청 진학지도 지원단" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="EBS 대입정보설명회 강사, 교육청 컨설팅 위원, 대학 정책교사 및 대학별고사 검토 위원" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="자격연수 초빙 강사 (교장 교감 진로진학교사 대상 )" />
                                </ListItem>
                            </List>
                            <Box p={1} fontSize={18} fontWeight="bold" color="#357190">[고등 반, 3-4회기]</Box>
                            <Box pl={1} pr={1} fontWeight="bold" >- 주제 : 학생부종합전형 대비 자소서 작성과 면접 전략의 실제</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 8px" }}>
                                <ListItem>
                                    <FPlistItemText secondary="1. 학생부종합전형 평가에 대한 이해" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="주요 대학별 특성 파악 방법" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="2. 학교생활기록부 분석 방법" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="3. 사례로 보는 자기소개서" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="대학이 바라보는 자기소개서" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="자기소개서의 역할 분석" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 16px 0px 24px" }}>
                                    <FPlistItemText secondary="자기소개서 문항별 강조점 및 작성법의 실제" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="4. 주요 대학별 면접평가 주안점" />
                                </ListItem>
                                <ListItem>
                                    <FPlistItemText secondary="5. 합격하는 면접 준비 꿀팁" />
                                </ListItem>
                            </List>
                            <Box pl={1} pr={1} fontWeight="bold" >- 연사 : 박정준</Box>
                            <List dense={true} style={{ padding: "0px 0px 24px 12px" }}>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="현. 오산고등학교 진학부장" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="서울시교육청 대입진학지원단" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="전. EBSi 학생부종합전형 면접 강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="교육부 논술강사" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="*지자체 수시 정시 상담 (서울시청, 동작구청, 양천구청, 금천구청, 동대문구청 외)" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="*교사 대상 직무연수 (교육부, 서울시교육청, 광주시 교육청, 대구시 교육청, 부산시 교육청 외 )" />
                                </ListItem>
                                <ListItem style={{ padding: "0px 0px 0px 8px" }}>
                                    <FPlistItemText secondary="*집필 (교육부 독서논술가이드북, 개정교육과정을 반영한  진로진학가이드북, 수시 /정시 대입의 이해)" />
                                </ListItem>
                            </List>
                        </Box>
                    </Box>
                </Box>
            </div>
        );

        switch (this.props.way) {
            case "U":
                return ontime;
            case "E":
                return admission;
            case "A":
                return academy;
            default: return (<div></div>);
        }
    }
};
InfoPresentation.propTypes = {
    way: PropTypes.string,
    visible: PropTypes.bool,
    onAlert: PropTypes.func,
};
InfoPresentation.defaultProps = {
    way: "E",
    visible: true,
    onAlert: function () { }
};