import React, { Component } from 'react';
import { TextField, InputBase, Button, Grid, Typography, Box } from '@material-ui/core';
import PropTypes from 'prop-types';
import { typography } from '@material-ui/system';

export class FPTextField extends Component {
    render() {
        return (
            <div>
                <InputBase
                    fullWidth
                    id={this.props.name}
                    placeholder={this.props.label}
                    name={this.props.name}
                    type={this.props.type}
                    autoComplete={this.props.name}
                    onChange={this.props.onChange}
                    autoFocus={this.props.autoFocus}
                    value={this.props.value}
                    disabled={this.props.disabled}
                    inputProps={this.props.inputProps}
                    onKeyPress={this.props.onKeyPress}
                    style={{ backgroundColor: this.props.disabled ? '#FAFAFA' : '#FFFFFF', color: '#010101' }}
                    onBlur={this.props.onBlur}
                />
            </div >
        );
    }
}

FPTextField.propTypes = {
    required: PropTypes.bool,
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    name: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    type: PropTypes.string,
    onChange: PropTypes.func,
    onKeyPress: PropTypes.func,
    inputProps: PropTypes.object,
    onBlur: PropTypes.func,
};

FPTextField.defaultProps = {
    required: false,
    autoFocus: false,
    disabled: false,
    name: "",
    label: "",
    value: "",
    type: "search",
    onChange: () => { console.error("handleChange function is not defined"); },
    onKeyPress: () => { },
    inputProps: {},
    onBlur: null,
};

export class FPTextFieldButton extends Component {
    constructor(props) {
        super(props);

        this.handlePreventClick = this.handlePreventClick.bind(this);

        this.state = {
            countTimeTick: -1
        }
    }

    handlePreventClick = (event) => {
        console.log(event);
        event.stopPropagation();
    }

    handleButtonClick = () => {
        this.props.onClick();
    }

    handleCountTime = (timetick) => {
        this.setState = {
            countTimeTick: timetick
        }

        this.timeCounter = setTimeout(() => {
            this.setState(({ countTimeTick }) => ({
                countTimeTick: countTimeTick - 1
            }));
            if (this.state.countTimeTick < 0) {
                clearTimeout(this.timeCounter);
            }
        }, 1000);
    }

    handleKeyPress = (e) => {
        if (e.charCode === 13 && this.props.onClick != null) {
            this.props.onClick();
        }
    }

    render() {
        return (

            <Box display="flex">
                <Box flexGrow={1} bgcolor="#fff">
                    <InputBase
                        fullWidth
                        id={this.props.name}
                        placeholder={this.props.label}
                        name={this.props.name}
                        type={this.props.type}
                        autoComplete={this.props.name}
                        onChange={this.props.onChange}
                        autoFocus={this.props.autoFocus}
                        value={this.props.value}
                        disabled={this.props.disabled || this.props.textDisabled}
                        inputProps={this.props.inputProps}
                        onKeyPress={this.props.onKeyPress}
                    />
                </Box>
                {this.state.countTimeTick > -1 ? (
                    <Box pt={2} bgcolor="#fff" color="#FF7C27">
                        {this.state.countTimeTick}
                    </Box>
                ) : null}
                <Box p={1} pl={0} bgcolor="#fff">
                    <Button type="button" size="small" variant="contained" style={{ borderRadius: '0px', height: '100%', padding: '4px 0px' }} onClick={this.handleButtonClick} color="primary" disabled={this.props.disabled}>
                        {this.props.buttonName}
                    </Button>
                </Box>
            </Box>
        );
    }
}

FPTextFieldButton.propTypes = {
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    textDisabled: PropTypes.bool,
    name: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    type: PropTypes.string,
    inputProps: PropTypes.object,
    buttonName: PropTypes.string,
    onChange: PropTypes.func,
    onClick: PropTypes.func,
    onKeyPress: PropTypes.func,
};

FPTextFieldButton.defaultProps = {
    autoFocus: false,
    disabled: false,
    textDisabled: false,
    name: "",
    label: "",
    value: "",
    type: "search",
    inputProps: {},
    buttonName: "확인",
    onChange: () => { console.error("handleChange function is not defined"); },
    onClick: null,
    onKeyPress: this.handleKeyPress,
};

export class FPTextArea extends Component {
    constructor(props) {
        super(props);

        this.state = {
            areavalue: this.props.value,
        }
    }

    handleChange = (e) => {
        let targetValue = e.target.value;
        this.setState({
            areavalue: targetValue,
        });
    }

    handleKeyPress = (e) => {
        //if (e.target.value.length >= this.props.maxLength) {
        //    console.log("prevent", e.target.value.length, this.props.maxLength);
        //    e.preventDefault();
        //}
    }

    handleBlur = () => {
        this.props.onChange(this.props.name, this.state.areavalue);
    }

    // 업데이트가 제대로 안되서 보험용
    componentWillReceiveProps(nextProps) {
        if (this.state.areavalue != nextProps.value) {
            this.setState({
                areavalue: nextProps.value,
            });
        }
    }


    render() {
        return (
            <Grid item xs={this.props.xs} >
                <InputBase
                    fullWidth
                    id={this.props.name}
                    placeholder={this.props.label}
                    name={this.props.name}
                    type={this.props.type}
                    autoComplete={this.props.name}
                    onChange={this.handleChange}
                    autoFocus={this.props.autoFocus}
                    value={this.state.areavalue}
                    disabled={this.props.disabled}
                    inputProps={this.props.inputProps}
                    onKeyPress={this.handleKeyPress}
                    multiline={true}
                    rows={this.props.rows}
                    onBlur={this.handleBlur}
                />
                {
                    this.props.maxLength < 0 ? null : (
                        <Box display="flex" flexDirection="row-reverse" p={1} pr={1}>
                            <Box color="#9A9A9A" >
                                [{this.props.minLength} 자 ~ {this.props.maxLength} 자]
                            </Box>
                            <Box color="#FF7C27" pr={0.5}>
                                {this.state.areavalue.length} 자
                            </Box>
                        </Box>
                    )
                }
            </Grid>
        )
    }
}

FPTextArea.propTypes = {
    autoFocus: PropTypes.bool,
    disabled: PropTypes.bool,
    name: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    type: PropTypes.string,
    maxLength: PropTypes.number,
    minLength: PropTypes.number,
    xs: PropTypes.number,
    rows: PropTypes.number,
    inputProps: PropTypes.object,
    onChange: PropTypes.func,
    onKeyPress: PropTypes.func,
};

FPTextArea.defaultProps = {
    autoFocus: false,
    disabled: false,
    name: "",
    label: "",
    value: "",
    type: "search",
    maxLength: -1,
    minLength: -1,
    xs: 12,
    rows: 1,
    inputProps: {},
    onChange: () => { console.log("handleChange function is not defined"); },
    onKeyPress: this.handleKeyPress,
};