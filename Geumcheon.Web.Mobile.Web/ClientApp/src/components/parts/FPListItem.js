import React, { Component } from 'react';
import { Container, Grid, Typography, Box, Chip, Button, FormControlLabel, Checkbox, InputBase, Avatar } from '@material-ui/core';
import PropTypes, { array } from 'prop-types';

import { FPTextField, FPTextArea } from './FPTextField';

// 신청 아이템
export class FPEntranceListItem extends Component {
    handleClick = () => {
        this.props.onClick(this.props.item);
    }


    // 과제 상태 컬러
    handleStatetoName = () => {
        const itemState = this.props.item.Progress;

        switch (itemState) {
            case "R": return "icon Ty1";
            case "I": return "icon Ty2";
            case "C": return "icon Ty3";
            case "W": return "icon Ty4";
            default: return "icon Ty4";
        }
    }

    // 과제 상태 문자
    convertStateToString = () => {
        const itemState = this.props.item.Progress;

        switch (itemState) {
            case "R": return "신청대기";
            case "I": return "신청승인";
            case "C": return "상담완료";
            case "W": return "대기예약";
            default: return "알수없음";
        }
    }

    convertDate = (data) => {
        if (data == null) {
            return "없음";
        }
        else {
            let date = new Date(data.replace(/-/gi, "/"));

            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let day = date.getDate();
            let week = "월";
            switch (date.getDay()) {
                case 0: week = "일"; break;
                case 1: week = "월"; break;
                case 2: week = "화"; break;
                case 3: week = "수"; break;
                case 4: week = "목"; break;
                case 5: week = "금"; break;
                case 6: week = "토"; break;
            }
            return year.toString() + "." + (month < 10 ? "0" : "") + month.toString() + "." + (day < 10 ? "0" : "") + day.toString() + "(" + week + ")";
        }
    }

    // 기간
    convertDateTime = (data) => {
        if (data == null) {
            return "없음";
        }
        else {
            let date = new Date(data.replace(/-/gi, "/"));

            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let day = date.getDate();
            let hour = date.getHours();
            let minute = date.getMinutes();
            let week = "월";
            switch (date.getDay()) {
                case 0: week = "일"; break;
                case 1: week = "월"; break;
                case 2: week = "화"; break;
                case 3: week = "수"; break;
                case 4: week = "목"; break;
                case 5: week = "금"; break;
                case 6: week = "토"; break;
            }
            return year.toString() + "." + (month < 10 ? "0" : "") + month.toString() + "." + (day < 10 ? "0" : "") + day.toString() + "(" + week + ") " + (hour < 10 ? "0" : "") + hour.toString() + ":" + (minute < 10 ? "0" : "") + minute.toString();
        }
    }

    convertTitle = () => {
        if (this.props.isWay) {
            switch (this.props.item.ConsultWay.toUpperCase()) {
                case "O":
                    return "[온라인] " + this.props.item.Title;
                case "I":
                    return "[대면] " + this.props.item.Title;
                case "V":
                    return "[비대면] " + this.props.item.Title;
                default:
                    return this.props.item.Title;
            }
            
        }
        else {
            return this.props.item.Title
        }
    }

    render() {
        return (
            <Container component="div" maxWidth="xl" onClick={this.handleClick}>
                <Box display="flex" pt={1.5} pb={1.5} alignItems="center">
                    <Box p={1}>
                        <Chip label={this.convertStateToString()} className={this.handleStatetoName()} />
                        
                        <Box color="#010101" fontWeight="bold" style={{ textAlign: "center" }}>
                            {this.props.item.StudentName}
                        </Box>
                    </Box>
                    <Box flexGrow={1} pl={1} whiteSpace="nowrap" textOverflow="ellipsis" fontWeight="fontWeightRegular" overflow="hidden">
                        <Box whiteSpace={this.props.overflow ? "normal" : "nowrap"} textOverflow="ellipsis" fontWeight="fontWeightRegular" overflow={this.props.overflow ? "auto" : "hidden"} pb={1}>
                            {this.convertTitle()}
                        </Box>
                        
                        <Box fontWeight="fontWeightRegular" overflow="hidden" color="#7a7a7a" fontSize={15} >
                            신청일 : {this.convertDate(this.props.item.CreateDate)}
                        </Box>
                        {
                            this.props.item.Progress == "C" ?
                                (<Box fontWeight="fontWeightRegular" overflow="hidden" color="#7a7a7a" fontSize={15} >
                                    완료일 : {this.convertDate(this.props.item.ConfirmDate)}
                                </Box>) : null
                        }
                        {
                            this.props.item.ConsultWay == "I" ?
                                (<Box fontWeight="fontWeightRegular" overflow="hidden" color="#7a7a7a" fontSize={15} >
                                    상담일 : {this.props.item.Schedule.ConsultDate || "알수 없음"}
                                </Box>) : null
                        }
                        
                    </Box>
                    <Box pt={0.5} pb={0.5}>
                        {this.props.children}
                    </Box>
                </Box>
            </Container>
        );
    }
}

FPEntranceListItem.propTypes = {
    item: PropTypes.object,
    taskState: PropTypes.number,
    overflow: PropTypes.bool,
    isWay: PropTypes.bool,
    onClick: PropTypes.func,
};

FPEntranceListItem.defaultProps = {
    item: {},
    taskState: -1,
    overflow: false,
    isWay: false,
    onClick: () => { console.error("click function is not defined"); }
};

// 신청 아이템
export class FPPresentationListItem extends Component {
    handleClick = () => {
        this.props.onClick(this.props.item);
    }


    // 과제 상태 컬러
    handleStatetoName = () => {
        const itemState = this.props.item.State;

        switch (itemState) {
            case "R": return "icon Ty1";
            case "I": return "icon Ty2";
            case "C": return "icon Ty3";
            case "W": return "icon Ty4";
            default: return "icon Ty4";
        }
    }

    // 과제 상태 문자
    convertStateToString = () => {
        const itemState = this.props.item.State;

        switch (itemState) {
            case "R": return "신청대기";
            case "I": return "신청승인";
            case "C": return "상담완료";
            case "W": return "대기예약";
            default: return "알수없음";
        }
    }

    convertDate = (data) => {
        if (data == null) {
            return "없음";
        }
        else {
            let date = new Date(Date.parse(data));
            if (isNaN(date)) {
                date = new Date(Date.parse(data.replace(/-/gi, "/")));
            }

            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let day = date.getDate();
            let week = "월";
            switch (date.getDay()) {
                case 0: week = "일"; break;
                case 1: week = "월"; break;
                case 2: week = "화"; break;
                case 3: week = "수"; break;
                case 4: week = "목"; break;
                case 5: week = "금"; break;
                case 6: week = "토"; break;
            }
            return year.toString() + "." + (month < 10 ? "0" : "") + month.toString() + "." + (day < 10 ? "0" : "") + day.toString() + "(" + week + ")";
        }
    }

    // 기간
    convertDateTime = (data) => {
        if (data == null) {
            return "없음";
        }
        else {
            let date = new Date(data);
            if (isNaN(date)) {
                date = new Date(data.replace(/-/gi, "/"));
            }
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let day = date.getDate();
            let hour = date.getHours();
            let minute = date.getMinutes();
            let week = "월";
            switch (date.getDay()) {
                case 0: week = "일"; break;
                case 1: week = "월"; break;
                case 2: week = "화"; break;
                case 3: week = "수"; break;
                case 4: week = "목"; break;
                case 5: week = "금"; break;
                case 6: week = "토"; break;
            }
            return year.toString() + "." + (month < 10 ? "0" : "") + month.toString() + "." + (day < 10 ? "0" : "") + day.toString() + "(" + week + ") " + (hour < 10 ? "0" : "") + hour.toString() + ":" + (minute < 10 ? "0" : "") + minute.toString();
        }
    }

    render() {
        return (
            <Container component="div" maxWidth="xl" onClick={this.handleClick}>
                <Box display="flex" pt={1.5} pb={1.5} alignItems="center">
                    <Box p={1}>
                        <Chip label={this.convertStateToString()} className={this.handleStatetoName()} />
                    </Box>
                    <Box flexGrow={1} pl={1} whiteSpace="nowrap" textOverflow="ellipsis" fontWeight="fontWeightRegular" overflow="hidden">
                        <Box color="#010101" fontWeight="bold" >
                            [ {this.props.item.PresentationName} ]
                        </Box>
                        <Box fontWeight="fontWeightRegular" overflow="hidden" color="#7a7a7a" fontSize={15} >
                            신청일 : {this.convertDate(this.props.item.CreateDate)}
                        </Box>
                        <Box fontWeight="fontWeightRegular" overflow="hidden" color="#7a7a7a" fontSize={15} >
                            행사일 : {this.convertDate(this.props.item.PresentationDate)}
                        </Box>
                    </Box>
                    <Box pt={0.5} pb={0.5}>
                        {this.props.children}
                    </Box>
                </Box>
            </Container>
        );
    }
}

FPPresentationListItem.propTypes = {
    item: PropTypes.object,
    taskState: PropTypes.number,
    overflow: PropTypes.bool,
    isWay: PropTypes.bool,
    onClick: PropTypes.func,
};

FPPresentationListItem.defaultProps = {
    item: {},
    taskState: -1,
    overflow: false,
    isWay: false,
    onClick: () => { console.error("click function is not defined"); }
};