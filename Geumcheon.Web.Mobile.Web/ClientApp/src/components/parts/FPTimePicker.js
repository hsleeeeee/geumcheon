import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Dialog, DialogTitle, DialogContent, DialogActions, Typography, Grid, Select, Button } from '@material-ui/core';
import { OpenModal, CloseModal } from '../../helper/appfunction';

const FPDialog = withStyles({
    paper: {
        boxShadow: 'none',
        borderRadius: '0px'
    }
})(Dialog);

class FPTimePicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            clock: 0,
            hour: 1,
            minute: 0,
        }
        this.hournum = [];
        this.timenum = [];

        for (let i = 1; i < 13; i++) {
            this.hournum.push(i.toString());
        }

        for (let i = 0; i < 60; i++) {
            this.timenum.push(i.toString());
        }
    }

    // 데이터 입력
    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.type == "checkbox" ? e.target.checked : e.target.value;
        this.setState(nextState);
    }

    handleDialogClose = (value) => {
        CloseModal();
        if (value) {
            this.props.onClose(this.state, this.props.name);
        }
        else {
            this.props.onClose(null, this.props.name);
        }
    }

    // 타임피커 열릴때마다
    handleDialogEnter = () => {
        let timeStr = this.props.item.split(":");
        let hour = parseInt(timeStr[0]) == 0 ? 24 : parseInt(timeStr[0]);
        let clock = 0;
        if (hour > 12) {
            clock = 1;
            hour = hour - 12;
        }

        this.setState({
            clock: clock,
            hour: hour,
            minute: parseInt(timeStr[1]),
        });

        OpenModal(this.handleDialogClose);
    }

    render() {
        return (
            <div>
                <FPDialog
                    open={this.props.open}
                    onClose={() => this.handleDialogClose(false)}
                    fullWidth
                    maxWidth="xl"
                    elevation={0}
                    onEnter={this.handleDialogEnter}
                >
                    <DialogTitle disableTypography style={{ margin: '0px', padding: '8px', paddingTop: '0px' }}>
                        <Typography align="center" variant="h6">
                            시간 설정
                        </Typography>
                    </DialogTitle>
                    <DialogContent style={{ textAlign: 'center' }}>
                        <Grid container spacing={1} alignItems="center" style={{ border: '1px solid #dbdbdb', marginBottom: '16px' }}>
                            <Grid item xs={4}>
                                <Select native fullWidth name="clock" value={this.state.clock} onChange={this.handleChange} style={{ padding: '0px', paddingLeft: '8px', backgroundColor: '#FAFAFA' }} >
                                    <option value={0}>AM</option>
                                    <option value={1}>PM</option>
                                </Select>
                            </Grid>
                            <Grid item xs={4}>
                                <Select native fullWidth name="hour" value={this.state.hour} onChange={this.handleChange} style={{ padding: '0px', backgroundColor: '#FAFAFA' }}>
                                    {
                                        this.hournum.map(function (idx) {
                                            let name;
                                            if (idx < 10) {
                                                name = "0" + idx.toString();
                                            }
                                            else {
                                                name = idx.toString();
                                            }
                                            return <option key={idx} value={idx}>{name}시</option>
                                        })
                                    }
                                </Select>
                            </Grid>
                            <Grid item xs={4}>
                                <Select native fullWidth name="minute" value={this.state.minute} onChange={this.handleChange} style={{ padding: '0px', backgroundColor: '#FAFAFA' }}>
                                    {
                                        this.timenum.map(function (idx) {
                                            let name;
                                            if (idx < 10) {
                                                name = "0" + idx.toString();
                                            }
                                            else {
                                                name = idx.toString();
                                            }
                                            return <option key={idx} value={idx}>{name}분</option>
                                        })
                                    }
                                </Select>
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions style={{ margin: '0px', padding: '0px' }}>
                        <Grid item xs={6}>
                            <Button fullWidth autoFocus onClick={() => this.handleDialogClose(false)} color="secondary" variant="contained">
                                취소
                            </Button>
                        </Grid>
                        <Grid item xs={6}>
                            <Button fullWidth autoFocus onClick={() => this.handleDialogClose(true)} color="primary" variant="contained">
                                확인
                            </Button>
                        </Grid>
                    </DialogActions>
                </FPDialog>
            </div>
        )
    }
}

FPTimePicker.propTypes = {
    open: PropTypes.bool,
    name: PropTypes.string,
    item: PropTypes.string,
    onClose: PropTypes.func,
};

FPTimePicker.defaultProps = {
    open: false,
    name: '',
    item: '00:00:00',
    onClose: () => { console.log("close function is not defined"); },
};

export default FPTimePicker