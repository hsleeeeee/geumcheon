import React, { Component } from 'react';
import { AppBar, Toolbar, IconButton, Typography } from '@material-ui/core';
import PropTypes from 'prop-types';
import { ArrowBackIos } from '@material-ui/icons'

class DialogAppbar extends Component {
    handleDialogClose = () => {
        this.props.onClose();
    };

    render() {
        return (
            <AppBar
                elevation={0}
                position="fixed"
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={this.handleDialogClose}
                        edge="start"
                        style={{ position: 'absolute', left: '14px', padding: '16px' }}

                    >
                        <ArrowBackIos />
                    </IconButton>
                    <Typography variant="h6" noWrap >
                        {this.props.title}
                    </Typography>
                </Toolbar>
            </AppBar>
        );
    };
}

DialogAppbar.propTypes = {
    title: PropTypes.string,
    onClose: PropTypes.func
};

DialogAppbar.defaultProps = {
    title: "더스쿨",
    onClose: () => { console.error("onClose function is not defined"); }
};

export default DialogAppbar