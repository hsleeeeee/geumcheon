import React, { Component, Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Collapse, Avatar, Divider, List, ListItem, ListItemIcon, ListItemText, ListItemAvatar, Paper, SwipeableDrawer, AppBar, Toolbar, IconButton, Typography, Button, Box, Backdrop, CircularProgress, BottomNavigation, BottomNavigationAction, Card, CardActions, CardMedia, CardContent, Select } from '@material-ui/core';
import { ArrowBackIos, ArrowForwardIos, Home, Event, Settings, Photo, Menu, Close, ExpandMore, Info, Videocam, Face, Message, School, BusinessCenter, SupervisorAccount, Assessment, Notifications, SubdirectoryArrowRight, MenuBook, Person } from '@material-ui/icons';
import ListIcon from '@material-ui/icons/List';
import { SnackbarProvider, useSnackbar } from 'notistack';

import LoginDialog from '../dialog/login.dialog';
import { GetUserInfoRole, GetUserInfo } from '../helper/appfunction';
import { subPath } from '../helper/appstring';


/// 사용자 프로필 시작

class ProfileStudent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const userinfo = JSON.parse(localStorage.getItem('user'));

        return (
            <Box display="flex" p={1}>
                <Box style={{ padding: '8px 12px 0px 12px' }}>
                    <Avatar>
                        <Photo />
                    </Avatar>
                </Box>

                {userinfo == null ? (
                    <Box display="flex" alignItems="center" >
                        <Typography variant="subtitle1">
                            로그인 후 신청 가능합니다.
                        </Typography>
                    </Box>
                ): (
                    <Box>
                        <Typography variant="subtitle1">
                            {userinfo.name || ""}
                        </Typography>
                        <Typography variant="body1">
                            {userinfo.school || ""}
                        </Typography>
                    </Box >
                )}
            </Box>
        )
    }
}

/// 사용자 프로필 끝


/// 푸시 관련 시작

function LocalPushNotification(props) {
    window.onPushNotificationOpen = function (title, content, data) {
        console.log(title, content, data);

        if (content == null && data == null) {
            props.onRedirect(title);
        }
        else if (title == null && content == null) {
            props.onRedirect(data);
        }
        else {
            enqueueSnackbar(JSON.stringify({
                title: title,
                content: content,
                data: data,
            }));
        }
    }

    const { enqueueSnackbar } = useSnackbar();

    const handleClick = () => {
        window.onPushNotificationOpen("asfklj", "ASfjjjjskdl", "asdkfljsafkljsadfklsda");

        enqueueSnackbar(JSON.stringify({
            title: "얘는 타이틀",
            content: "Callback fired when the component requests to be closed. The `reason` parameter can optionally be used to control the response to `onClose`, for example ignoring `clickaway`. @param {object} event The event source of the callback @param {string} reason Can be:`'timeout'` (`autoHideDuration` expired) or: `'clickaway'` or: `'maxsnack'` (snackbar is closed because `maxSnack` has reached.) @param {string|number} key key of a Snackbar",
            data: "얘는 링커",
        }));
    }

    return (
        <Fragment>
        </Fragment>
    );
}

const PushNotificationItem = React.forwardRef((props, ref) => {
    const { closeSnackbar } = useSnackbar();
    const [expanded, setExpanded] = useState(false);
    const [data, setData] = useState(JSON.parse(props.message));

    const handleExpand = (e) => {
        if (e != null) {
            e.stopPropagation();
        }
        setExpanded(!expanded);

        console.log(GetUserInfo("id"), data);

    };

    const handleReidrect = () => {
        if (data.data != null) {
            props.onRedirect(data.data);
        }
        else {
            console.log("no push data");
        }
        closeSnackbar(props.id);
    };

    const handleDismiss = () => {
        closeSnackbar(props.id);
    };

    return (
        <Card ref={ref} style={{ width: '100vh', margin: "0px 16px" }}>
            <CardActions style={{ backgroundColor: '#FF7704', paddingTop: '4px', paddingBottom: '4px' }}>
                <Box display="flex" width={1}>
                    <Box flexGrow={1} p={1.5} bgcolor="#FF7C27" color="#fff" style={{ textOverflow: 'ellipsis' }} onClick={handleReidrect}  >
                        {data.title}
                    </Box>
                    <Box>
                        <IconButton aria-label="Show more" onClick={handleExpand} style={{ transform: (expanded ? 'rotate(180deg)' : 'rotate(0deg') }} >
                            <ExpandMore />
                        </IconButton>
                    </Box>
                    <Box>
                        <IconButton onClick={handleDismiss}>
                            <Close />
                        </IconButton>
                    </Box>
                </Box>
            </CardActions>
            <Collapse in={expanded} unmountOnExit>
                <Divider />
                <Box p={1.5} maxHeight={100} fontWeight="fontWeightRegular" style={{ overflowY: 'auto' }}>
                    {data.content}
                </Box>
            </Collapse>
        </Card>
    )
});


/// 푸쉬 관련 끝



/// 왼쪽 메뉴 아이콘 시작
class NavItem extends Component {
    constructor(props) {
        super(props);

        this.components = {
            info: Info,
            interview: Person,
            video: Videocam,
            online: Message,
            ontime: School,
            admission: BusinessCenter,
            academy: MenuBook,
            report: ListIcon,
            board: Notifications
        };
    }

    handleMenu = (e, open, link, index, depth) => {
        this.props.onMenu(e, open, link, index, this.props.depth);
    }

    handleCheckActiveMenu = () => {
        if (this.props.parentIndex < 0 || this.props.depth == 0) {
            return this.props.activeMenu[this.props.depth] == this.props.index ? true : false;
        }
        else {
            if (this.props.activeMenu[this.props.depth] == this.props.index && this.props.activeMenu[this.props.depth - 1] == this.props.parentIndex) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    handleCheckSelectMenu = () => {
        if (this.props.parentIndex < 0 || this.props.depth == 0) {
            return this.props.selectMenu[this.props.depth] == this.props.index ? true : false;
        }
        else {
            if (this.props.selectMenu[this.props.depth] == this.props.index && this.props.selectMenu[this.props.depth - 1] == this.props.parentIndex) {
                return true;
            }
            else {
                return false;
            }
        }
    }


    render() {
        const IconComponent = this.components[this.props.item.Icon] || null;
        return (
            <div style={{ display: this.props.item.None ? "none" : "block" }}>
                <List style={{ paddingTop: this.props.depth == 0 ? '8px' : '4px', paddingBottom: this.props.depth == 0 ? '8px' : '0px' }}>
                    <ListItem button selected={this.handleCheckSelectMenu()} onClick={() => this.handleMenu(this, false, this.props.item.Link, this.props.index, this.props.depth)} style={{ paddingLeft: (this.props.depth * 40 + 16).toString() + 'px', paddingTop: '0px', paddingBottom: '0px' }}>
                        {
                            IconComponent != null ? (
                                <ListItemIcon>
                                    <IconComponent width="24" height="24" fill="#010101" />
                                </ListItemIcon>
                            ) :
                                (
                                    <ListItemIcon style={{ minWidth: '24px', paddingLeft: '6px' }}>
                                        <SubdirectoryArrowRight width={10} height={10} />
                                    </ListItemIcon>
                                )
                        }
                        <ListItemText primary={this.props.item.Name} />
                    </ListItem>
                    {
                        this.props.item.submenu != null && this.props.item.submenu.length > 1 ? (
                            <Collapse in={this.handleCheckActiveMenu()} timeout="auto" unmountOnExit >
                                {this.props.item.submenu.map((subitem, index) => (
                                    <NavItem key={index} item={subitem} index={index} activeMenu={this.props.activeMenu} selectMenu={this.props.selectMenu} onMenu={this.props.onMenu} depth={this.props.depth + 1} topSelect={this.props.topSelect} parentIndex={this.props.index} />
                                ))}
                            </Collapse>
                        ) : null
                    }
                </List>
                {
                    this.props.depth == 0 ?
                        (<Divider />) : null
                }
            </div>
        )
    }

}

NavItem.propTypes = {
    item: PropTypes.object,
    index: PropTypes.number,
    activeMenu: PropTypes.array,
    selectMenu: PropTypes.array,
    depth: PropTypes.number,
    topSelect: PropTypes.bool,
    parentIndex: PropTypes.number,
    onMenu: PropTypes.func,
};
NavItem.defaultProps = {
    item: {
        Icon: 'bell',
        Name: "알림신청",
        Link: "",
        submenu: [
            {
                Name: "가정통신문",
                Link: ""
            },
            {
                Name: "알림문자",
                Link: ""
            },
        ]
    },
    index: 0,
    activeMenu: [99],
    selectMenu: [99],
    depth: 0,
    topIndex: 0,
    parentIndex: 0,
    onMenu: () => { console.log("menu function is not defined"); },
};


/// 왼쪽 메뉴 아이콘 끝



class NavMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nav: false,
            activeMenu: this.props.selectMenu || [99],
            navList: this.makeNavList(),
            userrole: GetUserInfoRole(),
            loginOpen: false
        }

        window.updateToken = function (token, type) {

            if (localStorage.getItem("os") == null) {
                localStorage.setItem("os", JSON.stringify({ os: type }));
            }

            const userinfo = JSON.parse(localStorage.getItem("user"));

            if (userinfo != null) {
                this.props.onRequest('api/DeviceToken', 'put', {
                    token: token,
                    type: type,
                    refreshtoken: userinfo.refreshtoken
                }).then((result) => {
                    console.log(result);
                });
            }
        }.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        var nextState = [];

        // 로그인시 메뉴 재구성
        if (nextProps.login && !this.props.login) {
            nextState.navList = this.makeNavList();
        }

        if (this.props.isLogin != nextProps.isLogin && nextProps.isLogin) {
            nextState.loginOpen = true;
            this.props.onPopupLogin(false);
        }

        //// 앞, 뒤로가기 클릭시 처리
        //if (nextProps.history.action == "POP" && (this.props.location.pathname != nextProps.location.pathname)) {
        //    console.log("prev or pop", nextProps.history);
        //    this.funcSetNavIdx(nextProps.location.pathname);
        //}
        //// 메인에서 아이콘 클릭 시
        //else if (this.props.location.pathname != nextProps.location.pathname) {
        //    if (this.props.selectMenu == nextProps.selectMenu) {
        //        this.funcSetNavIdx(nextProps.location.pathname);
        //    }
        //}
        //// 왼쪽 메뉴 네비게이션 처리
        //else if (this.props.selectMenu != null && nextProps.selectMenu != null) {
        //    if (this.props.selectMenu != nextProps.selectMenu) {
        //        nextState.activeMenu = nextProps.selectMenu;
        //    }
        //}

        this.setState(nextState);
    }

    makeNavList = () => {
        const userinfo = JSON.parse(localStorage.getItem('user'));

        const makeNav = [
            {
                Idx: 1,
                Name: "홈",
                TitleName: "금천구 대입지원",
                Icon: "info",
                Link: "/",
            },
            {
                Idx: 2,
                Name: "종합컨설팅",
                TitleName: "종합컨설팅",
                Icon: "video",
                Link: "/Consult/video",
            },
            {
                Idx: 3,
                Name: "정시컨설팅",
                TitleName: "정시컨설팅",
                Icon: "interview",
                Link: "/Consult/interview",
            },
            {
                Idx: 4,
                Name: "온라인컨설팅",
                TitleName: "온라인컨설팅",
                Icon: "online",
                Link: "/Consult/online",
            },
            {
                Idx: 5,
                Name: "대입설명회",
                TitleName: "대입설명회",
                Icon: "ontime",
                Link: "/Presentation/ontime",
            },
            {
                Idx: 6,
                Name: "입학사정관특강",
                TitleName: "입학사정관특강",
                Icon: "admission",
                Link: "/Presentation/admission",
            },
            {
                Idx: 7,
                Name: "학부모아카데미",
                TitleName: "학부모아카데미",
                Icon: "academy",
                Link: "/Presentation/academy",
            },
            {
                Idx: 8,
                Name: "신청 및 상담내역",
                TitleName: "신청 및 상담내역",
                Icon: "report",
                Link: "/Board/report",
                None: userinfo == null ? true : false
            },
            
        ];

        return makeNav;
    }

    toggleDrawer = (event, open, link, idx, depth) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        };

        let nextState = { nav: open };

        if (idx != null) {
            nextState.activeMenu = [...this.state.activeMenu];
            if (nextState.activeMenu[depth] != idx) {
                while (nextState.activeMenu.length > depth + 1) {
                    nextState.activeMenu.pop();
                }
            }
            nextState.activeMenu[depth] = idx;
        }

        if (link != null || idx != null) {

            let subMenu = {};
            subMenu.submenu = [...this.state.navList];

            nextState.activeMenu.forEach(function (item, idx) {
                if (idx <= depth) {
                    subMenu = subMenu.submenu[item];

                }
                else {
                    return false;
                }
            });

            const nextProps = [...this.state.activeMenu];

            if (link == "userinfo") {
                link = "/Account";
                nextProps[0] = 99;
            }
            else if (link == "logout") {
                localStorage.removeItem('user');
                link = "/";
                nextProps[0]= 1;
            }
            else if (link == "login") {
                nextState.loginOpen = true;
                link = null;
            }
            else {
                nextProps[depth] = idx;
                if (subMenu.submenu != null && subMenu.submenu.length > 1) {
                    nextState.nav = true;
                }
            }

            if (nextState.nav == false) {
                this.props.onSelectNavMenu(nextProps);
            }
        }

        this.setState(nextState);

        if (nextState.nav != true && link != null) {
            this.props.history.push(link);
        }
    };

    navClick = (index, depth) => {
        const nextState = [...this.state.activeMenu];
        nextState[depth] = index;

        this.setState({
            activemenu: nextState
        });
    }

    // 푸시 클릭 시 이동
    handlePushRedirect = (data) => {
        if (GetUserInfo("id") == null) {
            this.props.onAlert({ message: "로그인이 필요합니다." });
            return;
        }
        const params = data.split(";");
        if (params.length > 1) {
            switch (params[0].toLowerCase()) {
                case "online":
                    this.props.history.push({
                        pathname: '/Consult/online',
                        state: {
                            idx: params[1]
                        }
                    });
                    break;
                case "video":
                    this.props.history.push({
                        pathname: '/Consult/video',
                        state: {
                            idx: params[1]
                        }
                    });
                    break;
                case "interview":
                    this.props.history.push({
                        pathname: '/Consult/interview',
                        state: {
                            idx: params[1]
                        }
                    });
                    break;
                case "admission":
                    this.props.history.push({
                        pathname: '/Consult/admission',
                        state: {
                            idx: params[1]
                        }
                    });
                    break;
                case "ontime":
                case "occasional":
                    this.props.history.push({
                        pathname: '/Consult/ontime',
                        state: {
                            idx: params[1]
                        }
                    });
                    break;
                case "parentacademy":
                    this.props.history.push({
                        pathname: '/Consult/parentacademy',
                        state: {
                            idx: params[1]
                        }
                    });
                    break;
                default: console.log("push redirect error : ", params);
            }
        }
    }

    // 홈버튼
    handleClickhome = () => {
        this.setState({
            activeMenu: [1]
        });
        this.props.onSelectNavMenu([0]);
        this.props.history.push("/");
    }

    // 뒤로가기
    handleClickPrev = () => {
        this.props.history.goBack();
    }

    // 앞으로가기
    handleClickNext = () => {
        this.props.history.goForward();
    }

    // 설정 버튼
    handleClickSetting = () => {
        if (GetUserInfo("id") == null) {
            this.props.onAlert({
                message: "로그인 후 이용 가능합니다."
            });
        }
        else {
            this.toggleDrawer(this, false, "userinfo", 99, 0);
        }
    }

    // 뒤로가기 앞으로가기 시 타이틀 및 왼쪽 메뉴 인덱스 설정
    funcSetNavIdx = (url) => {
        let isBreak = false;
        let activeMenu = [99];

        for (let i = 0; i < this.state.navList.length; i++) {
            const rtnName = this.funcChildNavLink(this.state.navList[i], url);
            if (rtnName != null) {
                activeMenu = (i.toString() + rtnName).split("||||")[0].split(",");
                break;
            }
        }



        //Idx.forEach(function (item) {
        //    submenu = submenu.submenu[item];
        //});

        //this.state.navList.forEach(function (item) {
        //    if (isBreak) {
        //        return false;
        //    }
        //    item.submenu.forEach(function (subitem, index) {
        //        if (subitem.Link == url) {
        //            curitem = item.Idx;
        //            curSubitem = index;
        //            isBreak = true;
        //            return false;
        //        }
        //    });
        //});

        //const nextProps = { ...this.state.activeMenu };
        //nextProps[0] = 99;
        //this.props.onSelectNavMenu(nextProps);

        this.setState({
            activeMenu: activeMenu
        });
        this.props.onSelectNavMenu(activeMenu);

    }

    funcChildNavLink = (parent, link) => {
        if (parent.Link == link && !parent.IsHome) {
            return "||||" + parent.TitleName;
        }
        else {
            if (parent.submenu != null) {
                for (let i = 0; i < parent.submenu.length; i++) {
                    const rtnName = this.funcChildNavLink(parent.submenu[i], link);
                    if (rtnName != null) {
                        return "," + i + rtnName;
                    }
                }
                return null;
            }
            else {
                return null;
            }
        }
    }

    navTitle = (Idx) => {
        switch (window.location.pathname.toLocaleLowerCase()) {
            case subPath() + "":
                return "금천구 대입지원";
            case subPath() + "/account":
                return "설정"
            default:
                let rtnName = "금천구 대입지원";

                let submenu = {};
                submenu.submenu = [...this.state.navList];

                //if (Array.isArray(Idx)) {
                //    Idx.forEach(function (item) {
                //        if (item !== 99) {
                //            submenu = submenu.submenu[item];
                //        }
                //    });
                //}
                //else {
                //    this.state.navList.forEach((item) => {
                //        if (item.Idx == Idx) {
                //            submenu = item;
                //            return false;
                //        }
                //    });
                //}

                Idx.forEach(function (item) {
                    if (item !== 99) {
                        submenu = submenu.submenu[item];
                    }
                });

                if (submenu != null && submenu.TitleName != null && submenu.TitleName != "") {
                    rtnName = submenu.TitleName;
                }

                return rtnName

            //if (this.state.navList[Idx] == null) {
            //    return "더스쿨";
            //}
            //else {
            //    return this.state.navList[Idx].Name;
            //}
        }
    }



    handleLoginClose = () => {
        this.setState({
            loginOpen: false
        });
    }

    render() {
        const userinfo = JSON.parse(localStorage.getItem('user'));

        const sideList = () => (
            <div
                role="presentation"
                style={{ width: '320px' }}
            >
                <Paper square={false} elevation={0}>
                    <ProfileStudent />
                    {this.props.login ? (
                            <Box display="flex" p={1} pt={0} pl={9} >
                                <Button onClick={() => this.toggleDrawer(this, false, "userinfo", 99, 0)} variant="contained" size="small" style={{ marginRight: '10px' }}>
                                    설정
                                </Button>

                                <Button onClick={() => this.toggleDrawer(this, false, "logout", 99, 0)} variant="contained" size="small">
                                    로그아웃
                                </Button>
                            </Box>
                    ) : (

                            <Box display="flex" p={1} pt={0} pl={9} >
                                <Button onClick={() => this.toggleDrawer(this, false, "login", 99, 0)} variant="contained" size="small">
                                    로그인
                                </Button>
                            </Box>
                    )}
                    <Divider />
                    {
                        this.state.navList.map((item, index) => (
                         <NavItem key={item.Idx} item={item} index={index} activeMenu={this.state.activeMenu} selectMenu={this.props.selectMenu} onMenu={this.toggleDrawer} depth={0} topSelect={this.props.selectMenu[0] == index} parentIndex={-1} />
                        ))
                    }
                </Paper>
            </div>
        );

        const headerBar = () => (
            <div>
                <AppBar
                    position="fixed"
                    elevation={0}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={() => this.toggleDrawer(this, true)}
                            edge="start"
                            style={{ position: 'absolute', left: '14px', padding: '16px' }}
                        >
                            <Menu />
                        </IconButton>
                        <Typography variant="h6" noWrap >
                            {  this.navTitle(this.props.selectMenu) }
                        </Typography>
                    </Toolbar>
                </AppBar>
                <SwipeableDrawer
                    anchor="left"
                    open={this.state.nav}
                    onClose={() => this.toggleDrawer(this, false)}
                    onOpen={() => this.toggleDrawer(this, true)}
                >
                    {sideList()}
                </SwipeableDrawer>
            </div>
        );

        return (
            <div>
                {headerBar()}
                {this.state.loginOpen ? (<LoginDialog open={this.state.loginOpen} onAlert={this.props.onAlert} onClose={this.handleLoginClose} onLogin={this.props.onLogin} onRequest={this.props.onRequest} onBackDrop={this.props.onBackDrop} />) : null}
                <Backdrop open={this.props.backDropWait} style={{ zIndex: 2000 }}>
                    <CircularProgress style={{ color: "#fff" }} />
                    <Box color="#fff" pl={1}>
                        {this.props.backDropMsg}
                    </Box>
                </Backdrop>
                <footer style={{ bottom: '0px', position: 'fixed', zIndex: 100, width: '100%' }}>
                    <Divider />
                    <BottomNavigation>
                        <BottomNavigationAction icon={<Home />} onClick={this.handleClickhome} />
                        <BottomNavigationAction icon={<ArrowBackIos />} onClick={this.handleClickPrev} />
                        <BottomNavigationAction icon={<ArrowForwardIos />} onClick={this.handleClickNext} />
                        <BottomNavigationAction icon={<Settings />} onClick={this.handleClickSetting} />
                    </BottomNavigation>
                </footer>
                <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} autoHideDuration={5000} content={(key, message) => (
                    <PushNotificationItem id={key} message={message} onRedirect={this.handlePushRedirect} />
                )}>
                    <LocalPushNotification onRedirect={this.handlePushRedirect} />
                </SnackbarProvider>
            </div>
        )
    }
}

NavMenu.propTypes = {
    login: PropTypes.bool,
    selectMenu: PropTypes.array,
    backDropWait: PropTypes.bool,
    backDropMsg: PropTypes.string,
    isLogin: PropTypes.bool,
    onSelectNavMenu: PropTypes.func,
    onLogin: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
    onRequest: PropTypes.func,
    onPopupLogin: PropTypes.func,
};
NavMenu.defaultProps = {
    login: false,
    selectMenu: [99],
    backDropWait: false,
    backDropMsg: "",
    isLogin: false,
    onSelectNavMenu: () => { console.log("selectnavmenu function is not defined"); },
    onLogin: () => { console.log("login function is not defined"); },
    onBackDrop: () => { console.info("backdrop function is not defined"); },
    onAlert: () => { console.info("alert function is not defined"); },
    onRequest: () => { console.info("request function is not defined"); },
    onPopupLogin: () => { console.info("request function is not defined"); }
};


// NavMenu 컴포넌트
export default withRouter(NavMenu)

