import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import NavMenu from './NavMenu';
import { isLogin } from '../actions/webApi';
import { connect } from 'react-redux';

import { requestLogin, requestWebAPI } from '../actions/webApi';
import { AppCloseModal, ResponseMessage } from '../helper/appfunction';
import { setBackDropState } from '../actions/backdropState';
import { setAlertState } from '../actions/alertGlobal';
import { clickNavMenu } from '../actions/headerBar';
import { popupLogin } from '../actions/popupLogin';
import AlertDialog from '../dialog/alert.dialog';


class Layout extends Component {
    constructor(props) {
        super(props);

        window.CloseModal = function () {
            AppCloseModal();
        }.bind(AppCloseModal);
        
        window.NeedAppUpdate = function () {
            console.log("need app update");
        }.bind(this);

        // 얼럿창 닫고 호출할 함수
        window.alertFunc = null;
    };

    handleSetBackDrop = (value) => {
        this.props.setBackDropState(value);
    }

    handleAlertOpen = (data, func = null) => {
        this.props.setAlertState(true, data);
        if (func != null) {
            window.alertFunc = func;
        }
    }

    handleSetNavSelect = (value) => {
        this.props.clickNavMenu(value);
    }

    handleAlertClose = (value) => {
        this.props.setAlertState(false);

        if (window.alertFunc != null) {
            window.alertFunc(value);
            window.alertFunc = null;
        }
    }

    handleLogin = (id, pw, token, type) => {
        this.props.setBackDropState(true, "로그인 중");
        return this.props.requestLogin(id, pw, token, type).then(
            () => {
                this.props.setBackDropState(false, "로그인 중");
                if (this.props.result.status === "SUCCESS") {
                    return true;
                }
                else {
                    console.log("failed");
                    return false;
                }
            }
        );
    }

    handleRequest = (url, type, data, auth) => {
        return this.props.requestWebAPI(url, type, data, auth).then(
            () => {
                var rtnValue = ResponseMessage(this.props.result.status, this.props.result);
                if (rtnValue == "logout") {
                    return {
                        "error": "로그인 정보가 만료되어 로그아웃됩니다."
                    }
                }
                else {
                    return rtnValue;
                }
            });
    }

    handlePopupLogin = (value) => {
        this.props.popupLogin(value);
    }

    render() {
        return (
            <div>
                <NavMenu login={isLogin()} onLogin={this.handleLogin} backDropWait={this.props.backDrop.backDrop == "WAIT"} backDropMsg={this.props.backDrop.msg} onBackDrop={this.handleSetBackDrop} onAlert={this.handleAlertOpen} onRequest={this.handleRequest} onSelectNavMenu={this.handleSetNavSelect} selectMenu={this.props.selectMenu.selectMenu} isLogin={this.props.isLogin || false} onPopupLogin={this.handlePopupLogin} />
                {this.props.children}
                <AlertDialog open={this.props.alertData.open} data={this.props.alertData.data} onClose={this.handleAlertClose} />
            </div>
        )
    };
}

const mapStateToProps = (state) => {
    return {
        backDrop: state.backDrop.result,
        alertData: state.alertData.result,
        selectMenu: state.navSelect.result,
        result: state.webapiData.result,
        isLogin: state.isLogin.result,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        requestWebAPI: (url, type, data, auth) => {
            return dispatch(requestWebAPI(url, type, data, auth));
        },
        requestLogin: (id, pwd, mtype, token, type) => {
            return dispatch(requestLogin(id, pwd, mtype, token, type));
        },
        clickNavMenu: (value) => {
            return dispatch(clickNavMenu(value));
        },
        setBackDropState: (value, msg) => {
            return dispatch(setBackDropState(value, msg));
        },
        setAlertState: (open, data) => {
            return dispatch(setAlertState(open, data));
        },
        popupLogin: (value) => {
            return dispatch(popupLogin(value));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout); 
