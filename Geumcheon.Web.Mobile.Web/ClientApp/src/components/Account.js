import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';

import { callNativeFunction } from '../helper/appfunction';
import { manualURL } from '../helper/appstring';
import { UserInfo, UniversityInfo, NotiInfo, PhoneInfo } from '../dialog/userinfo.dialog';
import TermsDialog from '../dialog/terms.dialog'
import { SettingsApplications, Description, AccountBox, Help } from '@material-ui/icons';


///////////////////// 아이디 찾기 **************************
export class AccountModify extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isSMSAuth: false,
            open: "",
            
        }
    }

    handleDownloadManual = () => {
        callNativeFunction("fileDownload", "금천구 대입지원 앱 사용법.pdf", manualURL, "");
    };

    handleDialogOpen = (open) => {
        this.setState({
            open: open
        });
    }

    handleDialogClose = () => {
        this.setState({
            open: ""
        });
    }

    render() {
        return (
            <div>
                <Box p={2} borderBottom={1}>
                    환경설정
                </Box>
                <Box p={2} bgcolor="#fff" display="flex" borderBottom={1} fontSize={15} onClick={() => this.handleDialogOpen("notiinfo")} >
                    <Box pl={2} flexGrow={1} >
                        알림설정
                    </Box>
                </Box>
                <Box p={2} borderBottom={1}>
                    약관과 정책
                </Box>
                <Box p={2} bgcolor="#fff" borderBottom={1} fontSize={15}>
                    <Box p={2} pt={0} pb={2} borderTop={0} onClick={() => this.handleDialogOpen("private")} >
                        개인정보 수집
                    </Box>
                    <Box p={2} pt={2} pb={0} borderTop={1} onClick={() => this.handleDialogOpen("offer")} >
                        개인정보 제 3자 제공
                    </Box>
                </Box>
                <Box p={2} borderBottom={1}>
                    계정
                </Box>
                <Box p={2} bgcolor="#fff" borderBottom={1} fontSize={15}>
                    <Box p={2} pt={0} borderBottom={1} onClick={() => this.handleDialogOpen("userinfo")} >
                        정보수정
                    </Box>
                    <Box p={2} borderBottom={1} onClick={() => this.handleDialogOpen("phoneinfo")} >
                        핸드폰 번호 수정
                    </Box>
                    <Box p={2} pb={0} onClick={() => this.handleDialogOpen("universityinfo")} >
                        희망대학 수정
                    </Box>
                </Box>
                <Box p={2} borderBottom={1}>
                    도움말
                </Box>
                <Box p={2} bgcolor="#fff" borderBottom={1} fontSize={15} onClick={this.handleDownloadManual}>
                    <Box pl={2} flexGrow={1}  >
                        금천구 대입지원 앱 사용법
                    </Box>
                </Box>

                {this.state.open == "userinfo" ? (<UserInfo open={true} onAlert={this.props.onAlert} onClose={this.handleDialogClose} onBackDrop={this.props.onBackDrop} onRequest={this.props.onRequest} />) : null}
                {this.state.open == "notiinfo" ? (<NotiInfo open={true} onAlert={this.props.onAlert} onClose={this.handleDialogClose} onBackDrop={this.props.onBackDrop} onRequest={this.props.onRequest} />) : null}
                {this.state.open == "universityinfo" ? (<UniversityInfo open={true} onAlert={this.props.onAlert} onClose={this.handleDialogClose} onBackDrop={this.props.onBackDrop} onRequest={this.props.onRequest} />) : null}
                {this.state.open == "private" || this.state.open == "offer" ? (<TermsDialog open={true} onClose={this.handleDialogClose} item={this.state.open} />) : null}
                {this.state.open == "phoneinfo" ? (<PhoneInfo open={true} onAlert={this.props.onAlert} onClose={this.handleDialogClose} onBackDrop={this.props.onBackDrop} onRequest={this.props.onRequest} />) : null}
            </div>
        );
    }
};

AccountModify.propTypes = {
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
    onRequest: PropTypes.func,
};
AccountModify.defaultProps = {
    onBackDrop: () => { console.log("backdrop function is not defined."); },
    onAlert: () => { console.log("alert function is not defined."); },
    onRequest: () => { console.log("request function is not defined."); },
};
///////////////////// 아이디 찾기 끝 ------------------------------

