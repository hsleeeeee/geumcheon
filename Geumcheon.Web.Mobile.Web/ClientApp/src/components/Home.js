import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import { SpeedDial, SpeedDialAction } from '@material-ui/lab';
import { Box, Button, Container, Badge, Grid, Paper, MobileStepper, Hidden } from '@material-ui/core';
import { ArrowBackIos, ArrowForwardIos, Event, Settings, Photo, Menu, Close, ExpandMore, Info, Videocam, Face, Message, School, BusinessCenter, SupervisorAccount, Assessment, Notifications, SubdirectoryArrowRight, MenuBook, Person } from '@material-ui/icons';

import { callNativeFunction } from '../helper/appfunction';
import { intro01, intro02, manualURL } from '../helper/appstring';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

const AutoSwipeableViews = autoPlay(SwipeableViews);

/// 상단 배너 시작
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    slideRoot: {
        padding: '0 30px 0px 15px',
    },
    slideContainer: {
        padding: '0 30px'
    },
    slide: {
        padding: '15px',
        minHeight: 100,
        color: '#fff',
        backgroundColor: '#39C362'
    },
    header: {
        display: 'flex',
        alignItems: 'center',
        height: 50,
        paddingLeft: theme.spacing(4),
        backgroundColor: "green",
    },
    img: {
        height: 255,
        display: 'block',
        maxWidth: 400,
        overflow: 'hidden',
        width: '100%',
    },
}));

// 상단 배너 컴포넌트
function SwipeableTextMobileStepper(props) {    
    const classes = useStyles();
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const maxSteps = props.bannerData.length || 0;
    const [autoplay, setAutoPlay] = React.useState(true);

    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    };

    const handleStepChange = (index) => {
        setActiveStep(index);
    }

    const handleGoURL = (link, idx) => {
        if (link.indexOf("http") == 0) {
            if (callNativeFunction("goLocationBrowser", link) == 0) {
                window.open(link);
            }
        }
        else {
            props.onLocation(link, [idx]);
        }
        
    }

    return (
        <div className={classes.root} >
            <AutoSwipeableViews enableMouseEvents className={classes.slideRoot} slideStyle={{ padding: "0px 0px 0px 15px", boxSizing: "border-box" }} onChangeIndex={handleStepChange} interval={5000} autoplay={autoplay} >
                {props.bannerData.map((item, index) => (
                    <Paper elevation={0} key={index} className={classes.slide} style={{ marginTop: '8px' }} style={{ backgroundColor: item.Color1 }} >
                        <Box color="#010101" p={1} whiteSpace="nowrap" textOverflow="ellipsis" overflow="hidden" fontWeight={600} >
                            {item.Title}
                        </Box>
                        <Box color="#fff" p={1} style={{ whiteSpace: 'pre-line', overflow: 'auto' }} height={96} pt={0} pb={0} mb={1}>
                            {item.Body}
                        </Box>
                        <Box p={1}>
                            <Button type="button" variant="contained" size="small" onClick={() => handleGoURL(item.Link, item.NavIdx)} style={{ backgroundColor: item.Color2, color: '#fff' }} >
                                {item.LinkTitle}
                            </Button>
                        </Box>
                    </Paper>
                ))}
            </AutoSwipeableViews>
            <MobileStepper
                steps={maxSteps}
                position="static"
                variant="dots"
                activeStep={activeStep}
                nextButton={
                    <Hidden >
                        <div />
                    </Hidden>
                }
                backButton={
                    <Hidden >
                        <div />
                    </Hidden>
                }
            />
        </div>
    );
}
/// 상단 배너 끝

// 메뉴 아이템 컴포넌트
class MenuItem extends Component {
    constructor(props) {
        super(props);
        this.components = {
            interview: Person,
            video: Videocam,
            online: Message,
            ontime: School,
            admission: BusinessCenter,
            academy: MenuBook,
            report: Assessment,
            board: Notifications
        };
    }
    

    handleMenu = () => {
        this.props.onMenu(this.props.link, this.props.index);
    }

    render() {
        const IconComponent = this.components[this.props.icon];
        return (
            <Box onClick={this.handleMenu} >
                <Box justifyContent="center" alignItems="center" display="flex">
                    <Box pt={2} pb={1}>
                        <Badge color="secondary" badgeContent={this.props.badge} >
                            <IconComponent width="36" height="36" fill="#010101" style={{ fontSize: '36px', color:"rgba(0, 0, 0, 0.54)" }} />
                        </Badge>
                    </Box>
                </Box>
                <Box justifyContent="center" alignItems="center" display="flex">
                    <Box p={1} fontSize={13} style={{ textAlign: "center" }}>
                        {this.props.name}
                    </Box>
                </Box>
            </Box>
        )
    }
}
// 메뉴 아이템 props
MenuItem.propTypes = {
    icon: PropTypes.string,
    badge: PropTypes.number,
    name: PropTypes.string,
    link: PropTypes.string,
    onMenu: PropTypes.func,
};
MenuItem.defaultProps = {
    icon: 'bell',
    badge: 0,
    name: '알림신청',
    link: '/bell',
    onMenu: () => { console.error("menu function is not defined"); },
};
/// 메뉴 아이템 끝


class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dialOpen: false,
            dialList: [
                {
                    icon: <Message />, name: "online", title: "온라인컨설팅"
                },
                {
                    icon: <Person />, name: "interview", title: "대면컨설팅"
                },
                {
                    icon: <Videocam />, name: "video", title: "화상컨설팅"
                }
            ],

            menuData: this.makeNavList(),
            bannerData: [{
                Tltie: "불러오는 중",
                Body: "불러오는 중",
                Link: "",
                LinkTitle: "바로가기",
                Color1: "#F7797A",
                Color2: "#CF5152",
                NavIdx: 0
            }],
            badge: []
        }
    };

    componentDidMount() {
        callNativeFunction("setSwipeRefresh", false);

        if (sessionStorage.getItem("banner") != null) {
            const banner = JSON.parse(sessionStorage.getItem("banner"));
            if (banner.expire < (new Date()).getTime()) {
                this.funcGetBannerData();
            }
            else {
                this.setState({
                    bannerData: banner.data
                });
            }
        }
        else {
            this.funcGetBannerData();
        }
    }

    funcGetBannerData = () => {
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/Banner', 'get', null, true).then((result) => {
            console.log(result);
            if (result.error == null) {
                sessionStorage.setItem("banner", JSON.stringify({ data: result.banners, expire: (new Date()).getTime() + parseInt(1 * 60 * 60 * 1000) }));
                this.setState({
                    bannerData: result.banners
                });
            }
        });
    }

    makeNavList = () => {
        const makeNav = [
            {
                Idx: 1,
                Name: "종합컨설팅",
                TitleName: "종합컨설팅",
                Icon: "video",
                Link: "/Consult/video",
            },
            {
                Idx: 2,
                Name: "정시컨설팅",
                TitleName: "정시컨설팅",
                Icon: "interview",
                Link: "/Consult/interview",
            },
            {
                Idx: 3,
                Name: "온라인컨설팅",
                TitleName: "온라인컨설팅",
                Icon: "online",
                Link: "/Consult/online",
            },
            {
                Idx: 4,
                Name: "대입설명회",
                TitleName: "대입설명회",
                Icon: "ontime",
                Link: "/Presentation/ontime",
            },
            {
                Idx: 5,
                Name: "입학사정관특강",
                TitleName: "입학사정관특강",
                Icon: "admission",
                Link: "/Presentation/admission",
            },
            {
                Idx: 6,
                Name: "학부모아카데미",
                TitleName: "학부모아카데미",
                Icon: "academy",
                Link: "/Presentation/academy",
            }
        ];

        return makeNav;
    }

    handleGoRequest = (link) => {
        this.props.onLocation("/Consult/" + link);
    };

    handleDownloadManual = () => {
        callNativeFunction("fileDownload", "금천구 대입지원 앱 사용법.pdf", manualURL, "");
    };

    handleDialClick = (name) => {
        this.props.onLocation("/Consult/" + name);
        if (name == "online") {
            this.props.onSelectNavMenu([3]);
        }
        else if (name == "video") {
            this.props.onSelectNavMenu([1]);
        }
        else if (name == "interview") {
            this.props.onSelectNavMenu([2]);
        }
        
    };

    handleIcon = (link, idx) => {
        //const menuArray = [];
        //menuArray.push(idx);
        //menuArray.push(0);

        //this.props.onSelectNavMenu(menuArray);
        this.props.onLocation(link, [idx]);
    }

    handleDialOpen = () => {
        this.setState({
            dialOpen : true
        });
    }

    handleDialClose = () => {
        this.setState({
            dialOpen: false
        });
    }
    
    render() {
        return (
            <div>
                <Box p={2} fontSize={18} fontWeight={600} bgcolor="#dadada" mb={2}>
                    - 현재 신청 접수중인 프로그램
                </Box>
                <SwipeableTextMobileStepper bannerData={this.state.bannerData || []} onLocation={this.props.onLocation} />
                <Box p={2} fontSize={18} fontWeight={600} bgcolor="#dadada" mt={2}>
                    - 신청 메뉴 바로가기
                </Box>
                <Grid container spacing={0} >
                    {this.state.menuData.map((item, index) => (
                        <Grid item xs={4} key={index} style={{}}>
                            <MenuItem icon={item.Icon} badge={this.state.badge[item.id] || 0} index={item.Idx} name={item.Name} link={item.Link} onMenu={this.handleIcon} />
                        </Grid>
                    ))}
                </Grid>
                <Box style={{ position: "fixed", right: "10%", bottom: "15%", display: "none" }}>
                    <SpeedDial ariaLabel="requestDial" onClose={this.handleDialClose} onOpen={this.handleDialOpen} open={this.state.dialOpen} icon={<Box><Box fontSize={14} fontWeight={500} color="#fff">신청</Box> <Box fontSize={14} fontWeight={500} color="#fff">메뉴</Box></Box>} >
                        {this.state.dialList.map((action, index) => (
                            <SpeedDialAction key={action.name} icon={action.icon} tooltipTitle={action.title} tooltipOpen onClick={() => this.handleDialClick(action.name)} />
                        ))}
                    </SpeedDial>
                </Box>      
            </div>
        );
    }
}

Home.propTypes = {
    title: PropTypes.string,
    onLocation: PropTypes.func,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onSelectNavMenu: PropTypes.func,
};

Home.defaultProps = {
    title: "더스쿨",
    onLocation: () => { console.log("location function is not defined"); },
    onRequest: () => { console.log("request function is not defined"); },
    onBackDrop: () => { console.log("backdrop function is not defined"); },
    onSelectNavMenu: () => { console.log("selectnavmenu function is not defined"); },
};

export default Home;
