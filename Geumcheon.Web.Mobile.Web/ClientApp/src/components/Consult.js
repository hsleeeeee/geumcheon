import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Box, Tabs, Tab, Fab, Divider, CircularProgress, Backdrop, Menu, MenuItem } from "@material-ui/core";
import SwipeableViews from 'react-swipeable-views';
import InfiniteScroll from 'react-infinite-scroller';

import { callNativeFunction, GetUserInfo, CloseModal, AppCloseModal, ajaxWebAPI } from '../helper/appfunction';
import { FPEntranceListItem } from './parts';
import RequestDialog from '../dialog/request.dialog';
import { InfoConsult } from './parts/Information';

// 커스텀 탭 디자인
const FPTabs = withStyles({
    indicator: {
        height: "3px",
        backgroundColor: "#ff7704"
    }
})(Tabs);

const FPTab = withStyles(theme => ({
    selected: {
        color: '#333333',
        fontWeight: '600',
    }
}))(props => <Tab disableRipple {...props} />);

// 탭 패널
class TabPanel extends Component {
    render() {
        const { children, value, index, ...other } = this.props;

        return (
            <div style={{ marginTop: '20px', marginBottom: '16px' }}>
                {value === index && <Box>{children}</Box>}
            </div>
        );
    }
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};


class Consult extends Component {
    constructor(props) {
        super(props);

        this.items = [];
        this.isSwipe = true;
        this.state = {
            tabState: 0,
            open: this.props.idx != null ? "request" : "",
            mode: this.props.idx != null ? "detail" : "",

            currentPage: 1,     // 현재 페이지
            totalCount: 0,      // 토탈 갯수
            isMore: true,       // 더 있는지
            isLast: false,      // 마지막인지
            isMoreShow: true,   // 로딩 중 표시
            itemCnt: 0,

            displayInputType: '',

            selectItem: this.props.idx != null ? { RequestIdx: this.props.idx } : {},     // 선택 아이템
            selectEIdx: 0,

            consultDate: [],

            requestIdx: this.props.idx,
            noti: false,

            bufferCnt: 1,

            showMenu: null,
            consultList: [],
        };

        window.refreshItemReload = function () {
            this.items = [];
            this.staticcurrentPage = 0;
            this.setState({
                currentPage: 1,
                isMore: false,       // 더 있는지
                isLast: false,      // 마지막인지
                itemCnt: 0,
            });
        }.bind(this);

        if (this.props.idx != null) {
            this.props.onPushInit();
        }
    }

    componentDidMount() {
        if (GetUserInfo("id") != null && GetUserInfo("id") != "") {
            this.props.onBackDrop(true, "로드 중");
            this.props.onRequest("api/Entrance?requestIdx=0&way=" + this.props.way + "&entranceIdx=0", "get").then((result) => {
                console.log(result);
                if (result.error == null) {
                    this.setState({
                        isMore: true,
                        consultList: result.Entities
                    });
                }
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.way != nextProps.way) {
            this.items = [];
            this.staticcurrentPage = 0;
            this.setState({
                currentPage: 1,
                isMore: false,       // 더 있는지
                isLast: false,      // 마지막인지
                itemCnt: 0,
                tabState: 0,
            });

            if (GetUserInfo("id") != null) {
                this.props.onBackDrop(true, "로드 중");
                this.props.onRequest("api/Entrance?requestIdx=0&way=" + nextProps.way + "&entranceIdx=0", "get").then((result) => {
                    console.log(result);
                    const nextState = {};
                    nextState.isMore = true;
                    if (result.error == null) {
                        nextState.consultList = result.Entities;
                    }

                    this.setState(nextState);
                    this.props.onBackDrop(false);
                });
            }
        }
        if (nextProps.idx != null && this.state.requestIdx != nextProps.idx) {
            this.funcOpenDialog(nextProps.idx);
        }
    }

    a11yProps = (index) => {
        return {
            id: `register-tab-${index}`,
            'aria-controls': `register-tabpanel-${index}`,
        };
    }

    // 탭 체인지 이벤트
    handleTabClick = (event, newValue) => {
        this.setState({
            tabState: newValue
        });
    };

    // 탭 컨텐츠 스와이프 이벤트
    handleSwipeChangeIndex = (idx) => {
        this.setState({
            tabState: idx
        });
    };

    handleShowMenu = (e) => {
        if (GetUserInfo("id") == null) {
            this.props.onAlert({
                message: "로그인 후 신청 가능합니다."
            }, function () { this.props.onPopupLogin(true); }.bind(this));
        }
        else if (this.state.consultList.length == 0) {
            const target = e.currentTarget;
            this.props.onBackDrop(true, "로드 중");
            this.props.onRequest("api/Entrance?requestIdx=0&way=" + this.props.way + "&entranceIdx=0", "get").then((result) => {
                console.log(result);
                if (result.error == null) {
                    this.setState({
                        isMore: true,
                        consultList: result.Entities
                    }, function () {
                        this.setState({
                            showMenu: target
                        });
                    }.bind(this));
                }
            });
        }
        else {
            if (this.state.consultList.length == 1) {
                this.setState({
                    selectEIdx: this.state.consultList[0].EntranceIdx,
                    displayInputType: this.state.consultList[0].DisplayInputType
                });
                this.props.onBackDrop(true, "불러오는 중...");
                this.props.onRequest("api/Entrance?requestIdx=0&way=" + this.props.way + "&entranceIdx=" + this.state.consultList[0].EntranceIdx, "get", null, true).then((result) => {
                    console.log(result);
                    if (result.error == null) {
                        if (result.BufferCount < 1 && result.BufferCount != -9999) {
                            this.props.onAlert({
                                message: "현재 신청인원이 초과하여 대기 신청 화면으로 이동합니다. 취소인원이 있을 때 담당자가 연락을 드립니다."
                            }, function () { this.handleRequest(); }.bind(this));
                        }
                        else {
                            this.handleRequest();
                        }
                    }
                });
            }
            else {
                const target = e.currentTarget;

                this.setState({
                    showMenu: target
                });
            }
        }
    }

    // 메뉴 숨기기
    handleHideMenu = () => {
        this.setState({
            showMenu: null,
        });
    }

    handleRequestDialog = (item) => {
        console.log(item);

        //this.setState({
        //    open: "request",
        //    mode: "request",
        //    selectItem: item
        //});

        this.setState({
            selectEIdx: item.EntranceIdx,
            displayInputType: item.DisplayInputType
        });

        this.handleHideMenu();
        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest("api/Entrance?requestIdx=0&way=" + this.props.way + "&entranceIdx=" + this.state.consultList[0].EntranceIdx, "get", null, true).then((result) => {
            console.log(result);
            if (result.error == null) {
                if (result.BufferCount < 1 && result.BufferCount != -9999) {
                    this.props.onAlert({
                        message: "현재 신청인원이 초과하여 대기 신청 화면으로 이동합니다. 취소인원이 있을 때 담당자가 연락을 드립니다."
                    }, function () { this.handleRequest(); }.bind(this));
                }
                else {
                    this.handleRequest();
                }
            }
        });
    }

    // 신청하기
    handleRequest = () => {
        //if (this.props.way == "I") {
        //    if (new Date() > Date.parse("2020-09-01")) {
        //        this.props.onAlert({
        //            message: "현재 신청 기간이 아닙니다."
        //        });
        //        return;
        //    }
        //}
        //else if (this.props.way == "V") {
        //    if (new Date() > Date.parse("2020-11-07")) {
        //        this.props.onAlert({
        //            message: "현재 신청 기간이 아닙니다."
        //        });
        //        return;
        //    }
        //}
        //else if (this.props.way == "O") {
        //    if (new Date() > Date.parse("2020-12-16")) {
        //        this.props.onAlert({
        //            message: "현재 신청 기간이 아닙니다."
        //        });
        //        return;
        //    }
        //}

        this.props.onBackDrop(true, "불러오는 중...");
        this.props.onRequest('api/Schedule', 'post', {
            type: "date",
            way: this.props.way,
        }).then((result) => {
            if (result.error == null) {
                this.setState({
                    consultDate: result.entities,
                    open: "request",
                    mode: "request",
                    selectItem: {
                        RequestIdx: 0
                    }
                });
            }
            else {
                this.props.onAlert({
                    message: result.error
                });
            }
        });
    }

    handleDialogClose = (modify) => {
        if (modify == "modify") {
            this.props.onBackDrop(true, "불러오는 중...");
            this.props.onRequest('api/Schedule', 'post', {
                type: "date",
                way: this.props.way,
            }).then((result) => {
                if (result.error == null) {
                    this.setState({
                        consultDate: result.entities,
                        open: "request",
                        mode: "modify",
                    });
                }
                else {
                    this.props.onAlert({
                        message: result.error
                    });
                }
            });
        }
        else {
            if (modify) {
                this.items = [];
                this.staticcurrentPage = 0;
                this.setState({
                    currentPage: 1,
                    isMore: true,       // 더 있는지
                    isLast: false,      // 마지막인지
                    itemCnt: 0,
                    open: "",
                    mode: "",
                    selectItem: null,
                    requestIdx: null,
                });
            }
            else {
                this.setState({
                    open: "",
                    mode: "",
                    selectItem: null,
                    requestIdx: null,
                });
            }
            this.props.onPushInit();
        }
    }

    // 아이템 로드
    handleLoadMore = (page) => {
        if (this.staticcurrentPage == this.state.currentPage) {
            return;
        }
        else {
            this.staticcurrentPage = this.state.currentPage;
        }

        this.setState({
            isMore: false,
            isMoreShow: true,
        });

        this.props.onRequest('api/Entrance?page=' + this.state.currentPage + "&way=" + this.props.way, 'post', {
            SearchType: "",
            Name: "",
            CompanyCode: "0001",
            ConsultWay: this.props.way,
            OrderType: "",
        }).then((result) => {
            console.log(result);
            if (result != null && result.error == null) {
                let nextState = {};
                nextState["totalCount"] = result.PagingInfo.TotalCount;
                nextState["currentPage"] = result.PagingInfo.CurrentPage + 1;

                if (result.Entities.length == result.PagingInfo.PageSize) {
                    nextState["isMore"] = true;
                }

                result.Entities.map((item, index) => {
                    this.items.push(
                        <div key={this.items.length + 1}>
                            <FPEntranceListItem item={item} onClick={this.handleListClick} >

                            </FPEntranceListItem>
                            <Divider />
                        </div>
                    );
                });

                this.setState({
                    itemCnt: this.items.length,
                });

                if (this.items.length >= result.PagingInfo.TotalCount) {
                    nextState["isMore"] = false;
                    nextState["isLast"] = true;
                    nextState["isMoreShow"] = false;
                }

                this.setState(nextState);

            }
            else if (result != null && result.error != null) {
                this.setState({
                    isMore: false,
                    isLast: true,
                    isMoreShow: false,
                });
            }
        });
    }

    // 리스트 아이템 클릭
    handleListClick = (item) => {
        if (GetUserInfo("id") == null) {
            this.props.onAlert("로그인 후 조회 가능합니다.");
        }
        else {
            this.setState({
                open: "request",
                mode: "detail",
                selectItem: item,
                displayInputType: item.DisplayInputType
            });
        }
    }

    funcOpenDialog = (idx) => {
        if (window.modalDialog != null && window.modalDialog.length != 0 && this.state.mode != "detail") {
            this.props.onAlert({
                mode: "yesorno",
                message: "저장하지 않은 내용은 저장되지 않습니다. 닫으시겠습니까?"
            }, function (bind, value) {
                if (value) {
                    this.setState({
                        noti: true,
                        open: "request",
                        mode: "detail",
                        selectItem: {
                            RequestIdx: parseInt(idx)
                        },
                        requestIdx: idx,
                    });
                    this.props.onPushInit();
                }
            }.bind(this, CloseModal));
        }
        else {
            this.setState({
                noti: true,
                open: "request",
                mode: "detail",
                selectItem: {
                    RequestIdx: parseInt(idx)
                },
                requestIdx: idx,
            });
            this.props.onPushInit();
        }
    }

    handleSwipeSwitching = (index, type) => {
        if (type == "end") {
            callNativeFunction("setSwipeRefresh", true);
            this.isSwipe = true;
        }
        else if (this.isSwipe) {
            this.isSwipe = false;
            callNativeFunction("setSwipeRefresh", false);
        }
    }

    handleNotiInit = () => {
        console.log("noti null");
        this.setState({
            noti: false
        });
    }

    render() {
        const tabstate2 = (
            <div style={{ height: this.state.tabState == 1 ? "auto" : "0px", minHeight: "calc(100vh - 163px)", marginTop: "43px" }}>
                <InfiniteScroll pageStart={0} loadMore={this.handleLoadMore} hasMore={this.state.isMore} >
                    <div className="tasks" style={{ backgroundColor: "#fff" }}>
                        {this.items}
                    </div>
                </InfiniteScroll>
                {this.state.isMoreShow ? (<Box p={2} color="#9A9A9A" textAlign="center" ><CircularProgress color="inherit" disableShrink /></Box>) : null}
                {this.state.isLast ? (<Box p={2} textAlign="center" color="#9A9A9A">{GetUserInfo("id") == null ? "로그인 후 조회 가능합니다." : (this.state.totalCount == 0 ? "내용이 없습니다." : "마지막입니다.")}</Box>) : null}
            </div>
        );

        const showConsultList = (
            <div>
                <Backdrop open={this.state.showMenu != null} style={{ zIndex: 1300 }} onClick={this.handleHideMenu}>
                </Backdrop>
                <Menu anchorEl={this.state.showMenu} keepMounted open={Boolean(this.state.showMenu)} onClose={this.handleHideMenu} style={{ textAlign: 'center' }}>
                    {this.state.consultList.length == 0 ? (<MenuItem onClick={this.handleHideMenu} display="flex" style={{ justifyContent: "space-around" }}>신청 가능한 컨설팅이 없습니다.</MenuItem>) : (
                        this.state.consultList.map((item, index) => (
                            <div key={index}>
                                <MenuItem onClick={() => this.handleRequestDialog(item)} display="flex" style={{ justifyContent: "space-around" }}>
                                    <Box textAlign="center">
                                        <Box fontWeight={600}>
                                            {item.Title}
                                        </Box>
                                        <Box>
                                            상담일 : {item.ConsultDt || "2020-1237-2983"}
                                        </Box>
                                    </Box>

                                </MenuItem>
                                {this.state.consultList.length - 1 != index ? <Divider /> : null}
                            </div>
                        )))}
                </Menu>
            </div>
        );

        return (
            <div>
                <Fab color="primary" arial-label="add" elevation={0} style={{ position: 'fixed', right: "10%", bottom: "15%", zIndex: '1', backgroundColor: (this.state.bufferCnt < 1 && this.state.bufferCnt > -9999) ? "#26c85e" : "#2f96d4", wordBreak: "keep-all" }} onClick={this.handleShowMenu} >
                    {(this.state.bufferCnt < 1 && this.state.bufferCnt > -9999) ? "대기 신청" : "신청"}
                </Fab>
                <FPTabs value={this.state.tabState} onChange={this.handleTabClick} aria-label="simple tabs example" variant="fullWidth" style={{ position: "fixed", zIndex: 1, width: "100%", backgroundColor: "#fff" }} >
                    <FPTab label="신청안내" {...this.a11yProps(0)} />
                    <FPTab label="신청내역" {...this.a11yProps(1)} />
                </FPTabs>
                <SwipeableViews enableMouseEvents
                    index={this.state.tabState}
                    onChangeIndex={this.handleSwipeChangeIndex}
                    onSwitching={this.handleSwipeSwitching}
                >
                    <InfoConsult way={this.props.way} visible={this.state.tabState == 0} buffer={this.state.bufferCnt < 1 && this.state.bufferCnt > -9999} />
                    {tabstate2}
                </SwipeableViews>
                {showConsultList}
                {this.state.open == "request" ? (<RequestDialog open={true} item={this.state.selectItem} onRequest={this.props.onRequest} onBackDrop={this.props.onBackDrop} onAlert={this.props.onAlert} onClose={this.handleDialogClose} onNoti={this.handleNotiInit} way={this.props.way} date={this.state.consultDate} mode={this.state.mode} noti={this.state.noti} eidx={this.state.selectEIdx} displayInputType={this.state.displayInputType} />) : null}
            </div>
        );
    }
};

Consult.propTypes = {
    way: PropTypes.string,
    idx: PropTypes.string,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
    onPushInit: PropTypes.func,
    onPopupLogin: PropTypes.func,
    onLocation: PropTypes.func,
};
Consult.defaultProps = {
    way: "I",
    idx: null,
    onBackDrop: () => console.log("backdrop function is not defined."),
    onAlert: () => console.log("alert function is not defined."),
    onRequest: () => console.log("request function is not defined."),
    onPushInit: () => console.log("pushinit function is not defined."),
    onPopupLogin: () => console.log("pushinit function is not defined."),
    onLocation: () => console.log("location function is not defined.")
};

export default Consult


// WEBPACK FOOTER //
// ./src/components/Consult.js