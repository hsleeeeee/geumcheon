import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Typography, CircularProgress, Box, Divider, Tab, Tabs } from "@material-ui/core";
import InfiniteScroll from 'react-infinite-scroller';
import SwipeableViews from 'react-swipeable-views';

import RequestDialog from '../dialog/request.dialog';
import PresentationRequestDialog from '../dialog/presentationRequest.dialog';
import { CloseModal, GetUserInfo, callNativeFunction } from '../helper/appfunction';
import { FPEntranceListItem, FPPresentationListItem } from './parts/FPListItem';

// 커스텀 탭 디자인
const FPTabs = withStyles({
    indicator: {
        height: "3px",
        backgroundColor: "#ff7704"
    }
})(Tabs);

const FPTab = withStyles(theme => ({
    selected: {
        color: '#333333',
        fontWeight: '600',
    }
}))(props => <Tab disableRipple {...props} />);

// 탭 패널
class TabPanel extends Component {
    render() {
        const { children, value, index, ...other } = this.props;

        return (
            <div style={{ marginTop: '20px', marginBottom: '16px' }}>
                {value === index && <Box>{children}</Box>}
            </div>
        );
    }
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

///////////////////// 결과리포트 **************************
export class ReportResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabState : 0,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.idx != null && this.state.requestIdx != nextProps.idx) {
            this.funcOpenDialog(nextProps.idx);
        }
    }

    a11yProps = (index) => {
        return {
            id: `register-tab-${index}`,
            'aria-controls': `register-tabpanel-${index}`,
        };
    }

    // 탭 체인지 이벤트
    handleTabClick = (event, newValue) => {
        this.setState({
            tabState: newValue
        });
    };

    // 탭 컨텐츠 스와이프 이벤트
    handleSwipeChangeIndex = (idx) => {
        this.setState({
            tabState: idx
        });
    };

    handleSwipeSwitching = (index, type) => {
        if (type == "end") {
            callNativeFunction("setSwipeRefresh", true);
            this.isSwipe = true;
        }
        else if (this.isSwipe) {
            this.isSwipe = false;
            callNativeFunction("setSwipeRefresh", false);
        }
    }

    render() {
        return (
            <div>
                <FPTabs value={this.state.tabState} onChange={this.handleTabClick} aria-label="simple tabs example" variant="fullWidth" style={{ position: "fixed", zIndex: 1, width: "100%", backgroundColor: "#fff" }} >
                    <FPTab label="컨설팅 신청내역" {...this.a11yProps(0)} />
                    <FPTab label="설명회 신청내역" {...this.a11yProps(1)} />
                </FPTabs>
                <SwipeableViews enableMouseEvents index={this.state.tabState} onChangeIndex={this.handleSwipeChangeIndex} onSwitching={this.handleSwipeSwitching} >
                    <MyConsultList currentType={this.state.tabState == 0 ? "consult" : "presentation"} onRequest={this.props.onRequest} onBackDrop={this.props.onBackDrop} onAlert={this.props.onAlert} />
                    <MyPresentationList currentType={this.state.tabState == 0 ? "consult" : "presentation"} onRequest={this.props.onRequest} onBackDrop={this.props.onBackDrop} onAlert={this.props.onAlert} />
                </SwipeableViews>
            </div>
        );
    }
};

ReportResult.propTypes = {
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
};
ReportResult.defaultProps = {
    onBackDrop: () => { console.log("backdrop function is not defined."); },
    onAlert: () => { console.log("alert function is not defined."); },
    onRequest: () => { console.log("request function is not defined."); },
};
///////////////////// 결과리포트 끝 ------------------------------

///////////////////// 내 상담리스트 ******************************
export class MyConsultList extends Component {
    constructor(props) {
        super(props);

        this.items = [];
        this.state = {
            open: "",
            mode: "",

            currentPage: 1,     // 현재 페이지
            totalCount: 0,      // 토탈 갯수
            isMore: true,       // 더 있는지
            isLast: false,      // 마지막인지
            isMoreShow: true,   // 로딩 중 표시
            itemCnt: 0,

            selectItem: {},     // 선택 아이템

            consultDate: [],
        };

        window.refreshItemReload = function () {
            if (this.props.currentType == "consult") {
                this.items = [];
                this.staticcurrentPage = 0;
                this.setState({
                    currentPage: 1,
                    isMore: true,       // 더 있는지
                    isLast: false,      // 마지막인지
                    itemCnt: 0,
                });
            }
        }.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.idx != null && this.state.requestIdx != nextProps.idx) {
            this.funcOpenDialog(nextProps.idx);
        }
    }

    handleDialogClose = (modify) => {
        if (modify == "modify") {
            this.props.onBackDrop(true, "불러오는 중...");
            this.props.onRequest('api/Schedule', 'post', {
                type: "date",
                way: this.state.selectItem.way,
            }).then((result) => {
                if (result.error == null) {
                    this.setState({
                        consultDate: result.entities,
                        open: "request",
                        mode: "modify",
                    });
                }
                else {
                    this.props.onAlert({
                        message: result.error
                    });
                }
            });
        }
        else {
            if (modify) {
                this.items = [];
                this.staticcurrentPage = 0;
                this.setState({
                    currentPage: 1,
                    isMore: true,       // 더 있는지
                    isLast: false,      // 마지막인지
                    itemCnt: 0,
                    open: "",
                    mode: "",
                    selectItem: null,
                    requestIdx: null,
                });
            }
            else {
                this.setState({
                    open: "",
                    mode: "",
                    selectItem: null,
                    requestIdx: null,
                });
            }
        }
    }

    // 아이템 로드
    handleLoadMore = (page) => {
        if (this.staticcurrentPage == this.state.currentPage) {
            return;
        }
        else {
            this.staticcurrentPage = this.state.currentPage;
        }

        this.setState({
            isMore: false,
            isMoreShow: true,
        });

        this.props.onRequest("api/MyPage?type=consult&page=" + this.state.currentPage, "post", {
            SearchType: "",
            Name: "",
            CompanyCode: "0001",
            OrderType: "",
        }).then((result) => {
            console.log(result);
            if (result != null && result.error == null) {
                let nextState = {};
                nextState["totalCount"] = result.PagingInfo.TotalCount;
                nextState["currentPage"] = result.PagingInfo.CurrentPage + 1;

                if (result.Entities.length == result.PagingInfo.PageSize) {
                    nextState["isMore"] = true;
                }

                result.Entities.map((item, index) => {
                    this.items.push(
                        <div key={this.items.length + 1}>
                            <FPEntranceListItem item={item} onClick={this.handleListClick} isWay={true} >

                            </FPEntranceListItem>
                            <Divider />
                        </div>
                    );
                });

                this.setState({
                    itemCnt: this.items.length,
                });

                if (this.items.length >= result.PagingInfo.TotalCount) {
                    nextState["isMore"] = false;
                    nextState["isLast"] = true;
                    nextState["isMoreShow"] = false;
                }

                this.setState(nextState);

            }
            else if (result != null && result.error != null) {
                this.setState({
                    isMore: false,
                    isLast: true,
                    isMoreShow: false,
                });
            }
        });
    }

    // 리스트 아이템 클릭
    handleListClick = (item) => {
        if (GetUserInfo("id") == null) {
            this.props.onAlert("로그인 후 조회 가능합니다.");
        }
        else {
            this.setState({
                open: "request",
                mode: "detail",
                selectItem: item
            });
        }
    }

    render() {
        return (
            <div style={{ marginTop: "43px" }}>
                <InfiniteScroll pageStart={0} loadMore={this.handleLoadMore} hasMore={this.state.isMore} >
                    <div className="tasks" style={{ backgroundColor: "#fff" }}>
                        {this.items}
                    </div>
                </InfiniteScroll>
                {this.state.isMoreShow ? (<Box p={2} color="#9A9A9A" textAlign="center" ><CircularProgress color="inherit" disableShrink /></Box>) : null}
                {this.state.isLast ? (<Box p={2} textAlign="center" color="#9A9A9A">{GetUserInfo("id") == null ? "로그인 후 조회 가능합니다." : (this.state.totalCount == 0 ? "내용이 없습니다." : "마지막입니다.")}</Box>) : null}
                {this.state.open == "request" ? (<RequestDialog open={true} item={this.state.selectItem} onRequest={this.props.onRequest} onBackDrop={this.props.onBackDrop} onAlert={this.props.onAlert} onClose={this.handleDialogClose} onNoti={this.handleNotiInit} way={this.state.selectItem.ConsultWay} date={this.state.consultDate} mode={this.state.mode} />) : null}
            </div>
        );
    }
};

MyConsultList.propTypes = {
    currentType: PropTypes.string,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
};
MyConsultList.defaultProps = {
    currentType: "consult",
    onBackDrop: () => { console.log("backdrop function is not defined."); },
    onAlert: () => { console.log("alert function is not defined."); },
    onRequest: () => { console.log("request function is not defined."); },
};
///////////////////// 내 상담리스트 끝 ----------------------

///////////////////// 그외 신청리스트 ******************************
export class MyPresentationList extends Component {
    constructor(props) {
        super(props);

        this.items = [];
        this.state = {
            open: "",
            mode: "",

            currentPage: 1,     // 현재 페이지
            totalCount: 0,      // 토탈 갯수
            isMore: false,       // 더 있는지
            isLast: true,      // 마지막인지
            isMoreShow: true,   // 로딩 중 표시
            itemCnt: 0,

            selectItem: {},     // 선택 아이템

            consultDate: [],

            currentType: ""
        };

        window.refreshItemReload = function () {
            if (this.props.currentType == "presentation") {
                this.items = [];
                this.staticcurrentPage = 0;
                this.setState({
                    currentPage: 1,
                    isMore: true,       // 더 있는지
                    isLast: false,      // 마지막인지
                    itemCnt: 0,
                });
            }
        }.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        console.log(this.state.currentType, nextProps.currentType);
        if (this.state.currentType == "" && nextProps.currentType == "presentation") {
            this.setState({
                currentType: "presentation",
                isMore: true,
            });
        }
    }

    handleDialogClose = (modify) => {
        if (modify == "modify") {
            this.props.onBackDrop(true, "불러오는 중...");
            this.props.onRequest('api/Schedule', 'post', {
                type: "date",
                way: this.state.selectItem.way,
            }).then((result) => {
                if (result.error == null) {
                    this.setState({
                        consultDate: result.entities,
                        open: "request",
                        mode: "modify",
                    });
                }
                else {
                    this.props.onAlert({
                        message: result.error
                    });
                }
            });
        }
        else {
            if (modify) {
                this.items = [];
                this.staticcurrentPage = 0;
                this.setState({
                    currentPage: 1,
                    isMore: true,       // 더 있는지
                    isLast: false,      // 마지막인지
                    itemCnt: 0,
                    open: "",
                    mode: "",
                    selectItem: null,
                    requestIdx: null,
                });
            }
            else {
                this.setState({
                    open: "",
                    mode: "",
                    selectItem: null,
                    requestIdx: null,
                });
            }
        }
    }

    // 아이템 로드
    handleLoadMore = (page) => {
        console.log("this presentation");
        if (this.staticcurrentPage == this.state.currentPage) {
            return;
        }
        else {
            this.staticcurrentPage = this.state.currentPage;
        }

        this.setState({
            isMore: false,
            isMoreShow: true,
        });

        this.props.onRequest("api/MyPage?type=presentation&page=" + this.state.currentPage, "post", {
            SearchType: "",
            Name: "",
            CompanyCode: "0001",
            OrderType: "",
        }).then((result) => {
            console.log(result);
            if (result != null && result.error == null) {
                let nextState = {};
                nextState["totalCount"] = result.PagingInfo.TotalCount;
                nextState["currentPage"] = result.PagingInfo.CurrentPage + 1;

                if (result.Entities.length == result.PagingInfo.PageSize) {
                    nextState["isMore"] = true;
                }

                result.Entities.map((item, index) => {
                    this.items.push(
                        <div key={this.items.length + 1}>
                            <FPPresentationListItem item={item} onClick={this.handleListClick} isWay={true} >

                            </FPPresentationListItem>
                            <Divider />
                        </div>
                    );
                });

                this.setState({
                    itemCnt: this.items.length,
                });

                if (this.items.length >= result.PagingInfo.TotalCount) {
                    nextState["isMore"] = false;
                    nextState["isLast"] = true;
                    nextState["isMoreShow"] = false;
                }

                this.setState(nextState);

            }
            else if (result != null && result.error != null) {
                this.setState({
                    isMore: false,
                    isLast: true,
                    isMoreShow: false,
                });
            }
        });
    }

    // 리스트 아이템 클릭
    handleListClick = (item) => {
        if (GetUserInfo("id") == null) {
            this.props.onAlert("로그인 후 조회 가능합니다.");
        }
        else {
            this.setState({
                open: "request",
                mode: "detail",
                selectItem: item
            });
        }
    }

    render() {
        return (
            <div style={{marginTop:"43px"}}>
                <InfiniteScroll pageStart={0} loadMore={this.handleLoadMore} hasMore={this.state.isMore} >
                    <div className="tasks" style={{ backgroundColor: "#fff" }}>
                        {this.items}
                    </div>
                </InfiniteScroll>
                {this.state.isMoreShow ? (<Box p={2} color="#9A9A9A" textAlign="center" ><CircularProgress color="inherit" disableShrink /></Box>) : null}
                {this.state.isLast ? (<Box p={2} textAlign="center" color="#9A9A9A">{GetUserInfo("id") == null ? "로그인 후 조회 가능합니다." : (this.state.totalCount == 0 ? "내용이 없습니다." : "마지막입니다.")}</Box>) : null}
                {this.state.open == "request" ? (<PresentationRequestDialog open={true} item={this.state.selectItem} onRequest={this.props.onRequest} onBackDrop={this.props.onBackDrop} onAlert={this.props.onAlert} onClose={this.handleDialogClose} onNoti={this.handleNotiInit} way={this.state.selectItem.ConsultWay} date={this.state.consultDate} mode={this.state.mode} />) : null}
            </div>
        );
    }
};

MyPresentationList.propTypes = {
    currentType: PropTypes.string,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onAlert: PropTypes.func,
};
MyPresentationList.defaultProps = {
    currentType: "presentation",
    onBackDrop: () => { console.log("backdrop function is not defined."); },
    onAlert: () => { console.log("alert function is not defined."); },
    onRequest: () => { console.log("request function is not defined."); },
};
///////////////////// 그외 신청리스트 끝 ----------------------

///////////////////// 공지사항 **************************
export class BoardNotification extends Component {
    render() {
        return (
            <div>
                <Typography variant="h4" style={{ padding: "8px", marginBottom: "16px" }}>
                    준비중입니다.
                </Typography>
            </div>
        );
    }
};

BoardNotification.propTypes = {

};
BoardNotification.defaultProps = {

};
///////////////////// 공지사항 끝 ------------------------------


///////////////////// 알림 **************************
export class PushNotification extends Component {
    render() {
        return (
            <div>
                <Typography variant="h4" style={{ padding: "8px", marginBottom: "16px" }}>
                    준비중입니다.
                </Typography>
            </div>
        );
    }
};

PushNotification.propTypes = {

};
PushNotification.defaultProps = {

};
///////////////////// 알림 끝 ------------------------------

