import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';

import { Link, Grid, Box, Typography, FormControl, InputLabel, NativeSelect, Container, TextField, ListSubheader, useMediaQuery, AppBar, Toolbar, Drawer, Select} from '@material-ui/core';
import { useTheme, makeStyles } from '@material-ui/core/styles';

//import FindidDialog from '../dialog/findid.dialog';
//import FindpwdDialog from '../dialog/findpwd.dialog';
import { FPTextField } from './parts';
import AlertDialog from '../dialog/alert.dialog';
import { callNativeFunction } from '../helper/appfunction';

const classes = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: '30px',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const LISTBOX_PADDING = 8; // px

function renderRow(props) {
    const { data, index, style } = props;
    return React.cloneElement(data[index], {
        style: {
            ...style,
            top: style.top + LISTBOX_PADDING,
        },
    });
}

const OuterElementContext = React.createContext({});

const OuterElementType = React.forwardRef((props, ref) => {
    const outerProps = React.useContext(OuterElementContext);
    return <div ref={ref} {...props} {...outerProps} />;
});

class Authentication extends Component {
    constructor(props) {
        super(props);

        const sch_code = localStorage.getItem("schoolcode");
        const sch_name = localStorage.getItem("schoolname");

        this.state = {
            userName: '',
            password: '',
            schoolCode: sch_code || '',
            schoolName: sch_name || '',
            regionCode: '',
            regions: [],
            schools: [],
            findidOpen: false,
            findpwdOpen: false,
            shouldOpenList: false,

            alertDialog: false, // 알림창 표시
            alertMessage: '',  // 알림창 내용

            schoolSelect: false,
        };

        window.LoginWeb = function (token, type, version, osversion) {

            if (osversion != null) {
                localStorage.setItem("os", JSON.stringify({
                    os: type,
                    version: osversion,
                }));
            }

            this.funcLogin(token, type, version);
        }.bind(this);
    }


    componentDidMount() {
        
    }

    

    // 로그인
    handleLogin = (e) => {
        if (callNativeFunction("loginWeb") == 0) {
            this.funcLogin("pc", "p", "0.0.0.1");
        }
    };

    funcLogin = (token, type, version) => {
        let schcode = this.state.schoolCode;
        let id = this.state.userName;
        let pw = this.state.password;

        localStorage.setItem("version", version);

        this.props.onLogin(schcode, id, pw, token, type).then(
            (success) => {
                if (!success) {
                    this.setState({
                        alertDialog: true,
                        alertMessage: "아이디 또는 비밀번호가 잘못 되었습니다.",
                    });

                    this.setState({
                        password: ''
                    });
                }
            }
        );
    }

    // 아이디, 비번 입력 시
    handleChange = (e) => {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }
    // enter 입력
    handleKeyPress = (e) => {
        if (e.charCode === 13) {
            this.handleLogin();
        }
    }

    // 학부모 회원가입
    handleRegister = () => {
        this.props.onRegister();
    }

    // 아이디 찾기
    handleModalOpen = () => {
        this.setState({
            findidOpen: true
        });
    };

    // 아이디 찾기 닫기
    handleModalClose = (findid) => {
        this.setState({
            findidOpen: false,
            userName: findid
        });
    }

    // 비밀번호 찾기
    handleModalOpen2 = () => {
        this.setState({
            findpwdOpen: true
        });
    }

    // 비밀번호 찾기 닫기
    handleModalClose2 = (value) => {
        console.log(value);
        this.setState({
            findpwdOpen: false,
        });
    }

    // 알림 창 닫기
    handleAlertClose = (value) => {
        this.setState({
            alertDialog: false
        });
    }

    handleSchoolSelect = (item, value) => {
        localStorage.setItem("schoolcode", value.sch_code);
        localStorage.setItem("schoolname", value.school_name);
        if (value != null) {
            this.setState({
                schoolCode: value.sch_code || "",
                schoolName: value.school_name || "",
                schoolSelect: false,
            });
        }
    }

    handleRegionSelect = (e) => {
        console.log(e.target.name, e.target.value);

        let nextState = {};
        let tValue = e.target.value;
        nextState[e.target.name] = tValue;
        this.setState(nextState);
        if (tValue != 0) {
            this.requestSchoolList(tValue);
        }
    }

    handleSelectSchool = (e) => {
        this.setState({
            schoolSelect: true,
        });
    }

    handleSchoolClose = (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        if (this.state.schoolCode != null && this.state.schoolCode != "") {
            this.setState({
                schoolSelect: false,
            });
        }
    }

    render() {
        return (
            <div>
                <AppBar
                    elevation={0}
                    position="fixed"
                    className={classes.appBar}
                >
                    <Toolbar>
                        <Typography variant="h6" noWrap >
                            로그인
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Container component="main" maxWidth="xl">
                    <Typography variant="subtitle1" gutterBottom >
                        회원 로그인
                    </Typography>
                    <Box mb={5}>
                        <Box border={1} mb={1.5}>
                            <FPTextField required={true} label="학번 및 아이디를 입력하세요." name="userName" onChange={this.handleChange} onKeyPress={this.handleKeyPress} autoFocus={true} value={this.state.userName} />
                        </Box>
                        <Box border={1} mb={2}>
                            <FPTextField required={true} name="password" label="초기비밀번호 및 개인비밀번호를 입력하세요." type="password" onChange={this.handleChange} onKeyPress={this.handleKeyPress} value={this.state.password} />
                        </Box>

                        <Box mb={2}>
                            <Button type="button" fullWidth variant="contained" color="primary" onClick={this.handleLogin}>
                                로그인
                            </Button>
                        </Box>

                        <Grid container justify="center" alignItems="center">
                            <Box onClick={this.handleModalOpen} color="#9A9A9A" fontSize={14}>
                                아이디 찾기
                            </Box>
                            <Box style={{ padding: '0px 8px' }} color="#9A9A9A" fontSize={14}>
                                |
                            </Box>
                            <Box onClick={this.handleModalOpen2} color="#9A9A9A" fontSize={14}>
                                비밀번호 찾기
                            </Box>
                        </Grid>
                    </Box>
                </Container>
                <div style={{ position: "fixed", bottom: "60px", color: "#9A9A9A", width: "100%", textAlign: "center" }}>
                    <Typography variant="h6" >
                        {this.state.schoolName}
                    </Typography>
                </div>

                <AlertDialog open={this.state.alertDialog} onClose={this.handleAlertClose} title="확인" message={this.state.alertMessage} />
            </div>
        );
    }
}

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

Authentication.propTypes = {
    onLogin: PropTypes.func,
    onRegister: PropTypes.func,
    onRequest: PropTypes.func,
    onBackDrop: PropTypes.func,
    onExpire: PropTypes.bool,
};

Authentication.defaultProps = {
    onLogin: (id, pw, mtype = "student") => { console.error("login function is not defined"); },
    onRegister: () => { console.error("register function is not defined"); },
    onRequest: () => { console.error("reqeust function is not defined"); },
    onBackDrop: () => { console.log("backdrop function is not defined"); },
    onExpire: false,
};

export default Authentication;

