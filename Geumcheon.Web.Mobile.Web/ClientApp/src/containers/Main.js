import React, { Component } from 'react';
import Home from '../components/Home';
import { connect } from 'react-redux';

import { requestWebAPI } from '../actions/webApi';
import { setBackDropState } from '../actions/backdropState';
import { clickNavMenu } from '../actions/headerBar';
import { ResponseMessage } from '../helper/appfunction';

class Main extends Component {
    handleLocation = (link, idx) => {
        this.props.history.push(link);
        if (idx != null) {
            this.props.clickNavMenu(idx);
        }
    }

    handleRequest = (url, type, data, auth) => {
        return this.props.requestWebAPI(url, type, data, auth).then(
            () => {
                if (auth) {
                    return this.props.result.data;
                }
                else {
                    var rtnValue = ResponseMessage(this.props.result.status, this.props.result);
                    if (rtnValue == "logout") {
                        this.props.history.push({
                            pathname: '/login',
                            state: {
                                expirelogout: true,
                            }
                        });
                        return {
                            "error": "로그인 정보가 만료되어 로그아웃 됩니다."
                        }
                    }
                    else {
                        return rtnValue;
                    }
                }
            }
        );
    }

    handleSetBackDrop = (value, msg) => {
        this.props.setBackDropState(value, msg);
    }

    handleSetNavSelect = (value) => {
        this.props.clickNavMenu(value);
    }

    render() {
        return (
            <div>
                <Home onLocation={this.handleLocation} onBackDrop={this.handleSetBackDrop} onSelectNavMenu={this.handleSetNavSelect} onRequest={this.handleRequest} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        result: state.webapiData.result,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setBackDropState: (value, msg) => {
            return dispatch(setBackDropState(value, msg));
        },
        clickNavMenu: (value) => {
            return dispatch(clickNavMenu(value));
        },
        requestWebAPI: (url, type, data, auth) => {
            return dispatch(requestWebAPI(url, type, data, auth));
        },  
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main); 