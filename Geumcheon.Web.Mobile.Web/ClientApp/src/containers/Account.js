import React, { Component } from 'react';
import Home from '../components/Home';
import { connect } from 'react-redux';

import { requestWebAPI } from '../actions/webApi';
import { ResponseMessage } from '../helper/appfunction';
import { setAlertState } from '../actions/alertGlobal';
import { AccountModify } from '../components/Account';
import { setBackDropState } from '../actions/backdropState';

class Account extends Component {
    constructor(props) {
        super(props);

        this.state = {
            param: this.props.match.params.param || "join"
        }

        window.alertFunc = null;
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.param.toLowerCase() != (nextProps.match.params.param || "").toLowerCase()) {
            this.setState({
                param: nextProps.match.params.param || "",
            });
        }
    }

    handleSetBackDrop = (value, msg) => {
        this.props.setBackDropState(value, msg);
    }

    handleRequest = (url, type, data, auth) => {
        return this.props.requestWebAPI(url, type, data, auth).then(
            () => {
                var rtnValue = ResponseMessage(this.props.result.status, this.props.result);
                if (rtnValue == "logout") {
                    return {
                        "error": "로그인 정보가 만료되어 로그아웃됩니다."
                    }
                }
                else {
                    return rtnValue;
                }
            }
        );
    }

    handleAlertOpen = (data, func = null) => {
        this.props.setAlertState(true, data);
        if (func != null) {
            window.alertFunc = func;
        }
    }

    render() {
        return (
            <AccountModify onBackDrop={this.handleSetBackDrop} onAlert={this.handleAlertOpen} onRequest={this.handleRequest} />  
        );
    }
}

const mapStateToProps = (state) => {
    return {
        backDrop: state.backDrop.result,
        result: state.webapiData.result,
        alertData: state.alertData.result,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setBackDropState: (value, msg) => {
            return dispatch(setBackDropState(value, msg));
        },
        requestWebAPI: (url, type, data, auth) => {
            return dispatch(requestWebAPI(url, type, data, auth));
        },
        setAlertState: (open, data) => {
            return dispatch(setAlertState(open, data));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account); 