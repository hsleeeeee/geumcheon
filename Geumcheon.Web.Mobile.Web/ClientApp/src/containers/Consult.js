import React, { Component } from 'react';
import Home from '../components/Home';
import { connect } from 'react-redux';

import ConsultPage from '../components/Consult';
import { requestWebAPI } from '../actions/webApi';
import { ResponseMessage } from '../helper/appfunction';
import { setAlertState } from '../actions/alertGlobal';
import { setBackDropState } from '../actions/backdropState';
import { popupLogin } from '../actions/popupLogin';
import { clickNavMenu } from '../actions/headerBar';

class Consult extends Component {
    constructor(props) {
        super(props);

        this.state = {
            param: this.props.match.params.param || "interview",
            idx: this.props.location.state ? this.props.location.state.idx : null
        }
    }

    componentWillReceiveProps(nextProps) {
        const nextState = {};
        if (this.state.param.toLowerCase() != (nextProps.match.params.param || "").toLowerCase()) {
            nextState.param = nextProps.match.params.param || "";
        }
        if (nextProps.location.state != null && this.state.idx != nextProps.location.state.idx) {
            console.log("set idx", nextProps, this.props.history);
            nextState.idx = nextProps.location.state.idx;
            this.props.history.replace({
                pathname: this.props.history.location.pathname,
                state: {}
            });
        }

        if (nextState != {}) {
            this.setState(nextState);
        }
    }

    handleLocation = (link, idx) => {
        this.props.history.push(link);
        if (idx != null) {
            this.props.clickNavMenu(idx);
        }
    }

    handleSetBackDrop = (value, msg) => {
        this.props.setBackDropState(value, msg);
    }

    handleRequest = (url, type, data, auth) => {
        return this.props.requestWebAPI(url, type, data, auth).then(
            () => {
                var rtnValue = ResponseMessage(this.props.result.status, this.props.result);
                if (rtnValue == "logout") {
                    return {
                        "error": "로그인 정보가 만료되어 로그아웃됩니다."
                    }
                }
                else {
                    return rtnValue;
                }
            }
        );
    }

    handleAlertOpen = (data, func = null) => {
        this.props.setAlertState(true, data);
        if (func != null) {
            window.alertFunc = func;
        }
    }

    handleSetWay = () => {
        switch (this.state.param.toLowerCase()) {
            case "interview": return "I";
            case "video": return "V";
            case "online": return "O";
            default: return "O"
        }
    }

    handleInitPushIdx = () => {
        console.log("call idx null");
        this.setState({
            idx: null
        });
    }

    handlePopupLogin = (value) => {
        this.props.popupLogin(value);
    }

    render() {
        return (
            <ConsultPage onBackDrop={this.handleSetBackDrop} onAlert={this.handleAlertOpen} onRequest={this.handleRequest} way={this.handleSetWay()} idx={this.state.idx} onPushInit={this.handleInitPushIdx} onPopupLogin={this.handlePopupLogin} onLocation={this.handleLocation} />
        );
    }

}

const mapStateToProps = (state) => {
    return {
        backDrop: state.backDrop.result,
        result: state.webapiData.result,
        alertData: state.alertData.result,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setBackDropState: (value, msg) => {
            return dispatch(setBackDropState(value, msg));
        },
        requestWebAPI: (url, type, data, auth) => {
            return dispatch(requestWebAPI(url, type, data, auth));
        },
        setAlertState: (open, data) => {
            return dispatch(setAlertState(open, data));
        },
        popupLogin: (value) => {
            return dispatch(popupLogin(value));
        },
        clickNavMenu: (value) => {
            return dispatch(clickNavMenu(value));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Consult); 