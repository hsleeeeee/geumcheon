import Board from './Board';
import Consult from './Consult';
import Main from './Main';
import Presentation from './Presentation';
import Account from './Account';

export { Board, Consult, Main, Presentation, Account }