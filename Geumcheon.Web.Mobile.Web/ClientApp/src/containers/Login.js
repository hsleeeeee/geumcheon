import React, { Component } from 'react';
import Authentication from '../components/Authentication';
import { connect } from 'react-redux';

import { requestLogin } from '../actions/webApi';
import { setBackDropState } from '../actions/backdropState';
import { ResponseMessage, GetUserInfoRole } from '../helper/appfunction';

class Login extends Component {
    state = {
        expirelogout: this.props.location.state ? this.props.location.state.expirelogout : false,
    }

    handleLogin = (schcode, id, pw, token, type) => {
        this.props.setBackDropState(true, "로그인 중");
        return this.props.requestLogin(schcode, id, pw, token, type).then(
            () => {
                this.props.setBackDropState(false, "로그인 중");
                console.log(this.props);
                console.log(this.state);
                if (this.props.result.status === "SUCCESS") {
                    // 학번 최초 로그인일 경우 등록 페이지 이동
                    // to-do
                    //this.props.history.push('/register/student');
                    //else
                    console.log("login success");

                    const userinfo = JSON.parse(localStorage.getItem("user"));

                    if (userinfo != null) {
                        if (userinfo.ukey != null || GetUserInfoRole() != "Parent") {
                            this.props.history.push('/');
                        }
                        else {
                            this.props.history.push({
                                pathname: '/Setting',
                                state: {
                                    registerstudent: true,
                                }
                            });
                        }

                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else if (this.props.result.status === "REGISTER") {
                    this.props.history.push({
                        pathname: '/register/student',
                        state: {
                            userkey: this.props.result.userinfo,
                            schcode: schcode,
                        }
                    });
                    return true;
                }
                else {
                    console.log("failed");
                    return false;
                }
            }
        );
    }

    handleRegister = () => {
        this.props.history.push('/register/parent');
    }

    handleRequest = (url, type, data, auth) => {
        
    }

    render() {
        return (
            <div>
                <Authentication onLogin={this.handleLogin} onRegister={this.handleRegister} onRequest={this.handleRequest} onBackDrop={this.handleSetBackDrop} onExpire={this.state.expirelogout} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        result: state.webapiData.result,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        requestLogin: (id, pwd, mtype, token, type) => {
            return dispatch(requestLogin(id, pwd, mtype, token, type));
        },
        setBackDropState: (value, msg) => {
            return dispatch(setBackDropState(value, msg));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login); 