import { OPENLOGIN, CLOSELOGIN } from './ActionTypes';

// 로그인 팝업
export function popupLogin(value) {
    return (dispatch) => {
        if (value) {
            dispatch(openLogin());
        }
        else {
            dispatch(closeLogin());
        }
    }
}

// 로그인 팝업 열기
function openLogin() {
    return {
        type: OPENLOGIN,
    }
}

// 로그인 팝업 닫기
function closeLogin() {
    return {
        type: CLOSELOGIN,
    }
}