//import { NAVCLOSEBTN_CLICK, NAVCLOSEBTN_SHOW, NAVCLOSEBTN_HIDE, NAVSEARCHBTN_CLICK, NAVSEARCHBTN_SHOW, NAVSEARCHBTN_HIDE, NAVSEARCHBTN_CLOSE, SELECTNAVMENU  } from './ActionTypes';
import { SELECTNAVMENU } from './ActionTypes';


//// 닫기
//export function setNavCloseBtn(value) {
//    return (dispatch) => {
//        console.log("call setnavclosebtn", value);
//        if (value) {
//            dispatch(showNavCloseBtn());
//        }
//        else {
//            dispatch(hideNavCloseBtn());
//        }
//    }
//}

//export function clickNavCloseBtn() {
//    return (dispatch) => {
//        dispatch(hideNavCloseBtn());
//    }
//}

//export function navCloseBtnClick() {
//    return {
//        type: NAVCLOSEBTN_CLICK
//    };
//}

//export function hideNavCloseBtn() {
//    return {
//        type: NAVCLOSEBTN_HIDE,
//    };
//}

//export function showNavCloseBtn() {
//    return {
//        type: NAVCLOSEBTN_SHOW,
//    }
//}






//// 검색
//export function setNavSearchBtn(value) {
//    return (dispatch) => {
//        console.log("call setNavSearchBtn", value);
//        if (value) {
//            dispatch(showNavSearchBtn());
//        }
//        else {
//            dispatch(hideNavSearchBtn());
//        }
//    }
//}

//export function clickNavSearchBtn(value) {
//    return (dispatch) => {
//        console.log("call clickNavSearchBtn", value);
//        if (value) {
//            dispatch(navSearchBtnClick());
//        }
//        else {
//            dispatch(navSearchBtnClose());
//        }
//    }
//}

//export function navSearchBtnClick() {
//    return {
//        type: NAVSEARCHBTN_CLICK
//    };
//}

//export function navSearchBtnClose() {
//    return {
//        type: NAVSEARCHBTN_CLOSE
//    };
//}

//export function hideNavSearchBtn() {
//    return {
//        type: NAVSEARCHBTN_HIDE,
//    };
//}

//export function showNavSearchBtn() {
//    return {
//        type: NAVSEARCHBTN_SHOW,
//    }
//}





// 왼쪽 메뉴
export function clickNavMenu(value, depth) {
    return (dispatch) => {
        dispatch(selectNavMenu(value, depth));
    }
}

export function selectNavMenu(value) {
    return {
        type: SELECTNAVMENU,
        data: value,
    }
}