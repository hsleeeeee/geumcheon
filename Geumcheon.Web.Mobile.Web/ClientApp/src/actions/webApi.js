import { AUTH_LOGIN, AUTH_LOGIN_SUCCESS, AUTH_LOGIN_FAILURE, WEBAPI_REQUEST, WEBAPI_RESPONSE_OK, WEBAPI_RESPONSE_FAILED } from './ActionTypes';
import axios from 'axios';
import { webApiURL } from '../helper/appstring';
import { resumeBackDrop } from './backdropState';

const setLoginInfo = (jwtData) => {
    const jwtDecode = require('jwt-decode');
    const decoded = jwtDecode(jwtData.access_token);

    const userinfo = {
        token: jwtData.access_token,
        type: jwtData.token_type,
        refreshtoken: jwtData.refresh_token,
        //expire: (new Date()).getTime() + parseInt(jwtData.expires_in * 1000),
        expire: (new Date()).getTime() + parseInt(24 * 60 * 60 * 1000),
        ukey: decoded.ukey,
        id: decoded.id,
        name: decoded.name,
        school: decoded.school,
    }

    localStorage.setItem('user', JSON.stringify(userinfo));
}

const requestRefreshLogin = (token, refreshToken, ukey) => {
    return axios({
        method: 'post',
        url: webApiURL() + 'oauth2/token',
        data: "grant_type=refresh_token&refresh_token=" + encodeURIComponent(refreshToken) + "&ukey=" + ukey,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
    }).then(function (response) {
        if (response.status === 200) {
            if (response.data.error != null) {
                // 로그아웃
                console.log("expired refreshtoken");
            }
            else {
                // jwt 토큰 저장
                setLoginInfo(response.data)
            }
        }
    }).catch(function (error) {
        console.log(error);
        localStorage.removeItem('user');
    });
}

export function requestWebAPI(url, type, data, noauth) {
    return (dispatch) => {
        if (noauth) {
            return axios({
                method: type,
                url: webApiURL() + url,
                data: data != null ? JSON.stringify(data) : '',
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(function (response) {
                dispatch(resumeBackDrop());
                dispatch(webapiResponseOK(response.data));
            }).catch(function (error) {
                console.log(error);
                dispatch(resumeBackDrop());
                dispatch(webapiResponseFailed(error));
            });
        }
        else {
            return getTokenPromise().then(function (token) {
                if (token == null) {
                    return dispatch(webapiResponseFailed("logout"));
                }
                else if (type.toLowerCase() == "delete") {
                    return axios.delete(webApiURL() + url, {
                        headers: {
                            'Authorization': token
                        }
                    }).then(function (response) {
                        dispatch(resumeBackDrop());
                        dispatch(webapiResponseOK(response.data));
                    }).catch(function (error) {
                        console.log(error);
                        dispatch(resumeBackDrop());
                        dispatch(webapiResponseFailed(error));
                    });
                }
                else {
                    return axios({
                        method: type,
                        url: webApiURL() + url,
                        data: data != null ? JSON.stringify(data) : '',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': token
                        }
                    }).then(function (response) {
                        dispatch(resumeBackDrop());
                        dispatch(webapiResponseOK(response.data));
                    }).catch(function (error) {
                        console.log(error);
                        dispatch(resumeBackDrop());
                        dispatch(webapiResponseFailed(error));
                    });
                }
            });
        }
    };
}

export function requestLogin(id, pwd, token, type, ) {
    return (dispatch) => {
        dispatch(login());
        return axios({
            method: 'post',
            url: webApiURL() + 'oauth2/token',
            data: "username=" + id + "&password=" + pwd + "&grant_type=password&dtype=" + type + "&token=" + token,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        }).then(function (response) {
            if (response.status === 200) {
                // jwt 토큰 저장
                setLoginInfo(response.data)
            }
            dispatch(loginSuccess(response));
        }).catch(function (error) {
            console.log(error);
        });
    };
}

export function webapiRequest() {
    return {
        type: WEBAPI_REQUEST
    };
}

export function webapiResponseOK(data) {
    return {
        type: WEBAPI_RESPONSE_OK,
        data
    };
}

export function webapiResponseFailed(data) {
    return {
        type: WEBAPI_RESPONSE_FAILED,
        data
    }
}

export function login() {
    return {
        type: AUTH_LOGIN
    };
}

export function loginSuccess(userinfo) {
    return {
        type: AUTH_LOGIN_SUCCESS,
        userinfo
    };
}

export function loginFailure() {
    return {
        type: AUTH_LOGIN_FAILURE
    }
}

export function setLogoutInfo() {
    localStorage.removeItem('user');
}

export function isLogin() {
    let userinfo = JSON.parse(localStorage.getItem('user'));

    if (userinfo == null || userinfo.length == 0) {
        return false;
    }
    else {
        return true;
    }
}



function getToken() {
    let userinfo = JSON.parse(localStorage.getItem('user'));

    if (userinfo == null || userinfo.length == 0) {
        return null;
    }
    else if ((new Date()).getTime() < userinfo.expire) {
        return userinfo.type + " " + userinfo.token;
    }
    else {
        return "";
    }
}

function getRefreshToken() {
    let userinfo = JSON.parse(localStorage.getItem('user'));

    if (userinfo == null || userinfo.length == 0) {
        return null;
    }
    else {
        return userinfo.refreshToken
    }
}

function getTokenPromise() {
    return new Promise(function (resolve, reject) {
        let userinfo = JSON.parse(localStorage.getItem('user'));
        if (userinfo == null || userinfo.token == null) {
            resolve(null);
        }
        else if ((new Date()).getTime() > userinfo.expire) {
            // 토큰 재발급
            return requestRefreshLogin(userinfo.type + " " + userinfo.token, userinfo.refreshtoken, userinfo.ukey).then(function () {
                let newuserinfo = JSON.parse(localStorage.getItem('user'));
                if (newuserinfo != null) {
                    resolve(newuserinfo.type + " " + newuserinfo.token);
                }
                else {
                    resolve(null);
                }
            }).catch(function () {
                resolve(null);
            });
        }
        else {
            resolve(userinfo.type + " " + userinfo.token);
        }
    });
}