import { BACKDROPRESUME, BACKDROPWAIT } from './ActionTypes';


// 닫기
export function setBackDropState(value, msg = '') {
    return (dispatch) => {
        if (value) {
            dispatch(waitBackDrop(msg));
        }
        else {
            dispatch(resumeBackDrop());
        }
    }
}

export function waitBackDrop(msg) {
    return {
        type: BACKDROPWAIT,
        msg: msg
    };
}

export function resumeBackDrop() {
    return {
        type: BACKDROPRESUME,
    };
}
