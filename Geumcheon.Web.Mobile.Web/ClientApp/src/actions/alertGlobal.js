import { ALERT_OPEN, ALERT_CLOSE } from './ActionTypes';


// 닫기
export function setAlertState(open, data) {
    return (dispatch) => {
        if (open) {
            dispatch(openAlert(data));
        }
        else {
            dispatch(closeAlert());
        }
    }
}

export function openAlert(data) {
    return {
        type: ALERT_OPEN,
        data: data,
    };
}

export function closeAlert() {
    return {
        type: ALERT_CLOSE,
    };
}
