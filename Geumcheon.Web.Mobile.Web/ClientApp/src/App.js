import React from 'react';
import { Route, Switch } from 'react-router';
import Layout from './components/Layout';
import { Main, Account, Board, Consult, Presentation } from './containers';

export default function () {
    return (
        <Layout>
            <Switch>
                <Route exact path='/' component={Main} />
                <Route path='/Account/:param?' component={Account} />
                <Route path='/Consult/:param?' component={Consult} />
                <Route path='/Presentation/:param?' component={Presentation} />
                <Route path='/Board/:param?' component={Board} />
            </Switch>
        </Layout>
    );
}

