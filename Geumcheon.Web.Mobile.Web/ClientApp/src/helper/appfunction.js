import { func } from "prop-types";
import axios from 'axios';
import { webApiURL } from './appstring';

export function callNativeFunction(name, ...param) {
    try {
        if (window.webkit != null && window.webkit.messageHandlers != null) {
            if (window.webkit.messageHandlers[name] != null) {
                if (param.length == 1) {
                    window.webkit.messageHandlers[name].postMessage(param[0]);
                }
                else {
                    var paramInfo = {};
                    param.forEach(function (item, idx) {
                        paramInfo["param" + idx.toString()] = item;
                    });

                    console.log("multiple param", paramInfo);
                    window.webkit.messageHandlers[name].postMessage(paramInfo);
                }
            }
            else {
                console.log(name + " function is not native");
            }

            return 1;
        }
        else if (window.JSBridge != null) {
            if (window.JSBridge[name] != null) {
                window.JSBridge[name](...param);
            }
            else {
                console.log(name + " function is not native");
            }

            return 1;
        }
        else {
            return 0;
        }
    }
    catch (ex) {
        console.log("is not native app");
        return 2;
    }
}

export function OpenModal(closeFunc) {
    if (window.modalDialog == null) {
        window.modalDialog = [];
    }

    window.modalDialog.push(closeFunc);
    callNativeFunction("setModalStatus", window.modalDialog.length);
}

export function CloseModal() {
    if (window.modalDialog != null && window.modalDialog.length != 0) {
        window.modalDialog.pop();
        callNativeFunction("setModalStatus", window.modalDialog.length);
    }
}

export function AppCloseModal() {
    if (window.modalDialog != null && window.modalDialog.length != 0) {
        window.modalDialog[window.modalDialog.length - 1]();
    }
}

export function ResponseMessage(status, data) {
    try {
        if (status == "SUCCESS") {
            if (data.data != null) {
                if (typeof data.data == "string") {
                    return JSON.parse(data.data);
                }
                else {
                    return data.data;
                }
            }
        }
        else if (status == "FAILURE") {
            if (data.data == "logout") {
                return "logout";
            }
            if (data.message == "Network Error") {
                return {
                    "error": "서버 접속 중 문제가 발생하였습니다."
                }
            }
            else if (data.response != null && data.response.status == 401) {
                return {
                    "error": "권한이 없습니다."
                }
            }
        }

        return {
            "error": "에러가 발생하였습니다."
        };
    }
    catch (ex) {
        console.log(ex);
        return {
            "error": "에러가 발생하였습니다."
        };
    }

    
    
}

export function GetUserInfoRole() {
    const userinfo = JSON.parse(localStorage.getItem("user"));

    if (userinfo != null) {
        const jwtDecode = require('jwt-decode');
        const decodedInfo = jwtDecode(userinfo.token);
        const roles = decodedInfo["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
        

        if (roles.forEach != null) {
            let rtnValue = "";
            roles.forEach(function (item) {
                if (item != "GEUMCHEON") {
                    rtnValue = item;
                    return false;
                }
            });

            return rtnValue;
        }
        else {
            return roles;
        }
    }

    return null;
}

export function GetUserInfo(name) {
    const userinfo = JSON.parse(localStorage.getItem("user"));

    if (userinfo != null) {
        const jwtDecode = require('jwt-decode');
        const decodedInfo = jwtDecode(userinfo.token);

        return decodedInfo[name] || "";
    }

    return null;
}

export function ajaxWebAPI(url, type, data, noauth) {
    return new Promise(function (resolve, reject) {
        if (noauth) {
            return axios({
                method: type,
                url: webApiURL() + url,
                data: data != null ? JSON.stringify(data) : '',
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then(function (response) {
                if (typeof response.data == "string") {
                    resolve(JSON.parse(response.data));
                }
                else {
                    resolve(response.data);
                }
            }).catch(function (error) {
                console.log(error);
                resolve({ error: error });
            });
        }
        else {
            const userinfo = JSON.parse(localStorage.getItem("user"));
            if (userinfo != null) {
                let token = userinfo.type + " " + userinfo.token;
                return axios({
                    method: type,
                    url: webApiURL() + url,
                    data: data != null ? JSON.stringify(data) : '',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': token
                    }
                }).then(function (response) {
                    if (typeof response.data == "string") {
                        resolve(JSON.parse(response.data));
                    }
                    else {
                        resolve(response.data);
                    }
                }).catch(function (error) {
                    console.log(error);
                    resolve({ error: error });
                });
            }
            else {
                resolve({ error: "권한이 없습니다." });
            }
        }
    });
}