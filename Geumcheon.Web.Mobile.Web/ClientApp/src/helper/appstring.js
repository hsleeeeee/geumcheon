//export const webApiURL = "http://localhost:4654/";
export const webApiURL = () => {
    if (window.location.hostname == "localhost") {
        return "http://localhost:59601/";
    }
    else {
        return window.location.protocol + "//" + window.location.hostname + "/mobilewebservice/";
    }


}

export const downloadURL = webApiURL() + "api/FileDownload?path=";
export const uploadURL = webApiURL() + "api/FileUpload";

export const congratulationImg = "https://theschool.theschools.co.kr/mobileweb/static/css/congratulation.png";
export const logoImg = webApiURL() + "/Resources/Images/logo_m.png";
export const logoImg2 = webApiURL() + "/Resources/Images/logo_h.png";

export const intro01 = webApiURL() + "/Resources/Images/intro01.jpg";
export const intro02 = webApiURL() + "/Resources/Images/intro02.jpg";
export const manualURL = window.location.protocol + "//" + window.location.hostname + "/금천구_대입지원_앱_사용법.pdf";

export const packageName_Android = "com.FuturePlan.TheSchool";
export const packageName_iOS = "https://itunes.apple.com/kr/app/id1501803902";

export const maxUploadSize = 30;

export const errorJsonMessage = { "error": "에러가 발생하였습니다." };

export const subPath = () => {
    if (window.location.hostname == "localhost") {
        return "";
    }
    else {
        return "/mobileweb";
    }
}