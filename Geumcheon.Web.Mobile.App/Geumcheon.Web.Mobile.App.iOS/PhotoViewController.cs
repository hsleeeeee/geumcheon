﻿using System;
using Foundation;
using UIKit;

namespace Geumcheon.Web.Mobile.App.iOS
{
    public partial class PhotoViewController : UIViewController, IUIScrollViewDelegate
    {
        private UIScrollView scrollView;
        private UIImageView imageView;
        public string imgUrl { get; set; }

        public PhotoViewController() : base("PhotoViewController", null)
        {

        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            try
            {
                scrollView = new UIScrollView(UIScreen.MainScreen.Bounds);

                View.Add(scrollView);


                UIImage image = UIImage.FromFile(imgUrl);
                imageView = new UIImageView(image);

                scrollView.AddSubview(imageView);
                scrollView.ContentSize = imageView.Frame.Size;

                nfloat scaleValue = UIScreen.MainScreen.Bounds.Width / imageView.Image.Size.Width;
                scrollView.MinimumZoomScale = (scaleValue * 0.8f) > 1 ? 1 : scaleValue * 0.8f;
                if (scaleValue > 5.0f)
                {
                    scrollView.MaximumZoomScale = scaleValue;
                }
                else
                {
                    scrollView.MaximumZoomScale = 5.0f;
                }   

                scrollView.Delegate = this;

                scrollView.ZoomScale = scaleValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                UIBarButtonItem barButton = new UIBarButtonItem();
                barButton.Title = "Back";
                barButton.Clicked += (s, e) =>
                {
                    this.DismissViewController(true, null);
                };
                this.NavigationItem.LeftBarButtonItem = barButton;
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        [Export("viewForZoomingInScrollView:")]
        public UIView ViewForZoomingInScrollView(UIScrollView scrollView)
        {
            return imageView;
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidAppear(animated);

            Console.WriteLine("Disapper Photo");
        }
    }
}

