﻿using System;

using UIKit;
using WebKit;
using Foundation;

namespace Geumcheon.Web.Mobile.App.iOS
{
    public partial class FileWebViewController : UIViewController
    {
        WKWebView webView;
        public NSUrl dataUrl;

        public FileWebViewController() : base("FileWebViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            try
            {
                this.webView = new WKWebView(
                frame: UIScreen.MainScreen.Bounds,
                configuration: new WKWebViewConfiguration())
                {
                    WeakNavigationDelegate = this
                };

                View.AddSubview(webView);

                webView.LoadFileUrl(dataUrl, dataUrl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                UIBarButtonItem barButton = new UIBarButtonItem();
                barButton.Title = "Back";
                barButton.Clicked += (s, e) =>
                {
                    this.webView.RemoveFromSuperview();
                    this.webView.Dispose();
                    this.DismissViewController(false, null);
                    
                };
                this.NavigationItem.LeftBarButtonItem = barButton;

                UIBarButtonItem barButton2 = new UIBarButtonItem();
                barButton2.Title = "Share";
                barButton2.Clicked += (s, e) =>
                {
                    var viewer = UIDocumentInteractionController.FromUrl(dataUrl);

                    if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
                    {
                        CoreGraphics.CGRect cRect = UIScreen.MainScreen.Bounds;
                        cRect.Y = -260;
                        cRect.Height = 320;
                        viewer.PresentOpenInMenu(cRect, this.View, true);
                    }
                    else
                    {
                        CoreGraphics.CGRect cRect = UIScreen.MainScreen.Bounds;
                        cRect.Y = -260;
                        cRect.Height = 320;
                        viewer.PresentOptionsMenu(cRect, this.View, true);
                    }
                };
                this.NavigationItem.RightBarButtonItem = barButton2;
            }

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidAppear(animated);

            Console.WriteLine("Disapper FileWeb");
        }
    }
}

