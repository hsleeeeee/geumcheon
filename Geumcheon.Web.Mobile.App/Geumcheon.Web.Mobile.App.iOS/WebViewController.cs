﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AssetsLibrary;
using AVFoundation;
using Foundation;
using MobileCoreServices;
using Newtonsoft.Json.Linq;
using Photos;
using Plugin.Connectivity;
using Ricardo.RMBProgressHUD.iOS;
using UIKit;
using WebKit;

namespace Geumcheon.Web.Mobile.App.iOS
{
    public partial class WebViewController : UIViewController, IWKScriptMessageHandler, IWKNavigationDelegate
    {
        const string siteURL = "http://geumcheon.eduplan.co.kr/MobileWeb";
        const string versionURL = "http://geumcheon.eduplan.co.kr/MobileWeb/version.txt";
        WKWebView webView;
        MBProgressHUD webViewDialog;
        UIRefreshControl refreshControl;

        bool initPageLoad = false;
        int modalStatus = 0;
        string notificationData;
        int maxSize = 30 * 1024 * 1024;

        List<UploadFileInfo> uploadFileInfos;

        public WebViewController(IntPtr handle) : base(handle)
        {
        }

        #region override

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            webViewDialog = MBProgressHUD.ShowHUD(View, true);
            webViewDialog.Label.Text = "Please wait...";
            initWebView();

            uploadFileInfos = new List<UploadFileInfo>();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
            NSUrlCache.SharedCache.RemoveAllCachedResponses();
        }

        // 웹뷰 생성 및 브릿지 설저
        public override void LoadView()
        {
            base.LoadView();

            var contentController = new WKUserContentController();
            contentController.AddScriptMessageHandler(this, name: "loginWeb");  // 로그인
            contentController.AddScriptMessageHandler(this, name: "openCamera");    // 카메라
            contentController.AddScriptMessageHandler(this, name: "openGallery");   // 갤러리
            contentController.AddScriptMessageHandler(this, name: "setModalStatus");    // 모달 상태
            contentController.AddScriptMessageHandler(this, name: "notiSetting");   // 알림 설정
            contentController.AddScriptMessageHandler(this, name: "logoutWeb");     // 로그아웃
            contentController.AddScriptMessageHandler(this, name: "getConnectionStatus");   // 인터넷 연결 상태
            contentController.AddScriptMessageHandler(this, name: "appTerminate");  // 앱 종료
            contentController.AddScriptMessageHandler(this, name: "fileDownload");  // 파일 다운로드
            contentController.AddScriptMessageHandler(this, name: "goAppUpdate");   // 앱 업데이트 페이지 가기
            contentController.AddScriptMessageHandler(this, name: "uploadFileSelect");  // 업로드 파일 선택 창 메뉴
            contentController.AddScriptMessageHandler(this, name: "uploadFile");    // 업로드 시작
            contentController.AddScriptMessageHandler(this, name: "uploadFileClear");   // 업로드 할 파일 정보 삭제
            contentController.AddScriptMessageHandler(this, name: "goLocationBrowser"); // 브라우져 열어서 이동
            contentController.AddScriptMessageHandler(this, name: "setSwipeRefresh");   // 리스트 새로 고침 용
            contentController.AddScriptMessageHandler(this, name: "webViewRefresh");    // 웹뷰 새로고침

            var config = new WKWebViewConfiguration();
            config.UserContentController = contentController;

            CoreGraphics.CGRect cGRect = UIScreen.MainScreen.Bounds;
            cGRect.Y = UIApplication.SharedApplication.StatusBarFrame.Size.Height;
            cGRect.Height = cGRect.Height - UIApplication.SharedApplication.StatusBarFrame.Size.Height;

            this.webView = new WKWebView(
                frame: cGRect,
                configuration: config)
            {
                WeakNavigationDelegate = this
            };

            if (VersionCheck())
            {
                var webSiteDataTypes = new NSSet<NSString>(new[]
                {
                    WKWebsiteDataType.DiskCache,
                    WKWebsiteDataType.MemoryCache,
                    WKWebsiteDataType.OfflineWebApplicationCache
                });


                WKWebsiteDataStore.DefaultDataStore.FetchDataRecordsOfTypes(webSiteDataTypes, (NSArray records) =>
                {
                    for (nuint i = 0; i < records.Count; i++)
                    {
                        var record = records.GetItem<WKWebsiteDataRecord>(i);
                        WKWebsiteDataStore.DefaultDataStore.RemoveDataOfTypes(record.DataTypes, new[] { record }, () => { Console.WriteLine("Clear cache"); });
                    }

                });
            }

            this.View.AddSubview(this.webView);

            refreshControl = new UIRefreshControl();
            CoreGraphics.CGRect rRect = refreshControl.Bounds;
            rRect.Y = 60;
            refreshControl.Bounds = rRect;
            refreshControl.AddTarget((s, e) =>
            {
                InvokeOnMainThread(() =>
                {
                    this.webView.EvaluateJavaScript("refreshItemReload()", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(error);
                    });
                });
                refreshControl.EndRefreshing();
            }, UIControlEvent.ValueChanged);
            refreshControl.Enabled = true;

            this.webView.ScrollView.AddSubview(refreshControl);
            this.webView.ScrollView.Bounces = true;
            this.webView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;



            

        }

        #endregion


        #region virtual

        [Export("webView:didFinishNavigation:")]
        public virtual void DidFinishNavigation(WKWebView webView, WKNavigation navigation)
        {
            if (!initPageLoad && webView.Url.ToString().ToLower().IndexOf(siteURL.ToLower()) > -1)
            {
                string deviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushToken");
                if (!string.IsNullOrEmpty(notificationData))
                {
                    this.webView.EvaluateJavaScript("setTimeout(function () {  updateToken('" + deviceToken + "', 'I'); onPushNotificationOpen(null, null, '" + notificationData + "'); }, 1000)", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(error);
                    });
                }
                else
                {
                    this.webView.EvaluateJavaScript("setTimeout(function () { updateToken('" + deviceToken + "', 'I'); }, 1000)", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(error);
                    });
                }

                if (!initPageLoad)
                {
                    initPageLoad = true;
                    webViewDialog.Hide(false);
                }
            }
        }

        [Export("webView:didFailNavigation:withError:")]
        public virtual void DidFailNavigation(WKWebView webView, WKNavigation navigation, NSError error)
        {
            if (webView.Url.ToString().ToLower() == siteURL.ToString())
            {
                string fileName = "Assets/notInternetAvailable.html";
                string htmlString = File.ReadAllText(fileName);
                string contentDirectoryPath = Path.Combine(NSBundle.MainBundle.BundlePath, "Assets/");
                this.webView.LoadHtmlString(htmlString, new NSUrl(contentDirectoryPath, true));
            }
        }

        [Export("webView:decidePolicyForNavigationAction:decisionHandler:")]
        public virtual void DecidePolicy(WKWebView webView, WKNavigationAction navigationAction, Action<WKNavigationActionPolicy> decisionHandler)
        {
            int extpos = navigationAction.Request.Url.AbsoluteString.LastIndexOf('.');
            int extlength = navigationAction.Request.Url.AbsoluteString.Length - (extpos + 1);

            if (navigationAction.Request.Url.Scheme.ToLower() == "tel")
            {
                UIApplication.SharedApplication.OpenUrl(navigationAction.Request.Url);
                decisionHandler(WKNavigationActionPolicy.Cancel);
            }
            else if (extpos > -1 && (extlength == 3 || extlength == 4))
            {
                UIApplication.SharedApplication.OpenUrl(navigationAction.Request.Url);
                decisionHandler(WKNavigationActionPolicy.Cancel);
            }
            else
            {
                decisionHandler(WKNavigationActionPolicy.Allow);
            }
        }

        // 앱 활성화 여부 설정
        public virtual void SetActive(bool isActive)
        {
            UIGraphics.BeginImageContextWithOptions(View.Bounds.Size, false, 0.0f);
            this.webView.EvaluateJavaScript("setActive('" + isActive.ToString().ToLower() + "')", (result, error) =>
            {
                if (error != null) Console.WriteLine(error);
            });
        }

        // 푸시 링크 데이터 연결
        public virtual void onNotification(string pData)
        {
            SetNotificationData(pData);
            if (initPageLoad)
            {
                this.webView.EvaluateJavaScript("onPushNotificationOpen(null, null, '" + notificationData + "')", (result, error) =>
                 {
                     if (error != null) Console.WriteLine(error);
                 });
            }
        }

        // 푸시 표시 및 링크 데이터 연결
        public virtual void onNotification(string pTitle, string pContent, string pData)
        {
            SetNotificationData(pData);
            if (initPageLoad)
            {
                this.webView.EvaluateJavaScript("onPushNotificationOpen('" + pTitle + "', '" + pContent.Replace("\r\n", "br />") + "', '" + notificationData + "')", (result, error) =>
                {
                    if (error != null) Console.WriteLine(error);
                });
            }
        }

        /// <summary>
        /// 화면 크기 수정
        /// </summary>
        /// <param name="height"></param>
        public virtual void Resize(nfloat height)
        {
            CoreGraphics.CGRect bounds = UIScreen.MainScreen.Bounds;

            if (View.Bounds.Height != bounds.Height)
            {
                bounds.Height = bounds.Height - height;
                View.Bounds = bounds;
            }

            foreach (UIView view in View.Subviews)
            {
                view.Frame = view.Bounds;
            }
        }

        #endregion

        #region public


        #endregion

        #region private

        /// <summary>
        /// 웹뷰 초기화
        /// </summary>
        private void initWebView()
        {
            try
            {
                if (IsConnection())
                {
                    initPageLoad = false;
                    webViewDialog.Show(false);
                    NSUrlCache.SharedCache.RemoveAllCachedResponses();
                    Uri uri = new Uri(siteURL);
                    NSUrl appUrl = new NSUrl(uri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.UriEscaped));
                    webView.LoadRequest(new NSUrlRequest(new NSUrl(siteURL), NSUrlRequestCachePolicy.UseProtocolCachePolicy, 20.0f));
                }
                else
                {
                    webViewDialog.Hide(false);
                    string fileName = "notInternetAvailable.html";
                    //string htmlString = File.ReadAllText(fileName);
                    string contentDirectoryPath = Path.Combine(NSBundle.MainBundle.BundlePath, "Assets/");
                    string htmlString = File.ReadAllText(Path.Combine(contentDirectoryPath, fileName));
                    this.webView.LoadHtmlString(htmlString, new NSUrl(contentDirectoryPath, true));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// 웹, 앱 업데이트 버전 체크
        /// </summary>
        /// <returns></returns>
        private bool VersionCheck()
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = Encoding.UTF8;
                    string data = webClient.DownloadString(new System.Uri(versionURL));
                    string[] versions = data.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                    Version recentlyWeb = new Version(versions[0]);
                    Version recentlyApp = new Version(versions[2]);

                    Version curWeb = new Version(NSUserDefaults.StandardUserDefaults.StringForKey("WebVersion"));
                    Version curApp = new Version(NSUserDefaults.StandardUserDefaults.StringForKey("AppVersion"));

                    if (recentlyApp > curApp)
                    {
                        NSUserDefaults.StandardUserDefaults.SetString(recentlyApp.ToString(), "AppVersion");
                        NSUserDefaults.StandardUserDefaults.Synchronize();
                        // 최신 앱을 받아주세요. 표시
                        webView.EvaluateJavaScript("NeedAppUpdate()", (result, error) =>
                        {
                            if (error != null)
                            {
                                Console.WriteLine(error);
                            }
                        });
                    }

                    if (recentlyWeb > curWeb)
                    {
                        NSUserDefaults.StandardUserDefaults.SetString(recentlyWeb.ToString(), "WebVersion");
                        NSUserDefaults.StandardUserDefaults.Synchronize();
                        return true;
                    }
                    else
                    {
                        if (recentlyWeb < Version.Parse("1.0.0.0"))
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        private void SetNotificationData(string pData)
        {
            notificationData = pData;
        }

        private bool SetWebviewUploadFileInfo(string fileName, string fileSize, string fileGuid)
        {
            string uploadJson = string.Format("[{{filename:\"{0}\", filesize:\"{1}\", fileguid:\"{2}\"}}]", fileName, fileSize, fileGuid);
            if (int.Parse(fileSize) > maxSize)
            {
                InvokeOnMainThread(() =>
                {
                    webView.EvaluateJavaScript("OverFileSize(" + uploadJson + ")", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(error);
                    });
                });
                
                return false;
            }
            else
            {
                InvokeOnMainThread(() =>
                {
                    webView.EvaluateJavaScript("SelectComplete(" + uploadJson + ")", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(error);
                    });
                });
                
                return true;
            }
        }

        /// <summary>
        /// 업로드 선택 창
        /// </summary>
        /// <param name="pType"></param>
        /// <param name="pSize"></param>
        private void RunUploadSelector(int pType, int pSize = 0)
        {
            if (pSize != 0)
            {
                maxSize = pSize * 1024 * 1024;
            }

            if (pType == 0)
            {
                FPMedia.TakePicture(this, (obj) =>
                {
                    UploadFileInfo fileInfo = new UploadFileInfo();

                    var photo = obj.ValueForKey(new NSString("UIImagePickerControllerOriginalImage")) as UIImage;

                    AutoResetEvent photoSaveEvent = new AutoResetEvent(false);


                    PHPhotoLibrary.SharedPhotoLibrary.PerformChanges(() =>
                    {
                        PHAssetChangeRequest request = PHAssetChangeRequest.FromImage(photo);

                        fileInfo.fileUri = request.PlaceholderForCreatedAsset.LocalIdentifier;

                        photoSaveEvent.Set();
                    }, (bool success, NSError error) =>
                    {
                        Console.WriteLine(error);
                    });

                    //var phasset = obj.ValueForKey(new NSString("UIImagePickerControllerPHAsset")) as PHAsset;

                    photoSaveEvent.WaitOne();

                    if (!string.IsNullOrEmpty(fileInfo.fileUri))
                    {
                        int tryCount = 0;
                        PHAsset asset = null;
                        while (tryCount < 5 && asset == null)
                        {
                            Thread.Sleep(500);
                            asset = PHAsset.FetchAssetsUsingLocalIdentifiers(new[] { fileInfo.fileUri }, null).firstObject as PHAsset;
                            tryCount += 1;
                        }
                        
                        fileInfo.fileName = asset.ValueForKey(new NSString("filename")).ToString();
                        fileInfo.fileSize = photo.AsJPEG().Length.ToString();
                        fileInfo.fileType = "Image";
                        fileInfo.fileGUID = Guid.NewGuid().ToString();

                        if(SetWebviewUploadFileInfo(fileInfo.fileName, fileInfo.fileSize, fileInfo.fileGUID))
                        {
                            uploadFileInfos.Add(fileInfo);
                        }
                    }
                });
            }
            else if (pType == 1)
            {
                FPMedia.SelectPicture(this, (obj) =>
                {
                    UploadFileInfo fileInfo = new UploadFileInfo
                    {
                        fileSize = "",
                        fileGUID = Guid.NewGuid().ToString()
                    };

                    PHAsset asset = null;
                    if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
                    {

                        if (obj.ValueForKey(UIImagePickerController.PHAsset) != null)
                        {
                            asset = obj.ValueForKey(UIImagePickerController.PHAsset) as PHAsset;
                        }
                        else if (obj.ValueForKey(UIImagePickerController.ReferenceUrl) != null)
                        {
                            var refUrl = obj.ValueForKey(UIImagePickerController.ReferenceUrl) as NSUrl;
                            var fetchAsset = PHAsset.FetchAssets(new[] { refUrl }, null);
                            asset = fetchAsset.firstObject != null ? fetchAsset.firstObject as PHAsset : null;
                        }
                    }
                    else
                    {
                        if (obj.ValueForKey(UIImagePickerController.ReferenceUrl) != null)
                        {
                            var refUrl = obj.ValueForKey(UIImagePickerController.ReferenceUrl) as NSUrl;
                            var fetchAsset = PHAsset.FetchAssets(new[] { refUrl }, null);
                            asset = fetchAsset.firstObject != null ? fetchAsset.firstObject as PHAsset : null;
                        }
                    }

                    if (asset != null)
                    {
                        fileInfo.fileName = asset.ValueForKey(new NSString("filename")).ToString();
                        fileInfo.fileUri = asset.LocalIdentifier;

                        
                            
                        //if (asset.PlaybackStyle == PHAssetPlaybackStyle.Image || asset.PlaybackStyle == PHAssetPlaybackStyle.ImageAnimated || asset.PlaybackStyle == PHAssetPlaybackStyle.LivePhoto)
                        if (asset.MediaType == PHAssetMediaType.Image)
                        {
                            var requestOptions = new PHImageRequestOptions();
                            requestOptions.Synchronous = true;
                            requestOptions.DeliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat;
                            PHImageManager.DefaultManager.RequestImageData(asset, requestOptions, (data, dataUti, orientation, info) =>
                            {
                                fileInfo.fileType = "Image";
                                fileInfo.fileSize = data.Length.ToString();

                                if (!string.IsNullOrEmpty(fileInfo.fileName))
                                {
                                    if (SetWebviewUploadFileInfo(fileInfo.fileName, fileInfo.fileSize, fileInfo.fileGUID))
                                    {
                                        uploadFileInfos.Add(fileInfo);
                                    }
                                }
                            });
                        }
                        //else if (asset.PlaybackStyle == PHAssetPlaybackStyle.LivePhoto)
                        //{
                        //    var imgUrl = obj.ValueForKey(UIImagePickerController.ImageUrl) as NSUrl;

                        //    fileInfo.fileSize = NSData.FromUrl(imgUrl).Length.ToString();
                        //    fileInfo.fileType = "FILE";
                        //    fileInfo.fileUri = imgUrl.ToString();
                        //    fileInfo.fileGUID = Guid.NewGuid().ToString();

                        //    if (!string.IsNullOrEmpty(fileInfo.fileName))
                        //    {
                        //        if (SetWebviewUploadFileInfo(fileInfo.fileName, fileInfo.fileSize, fileInfo.fileGUID))
                        //        {
                        //            uploadFileInfos.Add(fileInfo);
                        //        }
                        //    }

                        //    //var requestOptions = new PHLivePhotoRequestOptions();
                        //    //CoreGraphics.CGSize cgSize = new CoreGraphics.CGSize();
                        //    //cgSize.Width = asset.PixelWidth;
                        //    //cgSize.Height = asset.PixelHeight;
                        //    //requestOptions.DeliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat;

                        //    //PHImageManager.DefaultManager.RequestLivePhoto(asset, cgSize, PHImageContentMode.AspectFill, requestOptions, (result, info) =>
                        //    //{
                        //    //    fileInfo.fileType = "LivePhoto";
                        //    //    //fileInfo.fileSize = data.Length.ToString();

                                
                        //    //});
                        //}
                        //else if (asset.PlaybackStyle == PHAssetPlaybackStyle.Video || asset.PlaybackStyle == PHAssetPlaybackStyle.VideoLooping)
                        else if (asset.MediaType == PHAssetMediaType.Video)
                        {
                            var requestOptions = new PHVideoRequestOptions();
                            requestOptions.DeliveryMode = PHVideoRequestOptionsDeliveryMode.Automatic;
                            PHImageManager.DefaultManager.RequestAvAsset(asset, requestOptions, (avasset, audioMix, info) =>
                            {

                                fileInfo.fileType = "Video";

                                var urlasset = avasset as AVUrlAsset;

                                NSData data = NSData.FromUrl(urlasset.Url);
                                fileInfo.fileSize = data.Length.ToString();
                                fileInfo.fileUri = urlasset.Url.ToString();


                                //avasset.
                                //fileInfo.fileSize = data.Length.ToString();
                                if (!string.IsNullOrEmpty(fileInfo.fileName))
                                {
                                    if (SetWebviewUploadFileInfo(fileInfo.fileName, fileInfo.fileSize, fileInfo.fileGUID))
                                    {
                                        uploadFileInfos.Add(fileInfo);
                                    }
                                }
                            });
                        }
                    }
                    
                    
                });
            }
            else if (pType == 2)
            {
                FPMedia.SelectFile(this, (obj) =>
                {
                    UploadFileInfo fileInfo = new UploadFileInfo();

                    fileInfo.fileSize = NSData.FromUrl(obj).Length.ToString();
                    fileInfo.fileName = obj.LastPathComponent;
                    fileInfo.fileType = "FILE";
                    fileInfo.fileUri = obj.ToString();
                    fileInfo.fileGUID = Guid.NewGuid().ToString();

                    if (SetWebviewUploadFileInfo(fileInfo.fileName, fileInfo.fileSize, fileInfo.fileGUID))
                    {
                        uploadFileInfos.Add(fileInfo);
                    }
                });
            }
        }

        /// <summary>
        /// 파일 다운로드 및 실행
        /// </summary>
        /// <param name="url"></param>
        /// <param name="name"></param>
        /// <param name="token"></param>
        private void DownloadFile(string name, string url, string token, bool confirm = true)
        {
            try
            {
                if (!confirm && File.Exists(System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Personal), name)))
                {
                    webView.EvaluateJavaScript("confirmDownload()", (result, error) => {
                        if (error != null) Console.WriteLine(error);
                    });
                    return;
                }
                
                using (WebClient webClient = new WebClient())
                {
                    //ToastView.Instance.Short(this.View, "다운로드를 시작합니다.");
                    GlobalToast.Toast.ShowToast("다운로드를 시작합니다.");

                    webClient.DownloadDataCompleted += (s, e) =>
                    {
                        // 다운로드 폴더 경로 얻기
                        string storagePath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal); //NSSearchPath.GetDirectories(NSSearchPathDirectory.DownloadsDirectory, NSSearchPathDomain.User, true).First();
                        string fileName = name.Substring(0, name.LastIndexOf("."));
                        string fileExt = name.Substring(name.LastIndexOf(".") + 1, name.Length - name.LastIndexOf(".") - 1);

                        var mimeType = UTType.CreatePreferredIdentifier(UTType.TagClassFilenameExtension, fileExt, null);

                        string filePath = System.IO.Path.Combine(storagePath, name);

                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }

                        //int appendI = 1;
                        //while (File.Exists(filePath))
                        //{
                        //    filePath = System.IO.Path.Combine(storagePath, string.Format("{0} ({1}).{2}", fileName, appendI, fileExt));
                        //    appendI += 1;
                        //}

                        if (e.Error != null)
                        {
                            // to-do 에러 처리
                            GlobalToast.Toast.ShowToast(string.Format("{0}\r\n다운로드에 실패하였습니다.", name));
                        }
                        else
                        {
                            using (System.IO.Stream stream = File.Create(filePath))
                            {
                                stream.Write(e.Result, 0, e.Result.Length);
                            }

                            if (UTType.ConformsTo(mimeType, UTType.Image))
                            {
                                PHPhotoLibrary.SharedPhotoLibrary.PerformChanges(() =>
                                {
                                    PHAssetChangeRequest.FromImage((NSUrl.FromFilename(filePath)));
                                }, (result, error) =>
                                {
                                    if (error != null) Console.WriteLine(error);
                                });

                                InvokeOnMainThread(() => {
                                    PhotoViewController pv = new PhotoViewController();
                                    pv.imgUrl = filePath;

                                    UINavigationController nav = new UINavigationController(pv);
                                    nav.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;

                                    this.PresentViewController(nav , true, null);
                                });
                            }
                            else if (UTType.ConformsTo(mimeType, UTType.Movie) || UTType.ConformsTo(mimeType, UTType.QuickTimeMovie) || UTType.ConformsTo(mimeType, UTType.Video) || UTType.ConformsTo(mimeType, UTType.AVIMovie))
                            {
                                PHPhotoLibrary.SharedPhotoLibrary.PerformChanges(() =>
                                {
                                    PHAssetChangeRequest.FromVideo((NSUrl.FromFilename(filePath)));
                                }, (result, error) =>
                                {
                                    if (error != null) Console.WriteLine(error);
                                });

                                InvokeOnMainThread(() => {
                                    this.PresentViewController(VideoViewController.Instance, true, null);
                                    VideoViewController.Instance.Configuration(NSUrl.FromFilename(filePath));
                                    VideoViewController.Instance.Play();
                                });
                            }
                            else if (UTType.ConformsTo(mimeType, UTType.PDF)
                                || UTType.ConformsTo(mimeType, UTType.PlainText)
                                || UTType.ConformsTo(mimeType, "com.microsoft.word.doc")
                                || UTType.ConformsTo(mimeType, "com.microsoft.excel.xls")
                                || UTType.ConformsTo(mimeType, "com.microsoft.powerpoint.ppt")
                                || UTType.ConformsTo(mimeType, "org.openxmlformats.wordprocessingml.document")
                                || UTType.ConformsTo(mimeType, "org.openxmlformats.spreadsheetml.sheet")
                                || UTType.ConformsTo(mimeType, "org.openxmlformats.presentationml.presentation"))
                            {
                                InvokeOnMainThread(() => {
                                    FileWebViewController fv = new FileWebViewController();
                                    fv.dataUrl = NSUrl.FromFilename(filePath);

                                    UINavigationController nav = new UINavigationController(fv);
                                    nav.ModalPresentationStyle = UIModalPresentationStyle.FullScreen;

                                    this.PresentViewController(nav, true, null);
                                });
                            }
                            else
                            {
                                ExecuteFile(filePath);
                            }

                            GlobalToast.Toast.ShowToast(string.Format("{0}\r\n다운로드가 완료되었습니다.", name));
                        }
                    };

                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add(HttpRequestHeader.Authorization, token);
                    webClient.DownloadDataAsync(new System.Uri(url));
                }
            }
            catch (Exception ex)
            {
                GlobalToast.Toast.ShowToast("다운로드에 실패하였습니다.");
            }
        }

        /// <summary>
        /// 파일 실ㅅ
        /// </summary>
        /// <param name="path"></param>
        private void ExecuteFile(string path)
        {
            try
            {
                NSUrl fileUrl = NSUrl.FromFilename(path);
                var viewer = UIDocumentInteractionController.FromUrl(fileUrl);
                if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
                {
                    CoreGraphics.CGRect cRect = UIScreen.MainScreen.Bounds;
                    cRect.Y = -260;
                    cRect.Height = 320;
                    viewer.PresentOpenInMenu(cRect, this.View, true);
                }
                else
                {
                    CoreGraphics.CGRect cRect = UIScreen.MainScreen.Bounds;
                    cRect.Y = -260;
                    cRect.Height = 320;
                    viewer.PresentOptionsMenu(cRect, this.View, true);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private async void FileUploadAsync(string taskSeq, string uploadURL, string listData, string token)
        {
            MultiPartFormUpload multiPartFormUpload = new MultiPartFormUpload();
            NameValueCollection headers = new NameValueCollection();
            headers.Add("Authorization", token);
            headers.Add("taskseq", taskSeq);
            headers.Add("updatefile", listData);

            try
            {
                MultiPartFormUpload.UploadResponse response = await multiPartFormUpload.Upload(uploadURL, headers, new NameValueCollection() { }, uploadFileInfos);

                if (response.HttpStatusCode == HttpStatusCode.OK)
                {
                    InvokeOnMainThread(() =>
                    {
                        webView.EvaluateJavaScript("uploadFileComplete(\"complete\")", (result, error) =>
                        {
                            if (error != null) Console.WriteLine(error);
                        });
                    });
                    uploadFileInfos.Clear();
                }
                else
                {
                    InvokeOnMainThread(() =>
                    {
                        webView.EvaluateJavaScript("uploadFileComplete(\"fail\")", (result, error) =>
                        {
                            if (error != null) Console.WriteLine(error);
                        });
                    });
                }
            }
            catch (System.Exception ex)
            {
                InvokeOnMainThread(() =>
                {
                    webView.EvaluateJavaScript("uploadFileComplete(\"error\")", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(error);
                    });
                });

            }
        }

        /// <summary>
        /// 인터넷 연결 여
        /// </summary>
        /// <returns></returns>
        private bool IsConnection()
        {
            return CrossConnectivity.Current.IsConnected;
        }

        #endregion

        public UIViewController ViewControllerForPreview(UIDocumentInteractionController controller)
        {
            return this;
        }


        #region JSBridge

        /// <summary>
        /// 브릿지 설저
        /// </summary>
        /// <param name="userContentController"></param>
        /// <param name="message"></param>
        public void DidReceiveScriptMessage(WKUserContentController userContentController, WKScriptMessage message)
        {
            if (message.Name == "loginWeb")
            {
                string deviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushToken");
                this.webView.EvaluateJavaScript("LoginWeb('" + deviceToken + "', 'I', '" + NSUserDefaults.StandardUserDefaults.StringForKey("AppVersion") + "', '" + UIDevice.CurrentDevice.SystemVersion + "')", (result, error) =>
                {
                    if (error != null) Console.WriteLine(error);
                });
            }
            else if (message.Name == "logoutWeb")
            {

            }
            else if (message.Name == "setModalStatus")
            {
                int modalCount;
                if (int.TryParse(message.Body.ToString(), out modalCount))
                {
                    modalStatus = modalCount;
                }
            }
            else if (message.Name == "notiSetting")
            {

            }
            else if (message.Name == "logoutWeb")
            {

            }
            else if (message.Name == "getConnectionStatus")
            {
                if (IsConnection())
                {
                    this.webView.EvaluateJavaScript("InternetConnect(true)", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(result);
                    });
                }
                else
                {
                    this.webView.EvaluateJavaScript("InternetConnect(false)", (result, error) =>
                    {
                        if (error != null) Console.WriteLine(result);
                    });
                }
            }
            else if (message.Name == "appTerminate")
            {

            }
            else if (message.Name == "fileDownload")
            {
                string name = message.Body.ValueForKey(new NSString("param0")).ToString();
                string url = message.Body.ValueForKey(new NSString("param1")).ToString();
                string token = message.Body.ValueForKey(new NSString("param2")).ToString();
                bool popupConfirm = false;
                if (message.Body.ValueForKey(new NSString("param3")) != null)
                {
                    bool.TryParse(message.Body.ValueForKey(new NSString("param3")).ToString(), out popupConfirm);
                    DownloadFile(name, url, token, popupConfirm);
                }
                else
                {
                    DownloadFile(name, url, token);
                }


            }
            else if (message.Name == "goAppUpdate")
            {
                string appUrlString = message.Body.ToString();
                try
                {
                    Uri uri = new Uri(appUrlString);
                    NSUrl appUrl = new NSUrl(uri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.UriEscaped));
                    UIApplication.SharedApplication.OpenUrl(appUrl);
                }
                catch
                {

                }
            }
            else if (message.Name == "uploadFileSelect")
            {
                int selectType;
                int fileSize = 0;

                if (int.TryParse(message.Body.ValueForKey(new NSString("param0")).ToString(), out selectType))
                {
                    int.TryParse(message.Body.ValueForKey(new NSString("param1"))?.ToString(), out fileSize);

                    RunUploadSelector(selectType, fileSize);
                }

                //BeginInvokeOnMainThread(() =>
                //{
                //    if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad)
                //    {
                //        attachMenu.PopoverPresentationController.SourceView = this.View;
                //        attachMenu.PopoverPresentationController.SourceRect = new CoreGraphics.CGRect(this.View.Bounds.X, this.View.Bounds.Y, 0, 0);
                //        attachMenu.PopoverPresentationController.PermittedArrowDirections = UIPopoverArrowDirection.Any;
                //    }
                //    this.PresentViewController(attachMenu, true, null);

                //});
            }
            else if (message.Name == "uploadFile")
            {
                string taskSeq = message.Body.ValueForKey(new NSString("param0")).ToString();
                string uploadURL = message.Body.ValueForKey(new NSString("param1")).ToString();
                string listData = message.Body.ValueForKey(new NSString("param2")).ToString();
                string token = message.Body.ValueForKey(new NSString("param3")).ToString();

                FileUploadAsync(taskSeq, uploadURL, listData, token);



            }
            else if (message.Name == "uploadFileClear")
            {
                if (message.Body.ToString().ToLower() == "all")
                {
                    uploadFileInfos.Clear();
                }
                else
                {
                    var removeitem = uploadFileInfos.Where(x => x.fileGUID == message.Body.ToString()).FirstOrDefault();
                    if (removeitem != null)
                    {
                        uploadFileInfos.Remove(removeitem);
                    }
                }
            }
            else if (message.Name == "goLocationBrowser")
            {
                string urlString = message.Body.ToString();

                if (urlString.Trim().IndexOf("http://") != 0 && urlString.Trim().IndexOf("https://") != 0)
                {
                    urlString = "https://" + urlString;
                }

                try
                {
                    UIApplication.SharedApplication.OpenUrl(new NSUrl(urlString).AbsoluteUrl);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            else if (message.Name == "setSwipeRefresh")
            {
                bool isRefresh = false;
                bool.TryParse(message.Body.ToString(), out isRefresh);

                InvokeOnMainThread(() =>
                {
                    refreshControl.Enabled = isRefresh;
                });
            }
            else if (message.Name == "webViewRefresh")
            {
                if (IsConnection())
                {
                    initWebView();
                }
                else
                {
                    GlobalToast.Toast.ShowToast("인터넷에 연결할 수 없습니다.");
                }
                
            }
        }

        #endregion




    }


    public class UploadFileInfo
    {
        public string fileName { get; set; }
        public string fileSize { get; set; }
        public string fileType { get; set; }
        public string fileUri { get; set; }
        public string fileGUID { get; set; }
    }

    public class MultiPartFormUpload
    {
        public class MimePart
        {
            NameValueCollection _headers = new NameValueCollection();
            byte[] _header;

            public NameValueCollection Headers
            {
                get { return _headers; }
            }

            public byte[] Header
            {
                get { return _header; }
            }

            public long GenerateHeaderFooterData(string boundary)
            {
                System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();

                stringBuilder.Append("--");
                stringBuilder.Append(boundary);
                stringBuilder.AppendLine();
                foreach (string key in _headers.AllKeys)
                {
                    stringBuilder.Append(key);
                    stringBuilder.Append(": ");
                    stringBuilder.AppendLine(_headers[key]);
                }
                stringBuilder.AppendLine();

                _header = System.Text.Encoding.UTF8.GetBytes(stringBuilder.ToString());

                return _header.Length + fileLength + 2;
            }

            public int fileLength { get; set; }
            public System.IO.Stream Data { get; set; }
        }

        public class UploadResponse
        {
            public UploadResponse(HttpStatusCode httpStatusCode, string responseBody)
            {
                HttpStatusCode = httpStatusCode;
                ResponseBody = responseBody;
            }

            public HttpStatusCode HttpStatusCode { get; set; }

            public string ResponseBody { get; set; }
        }

        public async Task<UploadResponse> Upload(string url, NameValueCollection requestHeaders, NameValueCollection requestParameters, List<UploadFileInfo> files)
        {
            using (WebClient client = new WebClient())
            {
                List<MimePart> mimeParts = new List<MimePart>();

                try
                {
                    foreach (string key in requestHeaders.AllKeys)
                    {
                        client.Headers.Add(key, requestHeaders[key]);
                    }

                    foreach (string key in requestParameters.AllKeys)
                    {
                        MimePart part = new MimePart();

                        part.Headers["Content-Disposition"] = "form-data; name=\"" + key + "\"";
                        part.Data = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(requestParameters[key]));

                        mimeParts.Add(part);
                    }

                    foreach (UploadFileInfo file in files)
                    {
                        

                        byte[] imgData = null;

                        if (file.fileType == "Image" || file.fileType == "LivePhoto")
                        {
                            var assets = PHAsset.FetchAssetsUsingLocalIdentifiers(new[] { file.fileUri }, null);
                            if (assets.firstObject != null)
                            {
                                var asset = assets.firstObject as PHAsset;
                                var requestOptions = new PHImageRequestOptions();
                                CoreGraphics.CGSize rect = new CoreGraphics.CGSize();
                                rect.Width = asset.PixelWidth;
                                rect.Height = asset.PixelHeight;
                                requestOptions.Synchronous = true;
                                requestOptions.DeliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat;
                                AutoResetEvent getDataWait = new AutoResetEvent(false);
                                PHImageManager.DefaultManager.RequestImageData(asset as PHAsset, requestOptions, (data, dataUti, orientation, info) =>
                                {
                                    //NSData data = image.AsJPEG();
                                    imgData = new byte[data.Length];
                                    Marshal.Copy(data.Bytes, imgData, 0, (int)data.Length);
                                });
                            }
                        }
                        else if (file.fileType == "FILE" || file.fileType == "Video")
                        {
                            NSData data = NSData.FromUrl(NSUrl.FromString(file.fileUri));
                            imgData = new byte[data.Length];
                            Marshal.Copy(data.Bytes, imgData, 0, (int)data.Length);
                        }

                        if (imgData != null)
                        {
                            MimePart part = new MimePart();
                            string name = file.fileGUID;
                            string fileName = file.fileName;

                            part.Headers["Content-Disposition"] = "form-data; name=\"" + name + "\"; filename=\"" + fileName + "\"";
                            part.Headers["Content-Type"] = "application/octet-stream";

                            System.IO.Stream cfileStream = (new MemoryStream(imgData, false)) as System.IO.Stream;
                            imgData = null;

                            part.Data = cfileStream;
                            part.fileLength = Convert.ToInt32(cfileStream.Length);
                            mimeParts.Add(part);
                        }
                    }

                    string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                    client.Headers.Add(HttpRequestHeader.ContentType, "multipart/form-data; boundary=" + boundary);

                    long contentLength = 0;

                    byte[] _footer = System.Text.Encoding.UTF8.GetBytes("--" + boundary + "--\r\n");

                    foreach (MimePart mimePart in mimeParts)
                    {
                        contentLength += mimePart.GenerateHeaderFooterData(boundary);
                    }

                    byte[] buffer = new byte[8192];
                    byte[] afterFile = System.Text.Encoding.UTF8.GetBytes("\r\n");
                    int read;

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        foreach (MimePart mimePart in mimeParts)
                        {
                            memoryStream.Write(mimePart.Header, 0, mimePart.Header.Length);

                            while ((read = mimePart.Data.Read(buffer, 0, buffer.Length)) > 0)
                                memoryStream.Write(buffer, 0, read);

                            mimePart.Data.Dispose();

                            memoryStream.Write(afterFile, 0, afterFile.Length);
                        }

                        memoryStream.Write(_footer, 0, _footer.Length);
                        byte[] responseBytes = client.UploadData(url, memoryStream.ToArray());
                        string responseString = System.Text.Encoding.UTF8.GetString(responseBytes);
                        return new UploadResponse(HttpStatusCode.OK, responseString);
                    }
                }
                catch (System.Exception ex)
                {
                    foreach (MimePart part in mimeParts)
                        if (part.Data != null)
                            part.Data.Dispose();

                    if (ex.GetType().Name == "WebException")
                    {
                        WebException webException = (WebException)ex;
                        HttpWebResponse response = (HttpWebResponse)webException.Response;
                        string responseString;

                        using (StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8))
                        {
                            responseString = reader.ReadToEnd();
                        }

                        return new UploadResponse(response.StatusCode, responseString);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
    }
}

