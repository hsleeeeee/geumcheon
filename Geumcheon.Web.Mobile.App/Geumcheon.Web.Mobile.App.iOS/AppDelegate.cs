﻿using System;
using System.Collections.Generic;
using System.Linq;
using Firebase.CloudMessaging;
using Foundation;
using UIKit;
using UserNotifications;

namespace Geumcheon.Web.Mobile.App.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate, IUNUserNotificationCenterDelegate, IMessagingDelegate
    {
        public override UIWindow Window
        {
            get; set;
        }

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            // 10.0 이상
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                UNUserNotificationCenter.Current.Delegate = this;

                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
                {
                    Console.WriteLine(granted);
                    if (granted)
                    {
                        InvokeOnMainThread(UIApplication.SharedApplication.RegisterForRemoteNotifications);
                    }
                });
            }
            // 이하
            else
            {
                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }

            Firebase.Core.App.Configure();
            Messaging.SharedInstance.Delegate = this;

            Messaging.SharedInstance.ShouldEstablishDirectChannel = true;

            if (!string.IsNullOrEmpty(Messaging.SharedInstance.FcmToken))
            {
                NSUserDefaults.StandardUserDefaults.SetString(Messaging.SharedInstance.FcmToken, "PushToken");
                NSUserDefaults.StandardUserDefaults.Synchronize();
            }

            return true;
        }

        [Export("messaging:didReceiveRegistrationToken:")]
        public void DidReceiveRegistrationToken(Messaging messaging, string fcmToken)
        {
            NSUserDefaults.StandardUserDefaults.SetString(fcmToken, "PushToken");
            NSUserDefaults.StandardUserDefaults.Synchronize();

            var dict = new NSDictionary("token", fcmToken);

            NSNotificationCenter.DefaultCenter.PostNotificationName("FCMToken", dict);

            WebViewController rootViewController = (WebViewController)this.Window.RootViewController;
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            PassPushData(userInfo, application.ApplicationState == UIApplicationState.Active);
            //completionHandler(UIBackgroundFetchResult.NewData);
        }

        [Export("application:didRegisterForRemoteNotificationsWithDeviceToken:")]
        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Messaging.SharedInstance.ApnsToken = deviceToken;
            WebViewController rootViewController = (WebViewController)this.Window.RootViewController;
        }

        [Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]
        public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            //Console.WriteLine(center.Description);
            //Console.WriteLine(notification.Request.Content.UserInfo.ToString());
            //completionHandler(UNNotificationPresentationOptions.Alert);
            PassPushData(notification.Request.Content.UserInfo, UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active);
        }

        // iOS에서 알림 클릭시 앱이 열리면서 알림 데이터 넘겨 받음
        [Export("userNotificationCenter:didReceivNotificationResponse:withCompletionHandler:")]
        public void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completeHandler)
        {
            PassPushData(response.Notification.Request.Content.UserInfo, UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active);
        }


        public override void OnResignActivation(UIApplication application)
        {

        }

        // 백그라운드로 돌아갈때. 즉 앱 비활성화
        public override void DidEnterBackground(UIApplication application)
        {
            Messaging.SharedInstance.ShouldEstablishDirectChannel = false;
        }

        // 앱 활성화 될 때
        public override void WillEnterForeground(UIApplication application)
        {
            //base.WillEnterForeground(application);
        }

        public override void OnActivated(UIApplication application)
        {
            //base.OnActivated(application);
        }

        public override void WillTerminate(UIApplication application)
        {
            //base.WillTerminate(application);
        }

        public void ApplicationReceivedRemoteMessage(RemoteMessage remoteMessage)
        {

        }

        public void DidRefreshRegistrationToken(Messaging messaging, string fcmToken)
        {
            NSUserDefaults.StandardUserDefaults.SetString(fcmToken, "PushToken");
            NSUserDefaults.StandardUserDefaults.Synchronize();
        }

        private void PassPushData(NSDictionary options, bool pActive)
        {
            if (pActive)
            {
                if (options != null && options.ContainsKey(new NSString("aps")))
                {
                    NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

                    if (aps.ContainsKey(new NSString("alert")))
                    {
                        NSDictionary alert = aps.ObjectForKey(new NSString("alert")) as NSDictionary;

                        string titleData = string.Empty;
                        string contentData = string.Empty;
                        string pushData = string.Empty;

                        if (alert.ContainsKey(new NSString("title")))
                        {
                            titleData = alert.ObjectForKey(new NSString("title")).ToString();
                        }

                        if (alert.ContainsKey(new NSString("body")))
                        {
                            contentData = alert.ObjectForKey(new NSString("body")).ToString();
                        }

                        if (alert.ContainsKey(new NSString("gcm.notification.data")))
                        {
                            pushData = alert.ObjectForKey(new NSString("gcm.notification.data")).ToString();
                        }

                        WebViewController rootViewController = (WebViewController)this.Window.RootViewController;
                        rootViewController.onNotification(titleData, contentData, pushData);
                    }
                }
            }
            else
            {
                if (options != null && options.ContainsKey(new NSString("gcm.notification.data")))
                {
                    string pushData = string.Empty;
                    pushData = options.ObjectForKey(new NSString("gcm.notification.data")).ToString();

                    if (!string.IsNullOrEmpty(pushData))
                    {
                        WebViewController rootViewController = (WebViewController)this.Window.RootViewController;
                        rootViewController.onNotification(pushData);
                    }
                }
            }
        }
    }
}
