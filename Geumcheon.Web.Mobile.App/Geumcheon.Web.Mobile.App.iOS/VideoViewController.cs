﻿using System;
using AVFoundation;
using AVKit;
using Foundation;
using UIKit;

namespace Geumcheon.Web.Mobile.App.iOS
{
    public partial class VideoViewController : AVPlayerViewController
    {
        private static readonly Lazy<VideoViewController> _instance = new Lazy<VideoViewController>(() => new VideoViewController());

        public static VideoViewController Instance { get { return _instance.Value; } }

        private AVPlayer player;

        public VideoViewController() : base("VideoViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public void Configuration(NSUrl data)
        {

            player = new AVPlayer(data);
            player.ExternalPlaybackVideoGravity = AVLayerVideoGravity.Resize;
            this.Player = player;

        }

        public void Play()
        {
            if (player.TimeControlStatus != AVPlayerTimeControlStatus.Playing)
            {
                player.Play();
            }
        }
    }
}

