﻿using System;
using AVFoundation;
using Foundation;
using UIKit;

namespace Geumcheon.Web.Mobile.App.iOS
{
    public static class FPMedia
    {
        static UIImagePickerController imagePicker;
        static Action<NSDictionary> _imageCallback;
        static Action<NSUrl> _documentCallback;
        static UIDocumentPickerViewController filePicker;

        static void Init()
        {
            if (imagePicker != null && filePicker != null)
            {
                return;
            }

            imagePicker = new UIImagePickerController();
            if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                imagePicker.VideoExportPreset = AVAssetExportSessionPreset.Passthrough.GetConstant().ToString();
            }
            imagePicker.Delegate = new CameraDelegate();

            if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                filePicker = new UIDocumentPickerViewController(new[] { "public.item" }, UIDocumentPickerMode.Import);
                filePicker.Delegate = new DocumentPickerDelegate();
            }
        }

        class CameraDelegate : UIImagePickerControllerDelegate
        {
            public override void FinishedPickingMedia(UIImagePickerController picker, NSDictionary info)
            {
                var cb = _imageCallback;
                _imageCallback = null;

                picker.DismissModalViewController(true);
                cb(info);
            }
        }

        class DocumentPickerDelegate : UIDocumentPickerDelegate
        {
            public override void DidPickDocument(UIDocumentPickerViewController controller, NSUrl url)
            {
                var cb = _documentCallback;
                _documentCallback = null;

                controller.DismissModalViewController(true);
                cb(url);
            }
        }

        public static void TakePicture(UIViewController parent, Action<NSDictionary> callback)
        {
            Init();
            imagePicker.SourceType = UIImagePickerControllerSourceType.Camera;
            _imageCallback = callback;
            parent.PresentModalViewController(imagePicker, true);
        }

        public static void SelectPicture(UIViewController parent, Action<NSDictionary> callback)
        {
            Init();
            imagePicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePicker.MediaTypes = new[] { "public.image", "public.movie" };
            _imageCallback = callback;
            parent.PresentModalViewController(imagePicker, true);
        }

        public static void SelectFile(UIViewController parent, Action<NSUrl> callback)
        {
            Init();
            _documentCallback = callback;
            parent.PresentModalViewController(filePicker, true);
        }
    }
}
