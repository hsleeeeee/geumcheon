﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Geumcheon.Web.Mobile.App.Droid
{
    public class StoreApplication : Application
    {
        public static string deviceToken = string.Empty;
        public static string deviceID = string.Empty;
        public static string deviceOS = string.Empty;

        public static bool activityVisible = false;

        public static readonly string channelID = "GeumCheonConsult_Notification";
        public static int notificationID = 8489;
    }
}