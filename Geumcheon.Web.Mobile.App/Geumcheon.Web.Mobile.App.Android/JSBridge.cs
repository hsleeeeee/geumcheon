﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Java.Interop;

namespace Geumcheon.Web.Mobile.App.Droid
{
    public class JSBridge : Activity
    {
        Context _context;

        public JSBridge(Context context)
        {
            this._context = context;
        }

        /// <summary>
        /// 현재 디바이스의 Push 토큰과 디바이스 정보를 웹에 전달하도록 요청한다.
        /// </summary>
        [Export("loginWeb")]
        [JavascriptInterface]
        public void LoginWeb()
        {
            ((MainActivity)_context).LoginWeb();
        }

        /// <summary>
        /// 카메라 실행
        /// </summary>
        [Export("openCamera")]
        [JavascriptInterface]
        public void OpenCamera()
        {
            ((MainActivity)_context).OpenCamera();
        }

        /// <summary>
        /// 갤러리 열기
        /// </summary>
        [Export("openGallery")]
        [JavascriptInterface]
        public void OpenGallery()
        {
            ((MainActivity)_context).OpenGallery();
        }

        /// <summary>
        /// 모달 갯수 설정
        /// </summary>
        [Export("setModalStatus")]
        [JavascriptInterface]
        public void SetModalStatus(int pStatus)
        {
            ((MainActivity)_context).SetModalStatus(pStatus);
        }

        /// <summary>
        /// 알림 설정 창
        /// </summary>
        [Export("notiSetting")]
        [JavascriptInterface]
        public void NotiSetting()
        {
            // ((MainActivity)_context).NotiSetting();
        }

        /// <summary>
        /// 로그아웃
        /// </summary>
        [Export("logoutWeb")]
        [JavascriptInterface]
        public void LogoutWeb()
        {
            ((MainActivity)_context).LogoutWeb();
        }

        /// <summary>
        /// 현재 App의 버젼
        /// </summary>
        [Export("getAppVersion")]
        [JavascriptInterface]
        public void GetAppVersion()
        {
            //((MainActivity)_context).GetAppVersion();
        }

        /// <summary>
        /// 인터넷 연결 상태
        /// </summary>
        [Export("getConnectionStatus")]
        [JavascriptInterface]
        public void GetConnectionStatus()
        {
            ((MainActivity)_context).IsConnection();
        }

        /// <summary>
        /// 페이지 새로고침
        /// </summary>
        [Export("webViewRefresh")]
        [JavascriptInterface]
        public void WebViewRefresh()
        {
            ((MainActivity)_context).WebViewRefresh();
        }

        /// <summary>
        /// 앱종료
        /// </summary>
        [Export("appTerminate")]
        [JavascriptInterface]
        public void AppTerminate()
        {
            //((MainActivity)_context).AppTerminate();
        }

        [JavascriptInterface]
        [Export("fileDownload")]
        public void FileDownload(string name, string url, string token = null)
        {
            ((MainActivity)_context).DownloadFromURL(name, url, token, true);
        }

        [JavascriptInterface]
        [Export("fileDownload")]
        public void FileDownload(string name, string url, string token = null, bool isConfirm = true)
        {
            ((MainActivity)_context).DownloadFromURL(name, url, token, isConfirm);
        }

        [Export("goAppUpdate")]
        [JavascriptInterface]
        public void GoAppUpdate(string packageName)
        {
            ((MainActivity)_context).GoAppUpdate(packageName);
        }

        /// <summary>
        /// 파일 업로드 클릭 시 파일 선택 창 표시
        /// </summary>
        /// <param name="pType">사진 or 파일(photo or file)</param>
        [Export("uploadFileSelect")]
        [JavascriptInterface]
        public void UploadFileSelect(int pType, int maxSize)
        {
            ((MainActivity)_context).SetUploadSize(maxSize);

            if (pType == 0)
            {
                ((MainActivity)_context).OpenCamera();
            }
            else if (pType == 1)
            {
                ((MainActivity)_context).ShowFileSelector("image/*");
            }
            else if (pType == 2)
            {
                ((MainActivity)_context).ShowFileSelector("*/*");
            }
        }

        /// <summary>
        /// 파일 업로드 시작
        /// </summary>
        /// <param name="pType">사진 or 파일(photo or file)</param>
        [Export("uploadFile")]
        [JavascriptInterface]
        public void UploadFile(string taskSeq, string uploadURL, string listData, string token)
        {
            ((MainActivity)_context).UploadFile(taskSeq, uploadURL, listData, token);
        }

        /// <summary>
        /// 업로드 할 파일 정보 삭제
        /// </summary>
        /// <param name="pType">사진 or 파일(photo or file)</param>
        [Export("uploadFileClear")]
        [JavascriptInterface]
        public void UploadFileClear(string fileid)
        {
            ((MainActivity)_context).UploadFileClear(fileid);
        }

        /// <summary>
        /// 브라우져 이동
        /// </summary>
        /// <param name="pType">사진 or 파일(photo or file)</param>
        [Export("goLocationBrowser")]
        [JavascriptInterface]
        public void GoLocationBrowser(string url)
        {
            ((MainActivity)_context).GoLocationBrowser(url);
        }

        [Export("setSwipeRefresh")]
        [JavascriptInterface]
        public void SetSwipeRefresh(bool value)
        {
            ((MainActivity)_context).SetSwipeRefresh(value);
        }
    }
}