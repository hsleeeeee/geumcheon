﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Android.Webkit;
using Android.Content;
using Android;
using Android.Net;
using System.Net;
using Android.Support.V4.Content;
using Android.Media;
using Android.Gms.Common;
using Android.Provider;
using Java.IO;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Linq;
using Java.Lang;
using Android.Annotation;
using Android.Support.V4.Widget;
using System.Threading.Tasks;
using AndroidHUD;

namespace Geumcheon.Web.Mobile.App.Droid
{
    [Activity(Label = "금천구 대입지원", Icon = "@drawable/ic_launcher", Theme = "@style/splashscreen", MainLauncher = true, LaunchMode = Android.Content.PM.LaunchMode.SingleTop, ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.KeyboardHidden, WindowSoftInputMode = SoftInput.AdjustResize,
        HardwareAccelerated = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        bool initPageLoad = false;
        const string siteURL = "http://geumcheon.eduplan.co.kr/MobileWeb";
        const string versionURL = "https://geumcheon.eduplan.co.kr/MobileWeb/version.txt";
        WebView webView;
        Context MainContext = null;
        SwipeRefreshLayout refreshView;

        int modalStatus = 0;

        int requestActivity_Camera = 1000;
        int requestActivity_Gallery = 1001;
        int requestPermission_Camera = 1002;
        int requestPermission_Storage = 1003;
        int requestPermission_Camera_Use = 1004;
        int requestPermission_Storage_Use = 1005;
        int requestPermission_NetworkState = 1006;

        int maxSize = 30 * 1024 * 1024;

        string tempFileName = string.Empty;
        string tempFileUrl = string.Empty;
        string tempToken = string.Empty;
        bool tempConfirm = true;
        string photoFilePath = string.Empty;
        string photoFileName = string.Empty;
        bool doubleBacktoExit = false;
        bool onFileSelect = false;
        string ShowFileType = "";

        List<UploadFileInfo> uploadFileInfos;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            this.Window.AddFlags(WindowManagerFlags.ForceNotFullscreen);
            this.Window.ClearFlags(WindowManagerFlags.Fullscreen);
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            MainContext = this;

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            IsPlayServiceAvailable();
            CreateNotificationCannel();

            var sharedPreferences = Android.Preferences.PreferenceManager.GetDefaultSharedPreferences(this);
            StoreApplication.deviceToken = sharedPreferences.GetString("PushToken", "");

            bool clearCache = VersionCheck();

            // Get our button from the layout resource,
            // and attach an event to it
            webView = FindViewById<WebView>(Resource.Id.FP_webView);
            webView.SetWebViewClient(new FPWebViewClient(this)); // stops request going to Web Browser
            webView.Settings.UseWideViewPort = true;
            webView.Settings.JavaScriptEnabled = true;
            webView.Settings.AllowFileAccess = true;
            webView.Settings.AllowContentAccess = true;
            webView.Settings.SaveFormData = false;
            webView.Settings.DomStorageEnabled = true;
            webView.SetWebChromeClient(new WebChromeClient());
            webView.Settings.CacheMode = CacheModes.Default;
            webView.Settings.SetAppCacheEnabled(true);
            webView.Settings.MixedContentMode = MixedContentHandling.AlwaysAllow;
            //webView.Settings.SetAppCacheMaxSize(1024 * 1024 * 30);
            //webView.SetWebViewClient(new WebViewClientAuthentication());
            webView.AddJavascriptInterface(new JSBridge(this), "JSBridge");
            //webView.Settings.SetPluginState(WebSettings.PluginState.On);
#if DEBUG
            WebView.SetWebContentsDebuggingEnabled(true);
#else
            WebView.SetWebContentsDebuggingEnabled(false);
#endif

            if (clearCache)
            {
                webView.ClearCache(true);
            }

            refreshView = FindViewById<SwipeRefreshLayout>(Resource.Id.FP_swipeRefresh);
            refreshView.SetColorSchemeColors(Android.Graphics.Color.ParseColor("#FF7C27"));
            refreshView.Refresh += webView_Refresh;
            refreshView.Enabled = true;



            StoreApplication.deviceOS = "A";
            if (Intent.GetStringExtra("data") != null)
            {
                string notiData = Intent.GetStringExtra("data");
                Intent.RemoveExtra("NotificationData");
                Intent.PutExtra("waitNotification", notiData);
            }


            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.M)
            {
                if (CheckSelfPermission(Manifest.Permission.AccessNetworkState) != (int)Permission.Granted)
                {
                    if (ShouldShowRequestPermissionRationale(Manifest.Permission.AccessNetworkState))
                    {
                        RequestPermissions(new string[] { Manifest.Permission.AccessNetworkState }, requestPermission_NetworkState);
                    }
                    else
                    {
                        RequestPermissions(new string[] { Manifest.Permission.AccessNetworkState }, requestPermission_NetworkState);
                    }
                }
                else
                {
                    LoginWebView();
                }
            }
            else
            {
                LoginWebView();
            }

            uploadFileInfos = new List<UploadFileInfo>();
        }



        private void webView_Refresh(object sender, EventArgs e)
        {
            System.Console.WriteLine("refresh webview");
            refreshView.Refreshing = false;
            webView.Post(new Java.Lang.Runnable(() =>
            {
                webView.LoadUrl("javascript:refreshItemReload()");
            }));
        }

        public class FPWebViewClient : WebViewClient
        {
            MainActivity context;
            public FPWebViewClient(MainActivity pcontext)
            {
                this.context = pcontext;
            }

            public override void OnPageFinished(WebView view, string url)
            {
                base.OnPageFinished(view, url);
                if (!context.initPageLoad && url.IndexOf(siteURL) > -1)
                {
                    if (context.Intent.GetStringExtra("waitNotification") != null)
                    {
                        string notiData = context.Intent.GetStringExtra("waitNotification");
                        context.Intent.RemoveExtra("waitNotification");
                        view.LoadUrl("javascript:setTimeout(function () { updateToken('" + StoreApplication.deviceToken + "', 'A'); onPushNotificationOpen(null, null, '" + notiData + "'); }, 1000)");
                    }
                    else
                    {
                        view.LoadUrl("javascript:setTimeout(function () { updateToken('" + StoreApplication.deviceToken + "', 'A'); }, 1000)");
                    }
                }


                if (!context.initPageLoad)
                {
                    context.initPageLoad = true;
                    context.RunOnUiThread(() =>
                    {
                        AndHUD.Shared.Dismiss();
                    });
                }
            }

            public override void OnReceivedError(WebView view, IWebResourceRequest request, WebResourceError error)
            {
                if (request.Url.ToString().ToLower() == siteURL.ToLower())
                {
                    view.StopLoading();
                    view.LoadUrl("file:///android_asset/notInternetAvailable.html");
                    Toast.MakeText(context, error.Description, ToastLength.Long);
                    return;
                }
                else
                {
                    base.OnReceivedError(view, request, error);
                }
            }

            [TargetApi(Value = 24)]
            public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
            {
                if (ShouldOverrideUrlLoadingMethod(view, request.Url.ToString()))
                {
                    return true;
                }
                else
                {
                    return base.ShouldOverrideUrlLoading(view, request);
                }
            }

            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {
                if (ShouldOverrideUrlLoadingMethod(view, url))
                {
                    return true;
                }
                else
                {
                    return base.ShouldOverrideUrlLoading(view, url);
                }
            }

            private bool ShouldOverrideUrlLoadingMethod(WebView view, string url)
            {
                int extpos = url.LastIndexOf('.');
                int extlength = url.Length - (extpos + 1);

                if (Android.Net.Uri.Parse(url).Scheme.ToLower() == "tel" || Android.Net.Uri.Parse(url).Scheme.ToLower() == "market")
                {
                    Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
                    context.StartActivity(intent);

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void LoginWebView()
        {
            using (ConnectivityManager cm = GetSystemService(Context.ConnectivityService).JavaCast<ConnectivityManager>())
            {
                bool isconnect = false;
                if (cm.ActiveNetworkInfo != null)
                {
                    if (cm.ActiveNetworkInfo.IsConnected)
                    {
                        isconnect = true;
                        webView.Post(new Java.Lang.Runnable(() =>
                        {
                            webView.ClearHistory();
                            initPageLoad = false;
                            AndHUD.Shared.Show(MainContext, "Please wait...", -1, MaskType.None);
                            webView.LoadUrl(siteURL);
                        }));
                    }
                }
                if (!isconnect)
                {
                    webView.LoadUrl("file:///android_asset/notInternetAvailable.html");
                }
            }
        }

        #region override

        protected override void OnResume()
        {
            base.OnResume();
            StoreApplication.activityVisible = true;
        }

        protected override void OnPause()
        {
            base.OnPause();
            StoreApplication.activityVisible = false;
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            if (intent.GetStringExtra("title") != null && intent.GetStringExtra("body") != null && intent.GetStringExtra("data") != null)
            {
                string notiTitle = intent.GetStringExtra("title");
                intent.RemoveExtra("title");
                string notiContent = intent.GetStringExtra("body");
                intent.RemoveExtra("body");
                string notiData = intent.GetStringExtra("data");
                intent.RemoveExtra("data");

                bool activity = intent.GetBooleanExtra("activity", false);

                if (activity)
                {
                    webView.LoadUrl("javascript:onPushNotificationOpen('" + notiTitle + "', '" + notiContent.Replace("\r\n", "<br />") + "', '" + notiData + "')");
                }
                else
                {
                    webView.LoadUrl("javascript:onPushNotificationOpen('" + notiData + "')");
                }
                
            }
        }

        public override bool OnKeyDown([GeneratedEnum] Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                if (modalStatus > 0)
                {
                    webView.Post(new Java.Lang.Runnable(() =>
                    {
                        webView.LoadUrl("javascript:CloseModal()");
                    }));
                }
                else if (webView.Url.IndexOf(siteURL) < 0 && webView.CanGoBack())
                {
                    webView.GoBack();
                }
                else
                {
                    if (doubleBacktoExit)
                    {
                        webView.LoadUrl("javascript:terminateApp()");
                        Finish();
                        Android.OS.Process.KillProcess(Android.OS.Process.MyPid());

                        return true;
                    }
                    else
                    {
                        Toast.MakeText(ApplicationContext, "Press back again to exit", ToastLength.Long).Show();
                        doubleBacktoExit = true;

                        new Handler().PostDelayed(() =>
                        {
                            doubleBacktoExit = false;
                        }, 3500);

                        return true;
                    }
                    /*Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this, Resource.Style.AlertDialogStyle);
                    alert.SetMessage("앱 종료??"); //AppContext.GetString(Resource.String.ExitApp));

                    alert.SetPositiveButton("No", (sender, args) =>   //AppContext.GetString(Resource.String.Yes)
                    {
                        pressedTime = 0;
                    });
                    alert.SetNegativeButton("Yes", (sender, args) => //AppContext.GetString(Resource.String.No)
                    {
                        webView.LoadUrl("javascript:terminateApp()");
                        //OnDestroy();
                        Finish();
                        Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
                    });
                    alert.SetCancelable(false);

                    alert.Show();*/
                }

                return true;
            }

            return base.OnKeyDown(keyCode, e);
        }

        /// <summary>
        /// 회전 시 리프레시 방지
        /// </summary>
        /// <param name="newConfig"></param>
        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);
        }

        /// <summary>
        /// 권한 요청
        /// </summary>
        /// <param name="requestCode"></param>
        /// <param name="permissions"></param>
        /// <param name="grantResults"></param>
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            if (requestCode == requestPermission_Camera_Use)
            {
                if (grantResults.Length == 2 && grantResults[0] == Permission.Granted && grantResults[1] == Permission.Granted)
                {
                    OpenCamera();
                }
                else
                {
                    //Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(MainContext, Resource.Style.AlertDialogStyle);
                    //alert.SetMessage("카메라 사용 못함!!");
                    //alert.Show();

                    Toast.MakeText(ApplicationContext, "Camera is not available.", ToastLength.Long).Show();
                }
            }
            else if (requestCode == requestPermission_Storage_Use)
            {
                if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                {
                    if (!string.IsNullOrEmpty(tempFileName) && !string.IsNullOrEmpty(tempFileUrl))
                    {
                        RunOnUiThread(() => { RunDownloadUrl(tempFileName, tempFileUrl, tempToken, tempConfirm); });
                    }
                }
                else if (grantResults.Length == 2 && grantResults[0] == Permission.Granted && grantResults[1] == Permission.Granted)
                {
                    OpenGallery();
                }
                else
                {
                    //Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(MainContext, Resource.Style.AlertDialogStyle);
                    //alert.SetMessage("저장소 사용 못함!!");
                    //alert.Show();

                    Toast.MakeText(ApplicationContext, "You do not have permission to use the storage.", ToastLength.Long).Show();
                }
            }
            else if (requestCode == requestPermission_NetworkState)
            {
                if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                {
                    LoginWebView();
                }
                else
                {
                    //Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(MainContext, Resource.Style.AlertDialogStyle);
                    //alert.SetMessage("인터넷 연결 상태를 확인할 수 없습니다.");
                    //alert.Show();

                    Toast.MakeText(ApplicationContext, "Internet is not available.", ToastLength.Long).Show();
                }
            }
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if ((requestCode == requestActivity_Camera) && (resultCode == Result.Ok))
            {
                // 미디어 스토리지 스캐너 업데이트
                MediaScannerConnection.ScanFile(this, new string[] { photoFilePath }, null, null);

                Java.IO.File photoFile = new Java.IO.File(photoFilePath);
                Guid guid = Guid.NewGuid();

                if (SetWebviewUploadFileInfo(photoFileName, photoFile.Length().ToString(), guid.ToString()))
                {
                    uploadFileInfos.Add(new UploadFileInfo
                    {
                        fileUri = Android.Net.Uri.FromFile(photoFile),
                        fileName = photoFileName,
                        fileSize = photoFile.Length().ToString(),
                        fileGUID = guid.ToString(),
                    });
                }
            }
            else if ((resultCode == Result.Ok) && (data != null))
            {
                if (data.ClipData != null)
                {
                    System.Text.StringBuilder uploadInfo = null;

                    for (int i = 0; i < data.ClipData.ItemCount; i++)
                    {
                        string scriptstr = SetUploadFile(data.ClipData.GetItemAt(i));
                        if (scriptstr == "OVERSIZE")
                        {
                            webView.LoadUrl("javascript:OverFileSize(" + uploadInfo + ")");
                            break;
                        }

                        if (uploadInfo == null)
                        {
                            uploadInfo = new System.Text.StringBuilder(string.Format("[{0}", scriptstr));
                        }
                        else
                        {
                            uploadInfo.Append(string.Format(",{0}", scriptstr));
                        }
                    }

                    if (uploadInfo != null)
                    {
                        uploadInfo.Append("]");
                        webView.LoadUrl("javascript:SelectComplete(" + uploadInfo.ToString() + ")");
                    }
                }
                else
                {
                    UploadFileInfo uploadFileInfo = new UploadFileInfo();

                    using (var c1 = ContentResolver.Query(data.Data, null, null, null, null))
                    {
                        c1.MoveToFirst();
                        uploadFileInfo.fileName = c1.GetString(c1.GetColumnIndex(Android.Provider.OpenableColumns.DisplayName));
                        uploadFileInfo.fileSize = c1.GetString(c1.GetColumnIndex(Android.Provider.OpenableColumns.Size));
                    }

                    if (!string.IsNullOrEmpty(uploadFileInfo.fileName) && !string.IsNullOrEmpty(uploadFileInfo.fileSize))
                    {

                        Guid guid = Guid.NewGuid();
                        uploadFileInfo.fileUri = data.Data;
                        uploadFileInfo.fileGUID = Guid.NewGuid().ToString();

                        if (SetWebviewUploadFileInfo(uploadFileInfo.fileName, uploadFileInfo.fileSize, uploadFileInfo.fileGUID))
                        {
                            uploadFileInfos.Add(uploadFileInfo);

                            //string uploadInfo = string.Format("[{{filename:\"{0}\", filesize:\"{1}\", fileguid:\"{2}\"}}]", uploadFileInfo.fileName, uploadFileInfo.fileSize, uploadFileInfo.fileGUID);

                            //webView.LoadUrl("javascript:SelectComplete(" + uploadInfo + ")");
                        }

                        //uploadFileInfoDic.Add(guid.ToString(), uploadFileInfo);
                    }
                    else
                    {
                        // to-do 에러 메시지 표시
                    }
                }
            }

            onFileSelect = false;
        }


        #endregion

        #region JSBridge

        public void DownloadFromURL(string name, string url, string token, bool confirm)
        {
            if (Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.M)
            {
                RunDownloadUrl(name, url, token, confirm);
            }
            else
            {
                if (CheckSelfPermission(Manifest.Permission.WriteExternalStorage) != (int)Permission.Granted)
                {
                    tempFileName = name;
                    tempFileUrl = url;
                    tempToken = token;
                    tempConfirm = confirm;

                    if (ShouldShowRequestPermissionRationale(Manifest.Permission.WriteExternalStorage))
                    {
                        RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, requestPermission_Storage_Use);
                    }
                    else
                    {
                        RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, requestPermission_Storage_Use);
                    }
                }
                else
                {
                    RunDownloadUrl(name, url, token, confirm);
                }
            }

        }

        // 로그인에 필요한 정보 전달
        public void LoginWeb()
        {
            using (Android.Views.InputMethods.InputMethodManager imm = GetSystemService(Context.InputMethodService).JavaCast<Android.Views.InputMethods.InputMethodManager>())
            {
                imm.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, Android.Views.InputMethods.HideSoftInputFlags.NotAlways);
            }
            webView.Post(new Java.Lang.Runnable(() =>
            {
                var sharedPreferences = Android.Preferences.PreferenceManager.GetDefaultSharedPreferences(this);
                webView.LoadUrl("javascript:LoginWeb('" + StoreApplication.deviceToken + "', 'A', '" + sharedPreferences.GetString("AppVersion", "1.0.0.0") + "', '" + Build.VERSION.SdkInt.ToString() + "')");
            }));
        }

        /// <summary>
        /// 인터넷 연결 확인
        /// </summary>
        public void IsConnection()
        {
            if (GetConnectionStatus())
            {
                webView.Post(new Java.Lang.Runnable(() =>
                {
                    webView.LoadUrl("javascript:InternetConnect(true)");
                }));
            }
            else
            {
                webView.Post(new Java.Lang.Runnable(() =>
                {
                    webView.LoadUrl("javascript:InternetConnect(false)");
                }));
            }
        }

        // 열려라 카메라
        public void OpenCamera()
        {
            if (Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.M)
            {
                RunOpenCamera();
            }
            else
            {
                if (CheckSelfPermission(Manifest.Permission.Camera) != (int)Permission.Granted || CheckSelfPermission(Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                {
                    if (!ShouldShowRequestPermissionRationale(Manifest.Permission.Camera) && ShouldShowRequestPermissionRationale(Manifest.Permission.ReadExternalStorage))
                    {
                        RequestPermissions(new string[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage }, requestPermission_Camera_Use);
                    }
                    else
                    {
                        RequestPermissions(new string[] { Manifest.Permission.Camera, Manifest.Permission.ReadExternalStorage }, requestPermission_Camera_Use);
                    }
                }
                else
                {
                    RunOpenCamera();
                }
            }
        }

        // 열려라 갤러리
        public void OpenGallery()
        {
            if (Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.M)
            {
                RunOpenGallery();
            }
            else
            {
                if (CheckSelfPermission(Manifest.Permission.ReadExternalStorage) != (int)Permission.Granted)
                {
                    if (!ShouldShowRequestPermissionRationale(Manifest.Permission.ReadExternalStorage))
                    {
                        RequestPermissions(new string[] { Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, requestPermission_Storage_Use);
                    }
                    else
                    {
                        RequestPermissions(new string[] { Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, requestPermission_Storage_Use);
                    }
                }
                else
                {
                    RunOpenGallery();
                }
            }
        }

        /// <summary>
        /// 로컬 알림 일 경우 디바이스에 진동 또는 사운드
        /// </summary>
        /// <param name="notify"></param>
        public void DoNotify(string notify)
        {
            if (notify == "vibrate" || notify == "all")
            {
                Vibrator vibrator = (Vibrator)GetSystemService(Context.VibratorService);
                vibrator.Vibrate(100);
            }

            if (notify == "sound" || notify == "all")
            {
                Android.Net.Uri notification = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
                Ringtone ringtone = RingtoneManager.GetRingtone(this.ApplicationContext, notification);
                ringtone.Play();
            }
        }

        /// <summary>
        /// 포토 / 파일 선택 창 띄우기
        /// </summary>
        /// <param name="pFilter">파일 필터</param>
        /// <returns></returns>
        public void ShowFileSelector(string pFilter)
        {
            if (!onFileSelect)
            {
                onFileSelect = true;
                ShowFileType = pFilter;
                var fileIntent = new Intent();
                fileIntent.SetType(pFilter);
                fileIntent.SetAction(Intent.ActionGetContent);
                fileIntent.PutExtra(Intent.ExtraAllowMultiple, true);
                fileIntent.PutExtra(Intent.ExtraLocalOnly, true);
                fileIntent.AddCategory(Intent.CategoryOpenable);


                StartActivityForResult(Intent.CreateChooser(fileIntent, "파일 선택"), 0);
            }
        }

        /// <summary>
        /// 파일 업로드 시작
        /// </summary>
        /// <param name="uploadID"></param>
        /// <param name="uploaderType"></param>
        /// <param name="taskSeq"></param>
        /// <param name="uploadURL"></param>
        /// <param name="token"></param>
        public async void UploadFile(string taskSeq, string uploadURL, string listData, string token)
        {


            MultiPartFormUpload multiPartFormUpload = new MultiPartFormUpload();
            NameValueCollection headers = new NameValueCollection();
            headers.Add("Authorization", token);
            headers.Add("taskseq", taskSeq);
            headers.Add("upadtefile", listData);

            try
            {
                MultiPartFormUpload.UploadResponse response = await multiPartFormUpload.Upload(uploadURL, headers, new NameValueCollection() { }, uploadFileInfos, ContentResolver);

                if (response.HttpStatusCode == HttpStatusCode.OK)
                {
                    webView.Post(new Java.Lang.Runnable(() =>
                    {
                        //성공
                        webView.LoadUrl("javascript:uploadFileComplete(\"complete\")");
                    }));
                }
                else
                {
                    webView.Post(new Java.Lang.Runnable(() =>
                    {
                        // 실패
                        webView.LoadUrl("javascript:uploadFileComplete(\"fail\")");
                    }));
                }

                uploadFileInfos.Clear();
            }
            catch (System.Exception ex)
            {
                webView.Post(new Java.Lang.Runnable(() =>
                {
                    webView.LoadUrl("javascript:uploadFileComplete(\"error\")");
                }));

            }
        }

        /// <summary>
        /// 업로드 할려고 저장한 정보 삭제
        /// </summary>
        public void UploadFileClear(string fileid)
        {
            if (fileid.ToLower() == "all")
            {
                uploadFileInfos.Clear();
            }
            else
            {
                var removeitem = uploadFileInfos.Where(x => x.fileGUID == fileid).FirstOrDefault();
                if (removeitem != null)
                {
                    uploadFileInfos.Remove(removeitem);
                }
            }
        }

        /// <summary>
        /// 로그아웃
        /// </summary>
        public void LogoutWeb()
        {
            webView.Post(new Java.Lang.Runnable(() =>
            {
                webView.ClearHistory();
                webView.ClearCache(true);
            }));
        }

        /// <summary>
        /// 브라우져 이동
        /// </summary>
        /// <param name="url"></param>
        public void GoLocationBrowser(string url)
        {
            try
            {
                Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(url));
                StartActivity(intent);
            }
            catch (System.Exception ex)
            {
                // to-do 에러 처리
            }
        }

        public void SetSwipeRefresh(bool value)
        {
            refreshView.Post(new Java.Lang.Runnable(() =>
            {
                refreshView.Enabled = value;
            }));
        }

        public void GoAppUpdate(string packageName)
        {
            try
            {
                Intent appIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("market://details?id=" + packageName));
                StartActivity(appIntent);
            }
            catch (System.Exception ex)
            {
                Intent appIntent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("https://play.google.com/store/apps/details?id=" + packageName));
                StartActivity(appIntent);
            }
        }

        /// <summary>
        /// 모달창이 뜬 갯수 설정
        /// </summary>
        /// <param name="modalState"></param>
        public void SetModalStatus(int modalCount)
        {
            if (modalCount == 0)
            {
                SetSwipeRefresh(true);
            }
            else
            {
                SetSwipeRefresh(false);
            }
            modalStatus = modalCount;
        }


        /// <summary>
        /// 업로드 최대 용량 설정
        /// </summary>
        /// <param name="maxSize"></param>
        public void SetUploadSize(int maxSize)
        {
            this.maxSize = maxSize * 1024 * 1024;
        }

        /// <summary>
        /// 웹뷰 리프레시
        /// </summary>
        public void WebViewRefresh()
        {
            if (GetConnectionStatus())
            {
                webView.Post(new Java.Lang.Runnable(() =>
                {
                    initPageLoad = false;
                    webView.LoadUrl(siteURL);
                }));
            }
            else
            {
                Toast.MakeText(ApplicationContext, "인터넷에 연결할 수 없습니다.", ToastLength.Long).Show();
            }
        }

        #endregion

        #region Public Method

        /// <summary>
        /// 구글플레이서비스 사용 가능 여부
        /// </summary>
        /// <returns></returns>
        public bool IsPlayServiceAvailable()
        {

            int resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (resultCode != ConnectionResult.Success)
            {
                if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
                {
                    GoogleApiAvailability.Instance.GetErrorDialog(this, resultCode, 9000);
                }
                else
                {
                    // to-do 에러 (GooglePlayService 연결 실패)
                    Finish();
                }
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region Private Method

        /// <summary>
        /// 파일 다운로드 실행
        /// </summary>
        /// <param name="name"></param>
        /// <param name="url"></param>
        private void RunDownloadUrl(string name, string url, string token, bool confirm)
        {
            try
            {
                if (!confirm && System.IO.File.Exists(Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).Path, name)))
                {
                    webView.LoadUrl("javascript:confirmDownload()");
                    return;
                }

                tempFileName = string.Empty;
                tempFileUrl = string.Empty;

                //AutoResetEvent are = new AutoResetEvent(false);

                using (WebClient webClient = new WebClient())
                {
                    Toast.MakeText(ApplicationContext, "다운로드를 시작합니다.", ToastLength.Short).Show();

                    webClient.DownloadDataCompleted += (sender, args) =>
                    {
                        string storagePath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).Path;
                        string fileName = name.Substring(0, name.LastIndexOf("."));
                        string fileExt = name.Substring(name.LastIndexOf(".") + 1, name.Length - name.LastIndexOf(".") - 1);
                        string filePath = System.IO.Path.Combine(storagePath, name);

                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        }

                        if (args.Error != null)
                        {
                            this.RunOnUiThread(() =>
                            {
                                Toast.MakeText(ApplicationContext, string.Format("{0}\r\n다운로드에 실패하였습니다.", name), ToastLength.Short).Show();
                            });
                        }
                        else
                        {
                            using (System.IO.Stream stream = System.IO.File.Create(filePath))
                            {
                                stream.Write(args.Result, 0, args.Result.Length);
                            }
                            this.RunOnUiThread(() =>
                            {
                                Toast.MakeText(ApplicationContext, fileName + "." + fileExt + "\r\ndownload complete.", ToastLength.Short).Show();
                                ExecuteFile(filePath, fileExt);
                            });
                        }
                    };

                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add(HttpRequestHeader.Authorization, token);
                    webClient.DownloadDataAsync(new System.Uri(url));
                }
            }
            catch (System.Exception ex)
            {
                Toast.MakeText(ApplicationContext, "다운로드에 실패하였습니다.", ToastLength.Short).Show();
            }
        }

        private void ExecuteFile(string path, string ext)
        {
            if (System.IO.File.Exists(path))
            {
                MimeTypeMap mtm = MimeTypeMap.Singleton;
                string mimeType = mtm.GetMimeTypeFromExtension(ext);

                Android.Net.Uri fileUri = FileProvider.GetUriForFile(ApplicationContext, PackageName + ".provider", new Java.IO.File(path));

                MediaScannerConnection.ScanFile(ApplicationContext, new string[] { path }, new string[] { mimeType }, null);

                try
                {
                    Intent intent = new Intent();
                    intent.SetAction(Intent.ActionView);
                    intent.SetDataAndType(fileUri, mimeType);
                    intent.AddFlags(ActivityFlags.GrantReadUriPermission);
                    intent.AddFlags(ActivityFlags.NewTask);
                    StartActivity(intent);
                }
                catch (System.Exception ex)
                {

                }
            }
        }

        private void CreateNotificationCannel()
        {
            // API 26 이하는 필요 없어!
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                return;
            }

            var channel = new NotificationChannel(StoreApplication.channelID, "FCM Notifications", NotificationImportance.Default)
            {
                Description = "Firebase Cloud Messages appear in this channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }

        private void RunOpenCamera()
        {
            try
            {
                Intent intent = new Intent(MediaStore.ActionImageCapture);

                Java.IO.File photoFolder = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDcim);

                if (!photoFolder.Exists())
                {
                    photoFolder.Mkdirs();
                }

                System.Console.WriteLine(photoFolder.Exists().ToString());

                photoFileName = System.String.Format("TheSchool_{0}.jpg", DateTime.Now.ToString("yyyyMMdd_HHmmss"));

                using (Java.IO.File photoFile = new Java.IO.File(photoFolder.Path, photoFileName))
                {
                    photoFilePath = photoFile.Path;


                    if (Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.Lollipop)
                    {
                        intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(photoFile));
                    }
                    else
                    {
                        intent.PutExtra(MediaStore.ExtraOutput, FileProvider.GetUriForFile(this, PackageName + ".provider", photoFile));
                    }

                    StartActivityForResult(intent, requestActivity_Camera);
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
            }
        }

        private void RunOpenGallery()
        {
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.PutExtra(Intent.ExtraAllowMultiple, true);
            intent.SetAction(Intent.ActionGetContent);
            StartActivityForResult(Intent.CreateChooser(intent, "Select Picture"), requestActivity_Gallery);
        }

        private string SetUploadFile(ClipData.Item data)
        {
            string scriptstring = string.Empty;
            UploadFileInfo uploadFileInfo = new UploadFileInfo();

            using (var c1 = ContentResolver.Query(data.Uri, null, null, null, null))
            {
                c1.MoveToFirst();
                uploadFileInfo.fileName = c1.GetString(c1.GetColumnIndex(Android.Provider.OpenableColumns.DisplayName));
                uploadFileInfo.fileSize = c1.GetString(c1.GetColumnIndex(Android.Provider.OpenableColumns.Size));
            }

            if (!string.IsNullOrEmpty(uploadFileInfo.fileName) && !string.IsNullOrEmpty(uploadFileInfo.fileSize))
            {
                if (int.Parse(uploadFileInfo.fileSize) > maxSize)
                {
                    return "OVERSIZE";
                }
                else
                {
                    uploadFileInfo.fileUri = data.Uri;
                    uploadFileInfo.fileGUID = Guid.NewGuid().ToString();
                    uploadFileInfos.Add(uploadFileInfo);

                    scriptstring = string.Format("{{filename:\"{0}\", filesize:\"{1}\", fileguid:\"{2}\"}}", uploadFileInfo.fileName, uploadFileInfo.fileSize, uploadFileInfo.fileGUID);
                }

                //webView.LoadUrl("javascript:uploadFileSelectComplete(" + uploadInfo + ")");
                //uploadFileInfoDic.Add(guid.ToString(), uploadFileInfo);
            }
            else
            {
                // to-do 에러 메시지 표시
            }

            return scriptstring;
        }

        private bool VersionCheck()
        {
            try
            {
                WebClient webClient = new WebClient();
                webClient.Encoding = System.Text.Encoding.UTF8;
                string data = webClient.DownloadString(new System.Uri(versionURL));
                string[] versions = data.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                Version recentlyWeb = new Version(versions[0]);
                Version recentlyApp = new Version(versions[1]);

                var sharedPreferences = Android.Preferences.PreferenceManager.GetDefaultSharedPreferences(this);
                Version curWeb = new Version(sharedPreferences.GetString("WebVersion", "1.0.0.0"));
                Version curApp = new Version(sharedPreferences.GetString("AppVersion", "1.0.0.0"));

                if (recentlyApp > curApp)
                {
                    sharedPreferences.Edit().PutString("AppVersion", recentlyApp.ToString()).Commit();
                    // 최신 앱을 받아주세요 안내 문구 표시.
                    webView.Post(new Java.Lang.Runnable(() =>
                    {
                        webView.LoadUrl("javascript:NeedAppUpdate()");
                    }));
                }

                if (recentlyWeb > curWeb)
                {
                    sharedPreferences.Edit().PutString("WebVersion", recentlyWeb.ToString()).Commit();
                    return true;
                }
                else
                {
                    if (recentlyWeb < new Version("1.0.0.0"))
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (System.Exception ex)
            {
                return true;
            }
        }

        private bool SetWebviewUploadFileInfo(string fileName, string fileSize, string fileGuid)
        {
            string uploadInfo = string.Format("[{{filename:\"{0}\", filesize:\"{1}\", fileguid:\"{2}\"}}]", fileName, fileSize, fileGuid);
            if (int.Parse(fileSize) > maxSize)
            {
                webView.LoadUrl("javascript:OverFileSize(" + uploadInfo + ")");
                return false;
            }
            else
            {
                webView.LoadUrl("javascript:SelectComplete(" + uploadInfo + ")");
                return true;
            }
        }

        private bool GetConnectionStatus()
        {
            bool isConnect = false;
            using (ConnectivityManager cm = GetSystemService(Context.ConnectivityService).JavaCast<ConnectivityManager>())
            {
                if (cm.ActiveNetworkInfo != null)
                {
                    isConnect = cm.ActiveNetworkInfo.IsConnected;
                }
            }

            return isConnect;
        }

        #endregion
    }

    public class UploadFileInfo
    {
        public string fileName { get; set; }
        public string fileSize { get; set; }
        public Android.Net.Uri fileUri { get; set; }
        public string fileGUID { get; set; }
    }

    public class MultiPartFormUpload
    {
        public class MimePart
        {
            NameValueCollection _headers = new NameValueCollection();
            byte[] _header;

            public NameValueCollection Headers
            {
                get { return _headers; }
            }

            public byte[] Header
            {
                get { return _header; }
            }

            public long GenerateHeaderFooterData(string boundary)
            {
                System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();

                stringBuilder.Append("--");
                stringBuilder.Append(boundary);
                stringBuilder.AppendLine();
                foreach (string key in _headers.AllKeys)
                {
                    stringBuilder.Append(key);
                    stringBuilder.Append(": ");
                    stringBuilder.AppendLine(_headers[key]);
                }
                stringBuilder.AppendLine();

                _header = System.Text.Encoding.UTF8.GetBytes(stringBuilder.ToString());

                return _header.Length + fileLength + 2;
            }

            public int fileLength { get; set; }
            public System.IO.Stream Data { get; set; }
        }

        public class UploadResponse
        {
            public UploadResponse(HttpStatusCode httpStatusCode, string responseBody)
            {
                HttpStatusCode = httpStatusCode;
                ResponseBody = responseBody;
            }

            public HttpStatusCode HttpStatusCode { get; set; }

            public string ResponseBody { get; set; }
        }

        public async Task<UploadResponse> Upload(string url, NameValueCollection requestHeaders, NameValueCollection requestParameters, List<UploadFileInfo> files, ContentResolver cr)
        {
            using (WebClient client = new WebClient())
            {
                List<MimePart> mimeParts = new List<MimePart>();

                try
                {
                    foreach (string key in requestHeaders.AllKeys)
                    {
                        client.Headers.Add(key, requestHeaders[key]);
                    }

                    foreach (string key in requestParameters.AllKeys)
                    {
                        MimePart part = new MimePart();

                        part.Headers["Content-Disposition"] = "form-data; name=\"" + key + "\"";
                        part.Data = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(requestParameters[key]));

                        mimeParts.Add(part);
                    }

                    foreach (UploadFileInfo file in files)
                    {
                        MimePart part = new MimePart();
                        string name = file.fileGUID;
                        string fileName = file.fileName;

                        part.Headers["Content-Disposition"] = "form-data; name=\"" + name + "\"; filename=\"" + fileName + "\"";
                        part.Headers["Content-Type"] = "application/octet-stream";

                        System.IO.Stream cfileStream = cr.OpenInputStream(file.fileUri);
                        part.Data = cfileStream;
                        part.fileLength = Convert.ToInt32(file.fileSize);
                        mimeParts.Add(part);
                    }

                    string boundary = "----------" + DateTime.Now.Ticks.ToString("x");
                    client.Headers.Add(HttpRequestHeader.ContentType, "multipart/form-data; boundary=" + boundary);

                    long contentLength = 0;

                    byte[] _footer = System.Text.Encoding.UTF8.GetBytes("--" + boundary + "--\r\n");

                    foreach (MimePart mimePart in mimeParts)
                    {
                        contentLength += mimePart.GenerateHeaderFooterData(boundary);
                    }

                    byte[] buffer = new byte[8192];
                    byte[] afterFile = System.Text.Encoding.UTF8.GetBytes("\r\n");
                    int read;

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        foreach (MimePart mimePart in mimeParts)
                        {
                            memoryStream.Write(mimePart.Header, 0, mimePart.Header.Length);

                            while ((read = mimePart.Data.Read(buffer, 0, buffer.Length)) > 0)
                                memoryStream.Write(buffer, 0, read);

                            mimePart.Data.Dispose();

                            memoryStream.Write(afterFile, 0, afterFile.Length);
                        }

                        memoryStream.Write(_footer, 0, _footer.Length);
                        byte[] responseBytes = client.UploadData(url, memoryStream.ToArray());
                        string responseString = System.Text.Encoding.UTF8.GetString(responseBytes);
                        return new UploadResponse(HttpStatusCode.OK, responseString);
                    }
                }
                catch (System.Exception ex)
                {
                    foreach (MimePart part in mimeParts)
                        if (part.Data != null)
                            part.Data.Dispose();

                    if (ex.GetType().Name == "WebException")
                    {
                        WebException webException = (WebException)ex;
                        HttpWebResponse response = (HttpWebResponse)webException.Response;
                        string responseString;

                        using (StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8))
                        {
                            responseString = reader.ReadToEnd();
                        }

                        return new UploadResponse(response.StatusCode, responseString);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
    }
}