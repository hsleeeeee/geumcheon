﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;

namespace Geumcheon.Web.Mobile.App.Droid.FCM
{
    [Service(Exported = false, Name = "com.FuturePlan.GeumcheonConsult.GeumcheonConsultListenerService"), IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    class GeumcheonConsultListenerService : FirebaseMessagingService
    {
        public override void OnMessageReceived(RemoteMessage pMessage)
        {
            string message = "";
            string title = "";
            string data = "";

            try
            {
                title = pMessage.Data["title"].ToString();
                message = pMessage.Data["body"].ToString();
                data = pMessage.Data["data"]?.ToString();

                //message = pMessage.GetNotification().Body;
                //title = pMessage.GetNotification().Title;
            }
            catch (Exception ex)
            {

            }

            if (StoreApplication.activityVisible)
            {
                SendLocalNotification(title, message, data);
            }
            //else
            //{
            //    SendNotification(title, message, data);
            //}

        }

        public override void OnNewToken(string p0)
        {
            Console.WriteLine("push token : " + p0);
            var sharedPreferences = PreferenceManager.GetDefaultSharedPreferences(this);
            StoreApplication.deviceToken = p0;
            sharedPreferences.Edit().PutString("PushToken", p0).Commit();
        }

        void SendNotification(string title, string message, string data)
        {
            //var intent = new Intent(this, typeof(MainActivity));
            //intent.AddFlags(ActivityFlags.ClearTop);
            //intent.PutExtra("NotificationData", data);
            //var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.UpdateCurrent);

            //var notificationBuilder = new Notification.Builder(this)
            //    .SetSmallIcon(Resource.Drawable.ic_launcher)
            //    .SetContentTitle(title)
            //    .SetContentText(message)
            //    .SetAutoCancel(true)
            //    .SetDefaults(NotificationDefaults.All)
            //    .SetContentIntent(pendingIntent);

            //var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            //notificationManager.Notify(StoreApplication.notificationID, notificationBuilder.Build());




            ///*Intent intent2 = new Intent(this, typeof(NotiPopup));
            //intent2.SetFlags(ActivityFlags.NewTask | ActivityFlags.SingleTop | ActivityFlags.ClearTask);
            //intent2.PutExtra("Title", title);
            //intent2.PutExtra("Content", message);
            //var pendingIntent2 = PendingIntent.GetActivity(this, 0, intent2, PendingIntentFlags.OneShot);

            //RemoteViews rv = new RemoteViews(PackageName, Resource.Layout.NotiPopup);

            //var popup = new NotificationCompat.Builder(this)
            //    .SetSmallIcon(Resource.Drawable.Icon)
            //    .SetContentTitle(title)
            //    .SetContentText(message)
            //    .SetCategory(Notification.CategoryMessage)
            //    .SetVisibility((int)NotificationVisibility.Public)
            //    .SetPriority((int)NotificationPriority.Max)
            //    .SetWhen(DateTime.Now.Ticks)
            //    //.SetFullScreenIntent(pendingIntent2, true)
            //    .SetAutoCancel(false)
            //    .SetContent(rv)
            //    .SetDefaults(NotificationCompat.DefaultAll);
            //NotificationManager nm = (NotificationManager)GetSystemService(Context.NotificationService);
            //nm.Notify(0, popup.Build());*/

            //KeyguardManager km = GetSystemService(Context.KeyguardService).JavaCast<KeyguardManager>();



            //PowerManager pm = GetSystemService(Context.PowerService).JavaCast<PowerManager>();

            //if (km.InKeyguardRestrictedInputMode())
            //{
            //    Intent intent2 = new Intent(this, typeof(LockPopup));
            //    intent2.SetFlags(ActivityFlags.NewTask | ActivityFlags.SingleTop | ActivityFlags.ExcludeFromRecents | ActivityFlags.NoHistory);
            //    intent2.PutExtra("Title", title);
            //    intent2.PutExtra("Content", message);
            //    intent2.PutExtra("Data", data);
            //    var pendingIntent2 = PendingIntent.GetActivity(this, 0, intent2, PendingIntentFlags.OneShot);
            //    pendingIntent2.Send();


            //}
            //else
            //{
            //    Intent intent2 = new Intent(this, typeof(NotiPopup));
            //    intent2.SetFlags(ActivityFlags.NewTask | ActivityFlags.SingleTop | ActivityFlags.ExcludeFromRecents | ActivityFlags.NoHistory);
            //    intent2.PutExtra("Title", title);
            //    intent2.PutExtra("Content", message);
            //    intent2.PutExtra("Data", data);
            //    var pendingIntent2 = PendingIntent.GetActivity(this, 0, intent2, PendingIntentFlags.OneShot);
            //    pendingIntent2.Send();
            //}

            //if (Build.VERSION.SdkInt < Android.OS.BuildVersionCodes.Lollipop)
            //{
            //    if (km.InKeyguardRestrictedInputMode() || !pm.IsScreenOn)
            //    {
            //        //PowerManager.WakeLock wl = pm.NewWakeLock(WakeLockFlags.AcquireCausesWakeup | WakeLockFlags.ScreenBright | WakeLockFlags.OnAfterRelease, "ubibase.pushservice.tuv.wakefull");
            //        PowerManager.WakeLock wl = pm.NewWakeLock(WakeLockFlags.AcquireCausesWakeup | WakeLockFlags.ScreenBright | WakeLockFlags.OnAfterRelease, "com.tuvapp.app.wakefull");
            //        wl.Acquire(5000);
            //    }
            //}
            //else
            //{
            //    if (km.InKeyguardRestrictedInputMode() || !pm.IsInteractive)
            //    {
            //        //PowerManager.WakeLock wl = pm.NewWakeLock(WakeLockFlags.AcquireCausesWakeup | WakeLockFlags.ScreenBright | WakeLockFlags.OnAfterRelease, "ubibase.pushservice.tuv.wakefull");
            //        PowerManager.WakeLock wl = pm.NewWakeLock(WakeLockFlags.AcquireCausesWakeup | WakeLockFlags.ScreenBright | WakeLockFlags.OnAfterRelease, "com.tuvapp.app.wakefull");
            //        wl.Acquire(5000);
            //    }
            //}





            /*AlertDialog.Builder ADbuilder = new AlertDialog.Builder();
            ADbuilder.SetTitle(title);
            ADbuilder.SetIcon(Resource.Drawable.Icon);


            LayoutInflater inflater = GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();
            View popup = inflater.Inflate(Resource.Layout.NotiPopup, null);
            var nTitle = popup.FindViewById<TextView>(Resource.Id.tv_title);
            var nContent = popup.FindViewById<TextView>(Resource.Id.tv_content);
            var nCancel = popup.FindViewById<Button>(Resource.Id.btn_cancel);


            nTitle.Text = title;
            nContent.Text = message;
            nCancel.Click += delegate
            {
                //wm.RemoveView(popup);
            };

            ADbuilder.SetView(popup);
            AlertDialog ad = ADbuilder.Create();
            ad.SetCanceledOnTouchOutside(true);

            ad.Show();*/



            /*Intent popup = new Intent(this, typeof(NotiPopup));
            popup.SetFlags(ActivityFlags.NewTask | ActivityFlags.SingleTop | ActivityFlags.ClearTask);
            popup.PutExtra("Title", title);
            popup.PutExtra("Content", message);
            this.StartActivity(popup);*/

            /*try
            {
                LayoutInflater inflater = (LayoutInflater)GetSystemService(Context.LayoutInflaterService);
                View popup = inflater.Inflate(Resource.Layout.NotiPopup, null);
                var nTitle = popup.FindViewById<TextView>(Resource.Id.tv_title);
                var nContent = popup.FindViewById<TextView>(Resource.Id.tv_content);
                var nCancel = popup.FindViewById<Button>(Resource.Id.btn_cancel);
                

                nTitle.Text = title;
                nContent.Text = message;

                //PopupWindow pw = new PopupWindow(popup, RelativeLayout.LayoutParams.WrapContent, WindowManagerLayoutParams.WrapContent);

                

                IWindowManager wm = GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
                WindowManagerLayoutParams lp = new WindowManagerLayoutParams(WindowManagerLayoutParams.WrapContent, WindowManagerLayoutParams.WrapContent, WindowManagerTypes.SystemOverlay, WindowManagerFlags.NotFocusable, Android.Graphics.Format.A8);
                
                wm.AddView(pdirectoryopup, lp);

                nCancel.Click += delegate
                {
                    wm.RemoveView(popup);
                };

                //pw.ShowAtLocation(popup, GravityFlags.Center, 0, -100);
                /*handler.Post(new Java.Lang.Runnable(() =>
                {
                    try
                    {
                        pw.ShowAtLocation(popup, GravityFlags.Center, 0, 0);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }));*/
            /*}
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }*/


        }

        void SendLocalNotification(string title, string body, string data)
        {
            /*var intent = new Intent(this, typeof(MainActivity));

            intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
            intent.PutExtra("Data", data);

            this.StartActivity(intent);*/


            Intent intent = new Intent(this, typeof(MainActivity));
            intent.SetFlags(ActivityFlags.NewTask);
            intent.PutExtra("title", title);
            intent.PutExtra("body", body);
            intent.PutExtra("data", data);
            if (StoreApplication.activityVisible)
            {
                intent.PutExtra("activity", true);
            }
            this.StartActivity(intent);
        }
    }
}