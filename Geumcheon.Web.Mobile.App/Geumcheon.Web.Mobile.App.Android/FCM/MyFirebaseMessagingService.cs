﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;

namespace Geumcheon.Web.Mobile.App.Droid.FCM
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        public override void OnMessageReceived(RemoteMessage pMessage)
        {
            string message = "";
            string title = "";
            string data = "";

            try
            {
                title = pMessage.Data["title"].ToString();
                message = pMessage.Data["body"].ToString();
                data = pMessage.Data["data"]?.ToString();

                //message = pMessage.GetNotification().Body;
                //title = pMessage.GetNotification().Title;
            }
            catch (Exception ex)
            {

            }

            if (StoreApplication.activityVisible)
            {
                SendLocalNotification(title, message, data);
            }
            //else
            //{
            //    SendNotification(title, message, data);
            //}

        }

        public override void OnNewToken(string p0)
        {
            Console.WriteLine("push token : " + p0);
            var sharedPreferences = PreferenceManager.GetDefaultSharedPreferences(this);
            StoreApplication.deviceToken = p0;
            sharedPreferences.Edit().PutString("PushToken", p0).Commit();
        }

        void SendNotification(string messageBody)
        {
            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

            var notificationBuilder = new NotificationCompat.Builder(this, StoreApplication.channelID);

            notificationBuilder.SetContentTitle("FCM Message")
                        .SetSmallIcon(Resource.Drawable.icon)
                        .SetContentText(messageBody)
                        .SetAutoCancel(true)
                        .SetShowWhen(false)
                        .SetContentIntent(pendingIntent);

            var notificationManager = NotificationManager.FromContext(this);

            notificationManager.Notify(0, notificationBuilder.Build());
        }

        void SendLocalNotification(string title, string message, string data)
        {
            /*var intent = new Intent(this, typeof(MainActivity));

            intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop);
            intent.PutExtra("Data", data);

            this.StartActivity(intent);*/


            Intent intent = new Intent(this, typeof(MainActivity));
            intent.SetFlags(ActivityFlags.NewTask);
            intent.PutExtra("Title", title);
            intent.PutExtra("Content", message);
            intent.PutExtra("Data", data);
            this.StartActivity(intent);
        }
    }
}